package depth1;

import edge.Edge;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/boardsoccer.tomekslair.org/bruteForceGoingUpDepth1Calculator.xml",
        "classpath:/spring/boardsoccer.tomekslair.org/bruteForceGoingUpDepth1Calculator-dependencies.xml"
})
public class BruteForceGoingUpDepth1CalculatorTest {

    private static final Depth1CalculatorDto emptyInTheMiddle = new Depth1CalculatorDto(
            4,6, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE);

    @Autowired
    @Qualifier(value = "bruteForceGoingUpDepth1Calculator")
    Depth1Calculator goingUpDepth1Calculator;

    @Test
    public void doNothing() {

    }



}