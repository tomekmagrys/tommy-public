package depth1;

import edge.Edge;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/boardsoccer.tomekslair.org/hibernateGoingUpDepth1Calculator.xml",
        "classpath:/spring/boardsoccer.tomekslair.org/hibernateGoingUpDepth1Calculator-dependencies.xml",
        "classpath:/spring/boardsoccer.tomekslair.org/bruteForceGoingUpDepth1Calculator.xml"
})
public class GoingUpDepth1CalculatorTest {

    private static final Depth1CalculatorDto emptyInTheMiddle = new Depth1CalculatorDto(
            4, 6, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE, Edge.FREE);

    @Autowired
    @Qualifier(value = "hibernateGoingUpDepth1Calculator")
    Depth1Calculator hibernateGoingUpDepth1Calculator;

    @Autowired
    @Qualifier(value = "bruteForceGoingUpDepth1Calculator")
    Depth1Calculator bruteForceGoingUpDepth1Calculator;

    @Test
    public void testEmpty() {
        int hibernateRetVal = hibernateGoingUpDepth1Calculator.calculate(emptyInTheMiddle);

        assertEquals(hibernateRetVal, 7);
    }

    @Test
    public void doNothing() {

    }

    @Test
    public void testFull() {

        List<Depth1CalculatorDto> wrong = new ArrayList<>();

        for (Edge edgeNW : Edge.values()) {
            for (Edge edgeN : Edge.values()) {
                for (Edge edgeNE : Edge.values()) {
                    for (Edge edgeE : Edge.values()) {
                        for (Edge edgeSE : Edge.values()) {
                            for (Edge edgeS : Edge.values()) {
                                for (Edge edgeSW : Edge.values()) {
                                    for (Edge edgeW : Edge.values()) {
                                        Depth1CalculatorDto depth1CalculatorDto = new Depth1CalculatorDto(
                                                4, 6, edgeNW, edgeN, edgeNE, edgeE, edgeSE, edgeS, edgeSW, edgeW);
                                        int hibernateRetVal = hibernateGoingUpDepth1Calculator.calculate(depth1CalculatorDto);
                                        int bruteForceRetVal = bruteForceGoingUpDepth1Calculator.calculate(depth1CalculatorDto);
                                        if(hibernateRetVal != bruteForceRetVal){
                                            wrong.add(depth1CalculatorDto);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        assertEquals(0, wrong.size());

    }

}