package depth0;

public class Depth0CalculatorDto {

    private final int x;
    private final int y;

    public Depth0CalculatorDto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
