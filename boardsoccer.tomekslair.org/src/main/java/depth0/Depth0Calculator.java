package depth0;

public interface Depth0Calculator {

    int calculate(Depth0CalculatorDto depth0CalculatorDto);

}
