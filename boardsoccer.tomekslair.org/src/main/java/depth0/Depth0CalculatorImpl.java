package depth0;

public class Depth0CalculatorImpl implements Depth0Calculator {
    @Override
    public int calculate(Depth0CalculatorDto depth0CalculatorDto) {
        return depth0CalculatorDto.getY();
    }
}
