package depth0;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class HibernateDepth0Dao {

    private final SessionFactory sessionFactory;

    public HibernateDepth0Dao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public Integer store(Depth0CalculatorDto depth1CalculatorDto, int value) {
        Session session = sessionFactory.openSession();


        Criteria criteria = session.createCriteria(Depth0CalculatorDtoRow.class);

        criteria.add(Restrictions.eq("x", depth1CalculatorDto.getX()));
        criteria.add(Restrictions.eq("y", depth1CalculatorDto.getY()));

        List<Depth0CalculatorDtoRow> list = criteria.list();
        if(list.isEmpty()){
            Depth0CalculatorDtoRow depth1CalculatorDtoRow = new Depth0CalculatorDtoRow();
            depth1CalculatorDtoRow.setY(depth1CalculatorDto.getY());
            depth1CalculatorDtoRow.setX(depth1CalculatorDto.getX());
            depth1CalculatorDtoRow.setValue(value);
            session.save(depth1CalculatorDtoRow);
        } else {
            throw new IllegalArgumentException();
        }

        session.flush();
        session.close();
        return 0;


    }

}
