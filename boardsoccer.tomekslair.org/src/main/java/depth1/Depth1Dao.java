package depth1;

public interface Depth1Dao {


    boolean containsKey(Depth1CalculatorDto depth1CalculatorDto);

    void put(Depth1CalculatorDto depth1CalculatorDto, int i);

    int get(Depth1CalculatorDto depth1CalculatorDto);
}
