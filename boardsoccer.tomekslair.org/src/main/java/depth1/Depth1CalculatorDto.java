package depth1;

import depth0.Depth0CalculatorDto;
import edge.Edge;

class Depth1CalculatorDto extends Depth0CalculatorDto {

    private final Edge _0_0_NW;
    private final Edge _0_0_N;
    private final Edge _0_0_NE;
    private final Edge _0_0_E;
    private final Edge _0_0_SE;
    private final Edge _0_0_S;
    private final Edge _0_0_SW;
    private final Edge _0_0_W;


    public Depth1CalculatorDto(int x, int y, Edge nw, Edge n, Edge ne, Edge e, Edge se, Edge s, Edge sw, Edge w) {
        super(x, y);
        _0_0_NW = nw;
        _0_0_N = n;
        _0_0_NE = ne;
        _0_0_E = e;
        _0_0_SE = se;
        _0_0_S = s;
        _0_0_SW = sw;
        _0_0_W = w;
    }

    public Edge get_0_0_NW() {
        return _0_0_NW;
    }

    public Edge get_0_0_N() {
        return _0_0_N;
    }

    public Edge get_0_0_NE() {
        return _0_0_NE;
    }

    public Edge get_0_0_E() {
        return _0_0_E;
    }

    public Edge get_0_0_SE() {
        return _0_0_SE;
    }

    public Edge get_0_0_S() {
        return _0_0_S;
    }

    public Edge get_0_0_SW() {
        return _0_0_SW;
    }

    public Edge get_0_0_W() {
        return _0_0_W;
    }

}
