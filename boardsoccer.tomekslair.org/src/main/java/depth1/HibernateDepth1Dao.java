package depth1;

import edge.Edge;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.ArrayList;
import java.util.List;

public class HibernateDepth1Dao implements Depth1Dao {

    private final static Logger logger = Logger.getLogger(HibernateDepth1Dao.class);

    private final SessionFactory sessionFactory;

    private static List<String> simplePropertyNames = new ArrayList<>();

    static {
        {
            simplePropertyNames.add("x_0_y_0_NW");
            simplePropertyNames.add("x_0_y_0_N");
            simplePropertyNames.add("x_0_y_0_NE");
            simplePropertyNames.add("x_0_y_0_E");
            simplePropertyNames.add("x_0_y_0_SE");
            simplePropertyNames.add("x_0_y_0_S");
            simplePropertyNames.add("x_0_y_0_SW");
            simplePropertyNames.add("x_0_y_0_W");
        }
    }

    public HibernateDepth1Dao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean containsKey(Depth1CalculatorDto depth1CalculatorDto) {
        Session session = sessionFactory.openSession();

        Criteria criteria = session.createCriteria(Depth1CalculatorDtoRow.class);

        getRestrictions(depth1CalculatorDto, null).stream().forEach(criteria::add);

        boolean retVal = !criteria.list().isEmpty();

        session.flush();
        session.close();
        return retVal;
    }

    @Override
    public void put(Depth1CalculatorDto depth1CalculatorDto, int value) {
        Session session = sessionFactory.openSession();

        Depth1CalculatorDtoRow toBeAdded = new Depth1CalculatorDtoRow();

        toBeAdded.setX(depth1CalculatorDto.getX());
        toBeAdded.setY(depth1CalculatorDto.getY());

        if (depth1CalculatorDto.get_0_0_NW() == Edge.FREE) {
            toBeAdded.setX_0_y_0_NW_free(true);
        }
        if (depth1CalculatorDto.get_0_0_NW() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_NW_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_N() == Edge.FREE) {
            toBeAdded.setX_0_y_0_N_free(true);
        }
        if (depth1CalculatorDto.get_0_0_N() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_N_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_NE() == Edge.FREE) {
            toBeAdded.setX_0_y_0_NE_free(true);
        }
        if (depth1CalculatorDto.get_0_0_NE() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_NE_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_E() == Edge.FREE) {
            toBeAdded.setX_0_y_0_E_free(true);
        }
        if (depth1CalculatorDto.get_0_0_E() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_E_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_SE() == Edge.FREE) {
            toBeAdded.setX_0_y_0_SE_free(true);
        }
        if (depth1CalculatorDto.get_0_0_SE() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_SE_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_S() == Edge.FREE) {
            toBeAdded.setX_0_y_0_S_free(true);
        }
        if (depth1CalculatorDto.get_0_0_S() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_S_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_SW() == Edge.FREE) {
            toBeAdded.setX_0_y_0_SW_free(true);
        }
        if (depth1CalculatorDto.get_0_0_SW() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_SW_taken(true);
        }
        if (depth1CalculatorDto.get_0_0_W() == Edge.FREE) {
            toBeAdded.setX_0_y_0_W_free(true);
        }
        if (depth1CalculatorDto.get_0_0_W() == Edge.TAKEN) {
            toBeAdded.setX_0_y_0_W_taken(true);
        }
        toBeAdded.setValue(value);
        session.save(toBeAdded);

        collapse(session, toBeAdded);

        session.flush();
        session.close();
    }

    private void collapse(Session session, Depth1CalculatorDtoRow justAdded) {
        for (String simplePropertyName : simplePropertyNames) {
            Criteria criteria = session.createCriteria(Depth1CalculatorDtoRow.class);
            criteria.add(Restrictions.eq("x", justAdded.getX()));
            criteria.add(Restrictions.eq("y", justAdded.getY()));

            if (!"x_0_y_0_NW".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_NW_free", justAdded.isX_0_y_0_NW_free()));
                criteria.add(Restrictions.eq("x_0_y_0_NW_taken", justAdded.isX_0_y_0_NW_taken()));
            }

            if (!"x_0_y_0_N".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_N_free", justAdded.isX_0_y_0_N_free()));
                criteria.add(Restrictions.eq("x_0_y_0_N_taken", justAdded.isX_0_y_0_N_taken()));
            }

            if (!"x_0_y_0_NE".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_NE_free", justAdded.isX_0_y_0_NE_free()));
                criteria.add(Restrictions.eq("x_0_y_0_NE_taken", justAdded.isX_0_y_0_NE_taken()));
            }

            if (!"x_0_y_0_E".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_E_free", justAdded.isX_0_y_0_E_free()));
                criteria.add(Restrictions.eq("x_0_y_0_E_taken", justAdded.isX_0_y_0_E_taken()));
            }

            if (!"x_0_y_0_SE".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_SE_free", justAdded.isX_0_y_0_SE_free()));
                criteria.add(Restrictions.eq("x_0_y_0_SE_taken", justAdded.isX_0_y_0_SE_taken()));
            }

            if (!"x_0_y_0_S".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_S_free", justAdded.isX_0_y_0_S_free()));
                criteria.add(Restrictions.eq("x_0_y_0_S_taken", justAdded.isX_0_y_0_S_taken()));
            }

            if (!"x_0_y_0_SW".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_SW_free", justAdded.isX_0_y_0_SW_free()));
                criteria.add(Restrictions.eq("x_0_y_0_SW_taken", justAdded.isX_0_y_0_SW_taken()));
            }

            if (!"x_0_y_0_W".equals(simplePropertyName)) {
                criteria.add(Restrictions.eq("x_0_y_0_W_free", justAdded.isX_0_y_0_W_free()));
                criteria.add(Restrictions.eq("x_0_y_0_W_taken", justAdded.isX_0_y_0_W_taken()));
            }

            criteria.add(Restrictions.eq("value", justAdded.getValue()));

            List<Depth1CalculatorDtoRow> retVal = criteria.list();
            if (retVal.size() < 2) {
                return;
            }
            if (retVal.size() > 2) {
                throw new IllegalStateException();
            }

            Depth1CalculatorDtoRow first = retVal.get(0);
            Depth1CalculatorDtoRow second = retVal.get(1);

            Depth1CalculatorDtoRow toBeAdded = new Depth1CalculatorDtoRow();

            toBeAdded.setX(first.getX() | second.getX());
            toBeAdded.setY(first.getY() | second.getY());

            toBeAdded.setX_0_y_0_NW_free(first.isX_0_y_0_NW_free() | second.isX_0_y_0_NW_free());
            toBeAdded.setX_0_y_0_NW_taken(first.isX_0_y_0_NW_taken() || second.isX_0_y_0_NW_taken());

            toBeAdded.setX_0_y_0_N_free(first.isX_0_y_0_N_free() || second.isX_0_y_0_W_free());
            toBeAdded.setX_0_y_0_N_taken(first.isX_0_y_0_N_taken() || second.isX_0_y_0_N_taken());

            toBeAdded.setX_0_y_0_NE_free(first.isX_0_y_0_NE_free() || second.isX_0_y_0_NE_free());
            toBeAdded.setX_0_y_0_NE_taken(first.isX_0_y_0_NE_taken() || second.isX_0_y_0_NE_taken());

            toBeAdded.setX_0_y_0_E_free(first.isX_0_y_0_E_free() || second.isX_0_y_0_E_free());
            toBeAdded.setX_0_y_0_E_taken(first.isX_0_y_0_E_taken() || second.isX_0_y_0_E_taken());

            toBeAdded.setX_0_y_0_SE_free(first.isX_0_y_0_SE_free() || second.isX_0_y_0_SE_free());
            toBeAdded.setX_0_y_0_SE_taken(first.isX_0_y_0_SE_taken() || second.isX_0_y_0_SE_taken());

            toBeAdded.setX_0_y_0_S_free(first.isX_0_y_0_S_free() || second.isX_0_y_0_S_free());
            toBeAdded.setX_0_y_0_S_taken(first.isX_0_y_0_S_taken() || second.isX_0_y_0_S_taken());

            toBeAdded.setX_0_y_0_SW_free(first.isX_0_y_0_SW_free() || second.isX_0_y_0_SW_free());
            toBeAdded.setX_0_y_0_SW_taken(first.isX_0_y_0_SW_taken() || second.isX_0_y_0_SW_taken());

            toBeAdded.setX_0_y_0_W_free(first.isX_0_y_0_W_free() || second.isX_0_y_0_W_free());
            toBeAdded.setX_0_y_0_W_taken(first.isX_0_y_0_W_taken() || second.isX_0_y_0_W_taken());

            toBeAdded.setValue(first.getValue() | second.getValue());

            session.delete(first);
            session.delete(second);
            session.flush();
            session.save(toBeAdded);
            collapse(session, toBeAdded);
            return;

        }
    }


    @Override
    public int get(Depth1CalculatorDto depth1CalculatorDto) {
        Session session = sessionFactory.openSession();

        Criteria criteria = session.createCriteria(Depth1CalculatorDtoRow.class);

        getRestrictions(depth1CalculatorDto, null).stream().forEach(criteria::add);

        Depth1CalculatorDtoRow retVal = (Depth1CalculatorDtoRow) criteria.list().get(0);

        session.flush();
        session.close();
        return retVal.getValue();
    }

    private List<SimpleExpression> getRestrictions(Depth1CalculatorDto depth1CalculatorDto,
                                                   Integer value) {


        List<SimpleExpression> retVal = new ArrayList<>(10 + (value != null ? 1 : 0));

        retVal.add(Restrictions.eq("x", depth1CalculatorDto.getX()));
        retVal.add(Restrictions.eq("y", depth1CalculatorDto.getY()));

        if (depth1CalculatorDto.get_0_0_NW() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_NW_free", true));
        }
        if (depth1CalculatorDto.get_0_0_NW() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_NW_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_N() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_N_free", true));
        }
        if (depth1CalculatorDto.get_0_0_N() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_N_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_NE() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_NE_free", true));
        }
        if (depth1CalculatorDto.get_0_0_NE() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_NE_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_E() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_E_free", true));
        }
        if (depth1CalculatorDto.get_0_0_E() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_E_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_SE() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_SE_free", true));
        }
        if (depth1CalculatorDto.get_0_0_SE() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_SE_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_S() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_S_free", true));
        }
        if (depth1CalculatorDto.get_0_0_S() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_S_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_SW() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_SW_free", true));
        }
        if (depth1CalculatorDto.get_0_0_SW() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_SW_taken", true));
        }
        if (depth1CalculatorDto.get_0_0_W() == Edge.FREE) {
            retVal.add(Restrictions.eq("x_0_y_0_W_free", true));
        }
        if (depth1CalculatorDto.get_0_0_W() == Edge.TAKEN) {
            retVal.add(Restrictions.eq("x_0_y_0_W_taken", true));
        }
        return retVal;
    }


}
