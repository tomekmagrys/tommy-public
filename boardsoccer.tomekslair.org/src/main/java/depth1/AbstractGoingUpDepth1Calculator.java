package depth1;

import depth0.Depth0Calculator;
import depth0.Depth0CalculatorDto;
import edge.Edge;

public abstract class AbstractGoingUpDepth1Calculator implements Depth1Calculator {

    private final Depth0Calculator depth0Calculator;

    public AbstractGoingUpDepth1Calculator(Depth0Calculator depth0Calculator) {
        this.depth0Calculator = depth0Calculator;
    }

    public int internalCalculate(Depth1CalculatorDto depth1CalculatorDto) {

        int retVal = 0;
        int x = depth1CalculatorDto.getX();
        int y = depth1CalculatorDto.getY();

        if (depth1CalculatorDto.get_0_0_NW() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x - 1, y + 1)));
        }
        if (depth1CalculatorDto.get_0_0_N() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x, y + 1)));
        }
        if (depth1CalculatorDto.get_0_0_NE() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x + 1, y + 1)));
        }
        if (depth1CalculatorDto.get_0_0_E() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x + 1, y)));
        }
        if (depth1CalculatorDto.get_0_0_SE() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x + 1, y - 1)));
        }
        if (depth1CalculatorDto.get_0_0_S() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x, y - 1)));
        }
        if (depth1CalculatorDto.get_0_0_SW() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x - 1, y - 1)));
        }
        if (depth1CalculatorDto.get_0_0_W() == Edge.FREE) {
            retVal = Math.max(retVal, depth0Calculator.calculate(new Depth0CalculatorDto(x - 1, y)));
        }
        return retVal;
    }

}
