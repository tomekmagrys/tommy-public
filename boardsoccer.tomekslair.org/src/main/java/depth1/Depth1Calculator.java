package depth1;

public interface Depth1Calculator {

    int calculate(Depth1CalculatorDto depth1CalculatorDto);

}
