package depth1;

import depth0.Depth0Calculator;

public class BruteForceGoingUpDepth1Calculator extends AbstractGoingUpDepth1Calculator {

    public BruteForceGoingUpDepth1Calculator(Depth0Calculator depth0Calculator) {
        super(depth0Calculator);
    }

    @Override
    public int calculate(Depth1CalculatorDto depth1CalculatorDto) {
        return internalCalculate(depth1CalculatorDto);
    }
}
