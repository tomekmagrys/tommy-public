package depth1;

import javax.persistence.*;

@Entity
@Table(name="Depth1CalculatorDtoRow")
class Depth1CalculatorDtoRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int x;
    private int y;

    private boolean x_0_y_0_NW_free;
    private boolean x_0_y_0_NW_taken;

    private boolean x_0_y_0_N_free;
    private boolean x_0_y_0_N_taken;

    private boolean x_0_y_0_NE_free;
    private boolean x_0_y_0_NE_taken;

    private boolean x_0_y_0_E_free;
    private boolean x_0_y_0_E_taken;

    private boolean x_0_y_0_SE_free;
    private boolean x_0_y_0_SE_taken;

    private boolean x_0_y_0_S_free;
    private boolean x_0_y_0_S_taken;

    private boolean x_0_y_0_SW_free;
    private boolean x_0_y_0_SW_taken;

    private boolean x_0_y_0_W_free;
    private boolean x_0_y_0_W_taken;

    private int value;

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isX_0_y_0_NW_free() {
        return x_0_y_0_NW_free;
    }

    public void setX_0_y_0_NW_free(boolean x_0_y_0_NW_free) {
        this.x_0_y_0_NW_free = x_0_y_0_NW_free;
    }

    public boolean isX_0_y_0_NW_taken() {
        return x_0_y_0_NW_taken;
    }

    public void setX_0_y_0_NW_taken(boolean x_0_y_0_NW_taken) {
        this.x_0_y_0_NW_taken = x_0_y_0_NW_taken;
    }

    public boolean isX_0_y_0_N_free() {
        return x_0_y_0_N_free;
    }

    public void setX_0_y_0_N_free(boolean x_0_y_0_N_free) {
        this.x_0_y_0_N_free = x_0_y_0_N_free;
    }

    public boolean isX_0_y_0_N_taken() {
        return x_0_y_0_N_taken;
    }

    public void setX_0_y_0_N_taken(boolean x_0_y_0_N_taken) {
        this.x_0_y_0_N_taken = x_0_y_0_N_taken;
    }

    public boolean isX_0_y_0_NE_free() {
        return x_0_y_0_NE_free;
    }

    public void setX_0_y_0_NE_free(boolean x_0_y_0_NE_free) {
        this.x_0_y_0_NE_free = x_0_y_0_NE_free;
    }

    public boolean isX_0_y_0_NE_taken() {
        return x_0_y_0_NE_taken;
    }

    public void setX_0_y_0_NE_taken(boolean x_0_y_0_NE_taken) {
        this.x_0_y_0_NE_taken = x_0_y_0_NE_taken;
    }

    public boolean isX_0_y_0_E_free() {
        return x_0_y_0_E_free;
    }

    public void setX_0_y_0_E_free(boolean x_0_y_0_E_free) {
        this.x_0_y_0_E_free = x_0_y_0_E_free;
    }

    public boolean isX_0_y_0_E_taken() {
        return x_0_y_0_E_taken;
    }

    public void setX_0_y_0_E_taken(boolean x_0_y_0_E_taken) {
        this.x_0_y_0_E_taken = x_0_y_0_E_taken;
    }

    public boolean isX_0_y_0_SE_free() {
        return x_0_y_0_SE_free;
    }

    public void setX_0_y_0_SE_free(boolean x_0_y_0_SE_free) {
        this.x_0_y_0_SE_free = x_0_y_0_SE_free;
    }

    public boolean isX_0_y_0_SE_taken() {
        return x_0_y_0_SE_taken;
    }

    public void setX_0_y_0_SE_taken(boolean x_0_y_0_SE_taken) {
        this.x_0_y_0_SE_taken = x_0_y_0_SE_taken;
    }

    public boolean isX_0_y_0_S_free() {
        return x_0_y_0_S_free;
    }

    public void setX_0_y_0_S_free(boolean x_0_y_0_S_free) {
        this.x_0_y_0_S_free = x_0_y_0_S_free;
    }

    public boolean isX_0_y_0_S_taken() {
        return x_0_y_0_S_taken;
    }

    public void setX_0_y_0_S_taken(boolean x_0_y_0_S_taken) {
        this.x_0_y_0_S_taken = x_0_y_0_S_taken;
    }

    public boolean isX_0_y_0_SW_free() {
        return x_0_y_0_SW_free;
    }

    public void setX_0_y_0_SW_free(boolean x_0_y_0_SW_free) {
        this.x_0_y_0_SW_free = x_0_y_0_SW_free;
    }

    public boolean isX_0_y_0_SW_taken() {
        return x_0_y_0_SW_taken;
    }

    public void setX_0_y_0_SW_taken(boolean x_0_y_0_SW_taken) {
        this.x_0_y_0_SW_taken = x_0_y_0_SW_taken;
    }

    public boolean isX_0_y_0_W_free() {
        return x_0_y_0_W_free;
    }

    public void setX_0_y_0_W_free(boolean x_0_y_0_W_free) {
        this.x_0_y_0_W_free = x_0_y_0_W_free;
    }

    public boolean isX_0_y_0_W_taken() {
        return x_0_y_0_W_taken;
    }

    public void setX_0_y_0_W_taken(boolean x_0_y_0_W_taken) {
        this.x_0_y_0_W_taken = x_0_y_0_W_taken;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Depth1CalculatorDtoRow{" +
                "x=" + x +
                ", y=" + y +
                ", x_0_y_0_NW_free=" + x_0_y_0_NW_free +
                ", x_0_y_0_NW_taken=" + x_0_y_0_NW_taken +
                ", x_0_y_0_N_free=" + x_0_y_0_N_free +
                ", x_0_y_0_N_taken=" + x_0_y_0_N_taken +
                ", x_0_y_0_NE_free=" + x_0_y_0_NE_free +
                ", x_0_y_0_NE_taken=" + x_0_y_0_NE_taken +
                ", x_0_y_0_E_free=" + x_0_y_0_E_free +
                ", x_0_y_0_E_taken=" + x_0_y_0_E_taken +
                ", x_0_y_0_SE_free=" + x_0_y_0_SE_free +
                ", x_0_y_0_SE_taken=" + x_0_y_0_SE_taken +
                ", x_0_y_0_S_free=" + x_0_y_0_S_free +
                ", x_0_y_0_S_taken=" + x_0_y_0_S_taken +
                ", x_0_y_0_SW_free=" + x_0_y_0_SW_free +
                ", x_0_y_0_SW_taken=" + x_0_y_0_SW_taken +
                ", x_0_y_0_W_free=" + x_0_y_0_W_free +
                ", x_0_y_0_W_taken=" + x_0_y_0_W_taken +
                ", value=" + value +
                '}';
    }
}
