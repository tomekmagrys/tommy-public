package depth1;

import depth0.Depth0Calculator;

public class HibernateGoingUpDepth1Calculator extends AbstractGoingUpDepth1Calculator {

    private final Depth1Dao depth1Dao;

    public HibernateGoingUpDepth1Calculator(Depth0Calculator depth0Calculator, Depth1Dao depth1Dao) {
        super(depth0Calculator);
        this.depth1Dao = depth1Dao;
    }

    @Override
    public int calculate(Depth1CalculatorDto depth1CalculatorDto) {
        if(!depth1Dao.containsKey(depth1CalculatorDto)){
            depth1Dao.put(depth1CalculatorDto, internalCalculate(depth1CalculatorDto));
        }

        return depth1Dao.get(depth1CalculatorDto);
    }
}
