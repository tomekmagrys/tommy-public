import depth0.Depth0CalculatorDto;
import depth0.HibernateDepth0Dao;
import depth1.HibernateDepth1Dao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {

    public static void main(String[] args){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/boardsoccer.tomekslair.org/spring-main.xml");

        Depth0CalculatorDto depth0CalculatorDto = new Depth0CalculatorDto(6,4);

        HibernateDepth0Dao depth0Dao = (HibernateDepth0Dao) applicationContext.getBean("depth0Dao");
        depth0Dao.store(depth0CalculatorDto , 7);

        HibernateDepth1Dao depth1Dao = (HibernateDepth1Dao) applicationContext.getBean("depth1Dao");
    }

}
