<?php
$subdir [] = "gallery";
$subdir [] = basename(__DIR__) ;

require (__DIR__ . "/" . str_repeat ( "../", count ( $subdir ) ) . "declarations.php");

$HTMLCenter .= "          <table cellspacing='0' cellpadding='0'>\n";
$HTMLCenter .= "            <tr>\n";
$HTMLCenter .= "              <td align='center' bgcolor='#FFC433' colspan='6'>\n";
$HTMLCenter .= "                <font class='naglowek'><b>..:: ".albumTitle($subdir[1],false)." ::..</b></font>\n";
$HTMLCenter .= "              </td>\n";
$HTMLCenter .= "            </tr>\n";

$photos = array ();
if ($dh = opendir ( __DIR__ )) {
	while ( ($file = readdir ( $dh )) !== false ) {
		if (! is_file ( __DIR__ . "/" . $file )) {
			continue;
		}
		if (substr ( $file, 0, 1 ) === ".") {
			continue;
		}
		if (substr ( $file, - 4 ) === ".JPG") {
			if (substr ( $file, - 10 ) != "_small.JPG") {
				$photos [] = substr ( $file, 0, - 4 );
			}
		}
	}
	closedir ( $dh );
}
if (count ( $photos ) > 0) {
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= "              <td>&nbsp;</td>\n";
	$HTMLCenter .= "            </tr>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= "              <td colspan='3' align='center' class='prawa'><a class='thanks' href='allinone.zip'><b>allinone.zip</b></a><font class='thanks'> - Wszystko w jednym zip</font></td>\n";
	$HTMLCenter .= "            </tr>\n";	
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= "              <td>&nbsp;</td>\n";
	$HTMLCenter .= "            </tr>\n";	
	sort ( $photos );
}

$i = 0;
foreach ( $photos as $photo ) {
	if ($i % 3 == 0) {
		$HTMLtrup = "";
		$HTMLtrdown = "";
	}
	$i ++;
	$HTMLtrup .= "              <td class='prawa'><a href='" . $photo . ".JPG'><img border='0' width='143' src='" . $photo . "_small.JPG' /></a></td>\n";
	$HTMLtrdown .= "              <td align='center' class='prawa'><font class=thanks>" . $photo . "</font></td>\n";
	if ($i % 3 == 0) {
		$HTMLCenter .= "            <tr>\n";
		$HTMLCenter .= $HTMLtrup;
		$HTMLCenter .= "            </tr>\n";
		$HTMLCenter .= "            <tr>\n";
		$HTMLCenter .= $HTMLtrdown;
		$HTMLCenter .= "            </tr>\n";
	}
}

if ($i % 3 == 1) {
	$HTMLtrup = "              <td>&nbsp;</td>\n" . $HTMLtrup . "              <td>&nbsp;</td>\n";
	$HTMLtrdown = "              <td>&nbsp;</td>\n" . $HTMLtrdown . "              <td&nbsp;</td>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrup;
	$HTMLCenter .= "            </tr>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrdown;
	$HTMLCenter .= "            </tr>\n";
}
if ($i % 3 == 2) {
	$HTMLCenter .= "            <tr><td colspan='3' align='center'><table border='0'  cellspacing='0' cellpadding='0'><tr>\n";
	$HTMLCenter .= $HTMLtrup;
	$HTMLCenter .= "            </tr>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrdown;
	$HTMLCenter .= "            </tr></table></td></tr>\n";
}
$HTMLCenter .= "          </table>";

require (__DIR__ . "/" . str_repeat ( "../", count ( $subdir ) ) . "core.php");
?>