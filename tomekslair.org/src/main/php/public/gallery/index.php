<?php
$subdir [] = "gallery";
require (__DIR__ . "/" . str_repeat ( "../", count ( $subdir ) ) . "declarations.php");

$HTMLCenter .= "          <table cellspacing='0' cellpadding='0'>\n";
$HTMLCenter .= "            <tr>\n";
$HTMLCenter .= "              <td align='center' bgcolor='#FFC433' colspan='6'>\n";
$HTMLCenter .= "                <font class='naglowek'><b>..:: Galeria ::..</b></font>\n";
$HTMLCenter .= "              </td>\n";
$HTMLCenter .= "            </tr>\n";

$albums = array ();
if ($dh = opendir ( __DIR__ )) {
	while ( ($file = readdir ( $dh )) !== false ) {
		if (! is_dir ( __DIR__ . "/" . $file )) {
			continue;
		}
		if ($file == ".") {
			continue;
		}
		if ($file == "..") {
			continue;
		}
		$albums [] = $file;
	}
	closedir ( $dh );
}
if (count ( $albums ) > 0) {
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= "              <td>&nbsp;</td>\n";
	$HTMLCenter .= "            </tr>\n";
	sort($albums);
}

$i = 0;
foreach ( $albums as $album ) {
	if($i % 3 == 0){
		$HTMLtrup = "";
		$HTMLtrdown = "";
	}
	$i ++;
	$HTMLtrup .= "              <td class='prawa'><a href=\"".$album."\"><img border='0' width='143' src=\"" . $album . "-cover.jpg\" /></a></td>\n";
	$title = albumTitle($album,true);
	$HTMLtrdown .= "              <td align='center' class='prawa'><font class=thanks>" . $title . "</font></td>\n";
	if ($i % 3 == 0) {
		$HTMLCenter .= "            <tr>\n";
		$HTMLCenter .= $HTMLtrup;
		$HTMLCenter .= "            </tr>\n";
		$HTMLCenter .= "            <tr>\n";
		$HTMLCenter .= $HTMLtrdown;
		$HTMLCenter .= "            </tr>\n";
	}
}
if ($i % 3 == 1) {
	$HTMLtrup = "              <td width='143'>&nbsp;</td>\n" . $HTMLtrup . "              <td width='143'>&nbsp;</td>\n";
	$HTMLtrdown = "              <td width='143'>&nbsp;</td>\n" . $HTMLtrdown . "              <td width='143'>&nbsp;</td>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrup;
	$HTMLCenter .= "            </tr>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrdown;
	$HTMLCenter .= "            </tr>\n";
}
if ($i % 3 == 2) {
	$HTMLCenter .= "            <tr><td colspan='3' align='center'><table border='0'  cellspacing='0' cellpadding='0'><tr>\n";
	$HTMLCenter .= $HTMLtrup;
	$HTMLCenter .= "            </tr>\n";
	$HTMLCenter .= "            <tr>\n";
	$HTMLCenter .= $HTMLtrdown;
	$HTMLCenter .= "            </tr></table></td></tr>\n";
}
$HTMLCenter .= "          </table>";

require (__DIR__ . "/" . str_repeat ( "../", count ( $subdir ) ) . "core.php");
?>