package org.tomekslair.learning.concurrency.philosopher;

import java.util.concurrent.Semaphore;

class Philosopher implements Runnable {

	final Semaphore s1;
	final Semaphore s2;
	final int nr;

	public Philosopher(Semaphore s1, Semaphore s2, int nr) {
		this.s1 = s1;
		this.s2 = s2;
		this.nr = nr;
	}

	public void acquire() throws InterruptedException {
        s1.acquire();
        s2.acquire();
        System.out.println(nr + ": jem");
        s2.release();
        s1.release();
    }


	@Override
	public void run() {
		try {
			while (true) {
				acquire();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
