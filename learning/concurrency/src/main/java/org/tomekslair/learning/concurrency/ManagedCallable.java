package org.tomekslair.learning.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

abstract class ManagedCallable<T> implements Callable<T> {
	protected final CountDownLatch countDownLatch;
	protected final AtomicInteger counter;
	protected final AtomicInteger activeThreadCounter;

	public ManagedCallable(CountDownLatch countDownLatch, AtomicInteger counter,
			AtomicInteger activeThreadCounter) {
		this.countDownLatch = countDownLatch;
		this.counter = counter;
		this.activeThreadCounter = activeThreadCounter;
	}

	public abstract T exactCall() throws Exception;

	@Override
	public final T call() throws Exception {
		activeThreadCounter.incrementAndGet();
		T retVal = exactCall();
		activeThreadCounter.decrementAndGet();
		countDownLatch.countDown();
		return retVal;
	}
}
