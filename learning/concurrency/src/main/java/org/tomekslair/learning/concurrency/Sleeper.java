package org.tomekslair.learning.concurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

class Sleeper extends ManagedCallable<Void> {

	Sleeper(CountDownLatch countDownLatch, AtomicInteger counter,
			AtomicInteger activeThreadCounter) {
		super(countDownLatch, counter, activeThreadCounter);
	}

	@Override
	public Void exactCall() throws InterruptedException {
		System.out.println(Thread.currentThread().getName() + "::"
				+ System.currentTimeMillis() + "::Start");
		Thread.sleep(10);
		System.out.println(Thread.currentThread().getName() + "::"
				+ System.currentTimeMillis() + "::End");
		return null;
	}

}
