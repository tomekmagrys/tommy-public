package org.tomekslair.learning.concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class AlgorithmsExample {
		
	public static void main(String[] args) {
		List<Long> exampleList = new ArrayList<Long>();
		
		for(int i = 0 ; i < 1000000 ; i++){
			exampleList.add((new Random()).nextLong());
		}
		
		Collections.sort(exampleList);
		
		System.out.println(Collections.binarySearch(exampleList, 1L));
		
		Collections.reverse(exampleList);
		
		Collections.shuffle(exampleList);
		
		
		
	}
}
