package org.tomekslair.learning.concurrency;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetExample {
	
	private static void test(Set<Long> q) {
		SetAdder p = new SetAdder(q);
		new Thread(p).start();
	}
	
	public static void main(String[] args) {
		test(new TreeSet<Long>());
	}
}
