package org.tomekslair.learning.concurrency;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

class RandomSleeper extends ManagedCallable<String> {

	final String retVal;
	final Random random;
	
	RandomSleeper(CountDownLatch countDownLatch, AtomicInteger counter,
			AtomicInteger activeThreadCounter, String retVal) {
		super(countDownLatch, counter, activeThreadCounter);
		this.retVal = retVal;
		this.random = new Random();
	}

	@Override
	public String exactCall() throws InterruptedException {
		Thread.sleep(random.nextInt(1000));
		return retVal;
	}

}
