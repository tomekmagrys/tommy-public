package org.tomekslair.learning.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreExecutorExample {

	public static void main(String args[]) {
		Semaphore s1 = new Semaphore(1);
		Semaphore s2 = new Semaphore(1);
		Semaphore s3 = new Semaphore(1);
		Semaphore s4 = new Semaphore(1);
		Semaphore s5 = new Semaphore(1);

		Semaphore stol = new Semaphore(4);

		ExecutorService es = Executors.newFixedThreadPool(5);
		es.submit(new Philosophe(s1, s2, stol, 1));
		es.submit(new Philosophe(s2, s3, stol, 2));
		es.submit(new Philosophe(s3, s4, stol, 3));
		es.submit(new Philosophe(s4, s5, stol, 4));
		es.submit(new Philosophe(s5, s1, stol, 5));
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//es.shutdownNow();
		
		//(new Thread()).
		
	}
}