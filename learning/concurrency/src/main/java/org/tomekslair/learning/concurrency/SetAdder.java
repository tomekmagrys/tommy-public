package org.tomekslair.learning.concurrency;

import java.util.Random;
import java.util.Set;

class SetAdder implements Runnable {

	private final Set<Long> queue;

	SetAdder(Set<Long> q) {
		queue = q;
	}

	public void run() {
		while (true) {
			queue.add(produce());
		}
	}

	Long produce() {
		Long retVal = (new Random()).nextLong();
		System.out.println(retVal);
		return retVal;

	}
}
