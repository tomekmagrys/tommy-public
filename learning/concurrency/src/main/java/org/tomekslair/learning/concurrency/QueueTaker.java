package org.tomekslair.learning.concurrency;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

class QueueTaker implements Runnable {
	private final BlockingQueue<Long> queue;
	private final int howManyTimes;
	private final CountDownLatch countDownLatch;
	private final AtomicLong counter;

	QueueTaker(BlockingQueue<Long> q, int howManyTimes,
			CountDownLatch countDownLatch, AtomicLong counter) {
		queue = q;
		this.howManyTimes = howManyTimes;
		this.countDownLatch = countDownLatch;
		this.counter = counter;
	}

	public void run() {
		for (int i = 0; i < howManyTimes; i++) {
			try {
				consume(queue.take());
			} catch (InterruptedException e) {

			}
			counter.incrementAndGet();
		}
		countDownLatch.countDown();
	}

	void consume(Long x) {
	}
}
