package org.tomekslair.learning.concurrency;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {

	public static void main(String args[]) throws InterruptedException{
		(new Driver()).main();
	}
	
}

class Driver {
	public final static int N = 10;
	
	   void main() throws InterruptedException {
	     CountDownLatch startSignal = new CountDownLatch(1);
	     CountDownLatch doneSignal = new CountDownLatch(N);

	     for (int i = 0; i < N; ++i)
	       new Thread(new Worker(startSignal, doneSignal)).start();

	     System.out.println("Before everything");
	     startSignal.countDown();      // let all threads proceed
	     doneSignal.await();           // wait for all to finish
	     System.out.println("After everything");
	   }
	 }

class Worker implements Runnable {
	   private final CountDownLatch startSignal;
	   private final CountDownLatch doneSignal;
	   Worker(CountDownLatch startSignal, CountDownLatch doneSignal) {
	      this.startSignal = startSignal;
	      this.doneSignal = doneSignal;
	   }
	   public void run() {
	      try {
	        startSignal.await();
	        System.out.println("Ogarniam temat");
	        doneSignal.countDown();
	      } catch (InterruptedException ex) {} // return;
	   }

	 }
