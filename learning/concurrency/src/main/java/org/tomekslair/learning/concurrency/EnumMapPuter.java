package org.tomekslair.learning.concurrency;

import java.util.Map;
import java.util.Random;

class EnumMapPuter implements Runnable {
	   private final Map<ExtendEnumExample,Long> queue;
	   EnumMapPuter(Map<ExtendEnumExample,Long>  q) { queue = q; }
	   public void run() {
	     while (true) { queue.put(ExtendEnumExample.GRUBO,produce()); }
	   }
	   Long produce() { 
		   Long retVal = (new Random()).nextLong();
		   System.out.println(retVal);
		   return retVal;
				   
		   }
	 }
