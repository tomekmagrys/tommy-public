package org.tomekslair.learning.concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConvinientImplementationsExample {

	public static void main(String[] args) {
		List<Long> emptyList = (List<Long>) Collections.EMPTY_LIST;
		
		try {
			emptyList.add(Long.valueOf(0));
		} catch (UnsupportedOperationException e) {

		}
		
		List<Long> singletonList = (List<Long>) Collections.singletonList(Long.valueOf(0));
		
		try {
			emptyList.add(Long.valueOf(0));
			assert false;
		} catch (UnsupportedOperationException e) {

		}		
		
		List<Long> modifyable = new ArrayList<Long>();
		List<Long> unmodifyable = Collections.unmodifiableList(modifyable);
		
		try {
			unmodifyable.add(Long.valueOf(0));
			assert false;
		} catch (UnsupportedOperationException e) {

		}
		
		
	}
}
