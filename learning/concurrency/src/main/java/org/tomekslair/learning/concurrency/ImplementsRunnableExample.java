package org.tomekslair.learning.concurrency;

public class ImplementsRunnableExample implements Runnable {
	 
	Object lock;
	
    public ImplementsRunnableExample(Object lock) {
		this.lock = lock;
	}

	@Override
    public void run() {
        System.out.println("3. Doing heavy processing - START "+Thread.currentThread().getName());
        try {
        	System.out.println("4. Before sleep");
            Thread.sleep(1000);
            System.out.println("5. After sleep");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("6. Doing heavy processing - END "+Thread.currentThread().getName());
    }

 
}
