package org.tomekslair.learning.concurrency;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

class MapPuter implements Runnable {
	private final Map<Long, Long> mapToPut;
	private final Random random;
	private final int howManyTimes;
	private final CountDownLatch countDownLatch;
	private final AtomicLong counter;

	MapPuter(Map<Long, Long> mapToPut, int howManyTimes,
			CountDownLatch countDownLatch, AtomicLong counter) {
		this.mapToPut = mapToPut;
		this.random = new Random();
		this.howManyTimes = howManyTimes;
		this.countDownLatch = countDownLatch;
		this.counter = counter;
	}

	public void run() {
		for (int i = 0; i < howManyTimes; i++) {
			mapToPut.put(produce(), produce());
			counter.incrementAndGet();
		}
		countDownLatch.countDown();
	}

	Long produce() {
		Long retVal = random.nextLong();
		return retVal;
	}
}
