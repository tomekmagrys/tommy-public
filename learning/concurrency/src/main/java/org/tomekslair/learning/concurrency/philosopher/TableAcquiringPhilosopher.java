package org.tomekslair.learning.concurrency.philosopher;

import java.util.concurrent.Semaphore;

class TableAcquiringPhilosopher extends Philosopher {

    private final Semaphore stol;

    public TableAcquiringPhilosopher(Semaphore s1, Semaphore s2, Semaphore stol, int nr) {
        super(s1, s2, nr);
        this.stol = stol;
    }

    public void acquire() throws InterruptedException {
        stol.acquire();
        super.acquire();
        stol.release();
    }


    @Override
    public void run() {
        try {
            while (true) {
                acquire();
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
