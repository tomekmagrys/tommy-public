package org.tomekslair.learning.concurrency;

import java.util.EnumMap;

public class EnumMapExample {
	
	private static void test(EnumMap<ExtendEnumExample,Long> q) {
		EnumMapPuter p = new EnumMapPuter(q);
		new Thread(p).start();
	}
	
	public static void main(String[] args) {
		test(new EnumMap<ExtendEnumExample,Long>(ExtendEnumExample.class));
	}
}
