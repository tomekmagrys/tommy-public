package org.tomekslair.learning.concurrency;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

abstract class ManagedRunnable implements Runnable {
	protected final Random random;
	protected final int howManyTimes;
	protected final CountDownLatch countDownLatch;
	protected final AtomicInteger counter;
	protected final AtomicInteger activeThreadCounter;

	public ManagedRunnable(int howManyTimes, CountDownLatch countDownLatch,
			AtomicInteger counter, AtomicInteger activeThreadCounter) {
		this.random = new Random();
		this.howManyTimes = howManyTimes;
		this.countDownLatch = countDownLatch;
		this.counter = counter;
		this.activeThreadCounter = activeThreadCounter;
	}

	@Override
	public final void run() {
		activeThreadCounter.incrementAndGet();
		for (int i = 0; i < howManyTimes; i++) {
			doIt();
			counter.incrementAndGet();
		}
		countDownLatch.countDown();
		activeThreadCounter.decrementAndGet();
	}

	public abstract void doIt();
}
