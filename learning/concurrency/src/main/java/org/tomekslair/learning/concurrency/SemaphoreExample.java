package org.tomekslair.learning.concurrency;

import java.util.concurrent.Semaphore;

public class SemaphoreExample {

	public static void main(String args[]) {
		Semaphore s1 = new Semaphore(1);
		Semaphore s2 = new Semaphore(1);
		Semaphore s3 = new Semaphore(1);
		Semaphore s4 = new Semaphore(1);
		Semaphore s5 = new Semaphore(1);

		Semaphore stol = new Semaphore(4);

		(new Thread(new Philosophe(s1, s2, stol, 1))).start();
		(new Thread(new Philosophe(s2, s3, stol, 2))).start();
		(new Thread(new Philosophe(s3, s4, stol, 3))).start();
		(new Thread(new Philosophe(s4, s5, stol, 4))).start();
		(new Thread(new Philosophe(s5, s1, stol, 5))).start();

	}
}