package org.tomekslair.learning.concurrency;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

class ListAdder implements Runnable {
	private final List<Long> listToAdd;
	private final Random random;
	private final int howManyTimes;
	private final CountDownLatch countDownLatch;
	private final AtomicLong counter;

	ListAdder(List<Long> listToAdd, int howManyTimes,
			CountDownLatch countDownLatch, AtomicLong counter) {
		this.listToAdd = listToAdd;
		this.random = new Random();
		this.howManyTimes = howManyTimes;
		this.countDownLatch = countDownLatch;
		this.counter = counter;
	}

	public void run() {
		for (int i = 0; i < howManyTimes; i++) {
			listToAdd.add(produce());
			counter.incrementAndGet();
		}
		countDownLatch.countDown();
	}

	Long produce() {
		Long retVal = random.nextLong();
		return retVal;

	}
}
