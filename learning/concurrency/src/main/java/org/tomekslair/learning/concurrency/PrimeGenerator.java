package org.tomekslair.learning.concurrency;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

class PrimeGenerator extends ManagedRunnable {

	private final List<BigInteger> primes = new ArrayList<>();
	private volatile boolean cancelled = false;
	
	PrimeGenerator(int howManyTimes, CountDownLatch countDownLatch,
			AtomicInteger counter, AtomicInteger activeThreadCounter) {
		super(howManyTimes, countDownLatch, counter, activeThreadCounter);
	}

	public void cancel(){
		cancelled = true;
	}
	
	@Override
	public void doIt() {
		BigInteger p = BigInteger.ONE;
		
		while(!cancelled){
			p = p.nextProbablePrime();
			synchronized(this){
				primes.add(p);
			}
		}
	}
	
	public synchronized List<BigInteger> get() {
		return primes;
	}
	
}
