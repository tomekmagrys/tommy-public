package org.tomekslair.learning.concurrency;

public class ExtendsThreadExample extends Thread {

	Object lock;
	boolean isWakeupNeeded = false;
	
	public void setWakeupNeeded(){
		isWakeupNeeded = true;
	}
	
	public ExtendsThreadExample(String name, Object lock) {
		super(name);
		this.lock = lock;
	}

	
	
	@Override
	public void run() {
		try {
			synchronized (lock) {
			      while (!isWakeupNeeded) {
			          lock.wait();
			      }
			}
			System.out.println("3. " + Thread.currentThread().getName());
			System.out.println("4. Before sleep");
			Thread.sleep(1000);
			System.out.println("5. After sleep");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("6. MyThread - END "
				+ Thread.currentThread().getName());
	}

}
