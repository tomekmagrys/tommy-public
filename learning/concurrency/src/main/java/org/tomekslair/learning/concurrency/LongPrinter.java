package org.tomekslair.learning.concurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

class LongPrinter extends ManagedRunnable {

	LongPrinter(int howManyTimes, CountDownLatch countDownLatch,
			AtomicInteger counter, AtomicInteger activeThreadCounter) {
		super(howManyTimes, countDownLatch, counter, activeThreadCounter);
	}

	@Override
	public void doIt() {
		System.out.println("[" + activeThreadCounter.get() + "] "
				+ random.nextLong());
	}
}
