package org.tomekslair.learning.concurrency;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarierExample {

	static Runnable barrierAction;

	static {
		barrierAction = new Runnable() {
			public void run() {
				System.out.println("Checkpoint ");
			}
		};
	}

	public static void main(String args[]) {
		CyclicBarrier barrier = new CyclicBarrier(2, barrierAction);

		CyclicBarrierRunnable barrierRunnable1 = new CyclicBarrierRunnable(
				barrier,100);

		CyclicBarrierRunnable barrierRunnable2 = new CyclicBarrierRunnable(
				barrier,1000);

		new Thread(barrierRunnable1).start();
		new Thread(barrierRunnable2).start();
	}

}

class CyclicBarrierRunnable implements Runnable {

	CyclicBarrier barrier = null;

	final int timeout;
	
	public CyclicBarrierRunnable(CyclicBarrier barrier,int timeout) {

		this.barrier = barrier;
		this.timeout = timeout;
	}

	public void run() {
		try {
			for (int i = 0; i < 100; i++) {
				Thread.sleep(timeout);
				System.out.println(Thread.currentThread().getName()
						+ " waiting at barrier 1");
				this.barrier.await();
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}