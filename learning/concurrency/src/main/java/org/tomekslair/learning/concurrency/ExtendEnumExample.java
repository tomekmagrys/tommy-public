package org.tomekslair.learning.concurrency;

public enum ExtendEnumExample {
	GRUBO(0),SUABO(1);
	
	final int code;
	
	ExtendEnumExample(int code){
		this.code = code;
	}
}
