package org.tomekslair.learning.concurrency;

import java.util.Map;
import java.util.Random;

class MapGeter implements Runnable {
	private final Map<Long, Long> queue;

	MapGeter(Map<Long, Long> q) {
		queue = q;
	}

	public void run() {
		while (true) {
			queue.put(produce(), produce());
		}
	}

	Long produce() {
		Long retVal = (new Random()).nextLong();
		System.out.println(retVal);
		return retVal;

	}

}
