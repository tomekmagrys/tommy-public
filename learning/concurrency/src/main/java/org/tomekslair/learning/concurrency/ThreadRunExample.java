package org.tomekslair.learning.concurrency;

public class ThreadRunExample {

	public static void main(String[] args) {
		
		Object lock = new Object();
		
		ExtendsThreadExample t3 = new ExtendsThreadExample("t3",lock);
		ExtendsThreadExample t4 = new ExtendsThreadExample("t4",lock);
		System.out.println("1. Starting MyThreads");
		t3.start();
		t4.start();
		System.out.println("2. MyThreads has been started");
		synchronized (lock) {
			t3.setWakeupNeeded();
			t4.setWakeupNeeded();
			lock.notifyAll();
		}
		

	}
}
