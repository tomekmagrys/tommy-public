package org.tomekslair.learning.concurrency;

import java.util.EnumSet;

public class EnumSetExample {
	
	private static void test(EnumSet<ExtendEnumExample> q) {
		EnumSetPuter p = new EnumSetPuter(q);
		new Thread(p).start();
	}
	
	public static void main(String[] args) {
		test(EnumSet.noneOf(ExtendEnumExample.class));
	}
}
