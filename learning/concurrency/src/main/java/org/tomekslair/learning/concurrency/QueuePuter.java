package org.tomekslair.learning.concurrency;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

class QueuePuter implements Runnable {
	private final BlockingQueue<Long> queue;
	private final Random random;
	private final int howManyTimes;
	private final CountDownLatch countDownLatch;
	private final AtomicLong counter;

	QueuePuter(BlockingQueue<Long> q, int howManyTimes,
			CountDownLatch countDownLatch, AtomicLong counter) {
		queue = q;
		this.random = new Random();
		this.howManyTimes = howManyTimes;
		this.countDownLatch = countDownLatch;
		this.counter = counter;
	}

	public void run() {
		for (int i = 0; i < howManyTimes; i++) {
			try {
				queue.put(produce());
			} catch (InterruptedException e) {

			}
			counter.incrementAndGet();
		}
		countDownLatch.countDown();
	}

	Long produce() {
		Long retVal = random.nextLong();
		return retVal;
	}
}
