package com.hsbc.interview.homework.service;

import com.hsbc.interview.homework.db.Tweet;
import com.hsbc.interview.homework.db.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TweetServiceTest {

    @Autowired
    TweetService tweetService;

    @Autowired
    FollowService followService;

    @Autowired
    UserService userService;

    @Test
    public void contextLoads() {

    }

    @Test
    public void singleTweetTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you #dragons, Khaleese");
    }

    @Test
    public void TwoTweetsOfTheSameUserCheckThatAccountIsCreatedOnceTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you #dragons, @khaleese");
        tweetService.tweet(TestConstants.JOHN_SNOW, "It seems that Night King is really #powerfull...");

        Iterable<User> users = userService.findAll();
        Iterator<User> userIterator = users.iterator();
        Assert.assertTrue(userIterator.hasNext());

        User johnSnow = userIterator.next();

        Assert.assertEquals(johnSnow.getLogin(), TestConstants.JOHN_SNOW);
        Assert.assertFalse(userIterator.hasNext());
    }

    @Test
    public void TwoTweetsOfTheSameUserCheckThatWallIsReverseChronologicalTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you #dragons, @khaleese");
        tweetService.tweet(TestConstants.JOHN_SNOW, "It seems that Night King is really #powerfull...");
        tweetService.tweet(TestConstants.JOHN_SNOW, "Help from Loyd Baylish #littlefinger may be crucial");

        Iterator<Tweet> johnSnowTweetsIterator = tweetService.getWall(TestConstants.JOHN_SNOW).iterator();

        Tweet first = johnSnowTweetsIterator.next();
        Tweet second = johnSnowTweetsIterator.next();
        Tweet third = johnSnowTweetsIterator.next();

        Assert.assertTrue(first.getDateTime().isAfter(second.getDateTime()));
        Assert.assertTrue(second.getDateTime().isAfter(third.getDateTime()));

    }

    @Test
    public void FollowTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you #dragons, @khaleese");
        tweetService.tweet("khaleese", "I'm #coming");
        tweetService.tweet(TestConstants.JOHN_SNOW, "It seems that Night King is really #powerfull...");
        tweetService.tweet(TestConstants.ARYA_STARK, "Nobody is going to #follow me");

        followService.follow(TestConstants.JOHN_SNOW, "khaleese");
        followService.follow("khaleese", TestConstants.JOHN_SNOW);
        followService.follow(TestConstants.ARYA_STARK, "khaleese");
        followService.follow(TestConstants.ARYA_STARK, TestConstants.JOHN_SNOW);


        {

            Iterator<Tweet> johnSnowTimelineIterator = tweetService.getTimeline(TestConstants.JOHN_SNOW).iterator();

            Tweet first = johnSnowTimelineIterator.next();
            Assert.assertFalse(johnSnowTimelineIterator.hasNext());

        }

        {

            Iterator<Tweet> khaleeseTimelineIterator = tweetService.getTimeline("khaleese").iterator();

            Tweet first = khaleeseTimelineIterator.next();
            Tweet second = khaleeseTimelineIterator.next();

            Assert.assertTrue(first.getDateTime().isAfter(second.getDateTime()));
            Assert.assertFalse(khaleeseTimelineIterator.hasNext());

        }
        {

            Iterator<Tweet> khaleeseTimelineIterator = tweetService.getTimeline(TestConstants.ARYA_STARK).iterator();

            Tweet first = khaleeseTimelineIterator.next();
            Tweet second = khaleeseTimelineIterator.next();
            Tweet third = khaleeseTimelineIterator.next();

            Assert.assertTrue(first.getDateTime().isAfter(second.getDateTime()));
            Assert.assertTrue(second.getDateTime().isAfter(third.getDateTime()));

            Assert.assertFalse(khaleeseTimelineIterator.hasNext());

        }

    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void GetWallOfNotExistingUserTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you #dragons, @khaleese");

        tweetService.getWall("khalesee");

    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void emptyTweetTest() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "");
    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testTweetLongerThan140Letters() {
        tweetService.tweet(TestConstants.JOHN_SNOW, "I need you " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                "#dragons " +
                ", Khaleese");
    }


}