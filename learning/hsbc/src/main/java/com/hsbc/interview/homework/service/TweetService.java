package com.hsbc.interview.homework.service;


import com.hsbc.interview.homework.db.Tweet;
import com.hsbc.interview.homework.db.User;
import com.hsbc.interview.homework.repository.TweetRepository;
import com.hsbc.interview.homework.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TweetService {

    @Autowired
    TweetRepository tweetRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    public Tweet tweet(String login, String post){
        if(StringUtils.isEmpty(post)){
            throw new IllegalArgumentException("Cannot post empty tweet");
        }
        if(post.length() > 140){
            throw new IllegalArgumentException("Sorry, tweet is limited to 140 characters");
        }

        User user = userService.createIfNotExists(login);

        Tweet tweet = new Tweet();

        tweet.setAuthor(user);
        tweet.setPost(post);
        tweet.setDateTime(LocalDateTime.now());

        tweetRepository.save(tweet);

        return tweet;

    }

    public List<Tweet> getWall(String login){
        User user = userRepository.findByLogin(login);

        if(user == null) {
            throw new IllegalArgumentException("User not found");
        }

        return tweetRepository.findAllByAuthorOrderByDateTimeDesc(user);
    }

    public List<Tweet> getTimeline(String login){
        User user = userRepository.findByLogin(login);

        if(user == null) {
            throw new IllegalArgumentException("User not found");
        }

        return tweetRepository.getTimeline(user);
    }


}
