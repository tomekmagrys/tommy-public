package com.hsbc.interview.homework.common;

public class Constants {

    public static final String YOU_CANNOT_FOLLOW_YOURSELF_SORRY_ABOUT_THAT = "You cannot follow yourself. Sorry about that.";
    public static final String USER_NOT_FOUND = "User not found";

    private Constants() {

    }



}
