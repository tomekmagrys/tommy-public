package com.hsbc.interview.homework.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServicePerformanceTest {

    @Autowired
    UserService userService;

    @Test
    public void ParralerTest() throws InterruptedException {

        ExecutorService pool = Executors.newFixedThreadPool(100);

        CountDownLatch countDownLatch = new CountDownLatch(100);

        for (int i = 0; i < 100; i++) {
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    userService.createIfNotExists("john_show");
                    countDownLatch.countDown();

                }
            });
        }

        countDownLatch.await();
        Assert.assertEquals(1L, userService.findAll().spliterator().getExactSizeIfKnown());
    }

}