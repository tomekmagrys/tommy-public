import com.google.common.io.ByteStreams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

@RunWith(value = Parameterized.class)
public class Test913C {

	private String inputFile;
	private String outputFile;

	public Test913C(String inputFile, String outputFile) {
		this.inputFile = inputFile;
		this.outputFile = outputFile;
	}

	private static String getTestResourcesPath() {
		return "src" + File.separator + "test" + File.separator + "resources"
				+ File.separator + Test913C.class.getSimpleName().substring(4)
				+ File.separator;
	}

	private static Object[] getResourcesPathForTestNumber(int i){
		return new Object[] { getTestResourcesPath() + "test" + i + ".in",
				getTestResourcesPath() + "test" + i + ".out" };
	}
	
	private final static int NUMBER_OF_TESTS = 1;
	
	@Parameters
	public static Collection<Object[]> data() {
		List<Object[]> retVal = new ArrayList<>();
		for(int i = 1 ; i <= NUMBER_OF_TESTS ; i++)
		retVal.add(getResourcesPathForTestNumber(i));
		return retVal;
	}

	@Test
	public void main() throws NumberFormatException, IOException {
		InputStream is = new FileInputStream(inputFile);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Task913C.Solution.main(is, os);

		assertArrayEquals(
				ByteStreams.toByteArray(new FileInputStream(outputFile)),
				os.toByteArray());
	}

}
