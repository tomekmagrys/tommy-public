import java.util.*;

/**
 * Created by tommy on 4/17/15.
 */
public class Sabre {

    static int num_triplets(int t, int[] d) {

        Set<Integer> tmp = new TreeSet<>();

        for (int i : d) {
            tmp.add(i);
        }

        List<Integer> l = new ArrayList<>(tmp);

        int retVal = 0;

        for (int i = 0; i < l.size(); i++) {
            if(l.get(i) > t){
                break;
            }
            for (int j = i+1; j < l.size(); j++) {
                if(l.get(i) + l.get(j) >t){
                    break;
                }
                for (int k = j+1; k < l.size(); k++) {
                    if(l.get(i) + l.get(j) + l.get(k) <= t){
                        retVal ++;
                    } else {
                        break;
                    }
                }
            }
        }

        return retVal;
    }

}
