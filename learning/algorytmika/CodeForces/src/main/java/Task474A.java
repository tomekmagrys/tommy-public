import java.io.*;

public class Task474A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            char[][] keyb = new char[3][];

            keyb[0] = "qwertyuiop".toCharArray();
            keyb[1] = "asdfghjkl;".toCharArray();
            keyb[2] = "zxcvbnm,./".toCharArray();

            String shift = sc.next();

            String msg = sc.next();

            for (char c : msg.toCharArray()) {
                for (char[] line : keyb) {
                    for (int i = 0; i < line.length; i++) {
                        if (line[i] == c) {
                            if ("R".equals(shift)) {
                                pw.print(line[i - 1]);
                            } else {
                                pw.print(line[i + 1]);
                            }

                        }
                    }
                }
            }

            pw.flush();
            sc.close();
        }
    }

}