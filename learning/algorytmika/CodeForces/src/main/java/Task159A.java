import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task159A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int d = s.nextInt();

			ArrayList<String> a = new ArrayList<>(n);
			ArrayList<String> b = new ArrayList<>(n);
			ArrayList<Integer> t = new ArrayList<>(n);

			Set<String> answers = new TreeSet<>();

			for (int i = 0; i < n; i++) {
				String ai = s.next();
				String bi = s.next();
				int ti = s.nextInt();

				for (int j = a.size() - 1; j >= 0; j--) {
					if (ti - t.get(j) > d) {
						break;
					}
					if (a.get(j).equals(bi) && b.get(j).equals(ai)
							&& ti != t.get(j)) {
						if (ai.compareTo(bi) > 0) {
							answers.add(ai + " " + bi);
						} else {
							answers.add(bi + " " + ai);
						}
					}
				}
				a.add(ai);
				b.add(bi);
				t.add(ti);
			}

			pw.println(answers.size());
			for (String answer : answers) {
				pw.println(answer);
			}

			pw.flush();
			s.close();
		}
	}

}