import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task245A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			int alive[] = new int[3];
			int count[] = new int[3];
			
			for(int i = 0 ; i < n ; i++){
				int server = s.nextInt();
				count[server]++;
				alive[server] += s.nextInt();
				s.nextInt();
			}
			
			pw.write(alive[1] >= 5 * count[1] ? "LIVE\n" : "DEAD\n");
			pw.write(alive[2] >= 5 * count[2] ? "LIVE\n" : "DEAD\n");
			
			pw.flush();
			s.close();
		}
	}

}
