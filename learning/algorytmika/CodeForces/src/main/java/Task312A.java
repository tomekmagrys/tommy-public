import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task312A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = Integer.parseInt(s.nextLine());

			for (int i = 0; i < n; i++) {
				String chatLine = s.nextLine();
				if (chatLine.startsWith("miao.")) {
					if (chatLine.endsWith("lala.")) {
						pw.println("OMG>.< I don't know!");
					} else {
						pw.println("Rainbow's");
					}
				} else {
					if (chatLine.endsWith("lala.")) {
						pw.println("Freda's");
					} else {
						pw.println("OMG>.< I don't know!");
					}
				}
			}

			pw.flush();
			s.close();
		}
	}

}
