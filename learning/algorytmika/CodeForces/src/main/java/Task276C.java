import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task276C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int q = sc.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(sc.nextInt());
			}

			int modifiers[] = new int[n+1];
			
			for(int i = 0 ; i < q ; i++){
				modifiers[sc.nextInt()-1]++;
				modifiers[sc.nextInt()]--;
			}
			
			ArrayList<Integer> multipliers = new ArrayList<Integer>(n);
			int factor = 0;
			
			for(int i = 0 ; i < n ; i++){
				factor += modifiers[i];
				multipliers.add(factor);
			}
			
			Collections.sort(multipliers);
			Collections.sort(a);
			
			long retVal = 0;
			for(int i = 0 ; i < n ; i++){
				long add = multipliers.get(i);
				add *= a.get(i);
				retVal += add;
			}
			
			pw.println(retVal);
			
			sc.close();
			pw.flush();
		}
	}

}
