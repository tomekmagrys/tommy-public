import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task287A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			ArrayList<String> input = new ArrayList<>();

			for (int i = 0; i < 4; i++) {
				input.add(s.next());
			}

			boolean retVal = false;

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					if ((input.get(i).charAt(j) == '.' ? 0 : 1)
							+ (input.get(i + 1).charAt(j) == '.' ? 0 : 1)
							+ (input.get(i).charAt(j + 1) == '.' ? 0 : 1)
							+ (input.get(i + 1).charAt(j + 1) == '.' ? 0 : 1) != 2) {
						retVal = true;
					}
				}

			}
			pw.write(retVal ? "YES" : "NO");
			pw.flush();
			s.close();
		}
	}

}
