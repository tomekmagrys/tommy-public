import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task139A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> canRead = new ArrayList<>();
			for(int i = 0 ; i < 7 ; i++){
				canRead.add(s.nextInt());
			}
			int retVal = -1;
			while(n > 0){
				retVal = (retVal + 1) % 7;
				n -= canRead.get(retVal);
			}
			pw.print((retVal + 1)+"\n");
			pw.flush();
			s.close();
		}
	}

}
