import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task315A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			ArrayList<Integer> b = new ArrayList<>(n);
			ArrayList<Boolean> canOpen = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
				b.add(s.nextInt());
				canOpen.add(false);
			}

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (i != j) {
						if (a.get(i).equals(b.get(j))) {
							canOpen.set(i, true);
						}
					}
				}
			}

			int retVal = 0;
			for (boolean bb : canOpen) {
				if (!bb) {
					retVal++;
				}
			}
			pw.println(retVal);
			pw.flush();
			s.close();
		}
	}

}
