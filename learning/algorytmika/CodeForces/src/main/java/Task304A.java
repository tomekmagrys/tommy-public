import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task304A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int retVal = 0;

			for (int a = 1; a <= n; a++) {
				for (int b = a; a * a + b * b <= n*n; b++) {
					double candForC = Math.sqrt(a * a + b * b);
					double c = (int) candForC;
					if (c == candForC) {
						retVal++;
					}
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}