import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task318A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			long n = sc.nextLong();
			long k = sc.nextLong();

			Long retVal;

			if (k <= (n + 1) / 2) {
				retVal = 2 * k - 1;
			} else {
				retVal = 2 * (k - (n + 1) / 2);
			}

			os.write((retVal + System.lineSeparator()).getBytes());
			sc.close();
		}
	}

}
