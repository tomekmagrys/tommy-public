import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task347A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> input = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				input.add(s.nextInt());
			}

			int max = Collections.max(input);
			int min = Collections.min(input);

			input.remove(Integer.valueOf(min));
			input.remove(Integer.valueOf(max));

			Collections.sort(input);
			
			input.add(0,max);
			input.add(min);
			
			for(int i : input){
				pw.write(i + " ");
			}
			
			pw.flush();
			s.close();
		}
	}

}
