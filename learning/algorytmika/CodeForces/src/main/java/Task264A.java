import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task264A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Pair<T1, T2> {
        T1 x;
        T2 y;

        public Pair(T1 x, T2 y) {
            this.x = x;
            this.y = y;
        }
    }

    static class PairII extends Pair<Integer, Integer> {
        public PairII(Integer x, Integer y) {
            super(x, y);
        }
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            List<Integer> stonesOnRight = new ArrayList<>();
            List<Integer> stonesOnLeft = new ArrayList<>();

            String squirrel = sc.next();

            for (int i = 0; i < squirrel.length(); i++) {
                if (squirrel.charAt(i) == 'l') {
                    stonesOnRight.add(i + 1);
                } else {
                    stonesOnLeft.add(i + 1);
                }
            }

            Collections.reverse(stonesOnRight);

            for(int a : stonesOnLeft){
                pw.println(a);
            }

            for(int a : stonesOnRight){
                pw.println(a);
            }

            pw.flush();
            sc.close();
        }
    }

}