import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task007A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			char chess[][] = new char[8][];
			for (int i = 0; i < 8; i++) {
				chess[i] = s.next().toCharArray();
			}

			int rowsNeed = 0;
			int colsNeed = 0;

			for (int i = 0; i < 8; i++) {
				boolean need = true;
				for (int j = 0; j < 8; j++) {
					if (chess[i][j] == 'W') {
						need = false;
						break;
					}
				}
				if (need) {
					rowsNeed++;
				}
			}

			for (int i = 0; i < 8; i++) {
				boolean need = true;
				for (int j = 0; j < 8; j++) {
					if (chess[j][i] == 'W') {
						need = false;
						break;
					}
				}
				if (need) {
					colsNeed++;
				}
			}

			pw.println(colsNeed + rowsNeed == 16 ? 8 : colsNeed + rowsNeed);

			pw.flush();
			s.close();
		}
	}

}