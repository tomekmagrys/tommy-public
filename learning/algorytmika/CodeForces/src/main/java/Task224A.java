import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task224A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

		static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int ab = s.nextInt();
			int bc = s.nextInt();
			int ca = s.nextInt();
			
			int a = (int) Math.sqrt(ca*ab/bc);
			int b = (int) Math.sqrt(ab*bc/ca);
			int c = (int) Math.sqrt(bc*ca/ab);
			
			pw.print((4*(a+b+c))+"\n");
			pw.flush();
			s.close();
		}
	}
}
