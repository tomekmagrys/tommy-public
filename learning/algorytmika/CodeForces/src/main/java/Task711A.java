import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task711A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            List<char[]> seats = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                seats.add(sc.next().toCharArray());
            }

            boolean found = false;

            for (char[] row : seats) {
                if (row[0] == 'O' && row[1] == 'O') {
                    row[0] = '+';
                    row[1] = '+';
                    found = true;
                    break;
                }
                if (row[3] == 'O' && row[4] == 'O') {
                    row[3] = '+';
                    row[4] = '+';
                    found = true;
                    break;
                }
            }

            if (!found) {
                pw.println("NO");
            } else {
                pw.println("YES");
                seats.forEach(pw::println);
            }

            pw.flush();
            sc.close();
        }
    }

}