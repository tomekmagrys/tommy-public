import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task373A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int k = s.nextInt();
			int digitCount[] = new int[10];
			
			for(int i = 0 ; i < 4 ;i++){
				for(char c:s.next().toCharArray()){
					if(c != '.'){
						digitCount[c-'0']++;
					}
				}
			}
			
			boolean retVal = true;
			
			for(int i : digitCount){
				if(i > 2*k){
					retVal = false;
					break;
				}
			}
			
			pw.println(retVal ? "YES" : "NO");
			
			pw.flush();
			
			s.close();
		}
	}

}