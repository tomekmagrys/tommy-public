import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task166A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();
			
			ArrayList<Integer> results = new ArrayList<>();
			
			for(int i = 0 ; i < n ; i++){
				results.add(s.nextInt() * 100 - s.nextInt());
			}
			Collections.sort(results);
			
			int retVal = 0;
			
			for(int result:results){
				if(result == results.get(results.size()-k)){
					retVal++;
				}
			}
			
			pw.write(retVal+"");
			
			pw.flush();
			s.close();
		}
	}

}
