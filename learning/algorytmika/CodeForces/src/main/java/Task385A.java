import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task385A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int c = s.nextInt();

			ArrayList<Integer> x = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				x.add(s.nextInt());
			}

			int retVal = 0;

			for (int i = 0; i + 1 < n; i++) {
				retVal = Math.max(retVal, x.get(i) - x.get(i + 1) - c);
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}