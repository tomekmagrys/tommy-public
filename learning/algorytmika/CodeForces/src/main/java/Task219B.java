import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task219B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			long p = sc.nextLong();
			long d = sc.nextLong();

			ArrayList<Long> cands = new ArrayList<>();
			cands.add(p);

			if (p - (p % 10 + 1) >= p - d) {
				cands.add(p - (p % 10 + 1));
			}
			if (p - (p % 100 + 1) >= p - d) {
				cands.add(p - (p % 100 + 1));
			}
			if (p - (p % 1000 + 1) >= p - d) {
				cands.add(p - (p % 1000 + 1));
			}
			if (p - (p % 10000 + 1) >= p - d) {
				cands.add(p - (p % 10000 + 1));
			}
			if (p - (p % 100000 + 1) >= p - d) {
				cands.add(p - (p % 100000 + 1));
			}
			if (p - (p % 1000000 + 1) >= p - d) {
				cands.add(p - (p % 1000000 + 1));
			}
			if (p - (p % 10000000 + 1) >= p - d) {
				cands.add(p - (p % 10000000 + 1));
			}
			if (p - (p % 100000000 + 1) >= p - d) {
				cands.add(p - (p % 100000000 + 1));
			}
			if (p - (p % 1000000000 + 1) >= p - d) {
				cands.add(p - (p % 1000000000 + 1));
			}
			if (p - (p % 10000000000L + 1) >= p - d) {
				cands.add(p - (p % 10000000000L + 1));
			}
			if (p - (p % 100000000000L + 1) >= p - d) {
				cands.add(p - (p % 100000000000L + 1));
			}
			if (p - (p % 1000000000000L + 1) >= p - d) {
				cands.add(p - (p % 1000000000000L + 1));
			}
			if (p - (p % 10000000000000L + 1) >= p - d) {
				cands.add(p - (p % 10000000000000L + 1));
			}
			if (p - (p % 100000000000000L + 1) >= p - d) {
				cands.add(p - (p % 100000000000000L + 1));
			}
			if (p - (p % 1000000000000000L + 1) >= p - d) {
				cands.add(p - (p % 1000000000000000L + 1));
			}
			if (p - (p % 10000000000000000L + 1) >= p - d) {
				cands.add(p - (p % 10000000000000000L + 1));
			}
			if (p - (p % 100000000000000000L + 1) >= p - d) {
				cands.add(p - (p % 100000000000000000L + 1));
			}
			if (p - (p % 1000000000000000000L + 1) >= p - d) {
				cands.add(p - (p % 1000000000000000000L + 1));
			}

			Collections.sort(cands, new Comparator<Long>() {

				@Override
				public int compare(Long arg0, Long arg1) {
					int ninesArg0 = 0;
					while (arg0.toString().length() - 1 - ninesArg0 >= 0
							&& arg0.toString().charAt(
									arg0.toString().length() - 1 - ninesArg0) == '9') {
						ninesArg0++;
					}
					int ninesArg1 = 0;
					while (arg1.toString().length() - 1 - ninesArg1 >= 0
							&& arg1.toString().charAt(
									arg1.toString().length() - 1 - ninesArg1) == '9') {
						ninesArg1++;
					}
					if (ninesArg0 != ninesArg1) {
						return Integer.valueOf(ninesArg0).compareTo(ninesArg1);
					}
					return arg0.compareTo(arg1);
				}

			});

			pw.println(cands.get(cands.size() - 1));

			pw.flush();
			sc.close();
		}
	}

}