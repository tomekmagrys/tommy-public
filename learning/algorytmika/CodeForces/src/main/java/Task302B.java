import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task302B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> times = new ArrayList<>(n + 1);
			times.add(0);
			int currentTotalDuration = 0;

			for (int i = 0; i < n; i++) {
				int ci = s.nextInt();
				int ti = s.nextInt();
				currentTotalDuration += ci * ti;
				times.add(currentTotalDuration);
			}

			int currentPlayingIndex = 0;

			for (int i = 0; i < m; i++) {
				int vi = s.nextInt();
				while (times.get(currentPlayingIndex) < vi) {
					currentPlayingIndex++;
				}
				pw.println(currentPlayingIndex);
			}

			pw.flush();
			s.close();
		}
	}

}