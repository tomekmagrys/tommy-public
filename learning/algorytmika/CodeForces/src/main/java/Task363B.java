import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task363B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int k = sc.nextInt();

			ArrayList<Integer> h = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				h.add(sc.nextInt());
			}

			int curSum = 0;
			for (int i = 0; i < k; i++) {
				curSum += h.get(i);
			}
			int retVal = curSum;
			int retIndex = 1;

			for (int i = k; i < n; i++) {
				curSum += h.get(i) - h.get(i - k);
				if (curSum < retVal) {
					retVal = curSum;
					retIndex = i - k + 2;
				}
			}

			pw.println(retIndex);

			pw.flush();
			sc.close();
		}
	}

}