import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task289B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static int calculate(int d, ArrayList<ArrayList<Integer>> a) {
		int pattern = a.iterator().next().iterator().next();
		for (ArrayList<Integer> row : a) {
			for (Integer cell : row) {
				if ((cell - pattern) % d != 0) {
					return -1;
				}
			}
		}

		ArrayList<Integer> candidates = new ArrayList<Integer>();
		for (ArrayList<Integer> row : a) {
			for (Integer cell : row) {
				candidates.add(cell / d);
			}
		}

		Collections.sort(candidates);
		
		ArrayList<Integer> avgs = new ArrayList<>(2);
		if (candidates.size() % 2 == 0) {
			avgs.add(candidates.get(candidates.size()/2-1));
			avgs.add(candidates.get(candidates.size()/2));
		} else {
			avgs.add(candidates.get(candidates.size()/2));
		}

		ArrayList<Integer> retVals = new ArrayList<>();
		for (Integer avg : avgs) {
			int retVal = 0;
			for (Integer cell : candidates) {
				retVal += Math.abs(avg - cell);
			}
			retVals.add(retVal);
		}
		return Collections.min(retVals);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			int d = s.nextInt();

			ArrayList<ArrayList<Integer>> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				ArrayList<Integer> tmp = new ArrayList<>(m);
				for (int j = 0; j < m; j++) {
					tmp.add(s.nextInt());
				}
				a.add(tmp);
			}

			pw.println(calculate(d, a));

			pw.flush();
			s.close();
		}

	}

}
