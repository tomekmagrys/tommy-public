import java.io.*;

public class Task740A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Math2 {
        public static int sumOfDigits(int n) {
            int retVal = 0;

            while (n > 0) {
                retVal += n % 10;
                n /= 10;
            }

            return retVal;
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long n = sc.nextInt();
            long a = sc.nextInt();
            long b = sc.nextInt();
            long c = sc.nextInt();

            long retVal = Long.MAX_VALUE;

            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    for (int k = 0; k < 4; k++) {
                        if ((n + i + 2 * j + 3 * k) % 4 == 0) {
                            retVal = Math.min(retVal, a * i + b * j + c * k);
                        }
                    }
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();

        }
    }

}