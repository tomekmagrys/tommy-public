import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task143A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int r1 = s.nextInt();
			int r2 = s.nextInt();

			int c1 = s.nextInt();
			int c2 = s.nextInt();

			int d1 = s.nextInt();
			int d2 = s.nextInt();

			// + - -
			// | i j
			// | k l

			for (int i = 1; i < 10; i++) {
				for (int j = 1; j < 10; j++) {
					for (int k = 1; k < 10; k++) {
						for (int l = 1; l < 10; l++) {
							if (i != j && i != k && i != l && j != k && j != l
									&& k != l) {
								if (i + j == r1 && k + l == r2 && i + k == c1
										&& j + l == c2 && i + l == d1
										&& j + k == d2) {
									pw.println(i + " " + j);
									pw.println(k + " " + l);
									pw.flush();
									s.close();
									return;
								}
							}
						}
					}
				}
			}
			pw.println("-1");
			pw.flush();
			s.close();
		}
	}

}
