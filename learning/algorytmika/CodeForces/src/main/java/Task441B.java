import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task441B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Pair<T1, T2> {
        T1 x;
        T2 y;

        public Pair(T1 x, T2 y) {
            this.x = x;
            this.y = y;
        }
    }

    static class PairII extends Pair<Integer, Integer> {
        public PairII(Integer x, Integer y) {
            super(x, y);
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int v = sc.nextInt();

            ArrayList<PairII> trees = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                trees.add(new PairII(sc.nextInt(), sc.nextInt()));
            }

            Collections.sort(trees, new Comparator<PairII>() {
                @Override
                public int compare(PairII o1, PairII o2) {
                    return o1.x.compareTo(o2.x);
                }
            });

            int day = 1;
            int vLeft = v;
            int retVal = 0;


            while (!trees.isEmpty()){
                PairII tree = trees.get(0);
                if(tree.x > day){
                    day = tree.x;
                    vLeft = v;
                }
                if(tree.x + 1 < day){
                    trees.remove(0);
                    continue;
                }
                int canHarvest = Math.min(vLeft,tree.y);
                vLeft -= canHarvest;
                tree.y -= canHarvest;
                retVal += canHarvest;
                if(tree.y == 0){
                    trees.remove(0);
                }
                if (vLeft == 0) {
                    day++;
                    vLeft = v;
                }
            }

            pw.println(retVal);
            pw.flush();
            sc.close();
        }
    }

}