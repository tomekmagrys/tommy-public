import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task366A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int a1 = s.nextInt();
			int b1 = s.nextInt();
			int c1 = s.nextInt();
			int d1 = s.nextInt();

			int a2 = s.nextInt();
			int b2 = s.nextInt();
			int c2 = s.nextInt();
			int d2 = s.nextInt();

			int a3 = s.nextInt();
			int b3 = s.nextInt();
			int c3 = s.nextInt();
			int d3 = s.nextInt();

			int a4 = s.nextInt();
			int b4 = s.nextInt();
			int c4 = s.nextInt();
			int d4 = s.nextInt();

			if (Math.min(a1, b1) + Math.min(c1, d1) <= n) {
				pw.write("1 " + Math.min(a1, b1) + " " + (n-Math.min(a1, b1))
						+ "\n");
			} else if (Math.min(a2, b2) + Math.min(c2, d2) <= n) {
				pw.write("2 " + Math.min(a2, b2) + " " + (n-Math.min(a2, b2))
						+ "\n");
			} else if (Math.min(a3, b3) + Math.min(c3, d3) <= n) {
				pw.write("3 " + Math.min(a3, b3) + " " + (n-Math.min(a3, b3))
						+ "\n");
			} else if (Math.min(a4, b4) + Math.min(c4, d4) <= n) {
				pw.write("4 " + Math.min(a4, b4) + " " + (n-Math.min(a4, b4))
						+ "\n");
			} else {
				pw.write("-1\n");
			}

			pw.flush();
			s.close();
		}
	}

}
