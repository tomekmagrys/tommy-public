import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Task450A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();

            ArrayList<Integer> a = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                a.add(sc.nextInt());
            }

            int max = Collections.max(a);

            int roundsNeeded = (max + m - 1) / m;

            int retVal = 0;

            for (int i = 1; i < n; i++) {
                if (a.get(i) > m * (roundsNeeded - 1)) {
                    retVal = i;
                }
            }

            pw.println(retVal + 1);

            pw.flush();
            sc.close();
        }
    }

}