import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task203B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		private static boolean subsquareTrue(boolean board[][], int x, int y) {

			return board[x - 1][y + 1] && board[x][y + 1] && board[x + 1][y + 1] && 
				   board[x - 1][y]     && board[x][y]     && board[x + 1][y]     && 
				   board[x - 1][y - 1] && board[x][y - 1] && board[x + 1][y - 1];
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			boolean board[][] = new boolean[n][];
			for (int i = 0; i < n; i++) {
				board[i] = new boolean[n];
			}

			int m = sc.nextInt();

			for (int i = 0; i < m; i++) {
				int xi = sc.nextInt() - 1;
				int yi = sc.nextInt() - 1;

				board[xi][yi] = true;
				if (xi - 2 >= 0 && yi - 2 >= 0) {
					if (subsquareTrue(board, xi - 1, yi - 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi - 2 >= 0 && yi - 1 >= 0 && yi + 1 < n) {
					if (subsquareTrue(board, xi - 1, yi)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi - 2 >= 0 && yi + 2 < n) {
					if (subsquareTrue(board, xi - 1, yi + 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi - 1 >= 0 && xi + 1 < n && yi - 2 >= 0) {
					if (subsquareTrue(board, xi, yi - 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi - 1 >= 0 && xi + 1 < n && yi - 1 >= 0 && yi + 1 < n) {
					if (subsquareTrue(board, xi, yi)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi - 1 >= 0 && xi + 1 < n && yi + 2 < n) {
					if (subsquareTrue(board, xi, yi + 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi + 2 < n && yi - 2 >= 0) {
					if (subsquareTrue(board, xi + 1, yi - 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi + 2 < n && yi - 1 >= 0 && yi + 1 < n) {
					if (subsquareTrue(board, xi + 1, yi)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}
				if (xi + 2 < n && yi + 2 < n) {
					if (subsquareTrue(board, xi + 1, yi + 1)) {
						pw.println(i + 1);
						pw.flush();
						sc.close();
						return;
					}
				}				
			}

			pw.println(-1);
			pw.flush();
			sc.close();
		}
	}

}