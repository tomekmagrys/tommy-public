import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task246B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException {
            PrintWriter pw = new PrintWriter(os);
            Scanner s = new Scanner(is);

            int n = s.nextInt();
            int sum = 0;

            for (int i = 0; i < n; i++) {
                sum += s.nextInt();
            }

            int cand1 = Math.abs(sum) % n;
            int cand2 = n - Math.abs(sum) % n;
            int cand3 = n-1;
            
            pw.write("" + Math.max(Math.max(cand1, cand2),cand3));

            pw.flush();
            s.close();

        }
    }

}