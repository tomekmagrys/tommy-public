import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task109A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			int fours = 0;
			int sevens = 0;
			
			while(n % 7 != 0){
				if(n < 4){
					pw.println("-1");
					pw.flush();
					s.close();
					return;
				}
				fours++;
				n -= 4;
			}
			
			sevens = n/7;
			
			for(int i = 0 ; i < fours ; i++){
				pw.print("4");
			}
			for(int i = 0 ; i < sevens ; i++){
				pw.print("7");
			}
			pw.flush();
			s.close();
		}
	}

}
