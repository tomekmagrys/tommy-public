import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task001B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	private static String convertToExcel(int from) {
		StringBuilder retVal = new StringBuilder();
		if (from <= 26) {
			char d1 = (char) ((from - 1) % 26 + 'A');
			retVal.append("" + d1);
			return retVal.toString();
		}
		from -= 26;
		if (from <= 26 * 26) {
			char d1 = (char) ((from - 1) % 26 + 'A');
			char d2 = (char) (((from - 1) / 26) + 'A');
			retVal.append("" + d2);
			retVal.append("" + d1);
			return retVal.toString();
		}
		from -= 26 * 26;
		if (from <= 26 * 26 * 26) {
			char d1 = (char) ((from - 1) % 26 + 'A');
			char d2 = (char) (((from - 1) / 26) % 26 + 'A');
			char d3 = (char) ((from - 1) / (26 * 26) + 'A');
			retVal.append("" + d3);
			retVal.append("" + d2);
			retVal.append("" + d1);
			return retVal.toString();
		}
		from -= 26 * 26 * 26;
		if (from <= 26 * 26 * 26 * 26) {
			char d1 = (char) (((from - 1) % 26) + 'A');
			char d2 = (char) (((from - 1) / 26) % 26 + 'A');
			char d3 = (char) (((from - 1) / (26 * 26)) % 26 + 'A');
			char d4 = (char) ((from - 1) / (26 * 26 * 26) + 'A');
			retVal.append("" + d4);
			retVal.append("" + d3);
			retVal.append("" + d2);
			retVal.append("" + d1);
			return retVal.toString();
		}
		from -= 26 * 26 * 26 * 26;
		if (from <= 26 * 26 * 26 * 26 * 26) {
			char d1 = (char) (((from - 1) % 26) + 'A');
			char d2 = (char) (((from - 1) / 26) % 26 + 'A');
			char d3 = (char) (((from - 1) / (26 * 26)) % 26 + 'A');
			char d4 = (char) ((from - 1) / (26 * 26 * 26) % 26 + 'A');
			char d5 = (char) ((from - 1) / (26 * 26 * 26 * 26 ) + 'A');
			retVal.append("" + d5);
			retVal.append("" + d4);
			retVal.append("" + d3);
			retVal.append("" + d2);
			retVal.append("" + d1);
			return retVal.toString();
		}		
		return retVal.toString();
	}

	static int convertFromExcel(String from) {
		switch (from.length()) {
		case 1:
			char d1 = (char) (from.charAt(0) - 'A');
			return d1 + 1;
		case 2:
			d1 = (char) (from.charAt(0) - 'A');
			char d2 = (char) (from.charAt(1) - 'A');
			return d1 * 26 + d2 + 26 + 1;
		case 3:
			d1 = (char) (from.charAt(0) - 'A');
			d2 = (char) (from.charAt(1) - 'A');
			char d3 = (char) (from.charAt(2) - 'A');
			return d1 * 26 * 26 + d2 * 26 + d3 + 26 * 26 + 26 + 1;
		case 4:
			d1 = (char) (from.charAt(0) - 'A');
			d2 = (char) (from.charAt(1) - 'A');
			d3 = (char) (from.charAt(2) - 'A');
			char d4 = (char) (from.charAt(3) - 'A');
			return d1 * 26 * 26 * 26 + d2 * 26 * 26 + d3 * 26 + d4 + 26 * 26
					* 26 + 26 * 26 + 26 + 1;
		case 5:
			d1 = (char) (from.charAt(0) - 'A');
			d2 = (char) (from.charAt(1) - 'A');
			d3 = (char) (from.charAt(2) - 'A');
			d4 = (char) (from.charAt(3) - 'A');
			char d5 = (char) (from.charAt(4) - 'A');
			return d1 * 26 * 26 * 26 * 26 + d2 * 26 * 26 * 26 + d3 * 26 * 26 + d4 * 26 + d5 + 
					26 * 26 * 26 * 26 + 26 * 26 * 26 + 26 * 26 + 26 + 1;			
		default:
			return 0;
		}

	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			for (int i = 0; i < n; i++) {
				String toConvert = s.next();
				if (toConvert.matches("R[0-9]+C[0-9]+")) {
					int row = Integer.parseInt(toConvert.substring(1,
							toConvert.lastIndexOf("C")));
					int column = Integer.parseInt(toConvert.substring(toConvert
							.lastIndexOf("C") + 1));
					pw.print(convertToExcel(column));
					pw.print(row + "");
					pw.print("\n");
				} else {
					int beginRowIndex = 0;
					while (toConvert.charAt(beginRowIndex) >= 'A') {
						beginRowIndex++;
					}
					String column = toConvert.substring(0, beginRowIndex);
					String row = toConvert.substring(beginRowIndex);
					pw.print("R" + row + "C" + convertFromExcel(column));
					// System.out.println("R" + row + "C" +
					// convertFromExcel(column));

					pw.print("\n");
				}
			}

			pw.flush();
			s.close();
		}

	}

}
