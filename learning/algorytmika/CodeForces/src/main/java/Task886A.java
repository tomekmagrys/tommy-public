import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task886A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);


            List<Integer> a = new ArrayList<>(6);

            int sum = 0;

            for(int i = 0 ; i < 6 ; i++){
                int tmp = sc.nextInt();
                sum += tmp;
                a.add(tmp);
            }

            boolean found = false;

            for(int i = 0 ; i < 6 ; i++){
                for(int j = i +1 ; j < 6 ; j++){
                    for(int k = j + 1 ; k < 6 ; k++){
                        if(2 * (a.get(i) + a.get(j) + a.get(k)) == sum){
                            found = true;
                        }
                    }
                }
            }

            pw.println(found ? "YES" : "NO");

            pw.flush();
            sc.close();

        }
    }

}