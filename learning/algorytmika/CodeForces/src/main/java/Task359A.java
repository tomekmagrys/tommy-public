import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task359A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<ArrayList<Boolean>> table = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				table.add(new ArrayList<Boolean>(m));
				for (int j = 0; j < m; j++) {
					table.get(i).add(s.nextInt() == 1 ? true : false);
				}
			}

			int retVal = 4;

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					if (table.get(i).get(j)) {
						if (i == 0) {
							retVal = Math.min(retVal, 2);
							break;
						} else if (i == n - 1) {
							retVal = Math.min(retVal, 2);
							break;
						} else if (j == 0) {
							retVal = Math.min(retVal, 2);
							break;
						} else if (j == m - 1) {
							retVal = Math.min(retVal, 2);
							break;
						}
					}
				}
				if (retVal == 2) {
					break;
				}
			}

			pw.write(retVal+"");
			
			pw.flush();
			s.close();
		}
	}

}