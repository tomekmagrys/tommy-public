import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task203A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int t = s.nextInt();
			int a = s.nextInt();
			int b = s.nextInt();
			int da = s.nextInt();
			int db = s.nextInt();

			boolean retVal = false;

			if (x == 0) {
				retVal = true;
			}

			for (int i = 0; i < t; i++) {
				if (x == a - i * da) {
					retVal = true;
				}
			}
			for (int i = 0; i < t; i++) {
				if (x == b - i * db) {
					retVal = true;
				}
			}
			for (int i = 0; i < t; i++) {
				for (int j = 0; j < t; j++) {
					if (x == a - i * da + b - j * db) {
						retVal = true;
					}
				}
			}

			pw.write(retVal ? "YES" : "NO");

			pw.flush();
			s.close();
		}
	}

}