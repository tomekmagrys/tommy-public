import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task012A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			char password[][] = new char[3][];

			password[0] = s.next().toCharArray();
			password[1] = s.next().toCharArray();
			password[2] = s.next().toCharArray();

			if (password[0][0] == password[2][2]
					&& password[1][0] == password[1][2]
					&& password[2][0] == password[0][2]
					&& password[0][1] == password[2][1]) {
				pw.println("YES");
			} else {
				pw.println("NO");
			}

			pw.flush();
			s.close();
		}
	}

}