import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task043B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String s1 = sc.nextLine();
			String s2 = sc.nextLine();

			int occurence1[] = new int[256];
			int occurence2[] = new int[256];

			for (int i = 0; i < s1.length(); i++) {
				Character.valueOf(s1.charAt(i));
				if (Character.isLetter(s1.charAt(i))) {
					occurence1[s1.charAt(i)]++;
				}
			}

			for (int i = 0; i < s2.length(); i++) {
				Character.valueOf(s2.charAt(i));
				if (Character.isLetter(s2.charAt(i))) {
					occurence2[s2.charAt(i)]++;
				}
			}

			for (int i = 0; i < 256; i++) {
				if (occurence2[i] > occurence1[i]) {
					pw.println("NO");
					pw.flush();
					sc.close();
					return;
				}
			}

			pw.println("YES");
			pw.flush();
			sc.close();
		}
	}

}