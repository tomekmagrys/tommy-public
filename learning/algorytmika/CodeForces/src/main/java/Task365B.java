import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task365B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int retVal = Math.min(n, 2);
			int curRetVal = 2;

			for (int i = 2; i < n; i++) {
				if (a.get(i) == a.get(i - 1) + a.get(i - 2)) {
					curRetVal++;
				} else {
					curRetVal = 2;
				}
				retVal = Math.max(retVal, curRetVal);
			}

			pw.write("" + retVal);

			pw.flush();
			s.close();
		}
	}

}
