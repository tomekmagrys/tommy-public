import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task292B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			int degrees[] = new int[n];

			for (int i = 0; i < m; i++) {
				degrees[sc.nextInt()-1]++;
				degrees[sc.nextInt()-1]++;
			}

			int degree1 = 0;
			int degree2 = 0;
			int degreeN = 0;

			for (int i = 0; i < n; i++) {
				if (degrees[i] == 1) {
					degree1++;
				}
				if (degrees[i] == 2) {
					degree2++;
				}
				if (degrees[i] == n - 1) {
					degreeN++;
				}
			}

			if (degree2 == n) {
				pw.println("ring topology");
			} else if (degree1 == 2 && degree2 == n - 2) {
				pw.println("bus topology");
			} else if (degree1 == n - 1 && degreeN == 1) {
				pw.println("star topology");
			} else {
				pw.println("unknown topology");
			}

			pw.flush();
			sc.close();
		}
	}

}