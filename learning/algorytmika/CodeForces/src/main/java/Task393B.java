import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task393B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			double[][] W = new double[n][];

			for (int i = 0; i < n; i++) {
				W[i] = new double[n];
				for (int j = 0; j < n; j++) {
					W[i][j] = sc.nextInt();
				}
			}

			double[][] A = new double[n][];
			for (int i = 0; i < n; i++) {
				A[i] = new double[n];
			}

			for (int i = 0; i < n; i++) {
				for (int j = i; j < n; j++) {
					A[i][j] = (W[i][j] + W[j][i]) / 2;
					A[j][i] = A[i][j];
				}
			}

			double[][] B = new double[n][];
			for (int i = 0; i < n; i++) {
				B[i] = new double[n];
			}

			for (int i = 0; i < n; i++) {
				for (int j = i; j < n; j++) {
					B[i][j] = (W[i][j] - W[j][i]) / 2;
					B[j][i] = -B[i][j];
				}
			}

			for (int i = 0; i < n; i++) {
				for(int j = 0 ; j < n ; j++){
					pw.print(A[i][j]);
					pw.print(" ");
				}
				pw.println();
			}

			for (int i = 0; i < n; i++) {
				for(int j = 0 ; j < n ; j++){
					pw.print(B[i][j]);
					pw.print(" ");
				}
				pw.println();
			}
			
			pw.flush();
			sc.close();
		}
	}

}