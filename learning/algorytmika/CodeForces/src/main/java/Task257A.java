import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task257A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			int k = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}
			Collections.sort(a);

			int retVal = 0;
			while (m > k && !a.isEmpty()) {
				k += a.remove(a.size() - 1) - 1;
				retVal++;
			}
			if (m <= k) {
				pw.println(retVal);
			} else {
				pw.println(-1);
			}
			pw.flush();
			s.close();
		}
	}

}
