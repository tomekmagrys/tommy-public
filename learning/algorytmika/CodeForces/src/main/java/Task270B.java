import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task270B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int lcds = 1;
			
			while(n-lcds-1 >= 0 && a.get(n-lcds) > a.get(n-lcds-1)){
				lcds++;
			}
			pw.write(n-lcds+"");
			
			pw.flush();
			s.close();
		}
	}

}
