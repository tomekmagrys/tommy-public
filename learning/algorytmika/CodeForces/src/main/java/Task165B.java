import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task165B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static int numberOfLines(int v, int k) {
			int retVal = 0;
			while (v > 0) {
				retVal += v;
				v /= k;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			int l = 1;
			int r = n;

			while (l < r) {
				int p = (l + r) / 2;
				if (numberOfLines(p, k) >= n) {
					r = p;
				} else {
					l = p + 1;
				}
			}

			pw.println(l);

			pw.flush();
			s.close();
		}
	}

}