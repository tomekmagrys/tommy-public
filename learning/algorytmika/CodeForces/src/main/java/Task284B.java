import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task284B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			s.nextInt();

			String players = s.next();

			int in = 0;
			int allin = 0;
			for (char c : players.toCharArray()) {
				switch (c) {
				case 'A':
					allin++;
					break;
				case 'I':
					in++;
					break;
				case 'F':
					break;
				}
			}

			if(in == 0){
				pw.write(allin+"");
			} else {
				if(in == 1){
					pw.write("1");
				} else{
					pw.write("0");
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}
