import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class Task414B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int k = sc.nextInt();

            long[] dyn = new long[n + 1];

            Arrays.fill(dyn, 1);
            for (int i = 2; i <= k; i++) {
                long[] temp = new long[n + 1];
                for (int y = 1; y <= n; y++) {
                    for (int z = y; z <= n; z += y) {
                        temp[z] += dyn[y];
                    }
                }

                for (int a = 0; a <= n; a++) {
                    dyn[a] = temp[a] % 1000000007;
                }
            }

            long retVal = 0;
            for (int i = 1; i <= n; i++) {
                retVal += dyn[i];
                retVal %= 1000000007;
            }

            pw.println(retVal);
            pw.flush();
            sc.close();
        }
    }

}