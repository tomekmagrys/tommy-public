import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task146A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	private static boolean isMagicTicket(String ticket){
		for(int i = 0 ; i < ticket.length() ; i++){
			if(ticket.charAt(i) != '4' && ticket.charAt(i) != '7'){
				return false;
			}
		}
		int sumFirstHalf = 0;
		int sumSecondHalf = 0;
		for(int i = 0 ; i < ticket.length()/2 ; i++){
			sumFirstHalf += ticket.charAt(i);
		}
		for(int i = ticket.length()/2 ; i < ticket.length() ; i++){
			sumSecondHalf += ticket.charAt(i);
		}
		return sumFirstHalf == sumSecondHalf;
	}
	
	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			s.nextInt();
			if(isMagicTicket(s.next())){
				pw.print("YES");
			} else {
				pw.print("NO");
			}
			pw.print("\n");
			pw.flush();
			s.close();
		}
	}

}
