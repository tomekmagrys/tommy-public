import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task014A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<String> picture = new ArrayList<String>(n);

			for (int i = 0; i < n; i++) {
				picture.add(s.next());
			}

			int nStart = 0;
			int nStop = n;
			int mStart = m;
			int mStop = 0;

			while (picture.get(nStart).indexOf('*') == -1) {
				nStart++;
			}
			while (nStop - 1 > 0 && picture.get(nStop - 1).indexOf('*') == -1) {
				nStop--;
			}

			for (int i = nStart; i < nStop; i++) {
				if (picture.get(i).indexOf('*') != -1) {
					mStart = Math.min(mStart, picture.get(i).indexOf('*'));
				}
				if (picture.get(i).lastIndexOf('*') != -1) {
					mStop = Math
							.max(mStop, picture.get(i).lastIndexOf('*') + 1);
				}
			}

			for (int i = nStart; i < nStop; i++) {
				pw.println(picture.get(i).substring(mStart, mStop));
			}

			pw.flush();
			s.close();
		}
	}

}