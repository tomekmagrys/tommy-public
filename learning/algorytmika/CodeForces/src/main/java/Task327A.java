import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task327A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int n = s.nextInt();
			ArrayList<Integer> a = new ArrayList<>(n);
			
			for(int i = 0 ; i < n ; i++){
				a.add(s.nextInt());
			}
			int retVal = Integer.MIN_VALUE;
			
			for(int i = 0 ; i < n ; i++){
				for(int j = i ; j < n ; j++){
					int candidateForRetVal = 0;
					
					for(int k = 0 ; k < n ; k++){
						candidateForRetVal += (k < i || k > j ? a.get(k) : 1-a.get(k));
					}
					retVal = Math.max(retVal, candidateForRetVal);
				}
			}
			pw.write(retVal + "\n");
			pw.flush();
			s.close();
		}
	}

}
