import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task282B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int pay_a = 0;
			int pay_g = 0;
			
			StringBuilder retVal = new StringBuilder(n);
			
			for (int i = 0; i < n; i++) {
				int cur_a = s.nextInt();
				int cur_g = s.nextInt();
				
				if(Math.abs(pay_a+cur_a-pay_g) < Math.abs(pay_g+cur_g-pay_a)){
					pay_a += cur_a;
					retVal.append('A');
				} else {
					pay_g += cur_g;
					retVal.append('G');
				}
			}

			if(Math.abs(pay_a - pay_g) <= 500){
				pw.write(retVal.toString());
			} else {
				pw.write("-1");
			}
			
			pw.flush();
			s.close();
		}
	}

}