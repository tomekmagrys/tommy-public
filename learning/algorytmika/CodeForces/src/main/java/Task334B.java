import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task334B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			ArrayList<Integer> x = new ArrayList<>(8);
			ArrayList<Integer> y = new ArrayList<>(8);

			for (int i = 0; i < 8; i++) {
				x.add(s.nextInt());
				y.add(s.nextInt());
			}

			boolean overlapping = false;

			for (int i = 0; i < 8; i++) {
				for (int j = i + 1; j < 8; j++) {
					if (x.get(i).equals(x.get(j)) && y.get(i).equals(y.get(j))) {
						overlapping = true;
					}
				}
			}

			Collections.sort(x);
			Collections.sort(y);

			if (!overlapping && x.get(0).equals(x.get(1))
					&& x.get(1).equals(x.get(2)) && x.get(2) < x.get(3)
					&& x.get(3).equals(x.get(4)) && x.get(4) < x.get(5)
					&& x.get(5).equals(x.get(6)) && x.get(6).equals(x.get(7))
					&& y.get(0).equals(y.get(1)) && y.get(1).equals(y.get(2))
					&& y.get(2) < y.get(3) && y.get(3).equals(y.get(4))
					&& y.get(4) < y.get(5) && y.get(5).equals(y.get(6))
					&& y.get(6).equals(y.get(7))) {
				pw.println("respectable");
			} else {
				pw.println("ugly");
			}

			pw.flush();
			s.close();
		}
	}

}
