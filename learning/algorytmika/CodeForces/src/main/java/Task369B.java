import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task369B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();
			s.nextInt();
			s.nextInt();
			int sall = s.nextInt();
			int sk = s.nextInt();

			for (int i = 0; i < sk % k; i++) {
				pw.print(sk / k + 1 + " ");
			}
			for (int i = sk % k; i < k; i++) {
				pw.print(sk / k + " ");
			}
			if (n > k) {
				for (int i = 0; i < (sall - sk) % (n - k); i++) {
					pw.print((sall - sk) / (n - k) + 1 + " ");
				}
				for (int i = (sall - sk) % (n - k); i < (n - k); i++) {
					pw.print((sall - sk) / (n - k) + " ");
				}
			}
			pw.flush();
			s.close();
		}
	}

}