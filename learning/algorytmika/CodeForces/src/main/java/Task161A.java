import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task161A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}
	
	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			int x = s.nextInt();
			int y = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			ArrayList<Integer> b = new ArrayList<>(m);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			for (int i = 0; i < m; i++) {
				b.add(s.nextInt());
			}
			
			ArrayList<Pair<Integer,Integer>> match = new ArrayList<>();

			for(int i = 0, j = 0 ; i < n && j < m ; ){
				if(b.get(j) < a.get(i) - x){
					j++;
				} else if(b.get(j) > a.get(i) + y){
					i++;
				} else {
					match.add(new Pair<>(i+1,j+1));
					i++;
					j++;
				}
			}
			
			pw.write(match.size()+"\n");
			for(Pair<Integer,Integer> pair : match){
				pw.write(pair.x + " " + pair.y + "\n");
			}
			pw.flush();
			s.close();
		}
	}

}
