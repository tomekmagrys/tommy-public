import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task332B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int k = sc.nextInt();

			int x[] = new int[n];
			for (int i = 0; i < n; i++) {
				x[i] = sc.nextInt();
			}

			long sumLastK = 0;
			for (int i = 0; i < k; i++) {
				sumLastK += x[i];
			}
			long dyn1[] = new long[n];
			int retVals1[] = new int[n];

			dyn1[k - 1] = sumLastK;
			retVals1[k - 1] = 1;

			for (int i = k; i < n; i++) {
				sumLastK = sumLastK + x[i] - x[i - k];
				if (sumLastK > dyn1[i - 1]) {
					dyn1[i] = sumLastK;
					retVals1[i] = i + 1 - (k - 1);
				} else {
					dyn1[i] = dyn1[i - 1];
					retVals1[i] = retVals1[i - 1];
				}
			}

			sumLastK = 0;
			for (int i = k; i < 2 * k; i++) {
				sumLastK += x[i];
			}
			long dyn2[] = new long[n];
			dyn2[2 * k - 1] = sumLastK + dyn1[k - 1];

			int retVal1 = 1;
			int retVal2 = k + 1;

			for (int i = 2 * k; i < n; i++) {
				sumLastK = sumLastK + x[i] - x[i - k];
				if (sumLastK + dyn1[i - k] > dyn2[i - 1]) {
					dyn2[i] = sumLastK + dyn1[i - k];
					retVal1 = retVals1[i - k];
					retVal2 = i + 1 - (k - 1);
				} else {
					dyn2[i] = dyn2[i - 1];
				}
			}

			pw.print(retVal1);
			pw.print(" ");
			pw.print(retVal2);

			pw.flush();

			sc.close();
		}
	}

}