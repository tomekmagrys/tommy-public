import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task336A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int x = s.nextInt();
			int y = s.nextInt();

			int dim = Math.abs(x) + Math.abs(y);

			int x1 = 0;
			int x2 = 0;
			int y1 = 0;
			int y2 = 0;

			if (x < 0) {
				x1 = -dim;
				if (y < 0) {
					y2 = -dim;
				}
				if (y > 0) {
					y2 = dim;
				}
			}
			if (x > 0) {
				x2 = dim;
				if (y < 0) {
					y1 = -dim;
				}
				if (y > 0) {
					y1 = dim;
				}

			}

			pw.write(x1 + " " + y1 + " " + x2 + " " + y2 + "\n");

			pw.flush();
			s.close();
		}
	}

}
