import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task376B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			int debts[][] = new int[n][];
			for (int i = 0; i < n; i++) {
				debts[i] = new int[n];
			}

			for (int i = 0; i < m; i++) {
				int a = s.nextInt()-1;
				int b = s.nextInt()-1;
				int c = s.nextInt();
				debts[a][b] += c;
			}

			for (int k = 0; k < n; k++) {
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < n; j++) {
						int simplify = Math.min(debts[i][k], debts[k][j]);
						debts[i][k] -= simplify;
						debts[k][j] -= simplify;
						debts[i][j] += simplify;
					}
				}
			}

			int retVal = 0;
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					retVal += debts[i][j];
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}