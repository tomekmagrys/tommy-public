import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task313B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String tmp = s.next();
			ArrayList<Integer> cache = new ArrayList<>(tmp.length());
			int licznik = 0;
			cache.add(licznik);
			for(int i = 1 ; i < tmp.length() ; i++){
				if(tmp.charAt(i) == tmp.charAt(i-1)){
					licznik++;
				}
				cache.add(licznik);
			}
			int m = s.nextInt();
			for(int i = 0 ; i < m ; i++){
				int l = s.nextInt();
				int r = s.nextInt();
				pw.write((cache.get(r-1) - cache.get(l-1)) + "\n");
			}
			pw.flush();
			s.close();
		}
	}

}
