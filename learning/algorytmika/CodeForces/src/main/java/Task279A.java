import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task279A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static int spiral(int x, int y){
			if(x + y <= 1){
				if(x > y){
					if(x + y == 1){
						return 4*Math.max(Math.abs(x), Math.abs(y))-4;
					} else {
						return 4*Math.max(Math.abs(x), Math.abs(y));
					}
				} else {
					if(x == 0 && y == 0) {
						return 0;
					}
					if(x + y == 0){
						return 4*Math.max(Math.abs(x), Math.abs(y))-2;
					}
					if(x + y == 1){
						return 4*Math.max(Math.abs(x), Math.abs(y))-2;
					}
					return 4*Math.max(Math.abs(x), Math.abs(y))-1;
				}
			} else {
				if(x >= y){
					return 4*Math.max(Math.abs(x), Math.abs(y))-3;
				} else {
					return 4*Math.max(Math.abs(x), Math.abs(y))-2;
				}
			}
			
		}
		
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int y = s.nextInt();
			
			pw.println(spiral(x, y));
			
			pw.flush();
			s.close();
		}
	}

}