import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task344A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int retVal = 0;
			String last = null;

			for (int i = 0; i < n; i++) {
				String magnet = s.next();
				if (!magnet.equals(last)) {
					retVal++;
				}
				last = magnet;
			}
			pw.print(retVal);
			pw.print("\n");
			pw.flush();
			s.close();
		}
	}

}
