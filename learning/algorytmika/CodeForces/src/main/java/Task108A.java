import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task108A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.nextLine();
			int hour = Integer.parseInt(input.substring(0, 2));
			int minute = Integer.parseInt(input.substring(3, 5));

			minute++;
			if (minute == 60) {
				minute = 0;
				hour++;
			}
			if (hour == 24) {
				hour = 0;
			}
			while (hour / 10 != minute % 10 || hour % 10 != minute / 10) {
				minute++;
				if (minute == 60) {
					minute = 0;
					hour++;
				}
				if (hour == 24) {
					hour = 0;
				}
			}

			pw.print(hour / 10);
			pw.print(hour % 10);
			pw.print(':');
			pw.print(minute / 10);
			pw.print(minute % 10);

			pw.flush();
			s.close();
		}
	}

}