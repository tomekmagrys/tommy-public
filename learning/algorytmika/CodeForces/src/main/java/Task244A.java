import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task244A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
	        int k = s.nextInt();
	        
	        Set<Integer> pieces = new TreeSet<Integer>();
	        for(int i = 1 ; i <= n*k ; i++){
	        	pieces.add(i);
	        }
	        
	        ArrayList<ArrayList<Integer>> division = new ArrayList<>();
	        
	        for(int i = 0 ; i < k ; i++){
	        	ArrayList<Integer> tmpSet = new ArrayList<>();
	        	int tmp = s.nextInt();
	        	tmpSet.add(tmp);
	        	pieces.remove(tmp);
	        	division.add(tmpSet);
	        }
	        
	        Iterator<Integer> iter = pieces.iterator();
	        
	        for(int i = 0 ; i < k ; i++){
	        	for(int j = 1 ; j < n ; j++){
	        		division.get(i).add(iter.next());
	        	}
	        }
	        
	        for(int i = 0 ; i < k ; i++){
	        	for(int j = 0 ; j < n ; j++){
	        		pw.write(division.get(i).get(j)+" ");
	        	}
	        }
	        
			pw.flush();
			s.close();
		}
	}

}
