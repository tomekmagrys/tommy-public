import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task339B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			int actual = 1;
			long retVal = 0;

			for (int i = 0; i < k; i++) {
				int target = s.nextInt();
				retVal += (target + n - actual) % n;
				actual = target;
			}
			pw.print(retVal);
			pw.print("\n");
			pw.flush();
			s.close();
		}
	}

}
