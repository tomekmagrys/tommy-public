import java.io.*;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Task443A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            String set = sc.nextLine();

            set = set.substring(1, set.length() - 1);
            if (set.length() == 0) {
                pw.println(0);
                pw.flush();
                sc.close();
                return;
            }
            if (set.length() == 1) {
                pw.println(1);
                pw.flush();
                sc.close();
                return;
            }
            String[] letters = set.split(", ");

            Set<String> retVal = new TreeSet<>();
            Collections.addAll(retVal, letters);
            pw.println(retVal.size());

            pw.flush();
            sc.close();
        }
    }

}