import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task292A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> t = new ArrayList<>(n);
			ArrayList<Integer> c = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				t.add(s.nextInt());
				c.add(s.nextInt());
			}

			int time = 0;
			int qSize = 0;
			int maxQSize = 0;

			for (int i = 0; i < n; i++) {
				if (time <= t.get(i)) {
					int canSend = Math.min(t.get(i) - time, qSize);
					qSize -= canSend;
					time = t.get(i);
				}
				qSize += c.get(i);
				maxQSize = Math.max(maxQSize, qSize);
			}

			time += qSize;

			pw.println(time + " " + maxQSize);

			pw.flush();
			s.close();
		}
	}

}