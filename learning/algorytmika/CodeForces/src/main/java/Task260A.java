import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task260A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();
			int n = s.nextInt();

			if (a % b != 0) {
				for (int i = 0; i < 10; i++) {
					if ((10 * a + i) % b == 0) {
						a = 10 * a + i;
						n--;
						break;
					}
				}
			}

			if (a % b != 0){
				pw.write("-1");
			}else {			
				pw.write(""+a);
				for(int i = 0 ; i < n ; i++){
					pw.write("0");
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}