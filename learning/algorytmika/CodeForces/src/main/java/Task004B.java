import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task004B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int d = s.nextInt();
			int sumTime = s.nextInt();

			ArrayList<Integer> minTimes = new ArrayList<Integer>(d);
			ArrayList<Integer> maxTimes = new ArrayList<Integer>(d);

			for (int i = 0; i < d; i++) {
				minTimes.add(s.nextInt());
				maxTimes.add(s.nextInt());
			}

			int sumMinTime = 0;
			int sumMaxTime = 0;

			for (int i = 0; i < d; i++) {
				sumMinTime += minTimes.get(i);
				sumMaxTime += maxTimes.get(i);
			}

			if (sumMinTime > sumTime) {
				pw.println("NO");
			} else if (sumMaxTime < sumTime) {
				pw.println("NO");
			} else {
				int sumAddition = sumTime - sumMinTime;
				pw.println("YES");
				for (int i = 0; i < d; i++) {
					int addition = Math.min(sumAddition, maxTimes.get(i)
							- minTimes.get(i));
					pw.print(minTimes.get(i) + addition + " ");
					sumAddition -= addition;
				}
			}

			pw.flush();
			s.close();
		}
	}

}