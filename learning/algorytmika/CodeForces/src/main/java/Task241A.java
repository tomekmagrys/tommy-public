import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task241A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int m = sc.nextInt();
			int k = sc.nextInt();

			ArrayList<Integer> d = new ArrayList<Integer>(m);
			ArrayList<Integer> s = new ArrayList<Integer>(m);

			for (int i = 0; i < m; i++) {
				d.add(sc.nextInt());
			}
			for (int i = 0; i < m; i++) {
				s.add(sc.nextInt());
			}

			int retVal = 0;
			int fuel = 0;
			int best = 0;

			for (int i = 0; i < m; i++) {
				best = Math.max(best, s.get(i));
				fuel += s.get(i);

				fuel -= d.get(i);
				retVal += d.get(i);

				while (fuel < 0) {
					fuel += best;
					retVal += k;
				}

			}
			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}