import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task189A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();

			ArrayList<ArrayList<Integer>> dynamic = new ArrayList<>(2);
			dynamic.add(new ArrayList<Integer>(n + 1));
			dynamic.add(new ArrayList<Integer>(n + 1));

			dynamic.get(0).add(0);

			for (int i = 1; i <= n; i++) {
				dynamic.get(0).add(null);
			}

			for (int i = 0; i <= n; i++) {
				dynamic.get(0).add(null);
				dynamic.get(1).add(null);
			}

			int min = Math.min(a, Math.min(b, c));

			if (min == 1) {
				pw.write(n + "");
			} else {
				for (int i = 1; i <= n / min; i++) {
					for (int j = 0; j <= n; j++) {
						Integer retVal = dynamic.get((i - 1) % 2).get(j);

						if (j >= a
								&& dynamic.get((i - 1) % 2).get(j - a) != null) {
							retVal=Math.max(retVal == null ? 0 : retVal, dynamic.get((i - 1) % 2).get(j - a)+1);
						}
						if (b != a) {
							if (j >= b
									&& dynamic.get((i - 1) % 2).get(j - b) != null) {
								retVal=Math.max(retVal == null ? 0 : retVal, dynamic.get((i - 1) % 2).get(j - b)+1);
							}
						}
						if (c != a && c != b) {
							if (j >= c
									&& dynamic.get((i - 1) % 2).get(j - c) != null) {
								retVal=Math.max(retVal == null ? 0 : retVal, dynamic.get((i - 1) % 2).get(j - c)+1);
							}
						}
						dynamic.get(i % 2).set(j, retVal);
					}
				}
				pw.write(dynamic.get(n / min % 2).get(n) + "");
			}

			pw.flush();
			s.close();
		}
	}

}