import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task016A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<String> flag = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				flag.add(s.next());
			}

			boolean retVal = true;

			for (int i = 0; i < n; i++) {
				for (int j = 1; j < m; j++) {
					if (flag.get(i).charAt(j) != flag.get(i).charAt(0)) {
						retVal = false;
					}
				}
			}
			for (int i = 1; i < n; i++) {
				if (flag.get(i).charAt(0) == flag.get(i - 1).charAt(0)) {
					retVal = false;
				}
			}

			pw.println(retVal ? "YES" : "NO");

			pw.flush();
			s.close();
		}
	}

}