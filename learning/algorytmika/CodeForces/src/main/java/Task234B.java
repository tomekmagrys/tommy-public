import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task234B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			ArrayList<Pair<Integer, Integer>> a = new ArrayList<>(n);
			for (int i = 1; i <= n; i++) {
				a.add(new Pair<Integer, Integer>(s.nextInt(), i));
			}

			Collections.sort(a, new Comparator<Pair<Integer, Integer>>() {

				@Override
				public int compare(Pair<Integer, Integer> o1,
						Pair<Integer, Integer> o2) {
					int retVal = o1.x.compareTo(o2.x);
					if (retVal != 0) {
						return retVal;
					}
					return o1.y.compareTo(o2.y);
				}
			});

			pw.println(a.get(n-k).x);			
			for (int i = 0; i < k; i++) {
				pw.print(a.get(n - i - 1).y);
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}