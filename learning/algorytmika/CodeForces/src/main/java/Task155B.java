import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task155B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int retVal = 0;

			ArrayList<Pair<Integer, Integer>> cards = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				cards.add(new Pair<Integer, Integer>(s.nextInt(), s.nextInt()));
			}
			Collections.sort(cards, new Comparator<Pair<Integer, Integer>>() {

				@Override
				public int compare(Pair<Integer, Integer> arg0,
						Pair<Integer, Integer> arg1) {
					int retVal = -arg0.y.compareTo(arg1.y);
					if (retVal != 0) {
						return retVal;
					}
					return -arg0.x.compareTo(arg1.x);
				}

			});

			int movesLeft = 1;
			for (int i = 0; i < cards.size() && movesLeft > 0; i++, movesLeft--) {
				movesLeft += cards.get(i).y;
				retVal += cards.get(i).x;
			}

			pw.write("" + retVal);

			pw.flush();
			s.close();
		}
	}

}