import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task094A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String password = s.next();
			
			List<String> digits = new ArrayList<>();
			for(int i = 0 ; i < 10 ; i++){
				digits.add(s.next());
			}
			
			while(!password.isEmpty()){
				for(int i = 0 ; i < 10 ; i++){
					if(password.startsWith(digits.get(i))){
						pw.print(i);
						password = password.substring(10);
						break;
					}
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}
