import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task382A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String inp = s.next();

			int pipe = inp.indexOf('|');
			String left = inp.substring(0, pipe);
			String right = inp.substring(pipe + 1);

			String inp2 = s.next();
			int avail = inp2.length();
			int need = Math.abs(left.length() - right.length());

			if (avail >= need && (avail - need) % 2 == 0) {
				if (left.length() > right.length()) {
					right = right + inp2.substring(0, need);
					inp2 = inp2.substring(need);
				}
				if (right.length() > left.length()) {
					left = left + inp2.substring(0, need);
					inp2 = inp2.substring(need);
				}
				if (avail - need > 0) {
					left += inp2.substring(0, (avail - need) / 2);
					right += inp2.substring((avail - need) / 2);
				}
				pw.print(left);
				pw.print('|');
				pw.print(right);
			} else {
				pw.println("Impossible");
			}

			pw.flush();

			s.close();
		}
	}

}