import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task318B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();
			
			long retVal = 0;
			long incRetVal = 0;
			
			for(int i = 0 ; i + 5 <= input.length() ; i++ ){
				if("heavy".equals(input.substring(i,i+5))){
					incRetVal++;
				} else if("metal".equals(input.substring(i,i+5))){
					retVal += incRetVal;
				} 
			}
			
			pw.write(""+retVal);
			pw.flush();
			s.close();
		}
	}

}