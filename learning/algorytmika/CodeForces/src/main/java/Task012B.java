import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class Task012B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String n = sc.next();
			String m = sc.next();

			if (n.equals("0")) {
				if (m.equals("0")) {
					pw.println("OK");
					pw.flush();
					sc.close();
					return;
				} else {
					pw.println("WRONG_ANSWER");
					pw.flush();
					sc.close();
					return;
				}
			}
			char nChars[] = n.toCharArray();
			char mChars[] = m.toCharArray();

			if (nChars.length != mChars.length) {
				pw.println("WRONG_ANSWER");
				pw.flush();
				sc.close();
				return;
			}

			Arrays.sort(nChars);
			Arrays.sort(mChars);

			for (int i = 0; i < nChars.length; i++) {
				if (nChars[i] != mChars[i]) {
					pw.println("WRONG_ANSWER");
					pw.flush();
					sc.close();
					return;
				}
			}

			nChars = n.toCharArray();
			mChars = m.toCharArray();

			char minNChar = '9';
			for (char c : nChars) {
				if (c != '0') {
					minNChar = (char) Math.min(c, minNChar);
				}
			}

			if (mChars[0] != minNChar) {
				pw.println("WRONG_ANSWER");
				pw.flush();
				sc.close();
				return;
			}

			for (int i = 2; i < mChars.length; i++) {
				if (mChars[i] < mChars[i - 1]) {
					pw.println("WRONG_ANSWER");
					pw.flush();
					sc.close();
					return;
				}
			}

			pw.println("OK");
			pw.flush();
			sc.close();
		}
	}
}
