import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task259A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			boolean retVal = true;

			for (int i = 0; i < 8; i++) {
				String tmp = s.next();
				if(!"BWBWBWBW".equals(tmp) && !"WBWBWBWB".equals(tmp)){
					retVal = false;
				}
			}
			pw.write(retVal ? "YES" : "NO");
			pw.flush();
			s.close();
		}
	}

}
