import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task186A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String firstGenome = s.next();
			String secondGenome = s.next();

			if (firstGenome.length() != secondGenome.length()) {
				pw.println("NO");
			} else {
				ArrayList<Integer> differentPositions = new ArrayList<Integer>();
				for (int i = 0; i < firstGenome.length(); i++) {
					if (firstGenome.charAt(i) != secondGenome.charAt(i)) {
						differentPositions.add(i);
					}
				}
				if (differentPositions.size() != 2) {
					pw.println("NO");
				} else {
					int i = differentPositions.get(0);
					int j = differentPositions.get(1);
					if (firstGenome.charAt(i) == secondGenome.charAt(j)
							&& firstGenome.charAt(j) == secondGenome.charAt(i)) {
						pw.println("YES");
					} else {
						pw.println("NO");
					}
				}
			}

			pw.flush();
			s.close();
		}
	}

}