import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task337B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	public static int gcd(int a, int b) {
		return b == 0 ? a : gcd(b, a % b);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();
			int d = s.nextInt();

			if (a * d > b * c) {
				int newA = a * d;
				int newB = b * d;

				int newC = c * b;
				int newD = d * b;

				assert newC <= newA;
				assert newB == newD;

				int p = newA - newC;
				int q = newA;

				int nwdpq = gcd(p, q);

				p /= nwdpq;
				q /= nwdpq;

				pw.print(p);
				pw.print("/");
				pw.println(q);
			} else {
				int newA = a * c;
				int newB = b * c;

				int newC = c * a;
				int newD = d * a;

				assert newC == newA;
				assert newD <= newB;

				int p = newB - newD;
				int q = newB;

				int nwdpq = gcd(p, q);

				p /= nwdpq;
				q /= nwdpq;

				pw.print(p);
				pw.print("/");
				pw.println(q);
			}
			pw.flush();
			s.close();
		}
	}

}
