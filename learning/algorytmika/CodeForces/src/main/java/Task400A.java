import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Task400A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int t = sc.nextInt();
			while (t-- > 0) {
				char cards[] = sc.next().toCharArray();
				ArrayList<String> retVal = new ArrayList<>();
				if (cards[0] == 'X' || cards[1] == 'X' || cards[2] == 'X'
						|| cards[3] == 'X' || cards[4] == 'X'
						|| cards[5] == 'X' || cards[6] == 'X'
						|| cards[7] == 'X' || cards[8] == 'X'
						|| cards[9] == 'X' || cards[10] == 'X'
						|| cards[11] == 'X') {
					retVal.add("1x12");
				}
				if ((cards[0] == 'X' && cards[6] == 'X')
						|| (cards[1] == 'X' && cards[7] == 'X')
						|| (cards[2] == 'X' && cards[8] == 'X')
						|| (cards[3] == 'X' && cards[9] == 'X')
						|| (cards[4] == 'X' && cards[10] == 'X')
						|| (cards[5] == 'X' && cards[11] == 'X')) {
					retVal.add("2x6");
				}
				if ((cards[0] == 'X' && cards[4] == 'X' && cards[8] == 'X')
						|| (cards[1] == 'X' && cards[5] == 'X' && cards[9] == 'X')
						|| (cards[2] == 'X' && cards[6] == 'X' && cards[10] == 'X')
						|| (cards[3] == 'X' && cards[7] == 'X' && cards[11] == 'X')) {
					retVal.add("3x4");
				}
				if ((cards[0] == 'X' && cards[3] == 'X' && cards[6] == 'X' && cards[9] == 'X')
						|| (cards[1] == 'X' && cards[4] == 'X'
								&& cards[7] == 'X' && cards[10] == 'X')
						|| (cards[2] == 'X' && cards[5] == 'X'
								&& cards[8] == 'X' && cards[11] == 'X')) {
					retVal.add("4x3");
				}
				if ((cards[0] == 'X' && cards[2] == 'X' && cards[4] == 'X'
						&& cards[6] == 'X' && cards[8] == 'X' && cards[10] == 'X')
						|| (cards[1] == 'X' && cards[3] == 'X'
								&& cards[5] == 'X' && cards[7] == 'X'
								&& cards[9] == 'X' && cards[11] == 'X')) {
					retVal.add("6x2");
				}
				if (cards[0] == 'X' && cards[2] == 'X' && cards[4] == 'X'
						&& cards[6] == 'X' && cards[8] == 'X'
						&& cards[10] == 'X' && cards[1] == 'X'
						&& cards[3] == 'X' && cards[5] == 'X'
						&& cards[7] == 'X' && cards[9] == 'X'
						&& cards[11] == 'X') {
					retVal.add("12x1");
				}
				pw.print(retVal.size());
				for (String s : retVal) {
					pw.print(" ");
					pw.print(s);
				}
				pw.println();
			}

			pw.flush();
			sc.close();
		}
	}

}