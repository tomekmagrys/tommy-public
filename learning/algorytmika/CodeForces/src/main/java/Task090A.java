import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task090A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int r = sc.nextInt();
			int g = sc.nextInt();
			int b = sc.nextInt();

			ArrayList<Integer> retVals = new ArrayList<>();

			if (r > 0) {
				retVals.add(30 + 3 * ((r - 1) / 2));
			}
			if (g > 0) {
				retVals.add(31 + 3 * ((g - 1) / 2));
			}
			if (b > 0) {
				retVals.add(32 + 3 * ((b - 1) / 2));
			}
			int retVal = Collections.max(retVals);

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}