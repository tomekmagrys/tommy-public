import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task456A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Pair<X,Y> {
        private final X x;
        private final Y y;

        Pair(X x, Y y) {
            this.x = x;
            this.y = y;
        }

        public X getX() {
            return x;
        }

        public Y getY() {
            return y;
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            ArrayList<Pair<Integer,Integer>> a = new ArrayList<>(n);

            for(int i = 0 ; i < n ; i++){
                a.add(new Pair<>(sc.nextInt(),sc.nextInt()));
            }

            Collections.sort(a,new Comparator<Pair<Integer, Integer>>() {
                @Override
                public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
                    int retVal = o1.getX().compareTo(o2.getX());
                    if(retVal != 0){
                        return retVal;
                    }
                    return o1.getY().compareTo(o2.getY());
                }
            });

            for(int i = 0 ; i + 1 < a.size() ; i++){
                if(a.get(i).getY() > a.get(i+1).getY()){
                    pw.println("Happy Alex");
                    pw.flush();
                    sc.close();
                    return;
                }
            }

            pw.println("Poor Alex");
            pw.flush();
            sc.close();
        }
    }

}