import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task127A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			ArrayList<Integer> x = new ArrayList<>(n);
			ArrayList<Integer> y = new ArrayList<>(k);

			for (int i = 0; i < n; i++) {
				x.add(s.nextInt());
				y.add(s.nextInt());
			}

			double dist = 0;

			for (int i = 1; i < n; i++) {
				dist += Math.sqrt((x.get(i) - x.get(i - 1))
						* (x.get(i) - x.get(i - 1)) + (y.get(i) - y.get(i - 1))
						* (y.get(i) - y.get(i - 1)));
			}

			
			pw.write((dist * k / 50)+"");
			pw.flush();
			s.close();
		}
	}

}
