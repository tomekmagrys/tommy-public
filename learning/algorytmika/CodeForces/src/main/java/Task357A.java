import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task357A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int m = s.nextInt();
			
			ArrayList<Integer> marks = new ArrayList<>(m);
			
			for(int i = 0 ; i < m ; i++){
				marks.add(s.nextInt());
			}

			int x = s.nextInt();
			int y = s.nextInt();
			
			int retVal = 0;
			for(int k = 0 ; k < m ; k++){
				int sum1 = 0;
				int sum2 = 0;
				for(int i = 0 ; i < k; i++){
					sum1 += marks.get(i);
				}
				for(int i = k ; i < m ; i++){
					sum2 += marks.get(i);
				}
				if(x <= sum1 && sum1 <= y && x <= sum2 && sum2 <= y){
					retVal = k+1;
					break;
				}
				
			}
			
			pw.write(""+retVal);
			
			pw.flush();
			s.close();
		}
	}

}
