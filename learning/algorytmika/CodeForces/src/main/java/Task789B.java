import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Task789B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long b1 = sc.nextLong();
            long q = sc.nextLong();
            long l = sc.nextLong();
            long m = sc.nextLong();

            Set<Long> badNumbers = new HashSet<>();

            for (int i = 0; i < m; i++) {
                badNumbers.add(sc.nextLong());
            }

            if (q == 1) {
                if (badNumbers.contains(b1)) {
                    pw.println(0);
                } else {
                    pw.println("inf");
                }
            } else if (q == -1) {
                if (badNumbers.contains(b1) && badNumbers.contains(-b1)) {
                    pw.println(0);
                } else {
                    pw.println("inf");
                }
            } else if (q == 0) {
                if (badNumbers.contains(b1)) {
                    if(badNumbers.contains(0)){
                        pw.println(0);
                    } else {
                        pw.println("inf");
                    }
                } else {
                    if(badNumbers.contains(0)){
                        if(Math.abs(b1) <= Math.abs(l)) {
                            pw.println(1);
                        } else {
                            pw.println(0);

                        }
                    } else {
                        pw.println("inf");
                    }
                }
            } else {
                int retVal = 0;

                while (Math.abs(b1) <= Math.abs(l)) {
                    if (!badNumbers.contains(b1)) {
                        retVal++;
                    }
                    b1 *= q;
                }

                pw.println(retVal);
            }
            pw.flush();
            sc.close();
        }
    }

}