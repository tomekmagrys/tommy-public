import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task401C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			if (n > m + 1) {
				pw.println("-1");
				pw.flush();
				sc.close();
				return;
			}
			if (m > 2 * n + 2) {
				pw.println("-1");
				pw.flush();
				sc.close();
				return;
			}

			StringBuilder sb = new StringBuilder();
			while (n > 0 || m > 0) {
				if (n == m + 1) {
					sb.append("0");
					n--;
					continue;
				}
				if (n == m) {
					sb.append("10");
					n--;
					m--;
					continue;
				}
				if (n == m - 1) {
					sb.append("1");
					m--;
					continue;
				}
				if (n > 0) {
					sb.append("110");
					m -= 2;
					n--;
					continue;
				}
				sb.append("11");
				m -= 2;
				continue;
			}

			pw.println(sb.toString());
			pw.flush();
			sc.close();
		}
	}

}