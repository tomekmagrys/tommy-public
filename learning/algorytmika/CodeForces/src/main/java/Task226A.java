import java.io.*;

public class Task226A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {

        public static long pow3(long n, long mod) {
            if (n == 1) {
                return 3 % mod;
            }
            if (n % 2 == 0) {
                long retVal = pow3(n / 2, mod);
                retVal = retVal * retVal % mod;
                return retVal;
            } else {
                long retVal = pow3(n / 2, mod);
                retVal = retVal * retVal % mod;
                retVal = retVal * 3 % mod;
                return retVal;
            }
        }

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();

            pw.println((pow3(n, m) - 1 + m) % m);

            pw.flush();
            sc.close();
        }
    }

}