import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task236B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		static Map<Integer, Integer> dyn = new HashMap<>();

		public static int d(int n) {
			if (!dyn.containsKey(n)) {

				int retVal = 0;
				for (int i = 1; i * i <= n; i++) {
					if (n % i == 0) {
						if (n / i != i) {
							retVal += 2;
						} else {
							retVal++;
						}

					}
				}
				dyn.put(n, retVal);
			}
			return dyn.get(n);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();

			int retVal = 0;

			for (int i = 1; i <= a; i++) {
				for (int j = 1; j <= b; j++) {
					for (int k = 1; k <= c; k++) {
						retVal += d(i * j * k);
					}
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}