import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Task151B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			Set<String> taxi = new LinkedHashSet<String>();
			Set<String> pizza = new LinkedHashSet<String>();
			Set<String> girl = new LinkedHashSet<String>();

			int curMaxTaxi = 0;
			int curMaxPizza = 0;
			int curMaxGirl = 0;

			for (int i = 0; i < n; i++) {
				int si = sc.nextInt();
				String namei = sc.next();
				int taxiNumbers = 0;
				int pizzaNumbers = 0;
				int girlsNumbers = 0;

				for (int j = 0; j < si; j++) {
					char number[] = sc.next().toCharArray();
					if (number[0] == number[1] && number[1] == number[3]
							&& number[3] == number[4] && number[4] == number[6]
							&& number[6] == number[7]) {
						taxiNumbers++;
					} else if (number[0] > number[1] && number[1] > number[3]
							&& number[3] > number[4] && number[4] > number[6]
							&& number[6] > number[7]) {
						pizzaNumbers++;
					} else {
						girlsNumbers++;
					}
				}

				if (taxiNumbers > curMaxTaxi) {
					taxi.clear();
					curMaxTaxi = taxiNumbers;
				}
				if (taxiNumbers == curMaxTaxi) {
					taxi.add(namei);
				}

				if (pizzaNumbers > curMaxPizza) {
					pizza.clear();
					curMaxPizza = pizzaNumbers;
				}
				if (pizzaNumbers == curMaxPizza) {
					pizza.add(namei);
				}

				if (girlsNumbers > curMaxGirl) {
					girl.clear();
					curMaxGirl = girlsNumbers;
				}
				if (girlsNumbers == curMaxGirl) {
					girl.add(namei);
				}
			}

			{
				StringBuilder sb = new StringBuilder();
				sb.append("If you want to call a taxi, you should call: ");
				Iterator<String> iter = taxi.iterator();
				while (iter.hasNext()) {
					sb.append(iter.next());
					if (iter.hasNext()) {
						sb.append(", ");
					}
				}
				sb.append(".");
				pw.println(sb.toString());
			}

			{
				StringBuilder sb = new StringBuilder();
				sb.append("If you want to order a pizza, you should call: ");
				Iterator<String> iter = pizza.iterator();
				while (iter.hasNext()) {
					sb.append(iter.next());
					if (iter.hasNext()) {
						sb.append(", ");
					}
				}
				sb.append(".");
				pw.println(sb.toString());
			}

			{
				StringBuilder sb = new StringBuilder();
				sb.append("If you want to go to a cafe with a wonderful girl, you should call: ");
				Iterator<String> iter = girl.iterator();
				while (iter.hasNext()) {
					sb.append(iter.next());
					if (iter.hasNext()) {
						sb.append(", ");
					}
				}
				sb.append(".");
				pw.println(sb.toString());
			}

			pw.flush();
			sc.close();
		}
	}

}