import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task222A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();
			
			ArrayList<Integer> permutation = new ArrayList<>(n+1);
			permutation.add(null);
			
			for(int i = 0; i < n ; i++){
				permutation.add(s.nextInt());
			}
			
			int candidateForEquality = permutation.get(k);
			boolean can = true;
			
			for(int i = k+1 ; i <= n ; i++){
				if(candidateForEquality != permutation.get(i)){
					can = false;
					break;
				}
			}
			
			if(can){
				for(int i = k - 1; i >= 0 ; i--){
					if(!permutation.get(k).equals(permutation.get(i))){
						pw.write(i+"");
						break;
					}
				}
			} else {
				pw.write("-1");
			}
			
			pw.flush();
			s.close();
		}
	}

}
