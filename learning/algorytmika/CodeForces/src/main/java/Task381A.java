import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task381A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			ArrayList<Integer> a = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int p = 0;
			int q = a.size() - 1;

			int retVal1 = 0;
			int retVal2 = 0;
			boolean inc1 = true;

			while (p <= q) {
				int add;
				if (a.get(p) < a.get(q)) {
					add = a.get(q);
					q--;
				} else {
					add = a.get(p);
					p++;
				}
				if (inc1) {
					retVal1 += add;
				} else {
					retVal2 += add;
				}
				inc1 = !inc1;
			}

			pw.print(retVal1);
			pw.print(" ");
			pw.println(retVal2);
			pw.flush();
			s.close();
		}
	}

}