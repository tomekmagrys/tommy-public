import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task242B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> l = new ArrayList<>(n);
			ArrayList<Integer> r = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				l.add(s.nextInt());
				r.add(s.nextInt());
			}

			int minL = Collections.min(l);
			int maxR = Collections.max(r);

			int retVal = -1;

			for (int i = 0; i < n; i++) {
				if (l.get(i) == minL && r.get(i) == maxR) {
					retVal=i+1;
					break;
				}
			}

			pw.write(retVal + "");

			pw.flush();
			s.close();
		}
	}

}
