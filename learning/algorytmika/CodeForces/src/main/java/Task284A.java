import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task284A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static int pow(int a, int b, int p) {
			if (b == 0) {
				return 1;
			}
			if (b == 1) {
				return a % p;
			}
			if (b % 2 == 0) {
				int retVal = pow(a, b / 2, p);
				return retVal * retVal % p;
			} else {
				int retVal = pow(a, b / 2, p);
				return retVal * retVal % p * a % p;
			}
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int p = s.nextInt();
			int retVal = 0;

			for (int x = 1; x < p; x++) {
				boolean still = true;
				for (int i = 1; i <= p - 2; i++) {
					if (pow(x, i, p) - 1 == 0) {
						still = false;
						break;
					}
				}
				if (pow(x, p - 1, p) - 1 != 0) {
					still = false;
				}
				if (still) {
					retVal++;
				}
			}
			pw.println(retVal);
			pw.flush();
			s.close();
		}
	}

}