import java.io.*;

public class Task533A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Math2 {
        public static long newton(long n, long k) {
            long retVal = 1;
            for (long i = 1; i <= k; i++) {
                retVal *= n + 1 - i;
                retVal /= i;
                retVal %= 1000000007;
            }
            return retVal;
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int N = sc.nextInt();

            int retVal = Integer.MAX_VALUE;

            for (int i = 1; i <= 1000; i++) {
                for (int j = 1; j <= 1000; j++) {
                    if (i * j >= N) {
                        retVal = Math.min(retVal, 2 * (i + j));
                    }
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();

        }
    }

}