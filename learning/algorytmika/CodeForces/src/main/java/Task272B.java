import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task272B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static int f(int n) {
			if (n == 0)
				return 0;
			if (n % 2 == 0)
				return f(n / 2);
			return f(n / 2) + 1;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			long buckets[] = new long[30];
			
			for (int i = 0; i < n; i++) {
				buckets[f(s.nextInt())]++;
			}

			
			long retVal = 0;
			for(int i = 0 ; i < 30 ; i++){
				retVal += buckets[i] * (buckets[i]-1)/2;
			}

			pw.println(retVal);
			
			pw.flush();
			s.close();
		}
	}

}