import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Task441A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int v = sc.nextInt();

            ArrayList<Integer> retVal = new ArrayList<>();

            for(int i = 1 ; i <= n ; i++){
                int ki = sc.nextInt();
                ArrayList<Integer> k = new ArrayList<>(ki);
                for(int j = 0 ; j < ki ; j++){
                    k.add(sc.nextInt());
                }
                if(v > Collections.min(k)){
                    retVal.add(i);
                }
            }

            pw.println(retVal.size());

            for(Integer i : retVal){
                pw.print(i);
                pw.print(" ");
            }

            pw.flush();
            sc.close();
        }
    }

}