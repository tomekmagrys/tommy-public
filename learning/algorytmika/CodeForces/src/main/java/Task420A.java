import java.io.*;
import java.util.Objects;

public class Task420A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            String s = sc.next();


            if(s.indexOf('B') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('C') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('D') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('E') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('F') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('G') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('J') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('K') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('L') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('N') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('P') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('Q') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('R') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('S') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            if(s.indexOf('Z') != -1){
                pw.println("NO");
                pw.flush();
                sc.close();
                return;
            }
            for(int i = 0 ; i < s.length()/2 ; i++){
                if(s.charAt(i) != s.charAt(s.length()-1-i)){
                    pw.println("NO");
                    pw.flush();
                    sc.close();
                    return;
                }
            }
            pw.println("YES");
            pw.flush();
            sc.close();
        }
    }

}