import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task322B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int r = s.nextInt();
			int g = s.nextInt();
			int b = s.nextInt();

			int mixed = Math.min(Math.min(r, g), b);
			int red = (r - mixed) / 3;
			int green = (g - mixed) / 3;
			int blue = (b - mixed) / 3;

			if ((g - mixed - green * 3) + (b - mixed - blue * 3) == 4
					&& mixed > 0) {
				green++;
				blue++;
				mixed--;
			}
			if ((r - mixed - red * 3) + (b - mixed - blue * 3) == 4
					&& mixed > 0) {
				red++;
				blue++;
				mixed--;
			}
			if ((r - mixed - red * 3) + (g - mixed - green * 3) == 4
					&& mixed > 0) {
				red++;
				green++;
				mixed--;
			}
			pw.println(mixed + red + green + blue);

			pw.flush();
			s.close();
		}
	}

}