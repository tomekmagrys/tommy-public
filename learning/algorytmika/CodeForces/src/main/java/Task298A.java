import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task298A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			s.nextInt();
			String foots = s.next();

			int firstR = foots.indexOf('R');
			int firstL = foots.indexOf('L');

			if (firstR >= 0) {
				if (firstL >= 0) {
					pw.print(firstR + 1);
					pw.print(" ");
					pw.print(firstL);
				} else {
					pw.print(firstR+1);
					pw.print(" ");
					pw.print(foots.lastIndexOf('R') + 2);
				}
			} else {
				pw.print(foots.lastIndexOf('L') + 1);
				pw.print(" ");
				pw.print(firstL);
			}

			pw.flush();
			s.close();
		}
	}

}