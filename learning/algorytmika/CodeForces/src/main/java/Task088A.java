import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task088A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			boolean flags[] = new boolean[12];

			int tunes = 0;

			for (int i = 0; i < 3; i++) {
				switch (sc.next()) {
				case "C":
					flags[0] = true;
					break;
				case "C#":
					flags[1] = true;
					break;
				case "D":
					flags[2] = true;
					break;
				case "D#":
					flags[3] = true;
					break;
				case "E":
					flags[4] = true;
					break;
				case "F":
					flags[5] = true;
					break;
				case "F#":
					flags[6] = true;
					break;
				case "G":
					flags[7] = true;
					break;
				case "G#":
					flags[8] = true;
					break;
				case "A":
					flags[9] = true;
					break;
				case "B":
					flags[10] = true;
					break;
				case "H":
					flags[11] = true;
					break;
				default:
					throw new UnsupportedOperationException();
				}
			}

			for (int i = 0; i < 12; i++) {
				if (flags[i] && flags[(i + 4) % 12] && flags[(i + 7) % 12]) {
					pw.println("major");
					pw.flush();
					sc.close();
					return ;
				}
			}

			for (int i = 0; i < 12; i++) {
				if (flags[i] && flags[(i + 3) % 12] && flags[(i + 7) % 12]) {
					pw.println("minor");
					pw.flush();
					sc.close();
					return ;
				}
			}
			pw.println("strange");
			pw.flush();
			sc.close();
		}
	}

}