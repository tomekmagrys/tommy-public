import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task447B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            String s = sc.next();
            int k = sc.nextInt();
            List<Integer> w = new ArrayList<>(26);

            for (int i = 0; i < 26; i++) {
                w.add(sc.nextInt());
            }

            int tmp1 = s.length() + k;
            int tmp2 = s.length();

            tmp1 = tmp1 * (tmp1 + 1) / 2;
            tmp2 = tmp2 * (tmp2 + 1) / 2;

            int retVal = Collections.max(w) * (tmp1 - tmp2);

            for (int i = 0; i < s.length(); i++) {
                retVal += (i + 1) * w.get(s.charAt(i) - 'a');
            }

            pw.println(retVal);
            pw.flush();
            sc.close();
        }
    }

}