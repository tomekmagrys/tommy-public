import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task291B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);

			Scanner sc = new Scanner(is);
			String input = sc.nextLine();

			boolean insideQuote = false;
			boolean insideParameter = false;

			for (char c : input.toCharArray()) {
				switch (c) {
				case '"':
					insideQuote = !insideQuote;
					if (insideQuote) {
						pw.write("<");
					} else {
						pw.write(">\n");
					}
					break;
				case ' ':
					if (insideQuote) {
						pw.write(" ");
					} else {
						if (insideParameter) {
							pw.write(">\n");
							insideParameter = false;
						}
					}
					break;
				default:
					if (insideParameter || insideQuote) {
						pw.write(c);
					} else {
						insideParameter = true;
						pw.write("<");
						pw.write(c);
					}
				}

			}
			if(insideParameter && !insideQuote){
				pw.write(">\n");
			}
			pw.flush();
			sc.close();
		}
	}

}