import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task316A2 {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String inp = sc.next();

			int retVal = 1;

			boolean questionMarkAtBeginning = false;
			int qustionMarksInTheMiddle = 0;

			Character letterAtBeginning = null;
			Set<Character> lettersInTheMiddle = new TreeSet<>();

			if (inp.charAt(0) == '?') {
				questionMarkAtBeginning = true;
			}
			if (Character.isLetter(inp.charAt(0))) {
				letterAtBeginning = Character.valueOf(inp.charAt(0));
			}
			for (int i = 1; i < inp.length(); i++) {
				if (inp.charAt(i) == '?') {
					qustionMarksInTheMiddle++;
				}
				if (!Character.valueOf(inp.charAt(i)).equals(letterAtBeginning)
						&& Character.isLetter(inp.charAt(i))) {
					lettersInTheMiddle.add(Character.valueOf(inp.charAt(i)));
				}
			}

			if (questionMarkAtBeginning) {
				retVal *= 9;
			}

			if (letterAtBeginning != null) {
				retVal *= 9;
				int multiplier = 9;
				for (char c : lettersInTheMiddle) {
					retVal *= multiplier;
					multiplier--;
				}
			} else {
				int multiplier = 10;
				for (char c : lettersInTheMiddle) {
					retVal *= multiplier;
					multiplier--;
				}
			}

			pw.print(retVal);
			for (int i = 0; i < qustionMarksInTheMiddle; i++) {
				pw.print("0");
			}
			pw.println();
			pw.flush();
			sc.close();
		}
	}

}