import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task313C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		private static long beauty(List<Integer> a) {
			long retVal = 0;

			for (int i = 1; i <= a.size(); i *= 4) {
				for (int j = a.size() - i; j < a.size(); j++) {
					retVal += a.get(j);
				}
			}

			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(sc.nextInt());
			}

			Collections.sort(a);

			pw.println(beauty(a));

			pw.flush();
			sc.close();
		}
	}

}
