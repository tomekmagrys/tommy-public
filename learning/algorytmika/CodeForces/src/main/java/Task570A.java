import java.io.*;
import java.util.*;

public class Task570A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int cands = sc.nextInt();
            int cities = sc.nextInt();

            Map<Integer, Integer> winners = new TreeMap<>();

            for (int i = 0; i < cities; i++) {
                List<Integer> votes = new ArrayList<>(cands);
                for (int j = 0; j < cands; j++) {
                    votes.add(sc.nextInt());
                }

                int maxVotes = Collections.max(votes);

                int winner = votes.indexOf(maxVotes);

                if (!winners.containsKey(winner)) {
                    winners.put(winner, 0);
                }
                winners.put(winner, winners.get(winner));

            }

            int maxVotes = Collections.max(winners.values());

            for (Map.Entry<Integer,Integer> cand: winners.entrySet()) {
                if (cand.getValue() == maxVotes) {
                    pw.println(cand.getKey() + 1);
                    break;
                }
            }



            pw.flush();
            sc.close();
        }
    }

}