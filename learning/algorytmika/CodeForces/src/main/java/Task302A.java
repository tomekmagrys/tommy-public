import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task302A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			int minusCount = 0;
			int plusCount = 0;

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				int tmp = s.nextInt();
				if (tmp == 1) {
					plusCount++;
				}
				if (tmp == -1) {
					minusCount++;
				}
				a.add(tmp);
			}

			for (int i = 0; i < m; i++) {
				int l = s.nextInt();
				int r = s.nextInt();
				int size = r - l + 1;
				if (size % 2 == 0 && plusCount >= size / 2
						&& minusCount >= size / 2) {
					pw.write("1\n");
				} else {
					pw.write("0\n");
				}
			}

			pw.flush();
			s.close();
		}
	}

}