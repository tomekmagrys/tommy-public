import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task003A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String s = sc.next();
			String t = sc.next();

			int x1 = s.toCharArray()[0] - 'a' + 1;
			int y1 = s.toCharArray()[1] - '0';

			int x2 = t.toCharArray()[0] - 'a' + 1;
			int y2 = t.toCharArray()[1] - '0';

			pw.write(""+Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2))+"\n");

			while (x1 != x2 || y1 != y2) {
				switch ((int) Math.signum(x1 - x2)) {
				case 1:
					switch ((int) Math.signum(y1 - y2)) {
					case 1:
						pw.write("LD\n");
						y1--;
						break;
					case -1:
						pw.write("LU\n");
						y1++;
						break;
					default:
						pw.write("L\n");
					}
					x1--;
					break;
				case -1:
					switch ((int) Math.signum(y1 - y2)) {
					case 1:
						pw.write("RD\n");
						y1--;
						break;
					case -1:
						pw.write("RU\n");
						y1++;
						break;
					default:
						pw.write("R\n");
					}
					x1++;
					break;
				default:
					switch ((int) Math.signum(y1 - y2)) {
					case 1:
						pw.write("D\n");
						y1--;
						break;
					case -1:
						pw.write("U\n");
						y1++;
						break;
					default:
					}
					break;
				}
			}

			pw.flush();
			sc.close();
		}
	}

}