import java.io.*;
import java.util.ArrayList;

public class Task451B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            ArrayList<Integer> a = new ArrayList<>(n + 2);

            a.add(Integer.MIN_VALUE);

            for (int i = 0; i < n; i++) {
                a.add(sc.nextInt());
            }

            a.add(Integer.MAX_VALUE);

            ArrayList<Integer> tops = new ArrayList<>(1);
            ArrayList<Integer> downs = new ArrayList<>(1);

            for (int i = 1; i <= n; i++) {
                if (a.get(i - 1) < a.get(i) && a.get(i) > a.get(i + 1)) {
                    tops.add(i);
                }
                if (a.get(i - 1) > a.get(i) && a.get(i) < a.get(i + 1)) {
                    downs.add(i);
                }
            }

            if (tops.size() != downs.size()) {
                throw new RuntimeException("Bad assumption-1");
            }

            switch (tops.size()) {
                case 0:
                    pw.println("yes");
                    pw.println("1 1");
                    break;
                case 1:

                    Integer l = tops.get(0);
                    Integer r = downs.get(0);

                    if (a.get(l - 1) < a.get(r) && a.get(l) < a.get(r + 1)) {
                        pw.println("yes");
                        pw.print(l);
                        pw.print(" ");
                        pw.println(r);
                    } else {
                        pw.println("no");
                    }
                    break;
                default:
                    pw.println("no");
            }

            pw.flush();
            sc.close();
        }
    }

}