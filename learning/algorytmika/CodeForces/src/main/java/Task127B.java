import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task127B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int sticks[] = new int[101];

			for (int i = 0; i < n; i++) {
				sticks[s.nextInt()]++;
			}

			int pairsCount = 0;

			for (int i = 1; i <= 100; i++) {
				pairsCount += sticks[i]/2;
			}

			pw.write(pairsCount/2+"");
			
			pw.flush();
			s.close();
		}
	}

}
