import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task376A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.nextLine();

			int pivot = input.indexOf('^');

			long left = 0;
			long right = 0;

			for (int i = 0; i < pivot; i++) {
				if (Character.isDigit(input.charAt(i))) {
					left += (input.charAt(i) - '0') * (pivot - i);
				}
			}

			for (int i = pivot + 1; i < input.length(); i++) {
				if (Character.isDigit(input.charAt(i))) {
					right += (input.charAt(i) - '0') * (i - pivot);
				}
			}

			if (left > right) {
				pw.println("left");
			}
			if (left == right) {
				pw.println("balance");
			}
			if (left < right) {
				pw.println("right");
			}
			pw.flush();
			s.close();
		}
	}

}