import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task265A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			byte s[] = sc.next().getBytes();
			byte t[] = sc.next().getBytes();
			
			int pos = 0;
			
			for(int i = 0 ; i < t.length ; i++){
				if(t[i] == s[pos]){
					pos++;
				}
			}
			
			os.write((pos + 1 + System.lineSeparator()).getBytes());
			sc.close();
		}
	}

}
