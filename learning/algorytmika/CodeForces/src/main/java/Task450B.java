import java.io.*;

public class Task450B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {


        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long a = sc.nextLong();
            long b = sc.nextLong();
            int n = sc.nextInt();
            n--;
            n=n%6;
            long retVal=0;
            long mod = 1000000007;

            switch(n)
            {
                case(0):
                {
                    retVal=(a+mod)%mod;
                    break;
                }
                case(1):
                {
                    retVal=(b+mod)%mod;
                    break;
                }
                case(2):
                {
                    retVal=(b-a+2*mod)%mod;
                    break;
                }
                case(3):
                {
                    retVal=(mod-a)%mod;
                    break;
                }
                case(4):
                {
                    retVal=(mod-b)%mod;
                    break;
                }
                case(5):
                {
                    retVal=(a-b+2*mod)%mod;
                    break;
                }
            }
            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}