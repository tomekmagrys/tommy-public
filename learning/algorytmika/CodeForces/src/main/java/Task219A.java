import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task219A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int k = sc.nextInt();

			char[] letters = sc.next().toCharArray();

			Map<Character, Integer> lettersCount = new HashMap<>();

			for (char b : letters) {
				if (lettersCount.containsKey(b)) {
					lettersCount.put(b, lettersCount.get(b) + 1);
				} else {
					lettersCount.put(b, 1);
				}
			}

			for (char b : lettersCount.keySet()) {
				if (lettersCount.get(b) % k != 0) {
					os.write(("-1" + System.lineSeparator()).getBytes());
					sc.close();
					return;
				}
			}

			StringBuilder retVal = new StringBuilder();

			for (int i = 0; i < k; i++) {
				for (char b : lettersCount.keySet()) {
					for (int j = 0; j < lettersCount.get(b) / k; j++)
						retVal.append(b);
				}
			}

			os.write((retVal.toString() + System.lineSeparator()).getBytes());
			sc.close();
		}
	}

}
