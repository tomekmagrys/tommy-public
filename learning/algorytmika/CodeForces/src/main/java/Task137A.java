import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task137A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int retVal = 0;
			String tmp = s.next();

			while (!tmp.isEmpty()) {
				if (tmp.startsWith("CCCCC")) {
					tmp = tmp.substring(5);
					retVal++;
					continue;
				}
				if (tmp.startsWith("CCCC")) {
					tmp = tmp.substring(4);
					retVal++;
					continue;
				}
				if (tmp.startsWith("CCC")) {
					tmp = tmp.substring(3);
					retVal++;
					continue;
				}
				if (tmp.startsWith("CC")) {
					tmp = tmp.substring(2);
					retVal++;
					continue;
				}
				if (tmp.startsWith("C")) {
					tmp = tmp.substring(1);
					retVal++;
					continue;
				}
				if (tmp.startsWith("PPPPP")) {
					tmp = tmp.substring(5);
					retVal++;
					continue;
				}
				if (tmp.startsWith("PPPP")) {
					tmp = tmp.substring(4);
					retVal++;
					continue;
				}
				if (tmp.startsWith("PPP")) {
					tmp = tmp.substring(3);
					retVal++;
					continue;
				}
				if (tmp.startsWith("PP")) {
					tmp = tmp.substring(2);
					retVal++;
					continue;
				}
				if (tmp.startsWith("P")) {
					tmp = tmp.substring(1);
					retVal++;
					continue;
				}
			}

			pw.write(retVal + "");
			pw.flush();
			s.close();
		}
	}

}
