import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task216A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();

			int retVal = a * b + b * c + c * a - a - b - c + 1;
			pw.write("" + retVal);

			pw.flush();
			s.close();
		}
	}

}