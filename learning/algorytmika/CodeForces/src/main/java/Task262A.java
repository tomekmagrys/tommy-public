import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task262A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			
			int n = s.nextInt();
			int k = s.nextInt();
			
			String a[] = new String[n];
			
			int retVal = 0;
			
			for(int i = 0 ; i < n ; i++){
				a[i] = s.next();
				int magicNumberCount = 0;
				for(char c:a[i].toCharArray()){
					if(c == '4' || c == '7') magicNumberCount++;
				}
				if(magicNumberCount <= k) retVal++;
			}
			
			
			s.close();
			pw.println(retVal);
			pw.flush();
		}
	}

}
