import java.io.*;
import java.util.*;
import java.util.List;

public class Task432B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            List<Integer> a = new ArrayList<>(n);
            List<Integer> c = new ArrayList<>(n);

            for(int i = 0 ; i <= 10e5 ; i++){
                c.add(0);
            }

            for(int i = 0 ; i < n ; i++){
                int xi = sc.nextInt();
                int yi = sc.nextInt();
                c.set(xi,c.get(xi)+1);
                a.add(yi);
            }

            for(int i = 0 ; i < n ; i++){
                pw.print(n - 1 + c.get(a.get(i)));
                pw.print(" ");
                pw.print(n - 1 - c.get(a.get(i)));
                pw.println();
            }


            pw.flush();
            sc.close();
        }
    }

}