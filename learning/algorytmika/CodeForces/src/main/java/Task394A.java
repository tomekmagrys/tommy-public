import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task394A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String expression = sc.next();

			int plus = expression.indexOf('+');
			int eq = expression.indexOf('=');

			int a = plus;
			int b = eq - (plus + 1);
			int c = expression.length() - (eq + 1);

			if (a + b == c) {
				for (int i = 0; i < a; i++) {
					pw.print('|');
				}
				pw.print('+');
				for (int i = 0; i < b; i++) {
					pw.print('|');
				}
				pw.print('=');
				for (int i = 0; i < c; i++) {
					pw.print('|');
				}
			} else if (a + b + 1 == c - 1) {
				for (int i = 0; i < a; i++) {
					pw.print('|');
				}
				pw.print('+');
				for (int i = 0; i < b + 1; i++) {
					pw.print('|');
				}
				pw.print('=');
				for (int i = 0; i < c - 1; i++) {
					pw.print('|');
				}
			} else if (a + b - 1 == c + 1) {
				if (b > 1) {
					for (int i = 0; i < a; i++) {
						pw.print('|');
					}
					pw.print('+');
					for (int i = 0; i < b - 1; i++) {
						pw.print('|');
					}
					pw.print('=');
					for (int i = 0; i < c + 1; i++) {
						pw.print('|');
					}
				} else if (a > 1) {
					for (int i = 0; i < a - 1; i++) {
						pw.print('|');
					}
					pw.print('+');
					for (int i = 0; i < b; i++) {
						pw.print('|');
					}
					pw.print('=');
					for (int i = 0; i < c + 1; i++) {
						pw.print('|');
					}
				} else {
					pw.print("Impossible");
				}
			} else {
				pw.print("Impossible");
			}
			pw.println();

			pw.flush();
			sc.close();
		}
	}

}