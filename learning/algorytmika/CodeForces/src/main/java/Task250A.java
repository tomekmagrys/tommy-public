import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task250A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> retVal = new ArrayList<>();

			int all = 0;
			int negative = 0;

			for (int i = 0; i < n; i++) {
				int ai = s.nextInt();
				if (ai >= 0) {
					all++;
				} else {
					if (negative == 2) {
						retVal.add(all);
						negative = 1;
						all = 1;
					} else {
						negative++;
						all++;
					}
				}
			}
			retVal.add(all);

			pw.println(retVal.size());
			for (int i : retVal) {
				pw.print(i);
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}