import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task253B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> c = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				c.add(s.nextInt());
			}

			Collections.sort(c);

			int j = 0;

			int retVal = n;

			for (int i = 0; i < n; i++) {
				while (j + 1 < n && c.get(j + 1) <= 2 * c.get(i)) {
					j++;
				}
				retVal = Math.min(retVal, n - (j - i + 1));
			}

			pw.print(retVal);

			pw.flush();
			s.close();
		}
	}

}