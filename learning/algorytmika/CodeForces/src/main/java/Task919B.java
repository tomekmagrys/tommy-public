import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task919B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            List<Integer> perfects = new ArrayList<>();

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    if (i1 + i2 == 10) {
                        perfects.add(10 * i1 + i2);
                    }
                }
            }


            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        if (i1 + i2 + i3 == 10) {
                            perfects.add(100 * i1 + 10 * i2 + i3);
                        }
                    }
                }
            }

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        for (int i4 = 0; i4 <= 9; i4++) {
                            if (i1 + i2 + i3 + i4 == 10) {
                                perfects.add(1000 * i1 + 100 * i2 + 10 * i3 + i4);
                            }
                        }
                    }
                }
            }

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        for (int i4 = 0; i4 <= 9; i4++) {
                            for (int i5 = 0; i5 <= 9; i5++) {
                                if (i1 + i2 + i3 + i4 + i5 == 10) {
                                    perfects.add(10000 * i1 + 1000 * i2 + 100 * i3 + 10 * i4 + i5);
                                }
                            }
                        }
                    }
                }
            }

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        for (int i4 = 0; i4 <= 9; i4++) {
                            for (int i5 = 0; i5 <= 9; i5++) {
                                for (int i6 = 0; i6 <= 9; i6++) {
                                    if (i1 + i2 + i3 + i4 + i5 + i6 == 10) {
                                        perfects.add(100000 * i1 + 10000 * i2 + 1000 * i3 + 100 * i4 + 10 * i5 + i6);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        for (int i4 = 0; i4 <= 9; i4++) {
                            for (int i5 = 0; i5 <= 9; i5++) {
                                for (int i6 = 0; i6 <= 9; i6++) {
                                    for (int i7 = 0; i7 <= 9; i7++) {
                                        if (i1 + i2 + i3 + i4 + i5 + i6 + i7 == 10) {
                                            perfects.add(1000000 * i1 + 100000 * i2 + 10000 * i3 + 1000 * i4 + 100 * i5 + 10 * i6 + i7);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int i1 = 1; i1 <= 9; i1++) {
                for (int i2 = 0; i2 <= 9; i2++) {
                    for (int i3 = 0; i3 <= 9; i3++) {
                        for (int i4 = 0; i4 <= 9; i4++) {
                            for (int i5 = 0; i5 <= 9; i5++) {
                                for (int i6 = 0; i6 <= 9; i6++) {
                                    for (int i7 = 0; i7 <= 9; i7++) {
                                        for (int i8 = 0; i8 <= 9; i8++) {
                                            if (i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 == 10) {
                                                perfects.add(
                                                        10000000 * i1 +
                                                                1000000 * i2 +
                                                                100000 * i3 +
                                                                10000 * i4 +
                                                                1000 * i5 + 100 * i6 + 10 * i7 + i8);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int k = sc.nextInt();

            pw.println(perfects.get(k - 1));

            pw.flush();
            sc.close();
        }
    }

}