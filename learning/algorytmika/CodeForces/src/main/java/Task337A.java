import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task337A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int n = s.nextInt();
			int m = s.nextInt();
			ArrayList<Integer> f = new ArrayList<>(m);
			for(int i = 0 ; i < m ; i++){
				f.add(s.nextInt());
			}
			Collections.sort(f);
			int retVal = Integer.MAX_VALUE;
			for(int i = n-1 ; i < m ; i++){
				retVal = Math.min(retVal, f.get(i) - f.get(i-n+1));
			}
			pw.write(retVal + "\n");
			pw.flush();
			s.close();
		}
	}

}
