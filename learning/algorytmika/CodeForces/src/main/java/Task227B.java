import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task227B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int permutation[] = new int[n + 1];

			for (int i = 1; i <= n; i++) {
				permutation[s.nextInt()] = i;
			}

			int m = s.nextInt();

			long vasya = 0;
			long petya = 0;

			while (m-- > 0) {
				int query = s.nextInt();
				vasya += permutation[query];
				petya += n+1 - permutation[query];
			}

			pw.println(vasya + " " + petya);
			
			pw.flush();
			s.close();
		}
	}

}