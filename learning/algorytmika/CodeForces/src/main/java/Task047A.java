import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task047A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			for (int i = 1;; i++) {
				if (i * (i + 1) / 2 == n) {
					pw.println("YES");
					break;
				}
				if (i * (i + 1) / 2 > n) {
					pw.println("NO");
					break;
				}
			}

			pw.flush();
			s.close();
		}
	}

}