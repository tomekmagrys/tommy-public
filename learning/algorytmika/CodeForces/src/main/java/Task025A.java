import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task025A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int iloscParzystych = 0;
			int iloscNieparzystych = 0;
			
			Integer parzysta = null;
			Integer nieparzysta = null;
			
			for(int i = 0 ; i < n ; i++){
				int tmp = s.nextInt();
				if(tmp % 2 == 0){
					iloscParzystych++;
					parzysta = i+1;
				} else {
					iloscNieparzystych++;
					nieparzysta = i+1;
				}
			}
			if(iloscParzystych == 1){
				pw.write(parzysta.toString());
			}
			if(iloscNieparzystych == 1){
				pw.write(nieparzysta.toString());
			}
			pw.flush();
			s.close();
		}
	}

}
