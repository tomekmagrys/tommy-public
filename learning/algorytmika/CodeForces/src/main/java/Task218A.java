import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task218A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static int numberOfLines(int v, int k) {
			int retVal = 0;
			while (v > 0) {
				retVal += v;
				v /= k;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(2 * n + 1);
			for (int i = 0; i <= 2 * n; i++) {
				a.add(s.nextInt());
			}

			for (int i = 0, changed = 0; i < n && changed < k; i++) {
				int peakIndex = 2 * i + 1;
				if (a.get(peakIndex) - 1 > a.get(peakIndex - 1)
						&& a.get(peakIndex) - 1 > a.get(peakIndex + 1)) {
					a.set(peakIndex, a.get(peakIndex) - 1);
					changed++;
				}
			}

			for (int i = 0; i <= 2 * n; i++) {
				pw.print(a.get(i));
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}