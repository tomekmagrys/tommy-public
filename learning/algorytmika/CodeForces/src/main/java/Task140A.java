import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task140A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			double n = sc.nextInt();
			double R = sc.nextInt();
			double r = sc.nextInt();

			if (n == 1) {
				pw.println(r <= R ? "YES" : "NO");
			}
			if (n == 2) {
				pw.println(2 * r <= R ? "YES" : "NO");
			}
			if (n >= 3) {
				double RWeNeed = r / (Math.sin(Math.PI / n)) + r;
				if(RWeNeed <= R){
					pw.println("YES");
				} else {
					pw.println("NO");
				}
			}
			pw.flush();
			sc.close();
		}
	}

}