import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Task043A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			Map<String, Integer> goals = new HashMap<>();

			for (int i = 0; i < n; i++) {
				String tmp = s.next();
				if(goals.containsKey(tmp)){
					goals.put(tmp, goals.get(tmp)+1);
				} else {
					goals.put(tmp, 1);
				}
			}

			if(goals.size() == 1){
				pw.write(goals.keySet().iterator().next());
			} else {
				Iterator<String> teams = goals.keySet().iterator();
				String team1 = teams.next();
				String team2 = teams.next();
				if(goals.get(team1) > goals.get(team2)){
					pw.write(team1);
				} else {
					pw.write(team2);
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}
