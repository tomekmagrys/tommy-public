import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task401B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int x = sc.nextInt();
			int k = sc.nextInt();

			boolean tookPart[] = new boolean[x];

			for (int i = 0; i < k; i++) {
				switch (sc.nextInt()) {
				case 1:
					tookPart[sc.nextInt()] = true;
					tookPart[sc.nextInt()] = true;
					break;
				case 2:
					tookPart[sc.nextInt()] = true;
					break;
				default:
					throw new UnsupportedOperationException();
				}
			}

			int minRetVal = 0;
			int maxRetVal = 0;
			
			for(int i = 1 ; i < x ; i++){
				if(tookPart[i] == false){
					if(i + 1 < x && tookPart[i+1] == false){
						minRetVal += 1;
						maxRetVal += 2;
						tookPart[i] = true;
						tookPart[i+1] = true;
					} else {
						minRetVal += 1;
						maxRetVal += 1;
						tookPart[i] = true;					
					}
				}
			}
			
			pw.print(minRetVal);
			pw.print(" ");
			pw.print(maxRetVal);
			
			pw.flush();
			sc.close();
		}
	}

}