import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task369A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			int k = s.nextInt();

			int needM = 0;
			int needMorK = 0;

			for (int i = 0; i < n; i++) {
				if (s.nextInt() == 1) {
					needM++;
				} else {
					needMorK++;
				}
			}

			if (needM <= m) {
				if (needM + needMorK <= m + k) {
					pw.write("0");
				} else {
					pw.write((needM + needMorK - m - k) + "");
				}
			} else {
				if (needMorK <= k) {
					pw.write(needM - m + "");
				} else {
					pw.write(needM - m + needMorK - k + "");
				}

			}

			pw.flush();
			s.close();
		}
	}

}