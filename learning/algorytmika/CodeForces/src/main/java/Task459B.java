import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Task459B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            ArrayList<Integer> flowers = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                flowers.add(sc.nextInt());
            }

            Collections.sort(flowers);

            int min = Collections.min(flowers);
            int max = Collections.max(flowers);

            int minCount = 0;
            int maxCount = 0;

            for (Integer flower : flowers) {
                if (flower == min) {
                    minCount++;
                }
                if (flower == max) {
                    maxCount++;
                }
            }

            pw.print(max - min);
            pw.print(" ");
            if (min < max) {
                long retVal = 1;
                retVal *= minCount;
                retVal *= maxCount;
                pw.print(retVal);
            } else {
                long retVal = 1;
                retVal *= n;
                retVal *= n - 1;
                retVal /= 2;
                pw.print(retVal);
            }

            pw.flush();
            sc.close();
        }
    }

}