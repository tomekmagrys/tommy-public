import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task228B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int na = s.nextInt();
			int ma = s.nextInt();

			int a[][] = new int[na][];

			for (int i = 0; i < na; i++) {
				a[i] = new int[ma];
				char[] inp = s.next().toCharArray();
				for (int j = 0; j < ma; j++) {
					a[i][j] = inp[j] - '0';
				}
			}

			int nb = s.nextInt();
			int mb = s.nextInt();

			int b[][] = new int[nb][];

			for (int i = 0; i < nb; i++) {
				b[i] = new int[mb];
				char[] inp = s.next().toCharArray();
				for (int j = 0; j < mb; j++) {
					b[i][j] = inp[j] - '0';
				}
			}

			int retX = -na;
			int retY = -nb;
			int max = 0;

			for (int x = -na; x < nb; x++) {
				for (int y = -ma; y < mb; y++) {
					int cand = 0;
					for (int i = Math.max(0, -x); i < na && i + x < nb; i++) {
						for (int j = Math.max(0, -y); j < ma && j + y < mb; j++) {
							cand += a[i][j] * b[i + x][j + y];
						}
					}
					if (cand > max) {
						retX = x;
						retY = y;
						max = cand;
					}
				}
			}
			pw.print(retX);
			pw.print(" ");
			pw.println(retY);
			pw.flush();
			s.close();
		}
	}

}