import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task368A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int d = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int m = s.nextInt();

			int retVal = 0;
			Collections.sort(a);

			for (int i = 0; i < Math.min(m, n); i++) {
				retVal += a.get(i);
			}

			if (m > n) {
				retVal -= d * (m - n);
			}

			pw.write("" + retVal);

			pw.flush();
			s.close();
		}
	}

}