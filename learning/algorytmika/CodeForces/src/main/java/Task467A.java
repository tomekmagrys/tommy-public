import java.io.*;
import java.util.ArrayList;

public class Task467A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        private static int bits(int a) {
            int retVal = 0;

            while (a > 0) {
                if (a % 2 == 1) {
                    retVal++;
                }
                a = a / 2;
            }

            return retVal;
        }

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();
            int k = sc.nextInt();

            ArrayList<Integer> x = new ArrayList<>(m);

            for (int i = 0; i < m; i++) {
                x.add(sc.nextInt());
            }

            int fedor = sc.nextInt();

            int retVal = 0;

            for (int xi : x) {
                if (bits(xi ^ fedor) <= k) {
                    retVal++;
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}