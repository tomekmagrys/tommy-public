import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Task254A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}
	
	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			Map<Integer, ArrayList<Integer>> cards = new HashMap<>();

			for (int i = 1; i <= 2 * n; i++) {
				int card = sc.nextInt();
				if (!cards.containsKey(card)) {
					cards.put(card, new ArrayList<Integer>());
				}
				cards.get(card).add(i);
			}

			boolean skip = false;
			for (Integer card : cards.keySet()) {
				if (cards.get(card).size() % 2 != 0) {
					pw.println("-1");
					skip = true;
					break;
				}
			}

			if (!skip) {
				for (Integer card : cards.keySet()) {
					ArrayList<Integer> tmp = cards.get(card);
					for (int i = 0; i < tmp.size() / 2; i++) {
						pw.print(tmp.get(2 * i) + " " + tmp.get(2 * i + 1)
								+ "\n");
					}
				}
			}

			pw.flush();
			sc.close();
		}
	}

}