import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task353A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int nieparzystaZLewej = 0;
			int nieparzystaZPrawej = 0;
			int obieNieparzyste = 0;

			for (int i = 0; i < n; i++) {
				if (s.nextInt() % 2 == 0) {
					if (s.nextInt() % 2 == 1) {
						nieparzystaZPrawej++;
					}
				} else {
					if (s.nextInt() % 2 == 0) {
						nieparzystaZLewej++;
					} else {
						obieNieparzyste++;
					}

				}
			}

			if (obieNieparzyste % 2 == 0) {
				if (nieparzystaZLewej % 2 == 0) {
					if (nieparzystaZPrawej % 2 == 0) {
						pw.write("0");
					} else {
						pw.write("-1");
					}
				} else {
					if (nieparzystaZPrawej % 2 == 0) {
						pw.write("-1");
					} else {
						pw.write("1");
					}
				}
			} else {
				if (nieparzystaZLewej % 2 == 0) {
					if (nieparzystaZPrawej % 2 == 0) {
						if (nieparzystaZLewej > 0 || nieparzystaZPrawej > 0) {
							pw.write("1");
						} else {
							pw.write("-1");
						}
					} else {
						pw.write("-1");
					}
				} else {
					if (nieparzystaZPrawej % 2 == 0) {
						pw.write("-1");
					} else {
						pw.write("0");
					}
				}
			}
			pw.flush();
			s.close();
		}
	}

}
