import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task361A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();
			
			for(int i = 0 ; i < n ; i++){
				for(int j = 0 ; j < n ; j++){
					pw.write((i == j ? k : 0)+" ");
				}
				pw.write("\n");
			}

			pw.flush();
			s.close();
		}
	}

}