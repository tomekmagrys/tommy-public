import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task027A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			Set<Integer> indexes = new TreeSet<>();
			for (int i = 0; i < n; i++) {
				indexes.add(s.nextInt());
			}

			for (int i = 1;; i++) {
				if (!indexes.contains(i)) {
					pw.println(i);
					break;
				}
			}

			pw.flush();
			s.close();
		}
	}

}
