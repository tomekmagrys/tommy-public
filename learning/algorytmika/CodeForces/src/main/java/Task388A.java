import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task388A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            ArrayList<ArrayList<Integer>> retVal = new ArrayList<>();
            ArrayList<Integer> boxes = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                boxes.add(sc.nextInt());
            }

            Collections.sort(boxes);

            for (int i = 0; i < n; i++) {
                boolean added = false;
                for (ArrayList<Integer> aRetVal : retVal) {
                    if (aRetVal.size() <= boxes.get(i)) {
                        aRetVal.add(boxes.get(i));
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    ArrayList<Integer> tmp = new ArrayList<>();
                    tmp.add(boxes.get(i));
                    retVal.add(tmp);
                }
            }

            pw.println(retVal.size());

            pw.flush();
            sc.close();
        }
    }

}