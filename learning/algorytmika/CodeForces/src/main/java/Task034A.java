import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task034A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> soldiers = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				soldiers.add(s.nextInt());
			}

			int min = Math.abs(soldiers.get(n - 1) - soldiers.get(0));
			int minIndex = n - 1;

			for (int i = 1; i < n; i++) {
				if (Math.abs(soldiers.get(i) - soldiers.get(i - 1)) < min) {
					min = Math.abs(soldiers.get(i) - soldiers.get(i - 1));
					minIndex = i - 1;
				}
			}

			int retVal1 = minIndex + 1;
			int retVal2 = (minIndex + 1) % n + 1;

			pw.write(retVal1 + " " + retVal2);

			pw.flush();
			s.close();
		}
	}

}
