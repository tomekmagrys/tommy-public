import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task203C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class PairLI extends Pair<Long, Integer> {
		public PairLI(Long x, Integer y) {
			super(x, y);
		}
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			long d = sc.nextInt();

			long a = sc.nextInt();
			long b = sc.nextInt();

			ArrayList<PairLI> xy = new ArrayList<PairLI>(n);

			for (int i = 1; i <= n; i++) {
				xy.add(new PairLI(a * sc.nextInt() + b * sc.nextInt(), i));
			}

			Collections.sort(xy, new Comparator<PairLI>() {

				@Override
				public int compare(PairLI o1, PairLI o2) {
					return o1.x.compareTo(o2.x);
				}
			});

			int sum = 0;
			int retVal = 0;

			while (retVal < n && sum + xy.get(retVal).x <= d) {
				sum += xy.get(retVal).x;
				retVal++;
			}

			pw.println(retVal);

			for (int i = 0; i < retVal; i++) {
				pw.print(xy.get(i).y);
				pw.print(" ");
			}

			pw.flush();
			sc.close();
		}
	}

}