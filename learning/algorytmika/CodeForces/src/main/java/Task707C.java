import java.io.*;

public class Task707C {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long n = sc.nextLong();

            if (n < 3) {
                pw.println(-1);
            } else {

                int multiplier = 1;

                while (n % 2 == 0) {
                    n /= 2;
                    multiplier *= 2;
                }

                if (n == 1) {
                    pw.print(multiplier / 4 * 3);
                    pw.print(" ");
                    pw.print(multiplier / 4 * 5);

                } else {
                    pw.print(multiplier * (n * n / 2));
                    pw.print(" ");
                    pw.print(multiplier * (n * n / 2 + 1));
                }
            }
            pw.flush();
            sc.close();
        }
    }

}