import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Task215B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			ArrayList<Integer> x = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				x.add(sc.nextInt());
			}

			int m = sc.nextInt();
			ArrayList<Integer> y = new ArrayList<>(n);
			for (int i = 0; i < m; i++) {
				y.add(sc.nextInt());
			}

			int k = sc.nextInt();
			ArrayList<Integer> z = new ArrayList<>(n);
			for (int i = 0; i < k; i++) {
				z.add(sc.nextInt());
			}

			double A = sc.nextInt();
			double B = sc.nextInt();

			double r1 = Collections.max(x);
			double p1 = Collections.max(y);

			double p2 = Collections.min(z);

			double r2 = r1 / Math.sqrt(A / B * p2 / p1 + 1);

			pw.println(r2);

			pw.flush();
			sc.close();
		}
	}

}