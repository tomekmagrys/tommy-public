import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task339A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			List<String> lines = Arrays.asList(s.next().split("[+]"));
			Collections.sort(lines);
			boolean printPlus = false;
			for(String line:lines){
				if(printPlus) {
					pw.write("+");
				}
				pw.print(line);
				printPlus = true;
			}
			pw.print("\n");
			pw.flush();
			s.close();
		}
	}

}
