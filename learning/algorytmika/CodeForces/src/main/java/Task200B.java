import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class Task200B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			double suma = 0;
			for (int i = 0; i < n; i++) {
				suma += sc.nextInt();
			}

			BigDecimal retVal = new BigDecimal(suma / n);

			os.write((retVal.setScale(12, RoundingMode.HALF_UP).toPlainString() + System
					.lineSeparator()).getBytes());
			sc.close();
		}
	}

}
