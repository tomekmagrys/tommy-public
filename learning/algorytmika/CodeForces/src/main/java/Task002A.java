import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Task002A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1,T2>{
		T1 x;
		T2 y;
		
		public Pair(T1 x,T2 y){
			this.x = x;
			this.y = y;
		}
	}
	
	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			HashMap<String, Integer> points = new HashMap<>();
			ArrayList<Pair<String,Integer>> input = new ArrayList<>();
			
			for(int i = 0 ; i < n ; i++){
				input.add(new Pair<String,Integer>(s.next(),s.nextInt()));
			}
			for (int i = 0; i < n; i++) {
				String player = input.get(i).x;
				Integer score = input.get(i).y;

				if (!points.containsKey(player)) {
					points.put(player, 0);
				}
				input.get(i).y += points.get(player);
				points.put(player, points.get(player) + score);
			}

			int maxPoints = Integer.MIN_VALUE;
			
			for(Integer score:points.values()){
				maxPoints = Math.max(maxPoints,score);
			}
			
			
			for (int i = 0; i < n; i++) {
				String player = input.get(i).x;
				if(points.get(player) == maxPoints && input.get(i).y >= maxPoints){
					pw.write(player);
					break;
				}
			}

			pw.flush();
			s.close();
		}
	}

}