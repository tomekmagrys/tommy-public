import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task258A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();

			int charToRemoveIndex = input.indexOf("0");

			if (charToRemoveIndex >= 0) {
				pw.println(input.substring(0, charToRemoveIndex)
						+ input.substring(charToRemoveIndex + 1));
			} else {
				pw.println(input.substring(1));
			}

			pw.flush();
			s.close();
		}
	}

}