import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task384A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			pw.println((n * n + 1) / 2);
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if ((i + j) % 2 == 0) {
						pw.print('C');
					} else {
						pw.print('.');
					}
				}
				pw.println();
			}
			pw.flush();
			s.close();
		}
	}

}