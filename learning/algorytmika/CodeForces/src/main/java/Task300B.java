import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Task300B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			ArrayList<Integer> team3 = new ArrayList<>();
			ArrayList<Integer> team2 = new ArrayList<>();

			ArrayList<Set<Integer>> preferences = new ArrayList<>();
			preferences.add(null);
			for (int i = 1; i <= n; i++) {
				preferences.add(new TreeSet<Integer>());
			}

			for (int i = 0; i < m; i++) {
				int a = sc.nextInt();
				int b = sc.nextInt();
				preferences.get(a).add(b);
				preferences.get(b).add(a);
			}

			ArrayList<Integer> left = new ArrayList<>();
			for (int i = 1; i <= n; i++) {
				left.add(i);
			}

			for (int i = 1; i <= n; i++) {
				if (preferences.get(i).size() > 2) {
					pw.println("-1");
					pw.flush();
					sc.close();
					return;
				}
			}

			for (int i = 1; i <= n; i++) {
				if (left.contains(i) && preferences.get(i).size() == 2) {
					Iterator<Integer> it = preferences.get(i).iterator();
					int a = it.next();
					int b = it.next();

					for (int j : preferences.get(a)) {
						if (j != i && j != b) {
							pw.println("-1");
							pw.flush();
							sc.close();
							return;
						}
					}

					for (int j : preferences.get(b)) {
						if (j != a && j != i) {
							pw.println("-1");
							pw.flush();
							sc.close();
							return;
						}
					}

					team3.add(i);
					team3.add(a);
					team3.add(b);

					left.remove(Integer.valueOf(i));
					left.remove(Integer.valueOf(a));
					left.remove(Integer.valueOf(b));
				}
			}

			for (int i = 1; i <= n; i++) {
				if (left.contains(i) && preferences.get(i).size() == 1) {
					int a = preferences.get(i).iterator().next();

					if (preferences.get(a).size() > 0
							&& preferences.get(a).iterator().next() != i) {
						pw.println("-1");
						pw.flush();
						sc.close();
						return;
					}
					team2.add(i);
					team2.add(a);
					left.remove(Integer.valueOf(i));
					left.remove(Integer.valueOf(a));
				}
			}

			if (team2.size() / 2 > left.size()) {
				pw.println("-1");
				pw.flush();
				sc.close();
				return;
			}

			while (!team3.isEmpty()) {
				pw.print(team3.remove(0));
				pw.print(" ");
				pw.print(team3.remove(0));
				pw.print(" ");
				pw.println(team3.remove(0));
			}

			while (!team2.isEmpty()) {
				pw.print(team2.remove(0));
				pw.print(" ");
				pw.print(team2.remove(0));
				pw.print(" ");
				pw.println(left.remove(0));
			}

			while (!left.isEmpty()) {
				pw.print(left.remove(0));
				pw.print(" ");
				pw.print(left.remove(0));
				pw.print(" ");
				pw.println(left.remove(0));
			}

			pw.flush();
			sc.close();
		}
	}

}