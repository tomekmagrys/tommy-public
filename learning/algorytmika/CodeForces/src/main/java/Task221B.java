import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task221B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int retVal = 0;

			Set<Character> digitsOfX = new TreeSet<>();
			for (char c : Integer.valueOf(x).toString().toCharArray()) {
				digitsOfX.add(c);
			}

			for (int i = 1; i * i <= x; i++) {
				if (x % i == 0) {
					for (char c : Integer.valueOf(i).toString().toCharArray()) {
						if (digitsOfX.contains(c)) {
							retVal++;
							break;
						}
					}
					int i2 = x / i;
					if (i2 != i) {
						for (char c : Integer.valueOf(i2).toString()
								.toCharArray()) {
							if (digitsOfX.contains(c)) {
								retVal++;
								break;
							}
						}
					}
				}
			}

			pw.println(retVal);
			pw.flush();
			s.close();
		}
	}

}