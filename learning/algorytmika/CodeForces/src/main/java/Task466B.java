import java.io.*;

public class Task466B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long n = sc.nextLong() * 6;
            long a = sc.nextLong();
            long b = sc.nextLong();

            boolean swap = false;
            if(a > b){
                long tmp =a;
                a = b;
                b = tmp;
                swap = true;
            }


            long s = Long.MAX_VALUE;
            long a1 = -1;
            long b1 = -1;

            for (long i = 1; i * i <= n; i++) {
                long aCand = i;
                long bCand = (n - 1) / aCand + 1;

                aCand = Math.max(aCand,a);
                bCand = Math.max(bCand,b);

                if(aCand * bCand < s){
                    s = aCand * bCand;
                    a1 = aCand;
                    b1 = bCand;
                }

            }

            if(swap){
                long tmp = a1;
                a1 = b1;
                b1 = tmp;
            }

            pw.println(s);
            pw.print(a1);
            pw.print(" ");
            pw.print(b1);
            pw.flush();
            sc.close();
        }
    }

}