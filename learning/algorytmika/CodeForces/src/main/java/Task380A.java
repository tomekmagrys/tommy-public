import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task380A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int m = sc.nextInt();

			long retVal[][] = new long[m][];
			for (int i = 0; i < m; i++) {
				retVal[i] = new long[5];
			}
			int curRetValIndex = 0;
			long curRetValLenght = 0;

			while (m-- > 0) {
				switch (sc.nextInt()) {
				case 1:
					long xi = sc.nextInt();
					retVal[curRetValIndex][0] = curRetValLenght;
					retVal[curRetValIndex][1] = curRetValLenght + 1;
					retVal[curRetValIndex][2] = 1;
					retVal[curRetValIndex][3] = xi;
					curRetValIndex++;
					curRetValLenght++;
					break;
				case 2:
					long li = sc.nextInt();
					long ci = sc.nextInt();
					retVal[curRetValIndex][0] = curRetValLenght;
					retVal[curRetValIndex][1] = curRetValLenght + li * ci;
					retVal[curRetValIndex][2] = 2;
					retVal[curRetValIndex][3] = curRetValLenght;
					retVal[curRetValIndex][4] = li;
					curRetValIndex++;
					curRetValLenght = curRetValLenght + li * ci;
					break;
				default:
					throw new RuntimeException();
				}
			}

			long n = sc.nextInt();
			while (n-- > 0) {
				long index = sc.nextLong() - 1;
				boolean found = false;
				while (!found) {
					int l = 0;
					int p = curRetValIndex;

					boolean found2 = false;

					while (!found2) {
						int mid = (l + p) / 2;
						if (retVal[mid][0] > index) {
							p = mid;
							continue;
						}
						if (retVal[mid][1] <= index) {
							l = mid;
							continue;
						}
						found2 = true;
						if (retVal[mid][2] == 1) {
							pw.print(retVal[mid][3]);
							pw.print(" ");
							found = true;
						} else {
							index = (index - retVal[mid][3]) % retVal[mid][4];
						}
					}
				}

			}

			pw.println();

			pw.flush();
			sc.close();
		}
	}

}