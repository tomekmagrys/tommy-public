import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task230B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static boolean isPrime(long n) {
			if (n < 2) {
				return false;
			}
			for (long i = 2; i * i <= n; i++) {
				if (n % i == 0) {
					return false;
				}
			}
			return true;
		}

		private static boolean isTPrime(long n) {
			long onlyCandidate = (long) Math.sqrt(n);
			if (onlyCandidate * onlyCandidate != n)
				return false;
			return isPrime(onlyCandidate);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			for (int i = 0; i < n; i++) {
				pw.println(isTPrime(s.nextLong()) ? "YES" : "NO");
			}

			pw.flush();
			s.close();
		}
	}

}
