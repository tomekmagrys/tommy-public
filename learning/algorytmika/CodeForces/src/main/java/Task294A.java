import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task294A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			int[] a = new int[n];

			for (int i = 0; i < a.length; i++) {
				a[i] = sc.nextInt();
			}

			int m = sc.nextInt();

			for (int i = 0; i < m; i++) {
				int xi = sc.nextInt() - 1;
				int yi = sc.nextInt();

				if (xi > 0) {
					a[xi - 1] += yi - 1;
				}
				if (xi + 1 < n) {
					a[xi + 1] += a[xi] - yi;
				}
				a[xi] = 0;
			}
			for (int i = 0; i < n; i++) {
				os.write((a[i] + System.lineSeparator()).getBytes());
			}
			sc.close();
		}
	}
}
