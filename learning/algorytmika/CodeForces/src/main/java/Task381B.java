import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Task381B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			List<Integer> a = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			Collections.sort(a);

			Iterator<Integer> iter = a.iterator();
			ArrayList<Integer> output = new ArrayList<>();
			Integer lastAdd = iter.next();
			output.add(lastAdd);
			iter.remove();

			while (iter.hasNext()) {
				Integer tmp = iter.next();
				if (!tmp.equals(lastAdd)) {
					lastAdd = tmp;
					output.add(lastAdd);
					iter.remove();
				}
			}

			Collections.reverse(a);

			iter = a.iterator();
			while (iter.hasNext()) {
				Integer tmp = iter.next();
				if (!tmp.equals(lastAdd)) {
					lastAdd = tmp;
					output.add(lastAdd);
					iter.remove();
				}
			}

			pw.println(output.size());
			for (int i : output) {
				pw.print(i);
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}