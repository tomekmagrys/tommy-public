import java.io.*;
import java.util.*;
import java.util.List;

public class Task460B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static int sumOfDigits(long n){
            int retVal = 0;
            while (n > 0){
                retVal += n % 10;
                n /= 10;
            }
            return retVal;

        }

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();

            List<Long> retVal = new ArrayList<Long>();

            for(int i = 1 ; i <= 81 ; i++){
                long x = b * (long)Math.pow(i,a) + c;
                if(x >= 1000000000){
                    break;
                }
                if(x == b * (int)Math.pow(sumOfDigits(x),a) +c){
                    retVal.add(x);
                }
            }

            pw.println(retVal.size());
            for(long l : retVal){
                pw.print(l);
                pw.print(" ");
            }

            pw.flush();
            sc.close();
        }
    }

}