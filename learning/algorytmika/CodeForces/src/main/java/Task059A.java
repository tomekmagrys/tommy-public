import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task059A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);
			PrintWriter pw = new PrintWriter(os);

			char input[] = sc.nextLine().toCharArray();

			int lowerCaseCount = 0;

			for (char c : input) {
				if (Character.isLowerCase(c))
					lowerCaseCount++;
			}

			if (2 * lowerCaseCount >= input.length) {
				pw.println(new String(input).toLowerCase());
			} else {
				pw.println(new String(input).toUpperCase());
			}
			pw.flush();
			sc.close();
		}
	}

}
