import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task298B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			sc.nextInt();
			int sx = sc.nextInt();
			int sy = sc.nextInt();
			int ex = sc.nextInt();
			int ey = sc.nextInt();

			int s = 0;
			int n = 0;
			int w = 0;
			int e = 0;

			String input = sc.next();

			for (char c : input.toCharArray()) {
				switch (c) {
				case 'N':
					n++;
					break;
				case 'S':
					s++;
					break;
				case 'E':
					e++;
					break;
				case 'W':
					w++;
					break;
				}
			}

			int retVal = -1;

			if (ex <= sx + e && ex >= sx - w && ey <= sy + n && ey >= sy - s) {
				retVal = 0;
				while(sx != ex || sy != ey){					
					switch(input.charAt(retVal)){
					case 'N':
						if(sy < ey){
							sy++;
						}
						break;
					case 'S':
						if(sy > ey){
							sy--;
						}
						break;
					case 'E':
						if(sx < ex){
							sx++;
						}
						break;
					case 'W':
						if(sx > ex){
							sx--;
						}
						break;
					}
					retVal++;
				}
			}

			pw.write("" + retVal);

			pw.flush();
			sc.close();
		}
	}

}