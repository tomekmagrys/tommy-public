import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task355B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int c1 = s.nextInt();
			int c2 = s.nextInt();
			int c3 = s.nextInt();
			int c4 = s.nextInt();

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			ArrayList<Integer> b = new ArrayList<>(m);
			for (int i = 0; i < m; i++) {
				b.add(s.nextInt());
			}

			int min1 = 0;
			for (int i = 0; i < n; i++) {
				if (a.get(i) != 0) {
					min1 += Math.min(a.get(i) * c1, c2);
				}
			}

			int min2 = 0;
			for (int i = 0; i < m; i++) {
				if (b.get(i) != 0) {
					min2 += Math.min(b.get(i) * c1, c2);
				}
			}

			int min3 = Math.min(c3, min1);
			int min4 = Math.min(c3, min2);

			pw.println(Math.min(min3 + min4, c4));

			pw.flush();
			s.close();
		}
	}

}