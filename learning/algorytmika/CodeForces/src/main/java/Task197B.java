import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task197B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		private static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			ArrayList<Integer> p = new ArrayList<Integer>(n + 1);
			ArrayList<Integer> q = new ArrayList<Integer>(m + 1);

			for (int i = 0; i <= n; i++) {
				p.add(sc.nextInt());
			}
			for (int i = 0; i <= m; i++) {
				q.add(sc.nextInt());
			}

			if (p.size() > q.size()) {
				if (p.get(0) * q.get(0) > 0) {
					pw.println("Infinity");
				} else {
					pw.println("-Infinity");
				}
			}
			if (p.size() == q.size()) {
				int _gcd = gcd(Math.abs(p.get(0)), Math.abs(q.get(0)));
				if (p.get(0) * q.get(0) < 0) {
					pw.print("-");
				}
				pw.print(Math.abs(p.get(0)) / _gcd);
				pw.print("/");
				pw.println(Math.abs(q.get(0)) / _gcd);
			}
			if (p.size() < q.size()) {
				pw.println("0/1");
			}
			pw.flush();
			sc.close();
		}
	}

}