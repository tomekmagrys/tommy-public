import java.io.*;

public class Task288A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {


        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int k = sc.nextInt();

            if( n  < k || (k == 1 && n > 1)){
                pw.println(-1);
            } else {
                String ending1 = "abcdefghijklmnopqrstuvwxyz";
                String ending2 = "bacdefghijklmnopqrstuvwxyz";
                for(int i = 0 ; i < n-k ; i++){
                    if(i % 2 == 0){
                        pw.print('a');
                    } else {
                        pw.print('b');
                    }
                }
                if((n-k) % 2 == 0){
                    pw.println(ending1.substring(0,k));
                } else {
                    pw.println(ending2.substring(0,k));
                }
            }


            pw.flush();
            sc.close();
        }
    }

}