import java.io.*;

public class Task303A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            if (n % 2 == 0) {
                pw.println(-1);
            } else {
                for (int i = 0; i < n; i++) {
                    pw.print(i);
                    pw.print(" ");
                }
                pw.println();
                for (int i = 0; i < n; i++) {
                    pw.print(i);
                    pw.print(" ");
                }
                pw.println();
                for (int i = 0; i < n; i++) {
                    pw.print(2 * i % n);
                    pw.print(" ");
                }
                pw.println();
            }


            pw.flush();
            sc.close();
        }
    }

}