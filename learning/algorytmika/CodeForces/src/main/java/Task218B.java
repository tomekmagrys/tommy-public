import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task218B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			
			ArrayList<Integer> freeSeats1 = new ArrayList<>();
			ArrayList<Integer> freeSeats2 = new ArrayList<>();
			
			for(int i = 0 ; i < m ; i++){
				int tmp = s.nextInt();
				freeSeats1.add(tmp);
				freeSeats2.add(tmp);
			}
			
			Collections.sort(freeSeats1);
			Collections.sort(freeSeats2);
			
			int min = 0;
			
			for(int i = 0 ; i < n ; i++){
				min+= freeSeats1.get(0);
				freeSeats1.set(0,freeSeats1.get(0)-1);
				if(freeSeats1.get(0) == 0){
					freeSeats1.remove(0);
				}
			}
			
			int max = 0;
			
			for(int i = 0 ; i < n ; i++){
				max+= freeSeats2.get(freeSeats2.size()-1);
				freeSeats2.set(freeSeats2.size()-1,freeSeats2.get(freeSeats2.size()-1)-1);
				Collections.sort(freeSeats2);
				if(freeSeats2.get(0) == 0){
					freeSeats2.remove(0);
				}
			}
			
			pw.write(max + " " + min);
			
			pw.flush();
			s.close();
		}
	}

}
