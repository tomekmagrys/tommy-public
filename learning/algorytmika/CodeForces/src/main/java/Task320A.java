import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task320A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String toParse = s.next();
			while(true){
				//System.out.println(toParse);
				if(toParse.startsWith("144")){
					toParse = toParse.substring(3);
					continue;
				}
				if(toParse.startsWith("14")){
					toParse = toParse.substring(2);
					continue;
				}
				if(toParse.startsWith("1")){
					toParse = toParse.substring(1);
					continue;
				}
				if(toParse.isEmpty()){
					pw.write("YES");
					break;
				}
				pw.write("NO");
				break;
			}
			pw.write("\n");
			pw.flush();
			s.close();
		}
	}



}
