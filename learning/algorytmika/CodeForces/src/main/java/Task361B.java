import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task361B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			if (n == k) {
				pw.print(-1);
				pw.flush();
				s.close();
				return;
			}
			if (n - 1 == k) {
				for (int i = 1; i <= n; i++) {
					pw.print(i);
					pw.print(" ");
				}
				pw.flush();
				s.close();
				return;
			}
			pw.print(n);
			pw.print(" ");

			for (int i = 2; i < k + 2; i++) {
				pw.print(i);
				pw.print(" ");
			}
			pw.print(1);
			pw.print(" ");
			for (int i = 1; i < n - k - 1; i++) {
				pw.print(k + 1 + i);
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}