import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task355A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int k = s.nextInt();
			int d = s.nextInt();

			if (k == 1) {
				pw.write(""+d);
			} else {
				if(d == 0){
					pw.write("No solution");
				} else {
					pw.write("1");
					for(int i = 2 ; i < k ; i++){
						pw.write("0");
					}
					pw.write(""+(d-1));
				}
			}

			pw.flush();
			s.close();
		}
	}

}
