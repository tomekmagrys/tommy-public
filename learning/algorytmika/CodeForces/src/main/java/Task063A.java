import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task063A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Pair<String, String>> crew = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				crew.add(new Pair<String, String>(s.next(), s.next()));
			}

			Collections.sort(crew, new Comparator<Pair<String, String>>() {

				private int priority(String rank) {
					if ("rat".equals(rank)) {
						return 0;
					}
					if ("child".equals(rank)) {
						return 1;
					}
					if ("woman".equals(rank)) {
						return 1;
					}
					if ("man".equals(rank)) {
						return 2;
					}
					if ("captain".equals(rank)) {
						return 3;
					}
					throw new UnsupportedOperationException();
				}

				@Override
				public int compare(Pair<String, String> o1,
						Pair<String, String> o2) {
					return Integer.valueOf(priority(o1.y)).compareTo(
							priority(o2.y));
				}

			});

			for (int i = 0; i < crew.size(); i++) {
				pw.write(crew.get(i).x+"\n");
			}

			pw.flush();
			s.close();
		}
	}

}