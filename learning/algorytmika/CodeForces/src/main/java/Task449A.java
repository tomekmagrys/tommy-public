import java.io.*;

public class Task449A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {


        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long n = sc.nextInt();
            long m = sc.nextInt();
            long k = sc.nextInt();


            if(n-1+m-1 < k)
            {
                pw.println(-1);
                pw.flush();
                sc.close();
                return;
            }

            long ans1,ans2;

            if(n >= k+1 || m >= k+1)
            {
                ans1 = n/(k+1)*m;
                ans2 = m/(k+1)*n;
            }
            else
            {
                ans1 = n / (k - (m - 1) + 1);
                ans2 = m / (k - (n - 1) + 1);
            }
            pw.println(Math.max(ans1, ans2));

            pw.flush();
            sc.close();
        }
    }

}