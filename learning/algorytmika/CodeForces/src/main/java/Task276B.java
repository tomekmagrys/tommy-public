import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task276B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();

			int letterCount[] = new int[26];

			for (char c : input.toCharArray()) {
				letterCount[c - 'a']++;
			}

			int oddCount = 0;

			for (int i = 0; i < 26; i++) {
				if (letterCount[i] % 2 != 0) {
					oddCount++;
				}
			}

			pw.write(oddCount % 2 == 0 && oddCount > 0 ? "Second" : "First");

			pw.flush();
			s.close();
		}
	}

}
