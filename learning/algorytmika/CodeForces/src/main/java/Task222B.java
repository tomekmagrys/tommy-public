import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task222B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}
	
	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();
			int k = sc.nextInt();

			int input[][] = new int[n][];
			for (int i = 0; i < n; i++) {
				input[i] = new int[m];
				for (int j = 0; j < m; j++) {
					input[i][j] = sc.nextInt();
				}
			}

			int rowsSwaps[] = new int[n];
			int columnSwaps[] = new int[m];
			for (int i = 0; i < n; i++) {
				rowsSwaps[i] = i;
			}
			for (int j = 0; j < m; j++) {
				columnSwaps[j] = j;
			}

			while (k-- > 0) {
				char s = sc.next().charAt(0);
				int x = sc.nextInt() - 1;
				int y = sc.nextInt() - 1;
				switch (s) {
				case 'c':
					int tmp = columnSwaps[x];
					columnSwaps[x] = columnSwaps[y];
					columnSwaps[y] = tmp;
					break;
				case 'r':
					tmp = rowsSwaps[x];
					rowsSwaps[x] = rowsSwaps[y];
					rowsSwaps[y] = tmp;
					break;
				case 'g':
					pw.println(input[rowsSwaps[x]][columnSwaps[y]]);
					break;
				default:
					throw new UnsupportedOperationException();
				}

			}

			sc.close();
			pw.flush();
		}
	}

}
