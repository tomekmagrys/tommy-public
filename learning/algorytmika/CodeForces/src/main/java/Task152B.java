import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task152B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			long m = sc.nextInt();
			long n = sc.nextInt();

			long xc = sc.nextInt();
			long yc = sc.nextInt();

			long k = sc.nextInt();

			long retVal = 0;

			for (long i = 0; i < k; i++) {
				long steps = Integer.MAX_VALUE;
				long dx = sc.nextInt();
				long dy = sc.nextInt();

				if (dx > 0) {
					steps = Math.min(steps, (m - xc) / dx);
				}
				if (dx < 0) {
					steps = Math.min(steps, (1 - xc) / dx);
				}
				if (dy > 0) {
					steps = Math.min(steps, (n - yc) / dy);
				}
				if (dy < 0) {
					steps = Math.min(steps, (1 - yc) / dy);
				}

				xc += steps * dx;
				yc += steps * dy;

				retVal += steps;

			}

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}