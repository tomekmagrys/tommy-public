import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task182B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int d = sc.nextInt();
			int n = sc.nextInt();
			int retVal = 0;

			for (int i = 1; i <= n; i++) {
				int ai = sc.nextInt();
				if (i < n) {
					retVal += d - ai;
				}
			}
			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}