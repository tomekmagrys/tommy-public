import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Task152A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			Integer n = s.nextInt();
			Integer m = s.nextInt();
			
			ArrayList<String> marks = new ArrayList<>(n);
			for(int i = 0 ; i < n ; i++){
				marks.add(s.next());
			}
			
			HashSet<Integer> successfull = new HashSet<Integer>();
			
			for(int i = 0 ; i < m ; i++){
				ArrayList<Integer> candidates = new ArrayList<>();
				char curMax = '0';
				for(int j = 0 ; j < n ; j++){
					if(marks.get(j).charAt(i) > curMax){
						candidates.clear();
						curMax = marks.get(j).charAt(i);
					}
					if(marks.get(j).charAt(i) == curMax){
						candidates.add(j);
					}
				}
				successfull.addAll(candidates);
			}
			pw.write("" + successfull.size());
			pw.flush();
			s.close();
		}
	}

}
