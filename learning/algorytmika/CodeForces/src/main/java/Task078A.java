import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task078A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			String firstLine = sc.nextLine();
			String secondLine = sc.nextLine();
			String thirdLine = sc.nextLine();

			int count1 = 0;
			int count2 = 0;
			int count3 = 0;

			for (char c : firstLine.toCharArray()) {
				if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
					count1++;
				}
			}
			for (char c : secondLine.toCharArray()) {
				if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
					count2++;
				}
			}
			for (char c : thirdLine.toCharArray()) {
				if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
					count3++;
				}
			}

			os.write(count1 == 5 && count2 == 7 && count3 == 5 ? "YES"
					.getBytes() : "NO".getBytes());
			sc.close();
		}
	}

}
