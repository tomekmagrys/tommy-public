import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task120A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String door = sc.next();

			if (door.equals("front")) {
				if (sc.nextInt() == 1) {
					pw.println("L");
				} else {
					pw.println("R");
				}
			}
			if (door.equals("back")) {
				if (sc.nextInt() == 1) {
					pw.println("R");
				} else {
					pw.println("L");
				}
			}

			pw.flush();
			sc.close();
		}
	}

}