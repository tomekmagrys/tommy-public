import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task131C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		static long combinations(int n, int k) {
			long retVal = 1;
			for (int i = 0; i < k;) {
				retVal *= n - i;
				i++;
				retVal /= i;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			int t = s.nextInt();

			long retVal = 0;

			for (int boys = 4; boys <= n && t - boys >= 1; boys++) {
				retVal += combinations(n, boys) * combinations(m, t - boys);
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}