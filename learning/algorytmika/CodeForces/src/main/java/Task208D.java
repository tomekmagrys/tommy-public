import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task208D {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> p = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				p.add(s.nextInt());
			}

			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();
			int d = s.nextInt();
			int e = s.nextInt();

			long gotA = 0;
			long gotB = 0;
			long gotC = 0;
			long gotD = 0;
			long gotE = 0;

			long forPrizes = 0;
			for (int i = 0; i < n; i++) {
				forPrizes += p.get(i);

				gotE += forPrizes / e;
				forPrizes %= e;

				gotD += forPrizes / d;
				forPrizes %= d;

				gotC += forPrizes / c;
				forPrizes %= c;

				gotB += forPrizes / b;
				forPrizes %= b;

				gotA += forPrizes / a;
				forPrizes %= a;

			}

			pw.println(gotA + " " + gotB + " " + gotC + " " + gotD + " " + gotE);
			pw.println(forPrizes);

			pw.flush();
			s.close();
		}
	}

}