import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task365A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			int retVal = 0;

			for (int i = 0; i < n; i++) {
				boolean ok = true;
				String tmp = s.next();
				for (int j = 0; j <= k; j++) {
					if (!tmp.contains("" + j)) {
						ok = false;
						break;
					}
				}
				if (ok) {
					retVal++;
				}
			}
			pw.write("" + retVal);
			pw.flush();
			s.close();
		}
	}

}