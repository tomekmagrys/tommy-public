import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task366B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int retSum = Integer.MAX_VALUE;
			int retIndex = -1;

			for (int i = 0; i < k; i++) {
				int sum = 0;
				for (int j = i; j < n; j += k) {
					sum += a.get(j);
				}
				if (sum < retSum) {
					retSum = sum;
					retIndex = i + 1;
				}
			}

			pw.println(retIndex);

			pw.flush();
			s.close();
		}
	}

}