import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task233B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static long s(long x) {
			long retVal = 0;

			while (x > 0) {
				retVal += x % 10;
				x /= 10;
			}

			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			long n = s.nextLong();

			long cand = (long) Math.sqrt(n);

			for (int i = 0; i < 300 && cand > 0; i++) {
				if (cand * cand + s(cand) * cand == n) {
					pw.println(cand);
					pw.flush();
					s.close();
					return;
				}
				cand--;
			}

			pw.println("-1");
			pw.flush();
			s.close();
		}
	}

}