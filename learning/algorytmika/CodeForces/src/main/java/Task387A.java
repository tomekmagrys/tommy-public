import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task387A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String s = sc.next();

			int sh = Integer.parseInt(s.substring(0, 2));
			int sm = Integer.parseInt(s.substring(3));

			String t = sc.next();

			int th = Integer.parseInt(t.substring(0, 2));
			int tm = Integer.parseInt(t.substring(3));

			int sMinutes = 60 * sh + sm;
			int tMinutes = 60 * th + tm;

			int pMinutes = sMinutes - tMinutes;

			if (pMinutes < 0) {
				pMinutes += 24 * 60;
			}

			int ph = pMinutes / 60;
			int pm = pMinutes % 60;

			if (ph < 10) {
				pw.print("0");
			}
			pw.print(ph);
			pw.print(":");
			if (pm < 10) {
				pw.print("0");
			}
			pw.print(pm);
			pw.flush();
			sc.close();
		}
	}

}