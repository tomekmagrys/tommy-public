import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task032A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int d = s.nextInt();

			ArrayList<Integer> a = new ArrayList<Integer>();

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int retVal = 0;

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (i != j) {
						if (Math.abs(a.get(i) - a.get(j)) <= d) {
							++retVal;
						}
					}
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}