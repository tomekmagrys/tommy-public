import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task131B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			long n = s.nextInt();
			long opposites[] = new long[21];

			for(int i = 0 ; i < n ; i++){
				opposites[s.nextInt()+10]++;				
			}

			long retVal = 0;
			for(int i= 0 ;i < 10 ; i++){
				retVal += opposites[i]*opposites[20-i];
			}
			retVal += opposites[10]*(opposites[10]-1)/2;
			pw.write(retVal+"");
			pw.flush();
			s.close();
		}
	}

}
