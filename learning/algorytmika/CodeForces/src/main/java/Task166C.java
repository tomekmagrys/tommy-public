import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task166C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int x = s.nextInt();

			int retVal = 0;

			ArrayList<Integer> a = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			if (!a.contains(Integer.valueOf(x))) {
				a.add(x);
				retVal++;
			}

			Collections.sort(a);

			while (a.get((a.size() + 1) / 2 - 1) != x) {
				if (a.get((a.size() + 1) / 2 - 1) < x) {
					a.add(Integer.MAX_VALUE);
					retVal++;
				} else {
					a.add(0,Integer.MIN_VALUE);
					retVal++;
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}
