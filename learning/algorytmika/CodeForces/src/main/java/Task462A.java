import java.io.*;

public class Task462A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            char board[][] = new char[n][];
            for (int i = 0; i < n; i++) {
                board[i] = sc.next().toCharArray();
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    int sum = 0;
                    if (i > 0 && board[i - 1][j] == 'o') {
                        sum++;
                    }
                    if (j > 0 && board[i][j - 1] == 'o') {
                        sum++;
                    }
                    if (i < n - 1 && board[i + 1][j] == 'o') {
                        sum++;
                    }
                    if (j < n - 1 && board[i][j + 1] == 'o') {
                        sum++;
                    }
                    if(sum % 2 != 0){
                        pw.println("NO");
                        pw.flush();
                        sc.close();
                        return;
                    }
                }
            }

            pw.println("YES");
            pw.flush();
            sc.close();
        }
    }

}