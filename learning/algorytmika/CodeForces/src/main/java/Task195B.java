import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task195B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			final int m = sc.nextInt();

			ArrayList<Integer> retVal = new ArrayList<>(m);

			for (int i = 1; i <= m; i++) {
				retVal.add(i);
			}

			Collections.sort(retVal, new Comparator<Integer>() {

				@Override
				public int compare(Integer arg0, Integer arg1) {
					if (Math.abs(m + 1 - 2 * arg0) < Math.abs(m + 1 - 2 * arg1)) {
						return -1;
					}
					if (Math.abs(m + 1 - 2 * arg0) > Math.abs(m + 1 - 2 * arg1)) {
						return 1;
					}
					return arg0.compareTo(arg1);
				}

			});

			for (int i = 0; i < n; i++) {
				pw.println(retVal.get(i % m));
			}

			pw.flush();
			sc.close();
		}
	}

}