import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task011A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			long d = s.nextInt();

			ArrayList<Long> a = new ArrayList<Long>(n);
			for (int i = 0; i < n; i++) {
				a.add(s.nextLong());
			}

			long retVal = 0;

			for (int i = 1; i < n; i++) {
				if (a.get(i) <= a.get(i - 1)) {
					long inc = (a.get(i - 1) - a.get(i)) / d + 1;
					retVal += inc;
					a.set(i, a.get(i) + d * inc);
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}