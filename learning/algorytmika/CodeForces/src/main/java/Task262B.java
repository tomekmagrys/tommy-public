import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task262B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			int i = 0;
			while (i < k && i < n && a.get(i) <= 0) {
				a.set(i, -a.get(i));
				i++;
			}

			int min = Integer.MAX_VALUE;
			int minIndex = -1;

			for (int j = 0; j < n; j++) {
				if (Math.abs(a.get(j)) < min) {
					min = Math.abs(a.get(j));
					minIndex = j;
				}
			}

			while (i < k) {
				a.set(minIndex, -a.get(minIndex));
				i++;
			}

			int retVal = 0;
			for (Integer tmp : a) {
				retVal += tmp;
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}