import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task009A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int y = s.nextInt();
			int w = s.nextInt();

			switch (Math.max(y, w)) {
			case 1:
				pw.write("1/1");
				break;
			case 2:
				pw.write("5/6");
				break;
			case 3:
				pw.write("2/3");
				break;
			case 4:
				pw.write("1/2");
				break;
			case 5:
				pw.write("1/3");
				break;
			case 6:
				pw.write("1/6");
				break;
			}

			pw.flush();
			s.close();
		}
	}

}