import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task913B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            Map<Integer, List<Integer>> tree = new HashMap<>();

            for (int i = 2; i <= n; i++) {
                int pi = sc.nextInt();
                if (!tree.containsKey(pi)) {
                    tree.put(pi, new ArrayList<>());
                }
                tree.get(pi).add(i);
            }

            boolean retVal = true;

            for (Map.Entry<Integer, List<Integer>> edge : tree.entrySet()) {
                List<Integer> children = edge.getValue();
                if (!children.isEmpty()) {
                    int spruce = 0;
                    for (int child : children) {
                        if (!tree.containsKey(child)) {
                            spruce++;
                        }
                    }
                    if (spruce < 3) {
                        retVal = false;
                    }
                }
            }


            pw.println(retVal ? "Yes" : "No");
            pw.flush();
            sc.close();
        }
    }

}