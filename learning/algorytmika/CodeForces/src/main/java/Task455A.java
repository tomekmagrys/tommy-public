import java.io.*;

public class Task455A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            long a[] = new long[100001];
            long dp[] = new long[100001];

            for(int i = 0 ; i <= 100000 ; i++){
                a[i] = 0;
            }

            for(int i = 0 ; i < n ; i++){
                a[sc.nextInt()]++;
            }


            dp[0] = 0;
            dp[1] = a[1];

            for(int i = 2 ; i <= 100000 ; i++){
                dp[i] = Math.max(dp[i-1],i*a[i] + dp[i-2]);
            }

            pw.println(dp[100000]);

            pw.flush();
            sc.close();
        }
    }

}