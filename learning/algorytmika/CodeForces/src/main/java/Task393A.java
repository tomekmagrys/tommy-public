import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task393A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String inp = sc.next();

			int nCount = 0;
			int eCount = 0;
			int iCount = 0;
			int tCount = 0;

			for (char c : inp.toCharArray()) {
				switch (c) {
				case 'n':
					nCount++;
					break;
				case 'e':
					eCount++;
					break;
				case 'i':
					iCount++;
					break;
				case 't':
					tCount++;
					break;
				}
			}

			int retVal = 0;

			while (retVal + 1 <= tCount && retVal + 1 <= iCount
					&& 3 * (retVal + 1) <= eCount
					&& 2 * (retVal + 1) + 1 <= nCount) {
				retVal++;

			}

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}