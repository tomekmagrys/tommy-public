import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task049A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.nextLine();

			Set<Character> vovels = new TreeSet<>();
			vovels.add('a');
			vovels.add('e');
			vovels.add('i');
			vovels.add('o');
			vovels.add('u');
			vovels.add('y');
			vovels.add('A');
			vovels.add('E');
			vovels.add('I');
			vovels.add('O');
			vovels.add('U');
			vovels.add('Y');

			boolean retVal = false;

			for (int i = input.length() - 1; i >= 0; i--) {
				if (Character.isLetter(input.charAt(i))) {
					if (vovels.contains(input.charAt(i))) {
						retVal = true;
					}
					break;
				}
			}

			pw.print(retVal ? "YES" : "NO");

			pw.flush();
			s.close();
		}

	}

}
