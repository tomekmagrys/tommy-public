import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task279B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int t = s.nextInt();
			int retVal = 0;
			ArrayList<Integer> lines = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				lines.add(s.nextInt());
			}

			for (int i = 0, j = 0, sum = 0; i < n; i++) {
				while (j < n && sum + lines.get(j)<= t) {
					sum += lines.get(j);
					j++;
					retVal = Math.max(retVal, j - i);
				}
				sum -= lines.get(i);
			}
			pw.write("" + retVal);
			pw.flush();
			s.close();
		}
	}

}
