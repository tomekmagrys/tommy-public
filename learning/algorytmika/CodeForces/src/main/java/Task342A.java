import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task342A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int count[] = new int[8];

			for (int i = 0; i < n; i++) {
				count[s.nextInt()]++;
			}

			int _124 = 0;
			int _126 = 0;
			int _136 = 0;

			_124 = Math.min(Math.min(count[1], count[2]), count[4]);
			count[1] -= _124;
			count[2] -= _124;
			count[4] -= _124;
			_126 = Math.min(Math.min(count[1], count[2]), count[6]);
			count[1] -= _126;
			count[2] -= _126;
			count[6] -= _126;
			_136 = Math.min(Math.min(count[1], count[3]), count[6]);
			count[1] -= _136;
			count[3] -= _136;
			count[6] -= _136;

			if (_124 + _126 + _136 == n / 3) {
				for (int i = 0; i < _124; i++) {
					pw.write("1 2 4\n");
				}
				for (int i = 0; i < _126; i++) {
					pw.write("1 2 6\n");
				}
				for (int i = 0; i < _136; i++) {
					pw.write("1 3 6\n");
				}
			} else {
				pw.write("-1");
			}

			pw.flush();
			s.close();
		}
	}

}