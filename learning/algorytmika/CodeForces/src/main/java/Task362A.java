import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task362A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int t = s.nextInt();
			char chess[][] = new char[8][];
			while (t-- > 0) {
				for (int i = 0; i < 8; i++) {
					chess[i] = s.next().toCharArray();
				}
				ArrayList<Integer> knightsX = new ArrayList<>(2);
				ArrayList<Integer> knightsY = new ArrayList<>(2);

				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if (chess[i][j] == 'K') {
							knightsX.add(i);
							knightsY.add(j);
						}
					}
				}
				int x1 = knightsX.get(0);
				int y1 = knightsY.get(0);

				int x2 = knightsX.get(1);
				int y2 = knightsY.get(1);

				if ((x1 - x2 + 8) % 4 == 0) {
					if ((y1 - y2 + 8) % 4 == 0) {
						pw.println("YES");
						continue;
					}
				}

				pw.println("NO");

			}

			pw.flush();
			s.close();
		}
	}

}
