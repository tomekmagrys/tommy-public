import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task352A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int ilosc5 = 0;
			int ilosc0 = 0;

			for (int i = 0; i < n; i++) {
				if (s.next().equals("0")) {
					ilosc0++;
				} else {
					ilosc5++;
				}
			}

			ilosc5 = ilosc5 / 9 * 9;

			if (ilosc5 == 0) {
				if (ilosc0 == 0) {
					pw.write("-1");
				} else {
					pw.write("0");
				}
			} else {
				if (ilosc0 == 0) {
					pw.write("-1");
				} else {
					for (int i = 0; i < ilosc5; i++) {
						pw.write("5");
					}
					for (int i = 0; i < ilosc0; i++) {
						pw.write("0");
					}
				}
			}

			pw.write("\n");
			pw.flush();
			s.close();
		}
	}

}
