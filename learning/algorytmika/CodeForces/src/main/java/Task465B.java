import java.io.*;
import java.util.ArrayList;

public class Task465B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            ArrayList<Integer> letters = new ArrayList<>(n + 1);


            for (int i = 0; i < n; i++) {
                letters.add(sc.nextInt());
            }

            while (!letters.isEmpty() && letters.get(letters.size() - 1) == 0) {
                letters.remove(letters.size() - 1);
            }

            letters.add(0, 0);

            int retVal = 0;

            for (int i = 1; i < letters.size(); i++) {
                if (letters.get(i) == 1 || letters.get(i - 1) == 1) {
                    retVal++;
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}