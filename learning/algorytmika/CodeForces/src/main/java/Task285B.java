import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task285B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner scanner = new Scanner(is);

			int n = scanner.nextInt();
			int s = scanner.nextInt();
			int t = scanner.nextInt();
			
			ArrayList<Integer> permutation = new ArrayList<>(n+1);
			permutation.add(null);
			for(int i = 1 ; i <= n ; i++){
				permutation.add(scanner.nextInt());
			}
			
			int shuffles = 0;
			int actual = s;
			while(true){
				if(actual == t){
					break;
				}
				actual = permutation.get(actual);
				if(actual == s){
					shuffles = -1;
					break;
				}
				shuffles++;
			}
			pw.write(shuffles+"");
			pw.flush();
			scanner.close();
		}
	}

}
