import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task080B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String inp = s.nextLine();
			
			int h = Integer.parseInt(inp.substring(0,2));
			int m = Integer.parseInt(inp.substring(3));
			
			h = h % 12;
			
			pw.print((60 * h + m) * 360 / 720f);
			pw.print(" ");
			pw.println(m * 6);
			
			pw.flush();
			s.close();
		}
	}

}