import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task199A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			ArrayList<Integer> f = new ArrayList<Integer>();
			
			f.add(0);
			f.add(1);
			
			while(f.get(f.size()-1) + f.get(f.size()-2) <= n){
				f.add(f.get(f.size()-1) + f.get(f.size()-2));
			}
			
			String answer = "I'm too stupid to solve this problem";
			
			for(int i : f){
				for(int j : f){
					for(int k : f){
						if(i + j + k == n){
							answer = i + " " + j + " " + k;
						}
					}
				}
			}
			
			pw.write(answer);
			pw.flush();
			s.close();
		}
	}

}
