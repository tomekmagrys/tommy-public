import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task439B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int x = sc.nextInt();

            List<Integer> c = new ArrayList<Integer>(n);

            for (int i = 0; i < n; i++) {
                c.add(sc.nextInt());
            }

            Collections.sort(c);

            long retVal = 0;

            for (int ci : c) {
                long h = ci;
                h *= x;
                x = Math.max(x - 1, 1);
                retVal += h;
            }


            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}