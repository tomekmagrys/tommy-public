import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task172A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			Set<String> phones = new TreeSet<String>();
			for(int i = 0 ; i < n ; i++){
				phones.add(s.next());
			}
			
			String pattern = phones.iterator().next();
			
			for(int i = 0 ; i <= pattern.length() ; i++){
				boolean ok = true;
				for(String test : phones){
					if(!test.startsWith(pattern.substring(0, i))){
						ok = false;
						break;
					}
				}
				if(!ok){
					pw.write(""+(i-1));
					break;
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}