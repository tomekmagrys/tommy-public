import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task312B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			double a = s.nextInt();
			double b = s.nextInt();
			double c = s.nextInt();
			double d = s.nextInt();

			double p = a / b;
			double q = 1;
			double sumP = p;

			while (q > 0.0000001) {
				q *= (1 - c / d) * (1 - a / b);
				sumP += p * q;
			}
			pw.println(sumP);
			pw.flush();
			s.close();
		}
	}

}
