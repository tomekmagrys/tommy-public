import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task271B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static boolean isPrime(int n) {
			if (n < 2) {
				return false;
			}
			for (int i = 2; i * i <= n; i++) {
				if (n % i == 0) {
					return false;
				}
			}
			return true;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> primes = new ArrayList<>();
			for (int i = 2; ; i++) {
				if (isPrime(i)) {
					primes.add(i);
					if(i > 100000){
						break;
					}
				}
			}

			ArrayList<ArrayList<Integer>> matrix = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				matrix.add(new ArrayList<Integer>(m));
				for (int j = 0; j < m; j++) {
					matrix.get(i).add(s.nextInt());
				}
			}

			ArrayList<ArrayList<Integer>> operations = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				operations.add(new ArrayList<Integer>(m));
				for (int j = 0; j < m; j++) {
					Integer primeIndex = Collections.binarySearch(primes,
							matrix.get(i).get(j));
					if(primeIndex >= 0){
						operations.get(i).add(0);
					} else {
						operations.get(i).add(primes.get(Math.abs(primeIndex)-1)-matrix.get(i).get(j));
					}
				}
			}

			ArrayList<Integer> columnCosts = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				int sum = 0;
				for (int j = 0; j < m; j++) {
					sum += operations.get(i).get(j);
				}
				columnCosts.add(sum);
			}

			ArrayList<Integer> rowCosts = new ArrayList<>(m);
			for (int j = 0; j < m; j++) {
				int sum = 0;
				for (int i = 0; i < n; i++) {
					sum += operations.get(i).get(j);
				}
				rowCosts.add(sum);
			}

			int min = Math.min(Collections.min(columnCosts),
					Collections.min(rowCosts));

			pw.write("" + min);

			pw.flush();
			s.close();
		}
	}

}
