import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task875A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Math2 {
        public static int sumOfDigits(int n) {
            int retVal = 0;

            while (n > 0) {
                retVal += n % 10;
                n /= 10;
            }

            return retVal;
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            List<Integer> retVal = new ArrayList<>();

            for (int i = Math.max(n - 90, 1); i <= n; i++) {
                if (i + Math2.sumOfDigits(i) == n) {
                    retVal.add(i);
                }

            }

            pw.println(retVal.size());
            for (int i : retVal) {
                pw.println(i);
            }

            pw.flush();
            sc.close();

        }
    }

}