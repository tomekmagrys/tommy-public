import java.io.*;

public class Task948A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int r = sc.nextInt();
            int c = sc.nextInt();

            boolean retVal = true;

            char[][] board = new char[r][];

            for (int i = 0; i < r; i++) {
                board[i] = sc.next().toCharArray();
            }

            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    if (board[i][j] == '.') {
                        board[i][j] = 'D';
                    }
                }
            }

            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    if (board[i][j] == 'S') {
                        if (i > 0) {
                            if (board[i - 1][j] == 'W') {
                                retVal = false;
                            }
                        }
                        if (j > 0) {
                            if (board[i][j - 1] == 'W') {
                                retVal = false;
                            }
                        }
                        if (i + 1 < r) {
                            if (board[i + 1][j] == 'W') {
                                retVal = false;
                            }
                        }
                        if (j + 1 < c) {
                            if (board[i][j + 1] == 'W') {
                                retVal = false;
                            }
                        }
                    }
                }
            }

            if (retVal) {
                pw.println("Yes");
                for (char[] row : board) {
                    pw.println(row);
                }
            } else {
                pw.println("No");
            }

            pw.flush();
            sc.close();

        }
    }

}