import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task305A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int k = s.nextInt();

			ArrayList<Integer> zeros = new ArrayList<>();
			ArrayList<Integer> hundreds = new ArrayList<>();
			ArrayList<Integer> units = new ArrayList<>();
			ArrayList<Integer> decimals = new ArrayList<>();
			ArrayList<Integer> mixed = new ArrayList<>();

			for (int i = 0; i < k; i++) {
				int tmp = s.nextInt();
				if (tmp == 0) {
					zeros.add(tmp);
					continue;
				}
				if (tmp == 100) {
					hundreds.add(tmp);
					continue;
				}
				if (tmp / 10 == 0) {
					units.add(tmp);
					continue;
				}
				if (tmp % 10 == 0) {
					decimals.add(tmp);
					continue;
				}
				mixed.add(tmp);
			}

			ArrayList<Integer> retVal = new ArrayList<>();

			if (!zeros.isEmpty()) {
				retVal.add(0);
			}

			if (!units.isEmpty()) {
				retVal.add(units.iterator().next());
			}

			if (!decimals.isEmpty()) {
				retVal.add(decimals.iterator().next());
			}

			if (units.isEmpty() && decimals.isEmpty() && !mixed.isEmpty()) {
				retVal.add(mixed.iterator().next());
			}

			if (!hundreds.isEmpty()) {
				retVal.add(100);
			}

			pw.println(retVal.size());
			for (int i : retVal) {
				pw.print(i);
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}