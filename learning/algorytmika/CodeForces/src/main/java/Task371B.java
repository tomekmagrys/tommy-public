import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task371B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();

			int a2 = 0;
			int a3 = 0;
			int a5 = 0;

			int b2 = 0;
			int b3 = 0;
			int b5 = 0;

			while (a % 2 == 0) {
				a /= 2;
				a2++;
			}

			while (a % 3 == 0) {
				a /= 3;
				a3++;
			}

			while (a % 5 == 0) {
				a /= 5;
				a5++;
			}

			while (b % 2 == 0) {
				b /= 2;
				b2++;
			}

			while (b % 3 == 0) {
				b /= 3;
				b3++;
			}

			while (b % 5 == 0) {
				b /= 5;
				b5++;
			}

			if (a != b) {
				pw.println(-1);
			} else {
				pw.println(Math.abs(a2 - b2) + Math.abs(a3 - b3)
						+ Math.abs(a5 - b5));
			}

			pw.flush();
			s.close();
		}
	}

}