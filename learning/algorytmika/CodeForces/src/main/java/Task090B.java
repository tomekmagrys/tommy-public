import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task090B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			char crossword[][] = new char[n][];
			for (int i = 0; i < n; i++) {
				crossword[i] = sc.next().toCharArray();
			}

			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					boolean print = true;
					for (int k = 0; k < n; k++) {
						if (k != i && crossword[i][j] == crossword[k][j]) {
							print = false;
							break;
						}
					}
					for (int k = 0; k < m; k++) {
						if (k != j && crossword[i][j] == crossword[i][k]) {
							print = false;
							break;
						}
					}
					if (print) {
						sb.append(crossword[i][j]);
					}
				}
			}

			pw.println(sb);

			pw.flush();
			sc.close();
		}
	}

}