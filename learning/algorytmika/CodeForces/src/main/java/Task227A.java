import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task227A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			long xa = s.nextLong();
			long ya = s.nextLong();
			long xb = s.nextLong();
			long yb = s.nextLong();
			long xc = s.nextLong();
			long yc = s.nextLong();

			long ccw = (xb - xa) * (yc - yb) - (yb - ya) * (xc - xb);

			if (ccw > 0) {
				pw.println("LEFT");
			}
			if (ccw == 0) {
				pw.println("TOWARDS");
			}
			if (ccw < 0) {
				pw.println("RIGHT");
			}
			pw.flush();
			s.close();
		}
	}

}
