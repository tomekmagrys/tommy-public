import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task255B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os) throws IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);
			
			String input = sc.nextLine();
			int xCount = 0;
			int yCount = 0;

			for (char c : input.toCharArray()) {
				if (c == 'x') {
					xCount++;
				} else if (c == 'y') {
					yCount++;
				} else {
					throw new UnsupportedOperationException();
				}
			}

			int remove = Math.min(xCount, yCount);
			xCount -= remove;
			yCount -= remove;

			for (int i = 0; i < xCount; i++) {
				pw.write("x");
			}
			for (int i = 0; i < yCount; i++) {
				pw.write("y");
			}
			sc.close();
			pw.flush();
		}
	}

}
