import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task225A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int top = s.nextInt();
			int left = s.nextInt();
			int right = s.nextInt();
			
			boolean retVal = true;
			
			for(int i = 1 ; i < n ; i++){
				left = s.nextInt();
				right = s.nextInt();
				Set<Integer> candidatesForTop = new TreeSet<>();
				candidatesForTop.add(1);
				candidatesForTop.add(2);
				candidatesForTop.add(3);
				candidatesForTop.add(4);
				candidatesForTop.add(5);
				candidatesForTop.add(6);
				candidatesForTop.remove(7-top);
				candidatesForTop.remove(left);
				candidatesForTop.remove(7-left);
				candidatesForTop.remove(right);
				candidatesForTop.remove(7-right);
				if(candidatesForTop.size() > 1){
					retVal = false;					
				}
				top = candidatesForTop.iterator().next();
			}

			pw.write(retVal ? "YES":"NO");
			
			pw.flush();
			s.close();
		}
	}

}
