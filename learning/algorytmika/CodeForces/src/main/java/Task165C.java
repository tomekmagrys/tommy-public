import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task165C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static int numberOfLines(int v, int k) {
			int retVal = 0;
			while (v > 0) {
				retVal += v;
				v /= k;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int k = s.nextInt();
			String v = s.next();

			ArrayList<Integer> onesIndexes = new ArrayList<>(v.length() + 2);

			onesIndexes.add(-1);
			for (int i = 0; i < v.length(); i++) {
				if (v.charAt(i) == '1') {
					onesIndexes.add(i);
				}
			}
			onesIndexes.add(v.length());

			long retVal = 0;

			if (k == 0) {
				for (int i = 0; i + 1 < onesIndexes.size(); i++) {
					long zeros = onesIndexes.get(i + 1) - onesIndexes.get(i) - 1;
					retVal += zeros * (zeros + 1) / 2;
				}
			} else {
				for (int i = 1; i + k < onesIndexes.size(); i++) {
					long zerosBefore = onesIndexes.get(i)
							- onesIndexes.get(i - 1);
					long zerosAfter = onesIndexes.get(i + k)
							- onesIndexes.get(i + k - 1);
					retVal += zerosAfter * zerosBefore;
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}