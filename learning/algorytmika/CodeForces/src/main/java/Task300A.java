import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

public class Task300A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> dodatnie = new ArrayList<>();
			ArrayList<Integer> ujemne = new ArrayList<>();
			ArrayList<Integer> zera = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				int tmp = s.nextInt();
				if (tmp > 0) {
					dodatnie.add(tmp);
				}
				if (tmp < 0) {
					ujemne.add(tmp);
				}
				if (tmp == 0) {
					zera.add(0);
				}
			}

			if (dodatnie.size() == 0) {
				dodatnie.add(ujemne.remove(ujemne.size() - 2));
				dodatnie.add(ujemne.remove(ujemne.size() - 1));
			}
			if (ujemne.size() % 2 == 0) {
				zera.add(ujemne.remove(ujemne.size() - 1));
			}
			pw.write(formatList(new FormatCollectionDTO<>(ujemne, "", "\n",
					" ", true)));
			pw.write(formatList(new FormatCollectionDTO<>(dodatnie, "", "\n",
					" ", true)));
			pw.write(formatList(new FormatCollectionDTO<>(zera, "", "\n", " ",
					true)));
			pw.flush();
			s.close();
		}
	}

	private static class FormatCollectionDTO<T> {
		private final Collection<T> collectionToFormat;
		private final String beginMark;
		private final String endMark;
		private final String separator;
		private final Boolean appendSize;

		public FormatCollectionDTO(Collection<T> collectionToFormat,
				String beginMark, String endMark, String separator,
				Boolean appendSize) {
			this.collectionToFormat = collectionToFormat;
			this.beginMark = beginMark;
			this.endMark = endMark;
			this.separator = separator;
			this.appendSize = appendSize;
		}

	}

	private static <T> String formatList(
			FormatCollectionDTO<T> formatCollectionDTO) {
		StringBuilder sb = new StringBuilder();
		sb.append(formatCollectionDTO.beginMark);
		if (formatCollectionDTO.appendSize) {
			sb.append(formatCollectionDTO.collectionToFormat.size());
			sb.append(formatCollectionDTO.separator);
		}
		Iterator<T> iterator = formatCollectionDTO.collectionToFormat
				.iterator();
		if (iterator.hasNext()) {
			sb.append(iterator.next());
		}
		while (iterator.hasNext()) {
			sb.append(formatCollectionDTO.separator);
			sb.append(iterator.next());
		}
		sb.append(formatCollectionDTO.endMark);
		return sb.toString();
	}

}
