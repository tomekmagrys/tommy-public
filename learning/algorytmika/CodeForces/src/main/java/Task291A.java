import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task291A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	public static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int iloscSekretarek = sc.nextInt();

			List<Long> calls = new ArrayList<>();

			for (int i = 0; i < iloscSekretarek; i++) {
				calls.add(sc.nextLong());
			}

			calls.add(Long.MAX_VALUE);
			calls.add(Long.MAX_VALUE);

			Collections.sort(calls);

			int result = 0;

			for (int i = 0; i < iloscSekretarek; i++) {
				if (calls.get(i).equals(0L)) {
					continue;
				}
				if (calls.get(i).equals(calls.get(i + 1))) {
					if (calls.get(i + 1).equals(calls.get(i + 2))) {
						os.write(("-1" + System.lineSeparator()).getBytes());
						os.flush();
						sc.close();
						return;
					} else {
						result++;
					}
				}
			}

			os.write((result + System.lineSeparator()).getBytes());
			os.flush();
			sc.close();
		}
	}
}
