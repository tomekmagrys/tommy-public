import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task349B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int v = sc.nextInt();

			final ArrayList<Integer> costs = new ArrayList<>();
			costs.add(null);
			for (int i = 1; i <= 9; i++) {
				costs.add(sc.nextInt());
			}

			ArrayList<Integer> avail = new ArrayList<>();
			for (int i = 1; i <= 9; i++) {
				avail.add(i);
			}

			Collections.sort(avail, new Comparator<Integer>() {

				@Override
				public int compare(Integer arg0, Integer arg1) {
					int retVal = costs.get(arg0).compareTo(costs.get(arg1));
					if (retVal != 0)
						return retVal;
					return arg1.compareTo(arg0);
				}

			});

			if (v < costs.get(avail.get(0))) {
				pw.println(-1);
				pw.flush();
				sc.close();
				return;
			}

			int theCheapestDigit = avail.get(0);

			int printCount[] = new int[10];
			printCount[theCheapestDigit] = v / costs.get(theCheapestDigit);

			v -= printCount[theCheapestDigit] * costs.get(theCheapestDigit);

			for (int i = 9; i > theCheapestDigit; i--) {
				while (costs.get(i) - costs.get(theCheapestDigit) <= v) {
					printCount[theCheapestDigit]--;
					printCount[i]++;
					v -= costs.get(i) - costs.get(theCheapestDigit);
				}
			}

			for (int i = 9; i > 0; i--) {
				for (int j = 0; j < printCount[i]; j++) {
					pw.print(i);
				}
			}
			pw.println();
			pw.flush();
			sc.close();
		}
	}

}