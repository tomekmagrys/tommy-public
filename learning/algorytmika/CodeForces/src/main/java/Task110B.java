import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task110B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < n; i++) {
				switch (i % 4) {
				case 0:
					sb.append("a");
					break;
				case 1:
					sb.append("b");
					break;
				case 2:
					sb.append("c");
					break;
				case 3:
					sb.append("d");
					break;
				}
			}

			pw.print(sb.toString());

			pw.flush();
			sc.close();
		}
	}

}