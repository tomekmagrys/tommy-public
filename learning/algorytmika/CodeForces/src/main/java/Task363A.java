import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task363A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			if (n == 0) {
				pw.write("O-|-OOOO\n");
			} else {
				while (n > 0) {
					switch (n % 10) {
					case 0:
						pw.write("O-|-OOOO\n");
						break;
					case 1:
						pw.write("O-|O-OOO\n");
						break;
					case 2:
						pw.write("O-|OO-OO\n");
						break;
					case 3:
						pw.write("O-|OOO-O\n");
						break;
					case 4:
						pw.write("O-|OOOO-\n");
						break;
					case 5:
						pw.write("-O|-OOOO\n");
						break;
					case 6:
						pw.write("-O|O-OOO\n");
						break;
					case 7:
						pw.write("-O|OO-OO\n");
						break;
					case 8:
						pw.write("-O|OOO-O\n");
						break;
					case 9:
						pw.write("-O|OOOO-\n");
						break;
					}
					n = n / 10;
				}
			}

			pw.flush();
			s.close();
		}
	}

}