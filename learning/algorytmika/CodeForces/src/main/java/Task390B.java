import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Task390B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			ArrayList<Integer> b = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(sc.nextInt());
			}

			for (int i = 0; i < n; i++) {
				b.add(sc.nextInt());
			}

			long joy = 0;

			for (int i = 0; i < n; i++) {
				int maxTune = a.get(i);
				if (b.get(i) == 1 || 2 * maxTune < b.get(i)) {
					--joy;
				} else {
					long tune1 = b.get(i) / 2;
					long tune2 = (b.get(i) + 1) / 2;
					joy += tune1 * tune2;
				}
			}

			pw.println(joy);

			pw.flush();
			sc.close();
		}
	}

}