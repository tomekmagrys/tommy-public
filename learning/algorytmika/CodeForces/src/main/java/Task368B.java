import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task368B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			for(int i = 0 ; i < n ; i++){
				a.add(s.nextInt());
			}
			
			Set<Integer> distinct = new TreeSet<Integer>();
			List<Integer> distinctCount = new ArrayList<>(n); 
					
			for(int i = n-1 ; i>= 0 ; i--){
				distinct.add(a.get(i));
				distinctCount.add(distinct.size());
			}

			Collections.reverse(distinctCount);
			
			for(int i = 0 ; i < m ; i++ ){
				pw.write(distinctCount.get(s.nextInt()-1) + "\n");
			}
			
			pw.flush();
			s.close();
		}
	}

}