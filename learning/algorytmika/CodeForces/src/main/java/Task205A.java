import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task205A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int minIndex = -1;
			int min = Integer.MAX_VALUE;
			
			for (int i = 0; i < n; i++) {
				int tmp = s.nextInt();
				if(tmp == min){
					minIndex = -1;
				}
				if(tmp < min){
					min = tmp;
					minIndex = i;
				}
			}
			
			if(minIndex == -1){
				pw.write("Still Rozdil");
			} else {
				pw.write("" + (minIndex+1));
			}
			pw.flush();
			s.close();
		}
	}

}
