import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Task202A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static List<String> allSubseqences(String input) {
		if (input.length() == 0) {
			return Collections.singletonList("");
		} else {
			List<String> tmp = allSubseqences(input.substring(1));
			ArrayList<String> retVal = new ArrayList<>(tmp.size() * 2);
			retVal.addAll(tmp);
			for (String tmps : tmp) {
				retVal.add(input.substring(0, 1) + tmps);
			}
			return retVal;
		}
	}

	static boolean isPalindrom(String input) {
		for (int i = 0; i < input.length() / 2; i++) {
			if (input.charAt(i) != input.charAt(input.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String inp = s.next();

			List<String> subsequences = allSubseqences(inp);

			Iterator<String> iter = subsequences.iterator();
			while (iter.hasNext()) {
				if (!isPalindrom(iter.next())) {
					iter.remove();
				}
			}

			Collections.sort(subsequences);

			pw.println(subsequences.get(subsequences.size() - 1));

			pw.flush();
			s.close();
		}

	}

}
