import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task173A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			String A = sc.next();
			String B = sc.next();

			int m = A.length();
			int k = B.length();

			{
				StringBuilder sb = new StringBuilder(m * k);
				for (int i = 0; i < k; i++) {
					sb.append(A);
				}
				A = sb.toString();
			}
			{
				StringBuilder sb = new StringBuilder(m * k);
				for (int i = 0; i < m; i++) {
					sb.append(B);
				}
				B = sb.toString();
			}

			if (A.length() != B.length()) {
				pw.flush();
				sc.close();
				throw new RuntimeException("Something went wrong ;-)");
			}

			int retVal1Cycle = 0;
			int retVal1Modulo = 0;
			int retVal2Cycle = 0;
			int retVal2Modulo = 0;

			for (int i = 0; i < m * k; i++) {
				switch (A.charAt(i)) {
				case 'R':
					switch (B.charAt(i)) {
					case 'R':
						break;
					case 'P':
						retVal1Cycle++;
						break;
					case 'S':
						retVal2Cycle++;
						break;
					}
					break;
				case 'P':
					switch (B.charAt(i)) {
					case 'R':
						retVal2Cycle++;
						break;
					case 'P':
						break;
					case 'S':
						retVal1Cycle++;
						break;
					}
					break;
				case 'S':
					switch (B.charAt(i)) {
					case 'R':
						retVal1Cycle++;
						break;
					case 'P':
						retVal2Cycle++;
						break;
					case 'S':
						break;
					}
					break;
				}
			}

			for (int i = 0; i < n % (m * k); i++) {
				switch (A.charAt(i)) {
				case 'R':
					switch (B.charAt(i)) {
					case 'R':
						break;
					case 'P':
						retVal1Modulo++;
						break;
					case 'S':
						retVal2Modulo++;
						break;
					}
					break;
				case 'P':
					switch (B.charAt(i)) {
					case 'R':
						retVal2Modulo++;
						break;
					case 'P':
						break;
					case 'S':
						retVal1Modulo++;
						break;
					}
					break;
				case 'S':
					switch (B.charAt(i)) {
					case 'R':
						retVal1Modulo++;
						break;
					case 'P':
						retVal2Modulo++;
						break;
					case 'S':
						break;
					}
					break;
				}
			}

			int retVal1 = n / (m * k) * retVal1Cycle + retVal1Modulo;
			int retVal2 = n / (m * k) * retVal2Cycle + retVal2Modulo;

			pw.print(retVal1);
			pw.print(" ");
			pw.print(retVal2);

			pw.flush();
			sc.close();
		}
	}

}