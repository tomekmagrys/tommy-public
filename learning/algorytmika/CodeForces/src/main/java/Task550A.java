import java.io.*;

public class Task550A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            String s = sc.next();

            int ab1 = s.indexOf("AB");
            int ab2 = s.lastIndexOf("AB");

            int ba1 = s.indexOf("BA");
            int ba2 = s.lastIndexOf("BA");

            if (ab1 < 0 || ab2 < 0 || ba1 < 0 || ba2 < 0) {
                pw.println("NO");
            } else {
                if(ab1 + 1 < ba2){
                    pw.println("YES");
                } else if (ba1 + 1 < ab2){
                    pw.println("YES");
                } else {
                    pw.println("NO");
                }
            }


            pw.flush();
            sc.close();
        }
    }

}