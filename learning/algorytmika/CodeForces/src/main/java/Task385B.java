import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task385B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String s = sc.next();

			int retVal = 0;

			for (int i = 0; i < s.length(); i++) {
				int bearIndex = s.indexOf("bear",i);
				if (bearIndex >= 0) {
					retVal += s.length() - bearIndex - 3;
				}
			}

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}