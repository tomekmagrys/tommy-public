import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Task352B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			Map<Integer, ArrayList<Integer>> statistics = new TreeMap<>();
			for (int i = 0; i < n; i++) {
				if (!statistics.containsKey(a.get(i))) {
					statistics.put(a.get(i), new ArrayList<Integer>());
				}
				statistics.get(a.get(i)).add(i);
			}

			Iterator<Integer> iter = statistics.keySet().iterator();
			while (iter.hasNext()) {
				Integer key = iter.next();
				for (int i = 2; i < statistics.get(key).size(); i++) {
					if (statistics.get(key).get(i)
							- statistics.get(key).get(i - 1) != statistics.get(
							key).get(1)
							- statistics.get(key).get(0)) {
						iter.remove();
						break;
					}
				}
			}

			pw.write(statistics.keySet().size()+"\n");
			iter = statistics.keySet().iterator();
			while (iter.hasNext()) {
				Integer key = iter.next();
				if(statistics.get(key).size() <= 1){
					pw.write(key + " 0\n");
				} else {
					pw.write(key + " " + (statistics.get(key).get(1) - statistics.get(key).get(0)) + "\n");
				}
			}
			
			
			pw.flush();
			s.close();
		}
	}

}
