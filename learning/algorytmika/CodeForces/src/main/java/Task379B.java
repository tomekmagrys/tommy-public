import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task379B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			ArrayList<Integer> a = new ArrayList<Integer>();
			StringBuilder retVal = new StringBuilder();

			int curPos = 0;
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			for (int i = 0; i < n; i++) {
				if (a.get(i) > 0) {
					for (int j = curPos; j < i; j++) {
						retVal.append("R");
					}
					for (int j = 0; j < a.get(i); j++) {
						if (i > 0) {
							retVal.append("PLR");
						} else {
							retVal.append("PRL");
						}
					}
					curPos = i;
				}
			}

			pw.println(retVal.toString());
			pw.flush();

			s.close();
		}
	}

}