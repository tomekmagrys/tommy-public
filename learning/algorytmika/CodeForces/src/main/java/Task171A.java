import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task171A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int a1 = sc.nextInt();
			String a2 = sc.next();

			String a2reverse = "";

			for (char c : a2.toCharArray()) {
				a2reverse = c + a2reverse;
			}

			pw.println(a1 + Integer.parseInt(a2reverse));
			pw.flush();
			sc.close();
		}
	}

}