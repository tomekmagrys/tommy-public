import java.io.*;

public class Task454A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt() / 2;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n - i; j++) {
                    pw.print("*");
                }
                for (int j = 0; j <= 2 * i; j++) {
                    pw.print("D");
                }
                for (int j = 0; j < n - i; j++) {
                    pw.print("*");
                }
                pw.println();
            }
            for (int j = 0; j <= 2 * n; j++) {
                pw.print("D");
            }
            pw.println();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j <= i; j++) {
                    pw.print("*");
                }
                for (int j = 0; j <= 2 * (n - i - 1); j++) {
                    pw.print("D");
                }
                for (int j = 0; j <= i; j++) {
                    pw.print("*");
                }
                pw.println();
            }
            pw.flush();
            sc.close();
        }
    }

}