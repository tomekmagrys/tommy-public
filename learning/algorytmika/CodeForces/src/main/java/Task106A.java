import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task106A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static int value(char rank) {
			switch (rank) {
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case 'T':
				return 10;
			case 'J':
				return 11;
			case 'Q':
				return 12;
			case 'K':
				return 13;
			case 'A':
				return 14;
			}
			throw new UnsupportedOperationException();
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String trump = s.next();
			String firstCard = s.next();
			String secondCard = s.next();

			if (firstCard.endsWith(trump)) {
				if (secondCard.endsWith(trump)) {
					if (firstCard.charAt(1) == secondCard.charAt(1)) {
						if(value(firstCard.charAt(0)) > value(secondCard.charAt(0))){
							pw.println("YES");
						} else {
							pw.println("NO");
						}
					} else {
						pw.println("NO");
					}
				} else {
					pw.println("YES");
				}
			} else {
				if (firstCard.charAt(1) == secondCard.charAt(1)) {
					if(value(firstCard.charAt(0)) > value(secondCard.charAt(0))){
						pw.println("YES");
					} else {
						pw.println("NO");
					}
				} else {
					pw.println("NO");
				}
			}

			pw.flush();
			s.close();
		}
	}

}
