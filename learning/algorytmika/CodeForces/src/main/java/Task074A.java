import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task074A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			Map<String,Integer> results = new HashMap<>();
			
			for(int i = 0 ; i < n ; i++){
				results.put(s.next(), 100*s.nextInt() - 50*s.nextInt() + s.nextInt() + s.nextInt() + s.nextInt() + s.nextInt() + s.nextInt());
			}
			
			int maxPoints = Collections.max(results.values());
			
			for(String result : results.keySet()){
				if(results.get(result) == maxPoints){
					pw.println(result);
					break;
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}
