import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task215A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			ArrayList<Integer> pedalAxies = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				pedalAxies.add(s.nextInt());
			}

			int m = s.nextInt();
			ArrayList<Integer> rearAxies = new ArrayList<>();
			for (int i = 0; i < m; i++) {
				rearAxies.add(s.nextInt());
			}

			int max = 0;
			int maxCount = 0;

			for (Integer pedal : pedalAxies) {
				for (Integer rear : rearAxies) {
					if (rear % pedal == 0) {
						if (rear / pedal > max) {
							maxCount = 0;
							max = rear / pedal;
						}
						if(rear / pedal == max){
							maxCount++;
						}
					}
				}
			}

			pw.write(maxCount + "");
			pw.flush();
			s.close();
		}
	}

}
