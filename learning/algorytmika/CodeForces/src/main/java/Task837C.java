import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task837C {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);


            int n = sc.nextInt();
            int a = sc.nextInt();
            int b = sc.nextInt();

            List<Integer> x = new ArrayList<>(n);
            List<Integer> y = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                int tmp1 = sc.nextInt();
                int tmp2 = sc.nextInt();

                x.add(tmp1);
                y.add(tmp2);



            }

            int retVal = 0;

            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    if (x.get(i) + x.get(j) <= a) {
                        if (Math.max(y.get(i), y.get(j)) <= b) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (y.get(i) + x.get(j) <= a) {
                        if (Math.max(x.get(i), y.get(j)) <= b) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (x.get(i) + y.get(j) <= a) {
                        if (Math.max(y.get(i), x.get(j)) <= b) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (y.get(i) + y.get(j) <= a) {
                        if (Math.max(x.get(i), x.get(j)) <= b) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (x.get(i) + x.get(j) <= b) {
                        if (Math.max(y.get(i), y.get(j)) <= a) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (y.get(i) + x.get(j) <= b) {
                        if (Math.max(x.get(i), y.get(j)) <= a) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (x.get(i) + y.get(j) <= b) {
                        if (Math.max(y.get(i), x.get(j)) <= a) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                    if (y.get(i) + y.get(j) <= b) {
                        if (Math.max(x.get(i), x.get(j)) <= a) {
                            retVal = Math.max(retVal, x.get(i) * y.get(i) + x.get(j) * y.get(j));
                        }
                    }
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();

        }
    }

}