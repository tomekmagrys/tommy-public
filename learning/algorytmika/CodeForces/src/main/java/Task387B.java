import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task387B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			ArrayList<Integer> b = new ArrayList<>(m);

			for (int i = 0; i < n; i++) {
				a.add(sc.nextInt());
			}
			for (int i = 0; i < m; i++) {
				b.add(sc.nextInt());
			}

			Collections.sort(a);
			Collections.sort(b);
			Collections.reverse(b);

			int retVal = 0;

			for (int i = 0; i < a.size(); i++) {
				while (!b.isEmpty() && b.get(b.size() - 1) < a.get(i)) {
					b.remove(b.size() - 1);
				}
				if (!b.isEmpty()) {
					b.remove(b.size() - 1);
				} else {
					retVal++;
				}
			}

			pw.println(retVal);
			pw.flush();
			sc.close();
		}
	}

}