import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task357B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> colors = new ArrayList<>(n + 1);
			for (int i = 0; i <= n; i++) {
				colors.add(null);
			}

			for (int i = 0; i < m; i++) {
				int a = s.nextInt();
				int b = s.nextInt();
				int c = s.nextInt();

				Set<Integer> freeColors = new TreeSet<>();
				freeColors.add(1);
				freeColors.add(2);
				freeColors.add(3);
				if (colors.get(a) != null) {
					freeColors.remove(colors.get(a));
				}
				if (colors.get(b) != null) {
					freeColors.remove(colors.get(b));
				}
				if (colors.get(c) != null) {
					freeColors.remove(colors.get(c));
				}

				Iterator<Integer> iter = freeColors.iterator();
				if (colors.get(a) == null) {
					colors.set(a, iter.next());
				}
				if (colors.get(b) == null) {
					colors.set(b, iter.next());
				}
				if (colors.get(c) == null) {
					colors.set(c, iter.next());
				}

			}

			for (int i = 1; i <= n; i++) {
				pw.print(colors.get(i));
				pw.print(" ");
			}

			pw.flush();
			s.close();
		}
	}

}
