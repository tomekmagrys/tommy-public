import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task190A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			if (n == 0) {
				if (m == 0) {
					pw.println("0 0");
				} else {
					pw.println("Impossible");
				}
			} else {
				if (m <= n) {
					pw.println(n + " " + (n + Math.max(m - 1, 0)));
				} else {
					pw.println(m + " " + (n + m - 1));
				}
			}

			sc.close();
			pw.flush();
		}
	}

}
