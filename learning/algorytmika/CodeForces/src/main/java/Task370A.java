import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task370A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int r1 = s.nextInt();
			int c1 = s.nextInt();
			int r2 = s.nextInt();
			int c2 = s.nextInt();

			pw.print((int) (Math.signum(Math.abs(r1 - r2)) + Math.signum(Math
					.abs(c1 - c2))));
			pw.print(" ");
			if ((r1 - c1 + r2 - c2) % 2 != 0) {
				pw.print(0);
			} else {
				int rDiff = Math.abs(r1 - r2);
				int cDiff = Math.abs(c1 - c2);
				int move1 = (int) Math.signum(Math.max(rDiff, cDiff));
				int move2 = (int) Math.signum(Math.abs(rDiff - cDiff));
				pw.print(move1 + move2);
			}
			pw.print(" ");
			pw.println(Math.max(Math.abs(r1 - r2), Math.abs(c1 - c2)));

			pw.flush();
			s.close();
		}
	}

}