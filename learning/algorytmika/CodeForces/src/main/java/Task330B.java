import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Task330B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<HashSet<Integer>> nodes = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				nodes.add(new HashSet<Integer>());
			}

			for (int i = 0; i < m; i++) {
				int x = s.nextInt()-1;
				int y = s.nextInt()-1;
				nodes.get(x).add(y);
				nodes.get(y).add(x);
			}

			for(int i = 0 ; i < n ; i++){
				if(nodes.get(i).size() == 0){
					pw.write(""+(n-1)+"\n");
					for(int j = 0 ; j < n ; j++){
						if(j != i ){
							pw.write((i+1)+ " " + (j+1)+"\n");
						}
					}
					break;
				}
			}

			pw.flush();
			s.close();
		}
	}

}