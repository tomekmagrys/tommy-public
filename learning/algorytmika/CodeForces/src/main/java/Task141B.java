import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task141B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int a = sc.nextInt();
			int x = sc.nextInt();
			int y = sc.nextInt();

			if (-a < 2 * x && 2 * x < a && 0 < y && y < a) {
				pw.print("1");
				pw.flush();
				sc.close();
				return;
			}
			y = (y - a);
			int h = y / (2 * a);
			y = y % (2 * a);

			if (-a < 2 * x && 2 * x < a && 0 < y && y < a) {
				pw.print(2 + 3 * h);
				pw.flush();
				sc.close();
				return;
			}
			if (-a < x && x < 0 && a < y && y < 2 * a) {
				pw.print(3 + 3 * h);
				pw.flush();
				sc.close();
				return;
			}
			if (0 < x && x < a && a < y && y < 2 * a) {
				pw.print(4 + 3 * h);
				pw.flush();
				sc.close();
				return;
			}
			pw.print("-1");
			pw.flush();
			sc.close();
		}
	}

}