import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Task269A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			Map<Long, Long> boxes = new TreeMap<>();

			for (int i = 0; i < n; i++) {
				boxes.put(sc.nextLong(), sc.nextLong());
			}

			Long maxK = Collections.max(boxes.keySet());

			while (boxes.size() > 1 || boxes.values().iterator().next() > 1) {
				long k = boxes.keySet().iterator().next();
				long a = boxes.get(k);
				boxes.remove(k);

				while (true) {
					k++;
					a = (a + 3) / 4;
					if (boxes.containsKey(k)) {
						a = Math.max(a, boxes.get(k));
						break;
					}
					if (a == 1) {
						if (boxes.keySet().iterator().hasNext()) {
							k = boxes.keySet().iterator().next();
							a = Math.max(a, boxes.get(k));
						}
						break;
					}
				}

				boxes.put(k, a);

			}
			pw.println(Math.max(maxK + 1, boxes.keySet().iterator().next()));

			pw.flush();
			sc.close();
		}
	}

}