import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task263B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int k = sc.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(sc.nextInt());
			}

			Collections.sort(a);

			if (k > n) {
				os.write(("-1" + System.lineSeparator()).getBytes());
				sc.close();
				return;
			}

			if (k == n || a.get(n - k) > a.get(n - k - 1)) {
				os.write((a.get(n - k) + " " + a.get(n - k) + System
						.lineSeparator()).getBytes());
			}

			sc.close();
		}
	}

}
