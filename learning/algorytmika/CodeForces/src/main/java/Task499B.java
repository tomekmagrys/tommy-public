import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Task499B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();

            Map<String, String> translations = new HashMap<>();

            for (int i = 0; i < m; i++) {
                String tmp1 = sc.next();
                String tmp2 = sc.next();

                translations.put(tmp1, tmp2);
                translations.put(tmp2, tmp1);

            }

            for(int i = 0 ; i < n ; i++){
                String tmp  = sc.next();

                if(translations.get(tmp).length() < tmp.length()){
                    pw.print(translations.get(tmp));
                } else {
                    pw.print(tmp);
                }
                pw.print(" ");

            }

            pw.flush();
            sc.close();
        }
    }

}