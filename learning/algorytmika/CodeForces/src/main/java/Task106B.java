import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Task106B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<ArrayList<Integer>> laptops = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				ArrayList<Integer> laptop = new ArrayList<>(5);
				for (int j = 0; j < 4; j++) {
					laptop.add(s.nextInt());
				}
				laptop.add(i+1);
				laptops.add(laptop);
			}

			Iterator<ArrayList<Integer>> iter = laptops.iterator();

			while (iter.hasNext()) {
				ArrayList<Integer> laptop = iter.next();
				for (ArrayList<Integer> another : laptops) {
					if (laptop.get(0) < another.get(0)
							&& laptop.get(1) < another.get(1)
							&& laptop.get(2) < another.get(2)) {
						iter.remove();
						break;
					}
				}
			}

			int minCost = Integer.MAX_VALUE;
			for (ArrayList<Integer> laptop : laptops) {
				minCost = Math.min(minCost,laptop.get(3));
			}
			
			
			for (ArrayList<Integer> laptop : laptops) {
				if(laptop.get(3) == minCost){
					pw.println(laptop.get(4));
					break;
				}
			}
			pw.flush();
			s.close();
		}
	}

}