import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task177A1 {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			int a[][] = new int[n][];
			for (int i = 0; i < n; i++) {
				a[i] = new int[n];
			}

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					a[i][j] = sc.nextInt();
				}
			}

			int retVal = 0;

			for (int i = 0; i < n; i++) {
				retVal += a[i][i];
				retVal += a[i][n - i - 1];
				retVal += a[i][n / 2];
				retVal += a[n / 2][i];
			}

			retVal -= 3 * a[n / 2][n / 2];

			pw.print(retVal);
			pw.flush();
			sc.close();
		}
	}

}