import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task390A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int room[][] = new int[101][];
			for (int i = 0; i <= 100; i++) {
				room[i] = new int[101];
			}

			int n = sc.nextInt();

			for (int i = 0; i < n; i++) {
				room[sc.nextInt()][sc.nextInt()]++;
			}

			int nonZeroRow = 0;
			int nonZeroColumn = 0;

			for (int i = 0; i <= 100; i++) {
				for (int j = 0; j <= 100; j++) {
					if (room[i][j] != 0) {
						nonZeroRow++;
						break;
					}
				}
			}

			for (int i = 0; i <= 100; i++) {
				for (int j = 0; j <= 100; j++) {
					if (room[j][i] != 0) {
						nonZeroColumn++;
						break;
					}
				}
			}

			pw.println(Math.min(nonZeroRow, nonZeroColumn));
			pw.flush();
			sc.close();
		}
	}

}