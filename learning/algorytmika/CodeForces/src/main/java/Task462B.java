import java.io.*;
import java.util.*;

public class Task462B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int k = sc.nextInt();

            String cards = sc.next();

            Map<Character, Integer> counts = new TreeMap<>();

            for (char c = 'A'; c <= 'Z'; c++) {
                counts.put(c, 0);
            }

            for (char c : cards.toCharArray()) {
                int curCount = counts.get(c);
                counts.put(c, curCount + 1);
            }

            ArrayList<Integer> values = new ArrayList<>(counts.values());

            Collections.sort(values, Collections.reverseOrder());

            long retVal = 0;

            for(int value : values){
                long k2 = Math.min(k,value);
                retVal += k2 * k2;
                k -= k2;
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}