import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task259B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();

			int d = s.nextInt();
			s.nextInt();
			int f = s.nextInt();

			int g = s.nextInt();
			int h = s.nextInt();
			s.nextInt();

			int e = (b + h) / 2;
			int a = (d + e + f) - (b + c);
			int i = (d + e + f) - (g + h);

			pw.write(a + " " + b + " " + c + "\n");
			pw.write(d + " " + e + " " + f + "\n");
			pw.write(g + " " + h + " " + i + "\n");

			pw.flush();
			s.close();
		}
	}

}
