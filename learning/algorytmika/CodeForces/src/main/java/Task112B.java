import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task112B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt() / 2;
			int x = sc.nextInt();
			int y = sc.nextInt();

			if ((x == n || x == n + 1) && (y == n || y == n + 1)) {
				pw.println("NO");
			} else {
				pw.println("YES");
			}

			pw.flush();
			sc.close();
		}
	}

}