import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task060A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();
			sc.nextLine();

			int min = 1;
			int max = n;

			for (int i = 0; i < m; i++) {
				String tmp = sc.nextLine();
				if (tmp.startsWith("To the right of ")) {
					min = Math.max(min, Integer.parseInt(tmp
							.substring("To the right of ".length())) + 1);
				}
				if (tmp.startsWith("To the left of ")) {
					max = Math.min(max, Integer.parseInt(tmp
							.substring("To the left of ".length())) - 1);
				}
			}

			if (min <= max) {
				pw.println(max - min + 1);
			} else {
				pw.println("-1");
			}

			pw.flush();
			sc.close();
		}
	}

}