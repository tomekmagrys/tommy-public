import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Task122B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static boolean isMagicNumber(String s) {
			for (char c : s.toCharArray()) {
				if (c != '4' && c != '7') {
					return false;
				}
			}
			return true;
		}

		private static int stringOccurenceCount(String str, String findStr) {
			int lastIndex = 0;
			int count = 0;

			while (lastIndex != -1) {

				lastIndex = str.indexOf(findStr, lastIndex);

				if (lastIndex != -1) {
					count++;
					lastIndex += findStr.length();
				}
			}
			return count;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();

			Set<String> candidatesForMagicNumbers = new TreeSet<>();

			for (int i = 0; i < input.length(); i++) {
				for (int j = i + 1; j <= input.length(); j++) {
					candidatesForMagicNumbers.add(input.substring(i, j));
				}
			}

			Iterator<String> iter = candidatesForMagicNumbers.iterator();

			while (iter.hasNext()) {
				if (!isMagicNumber(iter.next())) {
					iter.remove();
				}
			}

			Map<String, Integer> magicNumberCount = new TreeMap<>();

			for (String magicNUmber : candidatesForMagicNumbers) {
				magicNumberCount.put(magicNUmber,
						stringOccurenceCount(input, magicNUmber));
			}

			if (magicNumberCount.size() == 0) {
				pw.write("-1");
			} else {
				int max = Collections.max(magicNumberCount.values());
				Iterator<String> iter2 = magicNumberCount.keySet().iterator();
				while (iter2.hasNext()) {
					String tmp = iter2.next();
					if (magicNumberCount.get(tmp) == max) {
						pw.write(tmp);
						break;
					}
				}
				

			}

			pw.flush();
			s.close();
		}
	}

}