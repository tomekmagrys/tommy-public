import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task629B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            List<Integer> m = new ArrayList<>(367);
            List<Integer> f = new ArrayList<>(367);

            for (int i = 0; i <= 366; i++) {
                m.add(0);
                f.add(0);
            }

            for (int i = 0; i < n; i++) {
                if ("M".equals(sc.next())) {
                    int a = sc.nextInt();
                    int b = sc.nextInt();
                    for (int j = a; j <= b; j++) {
                        m.set(j, m.get(j) + 1);
                    }
                } else {
                    int a = sc.nextInt();
                    int b = sc.nextInt();
                    for (int j = a; j <= b; j++) {
                        f.set(j, f.get(j) + 1);
                    }
                }
            }

            int retVal = 0;

            for (int i = 1; i <= 366; i++) {
                retVal = Math.max(retVal, Math.min(m.get(i), f.get(i)));
            }

            pw.println(2 * retVal);

            pw.flush();
            sc.close();

        }
    }

}