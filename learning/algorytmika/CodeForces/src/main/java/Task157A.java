import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task157A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			ArrayList<ArrayList<Integer>> board = new ArrayList<>(n);
			
			for(int i = 0 ; i < n ; i++){
				ArrayList<Integer> row = new ArrayList<>(n);
				for(int j = 0 ; j < n ; j++){
					row.add(s.nextInt());
				}
				board.add(row);
			}
			
			int retVal = 0;
			
			for(int i = 0 ; i < n ; i++){
				for(int j = 0 ; j < n ; j++){
					int rowSum = 0;
					for(int k = 0 ; k < n ; k++) rowSum += board.get(i).get(k);
					int columnSum = 0;
					for(int k = 0 ; k < n ; k++) columnSum += board.get(k).get(j);
					retVal += (columnSum > rowSum ? 1 : 0);
				}
			}
			
			pw.write(retVal + "");
			
			pw.flush();
			s.close();
		}
	}

}
