import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task347B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> permutation = new ArrayList<>(n);
			for(int i = 0; i < n ; i++){
				permutation.add(s.nextInt());
			}
			
			int maxIncFixedPoint = 0;
			int fixedPointCount = 0;
			for(int i = 0 ; i < n ; i++){
				if(permutation.get(i) != i){
					if(permutation.get(permutation.get(i)) == i){
						maxIncFixedPoint = 2;
					} else {
						maxIncFixedPoint = Math.max(maxIncFixedPoint, 1);
					}
				} else {
					fixedPointCount++;
				}
			}
			pw.write(fixedPointCount+maxIncFixedPoint+"");
			pw.flush();
			s.close();
		}
	}

}
