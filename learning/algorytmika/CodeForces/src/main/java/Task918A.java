import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task918A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            List<Integer> fib = new ArrayList<>(1000);

            fib.add(1);
            fib.add(2);

            while(true){
                int tmp = fib.get(fib.size()-1) + fib.get(fib.size() -2);
                if(tmp > 1000){
                    break;
                }
                fib.add(tmp);
            }

            int n = sc.nextInt();

            for(int i = 1 ; i <= n ; i++){
                if(fib.contains(i)){
                    pw.print("O");
                } else {
                    pw.print("o");
                }
            }


            pw.flush();
            sc.close();
        }
    }

}