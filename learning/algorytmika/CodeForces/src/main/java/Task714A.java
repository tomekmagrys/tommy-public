import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task714A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);


            long l1 = sc.nextLong();
            long r1 = sc.nextLong();

            long l2 = sc.nextLong();
            long r2 = sc.nextLong();

            long k = sc.nextLong();

            if (l1 >= l2 && r1 <= r2) {
                pw.println(r1 - l1 + 1 - ((k >= l1 && k <= r1) ? 1 : 0));
            } else if (l2 >= l1 && r2 <= r1) {
                pw.println(r2 - l2 + 1 - ((k >= l2 && k <= r2) ? 1 : 0));
            } else if (l1 <= r2 && r1 >= r2) {
                pw.println(r2 - l1 + 1 - ((k >= l1 && k <= r2) ? 1 : 0));
            } else if (l1 <= l2 && r1 >= l2) {
                pw.println(r1 - l2 + 1 - ((k >= l2 && k <= r1) ? 1 : 0));
            } else {
                pw.println(0);
            }


            pw.flush();
            sc.close();

        }
    }

}