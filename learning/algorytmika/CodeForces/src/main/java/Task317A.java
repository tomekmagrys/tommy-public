import java.io.*;

public class Task317A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {


        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long x = sc.nextLong();
            long y = sc.nextLong();
            long m = sc.nextLong();

            if(x >= m || y >= m){
                pw.println(0);
            } else {
                if(x <= 0 && y <= 0){
                    pw.println(-1);
                }
                else{
                    if(x < y){
                        long tmp = x;
                        x = y;
                        y = tmp;
                    }
                    long retVal = 0;
                    if (y < 0)
                    {
                        long n = (-y) / x + 1;
                        retVal += n;
                        y += x * n;
                    }
                    while (x < m)
                    {
                        y += x;
                        long tmp = x;
                        x = y;
                        y = tmp;
                        retVal += 1;
                    }
                    pw.println(retVal);
                }
            }

            pw.flush();
            sc.close();
        }
    }

}