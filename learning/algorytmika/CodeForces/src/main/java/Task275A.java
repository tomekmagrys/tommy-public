import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task275A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int lights[][] = new int[3][3];

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					lights[i][j] = 1;
				}
			}

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					boolean zwitch = sc.nextInt() % 2 == 1;
					if (!zwitch)
						continue;
					lights[i][j] = 1 - lights[i][j];
					if (i > 0) {
						lights[i - 1][j] = 1 - lights[i - 1][j];
					}
					if (i < 2) {
						lights[i + 1][j] = 1 - lights[i + 1][j];
					}
					if (j > 0) {
						lights[i][j - 1] = 1 - lights[i][j - 1];
					}
					if (j < 2) {
						lights[i][j + 1] = 1 - lights[i][j + 1];
					}
				}
			}

			for (int i = 0; i < 3; i++) {
				os.write(("" + lights[i][0] + lights[i][1] + lights[i][2] + System
						.lineSeparator()).getBytes());
			}
			sc.close();
		}
	}

}
