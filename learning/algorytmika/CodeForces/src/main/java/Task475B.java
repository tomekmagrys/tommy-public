import java.io.*;

public class Task475B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();

            String h = sc.next();
            String v = sc.next();

            boolean retVal = true;
            if (h.charAt(0) == '>' && v.charAt(0) == 'v') {
                retVal = false;
            }
            if (h.charAt(0) == '<' && v.charAt(m - 1) == 'v') {
                retVal = false;
            }
            if (h.charAt(n - 1) == '>' && v.charAt(0) == '^') {
                retVal = false;
            }
            if (h.charAt(n - 1) == '<' && v.charAt(m - 1) == '^') {
                retVal = false;
            }

            pw.println(retVal ? "YES" : "NO");

            pw.flush();
            sc.close();
        }
    }

}