import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task441C {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Pair<T1, T2> {
        T1 x;
        T2 y;

        public Pair(T1 x, T2 y) {
            this.x = x;
            this.y = y;
        }
    }

    static class PairII extends Pair<Integer, Integer> {
        public PairII(Integer x, Integer y) {
            super(x, y);
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();
            int k = sc.nextInt();

            List<String> retVal = new ArrayList<>(n * m);

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    if (i % 2 == 1) {
                        retVal.add(i + " " + j + " ");
                    } else {
                        retVal.add(i + " " + (m + 1 - j) + " ");
                    }
                }
            }

            int smallerSize = (n * m) / k;
            int biggerSize = smallerSize + 1;

            int biggerCount = (n * m) % k;
            int smallerCount = k - biggerCount;

            int i = 0;

            for(int j = 0 ; j < smallerCount ; j++){
                pw.print(smallerSize);
                pw.print(" ");
                for(int g = 0 ; g < smallerSize ; g++){
                    pw.print(retVal.get(i++));
                }
                pw.println();
            }

            for(int j = 0 ; j < biggerCount ; j++){
                pw.print(biggerSize);
                pw.print(" ");
                for(int g = 0 ; g < biggerSize ; g++){
                    pw.print(retVal.get(i++));
                }
                pw.println();
            }

            pw.flush();
            sc.close();
        }
    }

}