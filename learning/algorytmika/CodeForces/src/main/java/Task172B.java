import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task172B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int a = sc.nextInt();
			int b = sc.nextInt();
			int m = sc.nextInt();

			ArrayList<Integer> r = new ArrayList<>();
			Set<Integer> rSet = new TreeSet<>();

			int r0 = sc.nextInt();
			r.add(r0);
			rSet.add(r0);

			while (!rSet.contains((a * r0 + b) % m)) {
				r0 = (a * r0 + b) % m;
				r.add(r0);
				rSet.add(r0);
			}

			int retVal = 1;

			for (;; retVal++) {
				if (r.get(r.size() - retVal) == (a * r0 + b) % m) {
					break;
				}
			}

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}