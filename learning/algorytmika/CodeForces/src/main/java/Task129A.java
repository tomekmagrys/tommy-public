import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task129A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int suma = 0;
			int iloscParzystych = 0;
			int iloscNieparzystych = 0;

			for (int i = 0; i < n; i++) {
				int tmp = s.nextInt();
				suma += tmp;
				if(tmp % 2 == 0){
					iloscParzystych++;
				} else {
					iloscNieparzystych++;
				}
			}
			if(suma % 2 == 0){
				pw.print(iloscParzystych);
			} else {
				pw.print(iloscNieparzystych);
			}
			pw.print("\n");
			pw.flush();
			s.close();
		}
	}

}
