import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task296A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
	        int[] count = new int[1001];

	        for(int i = 1 ; i <= n ; i++){
	        	count[s.nextInt()]++;
	        }
	        
	        int max = Integer.MIN_VALUE;
	        
	        for(int i = 1 ; i <= 1000 ; i++){
	        	max = Math.max(max,count[i]);
	        }
	        
	        pw.write(max <= (n+1)/2 ? "YES" : "NO" );
	        
			pw.flush();
			s.close();
		}
	}

}
