import java.io.*;

public class Task475A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int k = sc.nextInt();

            char [][] output = {
                    "+------------------------+".toCharArray(),
                    "|#.#.#.#.#.#.#.#.#.#.#.|D|)".toCharArray(),
                    "|#.#.#.#.#.#.#.#.#.#.#.|.|".toCharArray(),
                    "|#.......................|".toCharArray(),
                    "|#.#.#.#.#.#.#.#.#.#.#.|.|)".toCharArray(),
                    "+------------------------+".toCharArray(),
            };

            for(int i = 0 ; i < k ; i++){
                if(i < 4){
                    output[i+1][1] = 'O';
                    continue;
                }
                int j = i-4;
                output[j % 3 < 2 ? j % 3 + 1 : j % 3 + 2][j/3 * 2 + 3] = 'O';
            }

            pw.println(output[0]);
            pw.println(output[1]);
            pw.println(output[2]);
            pw.println(output[3]);
            pw.println(output[4]);
            pw.println(output[5]);

            pw.flush();
            sc.close();
        }
    }

}