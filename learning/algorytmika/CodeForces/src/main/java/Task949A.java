import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task949A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);


            char[] in = sc.next().toCharArray();

            List<List<Integer>> zebrasEndingWith0 = new ArrayList<>();
            List<List<Integer>> zebrasEndingWith1 = new ArrayList<>();


            for (int i = 1; i <= in.length; i++) {
                if (in[i - 1] == '0') {
                    if (zebrasEndingWith1.isEmpty()) {
                        List<Integer> toAdd = new ArrayList<>();
                        toAdd.add(i);
                        zebrasEndingWith0.add(toAdd);
                    } else {
                        List<Integer> toAdd = zebrasEndingWith1.remove(zebrasEndingWith1.size() - 1);
                        toAdd.add(i);
                        zebrasEndingWith0.add(toAdd);
                    }
                } else {
                    if (zebrasEndingWith0.isEmpty()) {
                        pw.println(-1);
                        pw.flush();
                        sc.close();
                        return;
                    } else {
                        List<Integer> toAdd = zebrasEndingWith0.remove(zebrasEndingWith0.size() - 1);
                        toAdd.add(i);
                        zebrasEndingWith1.add(toAdd);
                    }
                }
            }


            if(zebrasEndingWith1.isEmpty()){
                pw.println(zebrasEndingWith0.size());
                for(List<Integer> row : zebrasEndingWith0){
                    pw.print(row.size());
                    for(int i : row){
                        pw.print(" ");
                        pw.print(i);
                    }
                    pw.println();
                }
            } else {
                pw.println(-1);
            }


            pw.flush();
            sc.close();

        }
    }

}