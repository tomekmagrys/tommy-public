import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Task400C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		long nextLong() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Long.parseLong(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			int x = sc.nextInt() % 4;
			int y = sc.nextInt() % 2;
			int z = sc.nextInt() % 4;

			if(z != 0){
				z = 4 - z;
			}

			int p = sc.nextInt();

			List<Integer> xS = new ArrayList<>(p);
			List<Integer> yS = new ArrayList<>(p);

			for(int i = 0 ; i < p ; i++){
				xS.add(sc.nextInt());
				yS.add(sc.nextInt());
			}

			for(int j = 0 ; j < x ; j++){
				for(int i = 0 ; i < p ; i++){
					int newX = yS.get(i);
					int newY = n + 1 - xS.get(i);
					yS.set(i,newY);
					xS.set(i,newX);
				}
				int tmp = n;
				n = m;
				m = tmp;
			}

			if(y != 0){
				for(int i = 0 ; i < p ; i++){
					int newY = m + 1 - yS.get(i);
					yS.set(i,newY);
				}
			}

			for(int j = 0 ; j < z ; j++){
				for(int i = 0 ; i < p ; i++){
					int newX = yS.get(i);
					int newY = n + 1 - xS.get(i);
					yS.set(i,newY);
					xS.set(i,newX);
				}
				int tmp = n;
				n = m;
				m = tmp;
			}

			for(int i = 0 ; i < p ; i++){
				pw.print(xS.get(i));
				pw.print(" ");
				pw.println(yS.get(i));
			}
			pw.flush();
			sc.close();
		}
	}

}