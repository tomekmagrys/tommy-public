import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Task887B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            long n = sc.nextLong();

            Set<Integer> possibleNumbers = new HashSet<>();

            Set<Integer> dices1 = new HashSet<>(6);
            Set<Integer> dices2 = new HashSet<>(6);
            Set<Integer> dices3 = new HashSet<>(6);

            for (int i = 0; i < 6; i++) {
                int tmp = sc.nextInt();
//                if (tmp == 6 || tmp == 9) {
//                    dices1.add(6);
//                    dices1.add(9);
//                }
                dices1.add(tmp);
            }

            if (n >= 2) {
                for (int i = 0; i < 6; i++) {
                    int tmp = sc.nextInt();
//                    if (tmp == 6 || tmp == 9) {
//                        dices2.add(6);
//                        dices2.add(9);
//                    }
                    dices2.add(tmp);
                }
            }

            if (n >= 3) {
                for (int i = 0; i < 6; i++) {
                    int tmp = sc.nextInt();
//                    if (tmp == 6 || tmp == 9) {
//                        dices3.add(6);
//                        dices3.add(9);
//                    }
                    dices3.add(tmp);
                }
            }

            for (Integer dice1 : dices1) {
                if (dice1 != 0) {
                    possibleNumbers.add(dice1);
                }
            }

            for (Integer dice2 : dices2) {
                if (dice2 != 0) {
                    possibleNumbers.add(dice2);
                }
            }

            for (Integer dice3 : dices3) {
                if (dice3 != 0) {
                    possibleNumbers.add(dice3);
                }
            }

            for (Integer dice1 : dices1) {
                for (Integer dice2 : dices2) {
                    if (dice1 != 0) {
                        possibleNumbers.add(dice1 * 10 + dice2);
                    }
                    if (dice2 != 0) {
                        possibleNumbers.add(dice2 * 10 + dice1);
                    }
                }
            }

            for (Integer dice2 : dices2) {
                for (Integer dice3 : dices3) {
                    if (dice2 != 0) {
                        possibleNumbers.add(dice2 * 10 + dice3);
                    }
                    if (dice3 != 0) {
                        possibleNumbers.add(dice3 * 10 + dice2);
                    }
                }
            }

            for (Integer dice1 : dices1) {
                for (Integer dice3 : dices3) {
                    if (dice1 != 0) {
                        possibleNumbers.add(dice1 * 10 + dice3);
                    }
                    if (dice3 != 0) {
                        possibleNumbers.add(dice3 * 10 + dice1);
                    }
                }
            }

            for (Integer dice1 : dices1) {
                for (Integer dice2 : dices2) {
                    for (Integer dice3 : dices3) {
                        if (dice1 != 0) {
                            possibleNumbers.add(dice1 * 100 + 10 * dice2 + dice3);
                            possibleNumbers.add(dice1 * 100 + 10 * dice3 + dice2);
                        }
                        if (dice2 != 0) {
                            possibleNumbers.add(dice2 * 100 + 10 * dice1 + dice3);
                            possibleNumbers.add(dice2 * 100 + 10 * dice3 + dice1);
                        }
                        if (dice3 != 0) {
                            possibleNumbers.add(dice3 * 100 + 10 * dice1 + dice2);
                            possibleNumbers.add(dice3 * 100 + 10 * dice2 + dice1);
                        }
                    }
                }
            }

            int retVal = 0;

            while (possibleNumbers.contains(retVal + 1)) {
                retVal++;
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}