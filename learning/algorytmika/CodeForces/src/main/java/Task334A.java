import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task334A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);
			int n = s.nextInt();
			
			ArrayList<ArrayList<Integer>> bags = new ArrayList<>(n);
			for(int i = 0 ; i < n ; i++){
				bags.add(new ArrayList<Integer>(n));
			}
			int bagNumber = 1;
			for(int i = 0 ; i < n ; i++){
				for(int j = 0 ; j < n ; j++){
					bags.get(i % 2 == 0 ? j : n-1-j).add(bagNumber++);
				}
			}
			for(int i = 0 ; i < n ; i++){
				for(int j = 0 ; j < n ; j++){
					pw.print(bags.get(i).get(j)+" ");
				}
				pw.write("\n");
			}
			pw.flush();
			s.close();
		}
	}

}
