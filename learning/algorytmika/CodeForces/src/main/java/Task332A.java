import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task332A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			String game = s.next();

			int retVal = 0;

			for (int i = n; i < game.length(); i += n) {
				if (game.charAt(i - 1) == game.charAt(i - 2)
						&& game.charAt(i - 2) == game.charAt(i - 3)) {
					retVal++;
				}
			}

			pw.write("" + retVal);

			pw.flush();
			s.close();
		}
	}

}