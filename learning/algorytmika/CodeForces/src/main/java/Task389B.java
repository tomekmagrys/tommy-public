import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task389B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			char board[][] = new char[n][];

			for (int i = 0; i < n; i++) {
				board[i] = sc.next().toCharArray();
			}

			for (int i = 1; i + 1 < n; i++) {
				for (int j = 1; j + 1 < n; j++) {
					if (board[i][j] == '#' && board[i + 1][j] == '#'
							&& board[i][j + 1] == '#' && board[i - 1][j] == '#'
							&& board[i][j - 1] == '#') {
						board[i][j] = '.';
						board[i + 1][j] = '.';
						board[i][j + 1] = '.';
						board[i - 1][j] = '.';
						board[i][j - 1] = '.';
					}
				}
			}

			boolean retVal = true;

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					retVal = retVal && (board[i][j] == '.');
				}
			}

			pw.println(retVal ? "YES" : "NO");

			pw.flush();
			sc.close();
		}
	}

}