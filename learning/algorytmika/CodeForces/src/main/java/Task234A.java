import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task234A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			String input = sc.next();

			for (int i = 0; i < n / 2; i++) {
				int l = i;
				int r = n / 2 + i;

				if (input.charAt(l) == 'R' && input.charAt(r) == 'L') {
					int tmp = l;
					l = r;
					r = tmp;
				}

				pw.print(l + 1);
				pw.print(" ");
				pw.println(r + 1);
			}

			pw.flush();
			sc.close();
		}
	}

}