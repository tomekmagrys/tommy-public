import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task278B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = Integer.parseInt(s.nextLine());

			Set<String> titles = new TreeSet<>();
			for (int i = 0; i < n; i++) {
				titles.add(s.nextLine());
			}

			String retVal = null;

			for (char lit = 'a'; lit <= 'z'; lit++) {
				boolean ok = true;
				for (String title : titles) {
					if (title.indexOf("" + lit) >= 0) {
						ok = false;
						break;
					}
				}
				if (ok) {
					retVal = "" + lit;
					break;
				}
			}

			if (retVal == null) {
				for (char lit = 'a'; lit <= 'z'; lit++) {
					for (char lit2 = 'a'; lit2 <= 'z'; lit2++) {
						boolean ok = true;
						for (String title : titles) {
							if (title.indexOf("" + lit + lit2) >= 0) {
								ok = false;
								break;
							}
						}
						if (ok) {
							retVal = "" + lit + lit2;
							break;
						}
					}
					if (retVal != null)
						break;
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}