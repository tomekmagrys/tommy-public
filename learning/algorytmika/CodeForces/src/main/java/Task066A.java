import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task066A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.nextLine();

			try {
				Byte.parseByte(input);
				pw.println("byte");
			} catch (NumberFormatException e) {
				try {
					Short.parseShort(input);
					pw.println("short");
				} catch (NumberFormatException e2) {
					try {
						Integer.parseInt(input);
						pw.println("int");
					} catch (NumberFormatException e3) {
						try {
							Long.parseLong(input);
							pw.println("long");
						} catch (NumberFormatException e4) {
							pw.println("BigInteger");
						}
					}
				}
			}

			pw.flush();
			s.close();
		}
	}

}
