import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task378A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();

			int win = 0;
			int draw = 0;
			int lose = 0;

			for (int i = 1; i <= 6; i++) {
				if (Math.abs(i - a) < Math.abs(i - b)) {
					win++;
				}
				if (Math.abs(i - a) == Math.abs(i - b)) {
					draw++;
				}

				if (Math.abs(i - a) > Math.abs(i - b)) {
					lose++;
				}

			}

			pw.println(win + " " + draw + " " + lose);
			pw.flush();

			s.close();
		}
	}

}