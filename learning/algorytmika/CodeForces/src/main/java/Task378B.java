import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task378B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);
			ArrayList<Integer> b = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
				b.add(s.nextInt());
			}

			int i = 0;
			int j = 0;

			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();

			while (i < a.size() && j < b.size()) {
				if (a.get(i) < b.get(j)) {
					if (i + j < n || i < n / 2) {
						sb1.append("1");
					} else {
						sb1.append("0");
					}
					i++;
				} else {
					if (i + j < n || j < n / 2) {
						sb2.append("1");
					} else {
						sb2.append("0");
					}
					j++;
				}
			}

			while (i < a.size()) {
				if (i + j < n || i < n / 2) {
					sb1.append("1");
				} else {
					sb1.append("0");
				}
				i++;
			}

			while (j < b.size()) {
				if (i + j < n || j < n / 2) {
					sb2.append("1");
				} else {
					sb2.append("0");
				}
				j++;
			}
			pw.println(sb1);
			pw.println(sb2);
			pw.flush();
			s.close();
		}
	}

}
