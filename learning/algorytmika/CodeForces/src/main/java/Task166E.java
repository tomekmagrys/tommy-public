import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task166E {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int a[] = new int[n + 1];
			int b[] = new int[n + 1];
			int c[] = new int[n + 1];
			int d[] = new int[n + 1];

			d[0] = 1;

			for (int i = 1; i <= n; i++) {
				a[i] = ((b[i - 1] + c[i - 1]) % 1000000007 + d[i - 1]) % 1000000007;
				b[i] = ((c[i - 1] + d[i - 1]) % 1000000007 + a[i - 1]) % 1000000007;
				c[i] = ((d[i - 1] + a[i - 1]) % 1000000007 + b[i - 1]) % 1000000007;
				d[i] = ((a[i - 1] + b[i - 1]) % 1000000007 + c[i - 1]) % 1000000007;
			}

			pw.println(d[n]);
			
			pw.flush();
			s.close();
		}
	}

}