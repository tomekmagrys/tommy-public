import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task197A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int a = sc.nextInt();
			int b = sc.nextInt();
			int r = sc.nextInt();
			
			if(2*r <= a && 2*r <= b){
				pw.println("First");
			} else {
				pw.println("Second");
			}
			
			pw.flush();
			sc.close();
		}
	}

}