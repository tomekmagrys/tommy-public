import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task253A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		FileInputStream a = new FileInputStream(new File("input.txt"));
		FileOutputStream b = new FileOutputStream(new File("output.txt"));
		Solution.main(a, b);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int b = s.nextInt();
			int g = s.nextInt();

			if (b == g) {
				for (int i = 0; i < b; i++) {
					pw.write("BG");
				}
			} else if (b == g - 1) {
				pw.write("G");
				for (int i = 0; i < b; i++) {
					pw.write("BG");
				}
			} else if (b == g + 1) {
				for (int i = 0; i < g; i++) {
					pw.write("BG");
				}
				pw.write("B");
			} else if (b > g) {
				for (int i = 0; i < g; i++) {
					pw.write("BG");
				}
				for (int i = 0; i < b - g; i++) {
					pw.write("B");
				}
			} else {
				pw.write("G");
				for (int i = 0; i < b; i++) {
					pw.write("BG");
				}
				for (int i = 0; i < g - b - 1; i++) {
					pw.write("G");
				}
			}

			pw.flush();
			s.close();
		}
	}

}