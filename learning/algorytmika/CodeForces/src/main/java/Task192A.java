import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task192A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		private static boolean isTriangle(int n) {
			int cand = (int) Math.sqrt(2 * n);
			return cand * (cand + 1) == 2 * n;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			boolean answer = false;

			for (int k = 1; k * (k + 1) / 2 < n; k++) {
				if (isTriangle(n - k * (k + 1) / 2)) {
					answer = true;
					break;
				}
			}

			pw.println(answer ? "YES" : "NO");

			s.close();
			pw.flush();
		}

	}

}
