import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task349A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			int ilosc25 = 0;
			int ilosc50 = 0;

			boolean answer = true;

			for (int i = 0; i < n; i++) {
				int tmp = s.nextInt();
				if (tmp == 25) {
					ilosc25++;
				}
				if (tmp == 50) {
					ilosc50++;
					ilosc25--;
				}
				if (tmp == 100) {
					if (ilosc50 > 0) {
						ilosc50--;
						ilosc25--;
					} else {
						ilosc25 -= 3;
					}
				}
				if (ilosc25 < 0 || ilosc50 < 0) {
					answer = false;
				}
			}

			pw.write((answer ? "YES" : "NO") + "\n");
			pw.flush();
			s.close();
		}
	}

}
