import java.io.*;

public class Task779A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);


            int n = sc.nextInt();

            int a1 = 0;
            int a2 = 0;
            int a3 = 0;
            int a4 = 0;
            int a5 = 0;

            int b1 = 0;
            int b2 = 0;
            int b3 = 0;
            int b4 = 0;
            int b5 = 0;


            for (int i = 0; i < n; i++) {
                switch (sc.nextInt()) {
                    case 1:
                        a1++;
                        break;
                    case 2:
                        a2++;
                        break;
                    case 3:
                        a3++;
                        break;
                    case 4:
                        a4++;
                        break;
                    case 5:
                        a5++;
                        break;
                }
            }

            for (int i = 0; i < n; i++) {
                switch (sc.nextInt()) {
                    case 1:
                        b1++;
                        break;
                    case 2:
                        b2++;
                        break;
                    case 3:
                        b3++;
                        break;
                    case 4:
                        b4++;
                        break;
                    case 5:
                        b5++;
                        break;
                }
            }

            if ((a1 + b1) % 2 == 0 &&
                    (a2 + b2) % 2 == 0 &&
                    (a3 + b3) % 2 == 0 &&
                    (a4 + b4) % 2 == 0 &&
                    (a5 + b5) % 2 == 0) {

                int avg1 = (a1 + b1) / 2;
                int avg2 = (a2 + b2) / 2;
                int avg3 = (a3 + b3) / 2;
                int avg4 = (a4 + b4) / 2;
                int avg5 = (a5 + b5) / 2;

                int retVal = Math.abs(a1 - avg1) +
                        Math.abs(a2 - avg2) +
                        Math.abs(a3 - avg3) +
                        Math.abs(a4 - avg4) +
                        Math.abs(a5 - avg5);

                pw.println(retVal / 2);

            } else {
                pw.println(-1);
            }


            pw.flush();
            sc.close();

        }
    }

}