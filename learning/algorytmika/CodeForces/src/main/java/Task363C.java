import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task363C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			char inp[] = sc.next().toCharArray();

			char out[] = new char[inp.length];
			int retVal = 0;

			for (int i = 0; i < inp.length; i++) {
				if (i < 2) {
					out[retVal++] = inp[i];
					continue;
				}
				if (inp[i] == out[retVal - 1]
						&& out[retVal - 1] == out[retVal - 2]) {
					continue;
				}
				if (retVal < 3) {
					out[retVal++] = inp[i];
					continue;
				}
				if (inp[i] == out[retVal - 1]
						&& out[retVal - 2] == out[retVal - 3]) {
					continue;
				}
				out[retVal++] = inp[i];
			}

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < retVal; i++) {
				sb.append(out[i]);
			}
			pw.println(sb.toString());

			pw.flush();
			sc.close();
		}
	}

}