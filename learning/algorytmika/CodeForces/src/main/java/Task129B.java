import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TreeSet;

public class Task129B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			ArrayList<TreeSet<Integer>> v = new ArrayList<>(n + 1);

			v.add(null);
			for (int i = 1; i <= n; i++) {
				v.add(new TreeSet<Integer>());
			}

			int m = sc.nextInt();
			for (int i = 0; i < m; i++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				v.get(x).add(y);
				v.get(y).add(x);
			}

			int retVal = 0;

			ArrayList<Integer> toRemove;
			while (true) {
				toRemove = new ArrayList<>();
				for (int i = 1; i <= n; i++) {
					if (v.get(i).size() == 1) {
						toRemove.add(i);
					}
				}

				if (toRemove.isEmpty()) {
					break;
				}

				for (int x : toRemove) {
					if (v.get(x).iterator().hasNext()) {
						int y = v.get(x).iterator().next();
						v.get(x).remove(y);
						v.get(y).remove(x);
					}
				}

				retVal++;
			}

			pw.println(retVal);

			pw.flush();
			sc.close();
		}
	}

}