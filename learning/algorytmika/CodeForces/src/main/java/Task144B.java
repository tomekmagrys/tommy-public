import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Task144B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int xa = s.nextInt();
			int ya = s.nextInt();
			int xb = s.nextInt();
			int yb = s.nextInt();

			ArrayList<Pair<Integer, Integer>> generals = new ArrayList<>();

			for (int i = Math.min(xa, xb); i <= Math.max(xa, xb); i++) {
				generals.add(new Pair<Integer, Integer>(i, ya));
				generals.add(new Pair<Integer, Integer>(i, yb));
			}

			for (int i = Math.min(ya, yb) + 1; i < Math.max(ya, yb); i++) {
				generals.add(new Pair<Integer, Integer>(xa, i));
				generals.add(new Pair<Integer, Integer>(xb, i));
			}

			int n = s.nextInt();

			ArrayList<Integer> x = new ArrayList<>(n);
			ArrayList<Integer> y = new ArrayList<>(n);
			ArrayList<Integer> r = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				x.add(s.nextInt());
				y.add(s.nextInt());
				r.add(s.nextInt());
			}

			int retVal = 0;
			Iterator<Pair<Integer, Integer>> iter = generals.iterator();
			while (iter.hasNext()) {
				Pair<Integer, Integer> general = iter.next();
				boolean leave = false;
				for (int i = 0; i < n; i++) {
					if ((x.get(i) - general.x) * (x.get(i) - general.x)
							+ (y.get(i) - general.y) * (y.get(i) - general.y) <= r
							.get(i) * r.get(i)) {
						leave = true;
						break;
					}
				}
				if (!leave) {
					retVal++;
					iter.remove();
				}
			}

			pw.println(retVal);

			pw.flush();
			s.close();
		}
	}

}
