import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task133B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Solution {

		private static long value(char c) {
			switch (c) {
			case '>':
				return 8;
			case '<':
				return 9;
			case '+':
				return 10;
			case '-':
				return 11;
			case '.':
				return 12;
			case ',':
				return 13;
			case '[':
				return 14;
			case ']':
				return 15;
			}
			throw new UnsupportedOperationException();
		}

		private static int expMod(int base, int radius, int mod) {
			int retVal = 1;
			for (int i = 0; i < radius; i++) {
				retVal *= base;
				retVal %= mod;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();

			long retVal = 0;

			for (int i = 0; i < input.length(); i++) {
				retVal += value(input.charAt(i))*expMod(16,input.length()-1-i,1000003);
				retVal %= 1000003;
			}

			pw.write(retVal + "");

			pw.flush();
			s.close();
		}
	}

}