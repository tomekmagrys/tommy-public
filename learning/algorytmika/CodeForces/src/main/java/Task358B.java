import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task358B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			StringBuilder words = new StringBuilder();
			words.append("<3");
			for (int i = 0; i < n; i++) {
				words.append(s.next());
				words.append("<3");
			}

			String pattern = words.toString();
			
			String decoded = s.next();
			
			int i = 0;
			int j = 0;
			
			while(i < pattern.length() && j < decoded.length() ){
				if(pattern.charAt(i) == decoded.charAt(j)){
					i++;
					j++;
				} else {
					j++;
				}
			}

			if (i != pattern.length()) {
				pw.println("no");
			} else {
				pw.print("yes");
			}

			pw.flush();
			s.close();
		}
	}

}