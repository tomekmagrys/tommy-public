import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task350A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				a.add(s.nextInt());
			}

			ArrayList<Integer> b = new ArrayList<>();
			for (int i = 0; i < m; i++) {
				b.add(s.nextInt());
			}

			int mina = Collections.min(a);
			int maxa = Collections.max(a);

			int minb = Collections.min(b);

			if (minb <= maxa) {
				pw.write("-1");
			} else {
				if (2 * mina >= minb) {
					pw.write("-1");
				} else {
					pw.write(Math.max(2*mina,maxa) + "");
				}
			}

			pw.flush();
			s.close();
		}
	}

}
