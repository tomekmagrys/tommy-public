import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task137B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			int permutation[] = new int[n];
			
			for(int i = 0 ; i < n ; i++){
				int tmp = s.nextInt();
				if(1 <= tmp && tmp <= n){
					permutation[tmp-1]++;
				}
			}
			
			int retVal = 0;
			
			for(int i = 0 ; i < n ; i++){
				if(permutation[i] == 0){
					retVal++;
				}
			}
			
			pw.write(retVal+"");
			
			pw.flush();
			s.close();
		}
	}

}
