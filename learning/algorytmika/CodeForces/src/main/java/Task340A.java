import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task340A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int y = s.nextInt();
			int a = s.nextInt();
			int b = s.nextInt();

			int nwwxy = x * y / gcd(x,y);

			int retVal = b/nwwxy - (a-1)/nwwxy;
			pw.write(retVal+"");
			pw.flush();
			s.close();
		}
	}

}
