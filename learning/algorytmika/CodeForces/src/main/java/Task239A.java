import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task239A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int y = s.nextInt();
			int k = s.nextInt();
			int n = s.nextInt();

			int x = (y + k) / k * k;
			if(x > n){
				pw.write("-1");
			} else {
				while(x <= n){
					pw.write(" "+(x-y));
					x += k;
				}
			}

			pw.flush();
			s.close();
		}
	}

}