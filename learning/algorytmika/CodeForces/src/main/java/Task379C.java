import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Task379C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Pair<T1, T2> {
		T1 x;
		T2 y;

		public Pair(T1 x, T2 y) {
			this.x = x;
			this.y = y;
		}
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}
	
	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			ArrayList<Pair<Integer, Integer>> a = new ArrayList<>(n);
			
			for (int i = 1; i <= n; i++) {
				a.add(new Pair<Integer, Integer>(sc.nextInt(), i));
			}

			Collections.sort(a, new Comparator<Pair<Integer, Integer>>() {

				@Override
				public int compare(Pair<Integer, Integer> arg0,
						Pair<Integer, Integer> arg1) {
					int retVal = arg0.x.compareTo(arg1.x);
					if (retVal != 0) {
						{
							return retVal;
						}
					}
					return arg0.y.compareTo(arg1.y);
				}
			});

			int minUsed = Integer.MIN_VALUE;

			for (int i = 0; i < n; i++) {
				int rating = Math.max(minUsed + 1, a.get(i).x);
				a.get(i).x = rating;
				minUsed = rating;
			}

			Collections.sort(a, new Comparator<Pair<Integer, Integer>>() {

				@Override
				public int compare(Pair<Integer, Integer> arg0,
						Pair<Integer, Integer> arg1) {
					return arg0.y.compareTo(arg1.y);
				}
			});

			for (int i = 0; i < n; i++) {
				pw.print(a.get(i).x);
				pw.print(" ");
			}

			pw.flush();
			sc.close();
		}
	}

}