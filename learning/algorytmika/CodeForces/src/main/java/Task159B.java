import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task159B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			Map<Integer, ArrayList<Integer>> markers = new HashMap<>();
			Map<Integer, ArrayList<Integer>> caps = new HashMap<>();

			for (int i = 0; i < n; i++) {
				int y = s.nextInt();
				int x = s.nextInt();
				if (!markers.containsKey(x)) {
					markers.put(x, new ArrayList<Integer>());
				}
				markers.get(x).add(y);
			}

			for (int i = 0; i < m; i++) {
				int b = s.nextInt();
				int a = s.nextInt();
				if (!caps.containsKey(a)) {
					caps.put(a, new ArrayList<Integer>());
				}
				caps.get(a).add(b);
			}

			int collored = 0;
			int beautifullyCollored = 0;

			for (Integer a : markers.keySet()) {
				if (caps.containsKey(a)) {
					ArrayList<Integer> markerColors = markers.get(a);
					ArrayList<Integer> capColors = caps.get(a);

					collored += Math.min(markerColors.size(), capColors.size());

					Collections.sort(markerColors);
					Collections.sort(capColors);

					int i = 0;
					int j = 0;

					while (i < markerColors.size() && j < capColors.size()) {
						switch (markerColors.get(i).compareTo(capColors.get(j))) {
						case -1:
							i++;
							break;
						case 1:
							j++;
							break;
						case 0:
							i++;
							j++;

							beautifullyCollored++;
						}
					}
				}
			}

			pw.println(collored + " " + beautifullyCollored);
			pw.flush();
			s.close();
		}
	}

}