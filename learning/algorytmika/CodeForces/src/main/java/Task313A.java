import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task313A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int tmp = sc.nextInt();

			List<Integer> candidates = new ArrayList<>();

			candidates.add(tmp);
			candidates.add(tmp / 10);
			candidates.add(tmp / 100 * 10 + tmp % 10);

			os.write((Collections.max(candidates) + System.lineSeparator())
					.getBytes());
			sc.close();
		}
	}

}
