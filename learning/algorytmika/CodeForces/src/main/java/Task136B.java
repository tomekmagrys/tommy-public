import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task136B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int c = s.nextInt();

			int b = 0;
			int power = 1;

			while (a > 0 || c > 0) {
				int r1 = a % 3;
				int r2 = c % 3;
				b += (r2 + 3 - r1) % 3 * power;
				a /= 3;
				c /= 3;
				power *= 3;
			}

			pw.write("" + b);

			pw.flush();
			s.close();
		}
	}

}
