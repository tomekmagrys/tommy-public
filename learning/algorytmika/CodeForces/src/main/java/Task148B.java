import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task148B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int vp = sc.nextInt();
			int vd = sc.nextInt();

			int t = sc.nextInt();
			int f = sc.nextInt();

			int c = sc.nextInt();

			double xp = t * vp;
			double xd = 0;
			int bijous = 0;

			if (vp < vd) {
				while (true) {
					double time = (xp - xd) / (vd - vp);
					xp += time * vp;
					xd += time * vd;
					if (xp > c - 0.0000001) {
						break;
					}
					bijous++;
					xp += ((xd / vd) + f) * vp;
					xd = 0.0;
				}
			}

			pw.println(bijous);
			pw.flush();
			sc.close();
		}
	}

}