import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task242A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int y = s.nextInt();
			int a = s.nextInt();
			int b = s.nextInt();
			
			int licznik = 0;
			
			for(int c = a ; c <= x ; c++){
				for(int d = b ; d <= y ; d++){
					if(c > d){
						licznik++;
					}
				}
			}
			
			pw.write(licznik+"\n");
			
			for(int c = a ; c <= x ; c++){
				for(int d = b ; d <= y ; d++){
					if(c > d){
						pw.write(c + " " + d + "\n");
					}
				}
			}
			
			pw.flush();
			s.close();
		}
	}

}
