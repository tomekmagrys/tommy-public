import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task344B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();

			int _2x_2y_2z = a + b + c;

			if (_2x_2y_2z % 2 == 1) {
				pw.write("Impossible");
			} else {
				int _x_y_z = _2x_2y_2z / 2;
				int z = _x_y_z - b;
				int x = _x_y_z - c;
				int y = _x_y_z - a;
				if (x < 0 || y < 0 || z < 0) {
					pw.write("Impossible");
				} else if (x == 0 && y == 0) {
					pw.write("Impossible");
				} else if (y == 0 && z == 0) {
					pw.write("Impossible");
				} else if (z == 0 && x == 0) {
					pw.write("Impossible");
				} else {
					pw.write(x + " " + y + " " + z);
				}
			}

			pw.flush();
			s.close();
		}
	}

}