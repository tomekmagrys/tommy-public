import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Task330A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			Scanner sc = new Scanner(is);

			int r = sc.nextInt();
			int c = sc.nextInt();

			byte cake[][] = new byte[r][];
			for (int i = 0; i < r; i++) {
				cake[i] = sc.next().getBytes();
			}
			
			boolean rOk[] = new boolean[r];
			boolean cOk[] = new boolean[c];
			
			for(int i = 0 ; i < r ; i++){
				rOk[i] = true;
			}
			
			for(int j = 0 ; j < c ; j++){
				cOk[j] = true;
			}
			
			for(int i = 0 ; i < r ; i++){
				for(int j = 0 ; j < c ; j++){
					if(cake[i][j] == 'S'){
						rOk[i] = false;
						cOk[j] = false;
					}
				}
			}
			
			int rOkCount = 0;
			for(int i = 0 ; i < r ; i++){
				if(rOk[i]) rOkCount++;
			}
			
			int cOkCount = 0;
			for(int j = 0 ; j < c ; j++){
				if(cOk[j]) cOkCount++;
			}
			
			int result = rOkCount * c + cOkCount * r - rOkCount * cOkCount;
			
			os.write((result + System.lineSeparator()).getBytes());
			sc.close();
		}
	}

}
