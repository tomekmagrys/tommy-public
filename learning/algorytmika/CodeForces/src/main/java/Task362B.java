import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task362B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();

			ArrayList<Integer> dirtyStairs = new ArrayList<>(m);

			for (int i = 0; i < m; i++) {
				dirtyStairs.add(s.nextInt());
			}

			Collections.sort(dirtyStairs);

			boolean retVal = true;
			
			for (int i = 0; i < m - 2; i++) {
				if (dirtyStairs.get(i) + 1 == dirtyStairs.get(i + 1)
						&& dirtyStairs.get(i) + 2 == dirtyStairs.get(i + 2)) {
					retVal = false;
				}
			}

			if(m > 0 && dirtyStairs.get(0) == 1){
				retVal = false;
			}
			
			if(m > 0 && dirtyStairs.get(dirtyStairs.size()-1) == n){
				retVal = false;
			}

			
			
			pw.write(retVal ? "YES" : "NO");
			
			pw.flush();
			s.close();
		}
	}

}