import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task358A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> points = new ArrayList<>(n);

			for (int i = 0; i < n; i++) {
				points.add(s.nextInt());
			}

			boolean intersect = false;

			for (int i = 0; i < n - 1; i++) {
				for (int j = i+1; j < n - 1; j++) {
					int x1 = points.get(i);
					int y1 = points.get(i + 1);
					int x2 = points.get(j);
					int y2 = points.get(j + 1);

					if (Math.max(x1, y1) <= Math.min(x2, y2)) {
						continue;
					}
					if (Math.max(x2, y2) <= Math.min(x1, y1)) {
						continue;
					}

					if (Math.min(x1, y1) <= Math.min(x2, y2)
							&& Math.max(x2, y2) <= Math.max(x1, y1)) {
						continue;
					}
					if (Math.min(x2, y2) <= Math.min(x1, y1)
							&& Math.max(x1, y1) <= Math.max(x2, y2)) {
						continue;
					}

					intersect = true;

				}
			}

			pw.write(intersect ? "yes" : "no");

			pw.flush();

			s.close();
		}
	}

}