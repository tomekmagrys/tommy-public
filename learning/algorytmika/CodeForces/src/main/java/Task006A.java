import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task006A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	private static boolean canBeTriangle(int a, int b, int c) {
		if (a >= b + c) {
			return false;
		}
		if (b >= a + c) {
			return false;
		}
		if (c >= b + a) {
			return false;
		}
		return true;
	}

	private static boolean canBeDegeneratedTriangle(int a, int b, int c) {
		if (a == b + c) {
			return true;
		}
		if (b == a + c) {
			return true;
		}
		if (c == b + a) {
			return true;
		}
		return false;
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int a = s.nextInt();
			int b = s.nextInt();
			int c = s.nextInt();
			int d = s.nextInt();

			if (canBeTriangle(a, b, c) || canBeTriangle(b, c, d)
					|| canBeTriangle(c, d, a) || canBeTriangle(d, a, b)) {
				pw.println("TRIANGLE");
			} else {
				if (canBeDegeneratedTriangle(a, b, c)
						|| canBeDegeneratedTriangle(b, c, d)
						|| canBeDegeneratedTriangle(c, d, a)
						|| canBeDegeneratedTriangle(d, a, b)) {
					pw.println("SEGMENT");
				} else {
					pw.println("IMPOSSIBLE");
				}
			}

			pw.flush();
			s.close();
		}
	}

}
