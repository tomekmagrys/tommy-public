import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Task143B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String inp = sc.next();

			boolean minus = false;

			if (inp.startsWith("-")) {
				minus = true;
				inp = inp.substring(1);
			}

			String integerPart = "";
			String decimalPart = "";

			if (inp.indexOf(".") >= 0) {
				integerPart = inp.substring(0, inp.indexOf("."));
				decimalPart = inp.substring(inp.indexOf(".") + 1);
				decimalPart = decimalPart + "00";
				decimalPart = decimalPart.substring(0, 2);
			} else {
				integerPart = inp;
				decimalPart = "00";
			}

			if (minus) {
				pw.print("(");
			}
			pw.print("$");
			for (int i = 0; i < integerPart.length(); i++) {
				if (i > 0 && i % 3 == integerPart.length() % 3) {
					pw.print(",");
				}
				pw.print(integerPart.substring(i, i + 1));
			}
			pw.print(".");
			pw.print(decimalPart);
			if (minus) {
				pw.print(")");
			}

			pw.flush();
			sc.close();
		}
	}

}