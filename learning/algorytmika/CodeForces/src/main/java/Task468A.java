import java.io.*;

public class Task468A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            if (n < 4) {
                pw.println("NO");
            }
            else {
                pw.println("YES");
                if (n % 2 == 1) {
                    pw.println("4 * 5 = 20");
                    pw.println("20 + 3 = 23");
                    pw.println("23 + 2 = 25");
                    pw.println("25 - 1 = 24");

                    for (int i = 6; i <= n; i += 2) {
                        pw.println(i + 1 + " - " +  i + " = 1");
                        pw.println("24 * 1 = 24");
                    }

                } else {
                    int c = 1;
                    for (int i = 2; i <= 4; i++) {
                        pw.print(c);
                        pw.print(" * ");
                        pw.print(i);
                        pw.print(" = ");
                        pw.println(c * i);
                        c = c * i;
                    }

                    for (int i = 5; i <= n; i += 2) {
                        pw.println(i + 1 + " - " +  i + " = 1");
                        pw.println("24 * 1 = 24");
                    }
                }
            }

            pw.flush();
            sc.close();
        }
    }

}