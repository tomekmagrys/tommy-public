import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task157B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> circles = new ArrayList<>(n);
			for (int i = 0; i < n; i++) {
				circles.add(s.nextInt());
			}

			Collections.sort(circles);

			int area = 0;
			for (int i = 0; i < n; i++) {
				if (i % 2 == 0) {
					area += circles.get(i) * circles.get(i);
				} else {
					area -= circles.get(i) * circles.get(i);
				}
			}

			if (n % 2 == 0) {
				area = -area;
			}
			
			pw.write("" + (Math.PI * area));
		
			
			pw.flush();
			s.close();
		}
	}

}
