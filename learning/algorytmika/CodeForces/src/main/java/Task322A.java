import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task322A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int m = s.nextInt();
			
			pw.write((n+m-1) + "\n"); 
			for(int i = 1 ; i <= n ; i++){
				pw.write(i + " 1\n");
			}
			for(int i = 2 ; i <= m ; i++){
				pw.write("1 " + i + "\n");
			}
			pw.flush();
			s.close();
		}
	}

}
