import java.io.*;

public class Task411A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }


    static class Solution {


        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            String passwd = sc.next();

            long retVal = 0;

            if(passwd.length() >= 5){
                retVal++;
            }

            for(char c : passwd.toCharArray()){
                if(Character.isLowerCase(c)){
                    retVal++;
                    break;
                }
            }

            for(char c : passwd.toCharArray()){
                if(Character.isUpperCase(c)){
                    retVal++;
                    break;
                }
            }

            for(char c : passwd.toCharArray()){
                if(Character.isDigit(c)){
                    retVal++;
                    break;
                }
            }

            if(retVal == 4){
                pw.println("Correct");
            } else {
                pw.println("Too weak");
            }

            pw.flush();
            sc.close();
        }
    }

}