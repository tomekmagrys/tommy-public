import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task371A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			int k = s.nextInt();
			
			ArrayList<Integer> a = new ArrayList<>();
			for(int i = 0 ; i < n ; i++){
				a.add(s.nextInt());
			}

			int oneCount[] = new int[k];
			int twoCount[] = new int[k];
			
			for(int i = 0 ; i < n/k ; i++){
				for(int j = 0 ; j < k ; j++){
					if(a.get(i*k+j) == 1){
						oneCount[j]++;
					} else {
						twoCount[j]++;
					}
				}
			}
			
			int retVal = 0;
			for(int i = 0 ; i < k ; i++){
				retVal += Math.min(oneCount[i], twoCount[i]);
			}
			
			pw.println(retVal);
			
			pw.flush();
			s.close();
		}
	}

}