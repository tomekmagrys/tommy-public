import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task248A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static int gcd(int a, int b) {
			return b == 0 ? a : gcd(b, a % b);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			
			int left = 0;
			int right = 0;
			
			for(int i = 0 ;i < n ; i++){
				left += s.nextInt();
				right += s.nextInt();
			}
			
			pw.write(""+(Math.min(left,n-left)+Math.min(right,n-right)));
			pw.flush();
			s.close();
		}
	}

}
