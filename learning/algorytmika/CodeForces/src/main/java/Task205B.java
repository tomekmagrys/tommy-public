import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task205B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();

			ArrayList<Integer> a = new ArrayList<>(n);

			long retVal = 0;

			a.add(s.nextInt());
			for (int i = 1; i < n; i++) {
				int tmp = s.nextInt();
				if (tmp < a.get(a.size() - 1)) {
					retVal += a.get(a.size() - 1) - tmp;

				}
				a.add(tmp);
			}

			pw.println(retVal);
			pw.flush();
			s.close();
		}
	}

}