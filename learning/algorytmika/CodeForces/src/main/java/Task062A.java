import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task062A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		private static boolean canHold(int boy, int girl) {
			return (boy >= girl - 1) && (boy <= 2 * girl + 2);
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int gl = s.nextInt();
			int gr = s.nextInt();

			int bl = s.nextInt();
			int br = s.nextInt();

			boolean retVal = canHold(bl, gr) || canHold(br, gl);

			pw.println(retVal ? "YES" : "NO");

			pw.flush();
			s.close();
		}
	}

}