import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task285C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {
		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int n = s.nextInt();
			ArrayList<Integer> permutation = new ArrayList<>();
			
			for(int i = 0 ; i < n ; i++){
				permutation.add(s.nextInt());
			}
			
			Collections.sort(permutation);
			
			long retVal = 0;
			
			for(int i = 0 ; i < n ; i++){
				retVal += Math.abs(permutation.get(i)-(i+1));
			}
			
			pw.write(retVal + "\n");
			pw.flush();
			s.close();
		}
	}

}
