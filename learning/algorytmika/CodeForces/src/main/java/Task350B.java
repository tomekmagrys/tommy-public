import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Task350B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();

			ArrayList<Integer> type = new ArrayList<Integer>(n + 1);
			type.add(null);
			for (int i = 0; i < n; i++) {
				type.add(sc.nextInt());
			}

			int degrees[] = new int[n + 1];

			ArrayList<Integer> a = new ArrayList<Integer>(n + 1);
			a.add(null);
			for (int i = 0; i < n; i++) {
				int tmp = sc.nextInt();
				a.add(tmp);
				degrees[tmp]++;
			}

			ArrayList<Integer> retVal = new ArrayList<>();

			for (int i = 1; i <= n; i++) {
				if (type.get(i) == 1) {
					ArrayList<Integer> cand = new ArrayList<>();
					int tmp = i;
					while (true) {
						cand.add(tmp);
						tmp = a.get(tmp);
						if (tmp == 0) {
							break;
						}
						if (type.get(tmp) == 1) {
							break;
						}
						if (degrees[tmp] > 1) {
							break;
						}
					}
					if (cand.size() > retVal.size()) {
						retVal = cand;
					}
				}
			}

			Collections.reverse(retVal);
			
			pw.println(retVal.size());
			for (int i = 0; i < retVal.size(); i++) {
				pw.print(retVal.get(i));
				pw.print(" ");
			}

			pw.flush();
			sc.close();
		}
	}

}