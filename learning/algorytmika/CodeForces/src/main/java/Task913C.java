import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Task913C {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Pair {

        private final int ci;
        private final int vi;


        Pair(int ci, int vi) {
            this.ci = ci;
            this.vi = vi;
        }

        public int getCi() {
            return ci;
        }

        public int getVi() {
            return vi;
        }
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int L = sc.nextInt();

            List<Pair> costs = new ArrayList<>(n);
            int vi = 1;

            for (int i = 0; i < n; i++) {
                costs.add(new Pair(sc.nextInt(), vi));
                vi *= 2;
            }

            Collections.sort(costs, new Comparator<Pair>() {
                @Override
                public int compare(Pair o1, Pair o2) {
                    long c1 = o1.getCi();
                    long v1 = o1.getVi();
                    long c2 = o2.getCi();
                    long v2 = o2.getVi();
                    return  Long.valueOf(c1 * v2).compareTo(c2 * v1);
                }
            });

            int consideredPoerOf2 = 0x40000000;

            long retVal =0 ;

            while (L > 0) {
                if(consideredPoerOf2 > L){
                    consideredPoerOf2 /= 2;
                    continue;
                }

                long costCand = Long.MAX_VALUE;
                Long LCand = null;

                for(Pair pair : costs){
                    long count = (pair.getVi() + consideredPoerOf2 - 1)/ pair.getVi();
                    if(costCand > count * pair.getCi()){
                        costCand = count * pair.getCi();
                        LCand = count * pair.getVi();
                    }
                }

                retVal += costCand;
                L -= LCand;
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}