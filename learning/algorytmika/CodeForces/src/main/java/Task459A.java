import java.io.*;

public class Task459A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int x1 = sc.nextInt();
            int y1 = sc.nextInt();

            int x2 = sc.nextInt();
            int y2 = sc.nextInt();

            if (x1 == x2) {
                int size = Math.abs(y1 - y2);
                pw.print(x1 + size);
                pw.print(" ");
                pw.print(y1);
                pw.print(" ");
                pw.print(x2 + size);
                pw.print(" ");
                pw.print(y2);
            } else if (y1 == y2) {
                int size = Math.abs(x1 - x2);
                pw.print(x1);
                pw.print(" ");
                pw.print(y1 + size);
                pw.print(" ");
                pw.print(x2);
                pw.print(" ");
                pw.print(y2 + size);
            } else {
                if (Math.abs(x1 - x2) == Math.abs(y1 - y2)) {
                    pw.print(x1);
                    pw.print(" ");
                    pw.print(y2);
                    pw.print(" ");
                    pw.print(x2);
                    pw.print(" ");
                    pw.print(y1);
                } else {
                    pw.println(-1);
                }

            }

            pw.flush();
            sc.close();
        }
    }

}