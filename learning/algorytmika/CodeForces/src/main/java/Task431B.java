import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Task431B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int[][] g = new int[5][];
            for (int i = 0; i < 5; i++) {
                g[i] = new int[5];
            }

            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    g[i][j] = sc.nextInt();
                }
            }

            int retVal =0;

            for(int i = 0 ; i < 5 ; i++){
                for(int j = 0 ; j < 5 ; j++){
                    for(int k = 0 ; k < 5 ; k++){
                        for(int n = 0 ; n < 5; n++){
                            for(int m = 0 ; m < 5 ; m++){
                                Set<Integer> permutationCheck = new HashSet<>(5);
                                permutationCheck.add(i);
                                permutationCheck.add(j);
                                permutationCheck.add(k);
                                permutationCheck.add(n);
                                permutationCheck.add(m);

                                if(permutationCheck.size() < 5){
                                    continue;
                                }

                                retVal = Math.max(retVal,g[i][j] + g[j][i] + g[k][n] + g[n][k] +
                                                        g[j][k] + g[k][j] + g[n][m] + g[m][n] +
                                        + g[k][n] + g[n][k] +
                                        + g[n][m] + g[m][n] );

                            }
                        }
                    }
                }
            }

            pw.println(retVal);

            pw.flush();
            sc.close();
        }
    }

}