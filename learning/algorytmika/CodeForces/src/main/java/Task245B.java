import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task245B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		static long combinations(int n, int k) {
			long retVal = 1;
			for (int i = 0; i < k;) {
				retVal *= n - i;
				i++;
				retVal /= i;
			}
			return retVal;
		}

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			String input = s.next();
			if (input.startsWith("http")) {
				pw.write("http://");
				input = input.substring(4);
			} else {
				pw.write("ftp://");
				input = input.substring(3);
			}

			int domainEnd = input.indexOf("ru", 1);

			pw.print(input.substring(0, domainEnd));
			pw.print(".ru");
			domainEnd += 2;
			if (domainEnd < input.length()) {
				pw.print("/" + input.substring(domainEnd));
			}
			pw.println();
			pw.flush();
			s.close();
		}
	}

}