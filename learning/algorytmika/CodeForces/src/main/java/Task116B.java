import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Task116B {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException {
            PrintWriter pw = new PrintWriter(os);
            Scanner s = new Scanner(is);

            int n = s.nextInt();
            int m = s.nextInt();

            ArrayList<String> pigs = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                pigs.add(s.next());
            }

            int retVal = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (pigs.get(i).charAt(j) == 'P') {
                        if (i > 0 && pigs.get(i - 1).charAt(j) == 'W') {
                            pigs.set(i-1,pigs.get(i-1).substring(0,j)+'.'+pigs.get(i-1).substring(j+1));
                            retVal++;
                        }
                        if (j > 0 && pigs.get(i).charAt(j - 1) == 'W') {
                            pigs.set(i,pigs.get(i).substring(0,j-1)+'.'+pigs.get(i).substring(j));
                            retVal++;
                        }
                        if (i + 1 < n && pigs.get(i + 1).charAt(j) == 'W') {
                            pigs.set(i+1,pigs.get(i+1).substring(0,j)+'.'+pigs.get(i+1).substring(j+1));
                            retVal++;
                        }
                        if (j + 1 < m && pigs.get(i).charAt(j + 1) == 'W') {
                            pigs.set(i,pigs.get(i).substring(0,j+1)+'.'+pigs.get(i).substring(j+2));
                            retVal++;
                        }
                    }
                }
            }

            pw.write(retVal+"");

            pw.flush();
            s.close();

        }
    }

}