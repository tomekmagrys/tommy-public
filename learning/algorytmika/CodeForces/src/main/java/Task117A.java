import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task117A {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			int n = sc.nextInt();
			int m = sc.nextInt();

			int period = 2 * (m - 1);

			for (int i = 0; i < n; i++) {
				int s = sc.nextInt() - 1;
				int f = sc.nextInt() - 1;
				int t = sc.nextInt();

				if (s == f) {
					pw.println(t);
					continue;
				}
				int base = t / period * period;

				ArrayList<Integer> getIntoCandidates = new ArrayList<Integer>(3);

				if (base + s >= t) {
					getIntoCandidates.add(base + s);
				}
				if (base + period - s >= t) {
					getIntoCandidates.add(base + period - s);
				}
				getIntoCandidates.add(base + period + s);
				int getInto = Collections.min(getIntoCandidates);

				ArrayList<Integer> getOutCandidates = new ArrayList<Integer>(3);

				base = getInto / period * period;
				if (base + f >= getInto) {
					getOutCandidates.add(base + f);
				}
				if (base + period - f >= getInto) {
					getOutCandidates.add(base + period - f);
				}
				getOutCandidates.add(base + period + f);
				pw.println(Collections.min(getOutCandidates));
			}

			pw.flush();
			sc.close();
		}
	}

}