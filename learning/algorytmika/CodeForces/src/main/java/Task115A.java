import java.io.*;
import java.util.ArrayList;

public class Task115A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();

            ArrayList<Integer> p = new ArrayList<>(n);
            for(int i = 0 ; i < n ; i++){
                p.add(sc.nextInt()-1);
            }

            int retVal = 0;

            for(int i = 0 ; i < n ; i++){
                int tempRetVal = 1;
                int menago = p.get(i);
                while(menago != -2){
                    menago = p.get(menago);
                    tempRetVal++;
                }
                retVal = Math.max(retVal,tempRetVal);
            }

            pw.println(retVal);
            pw.flush();
            sc.close();
        }
    }

}