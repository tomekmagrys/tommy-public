import java.io.*;

public class Task377A {

    public static void main(String... args) throws NumberFormatException,
            IOException {
        Solution.main(System.in, System.out);
    }

    static class Scanner {

        private final BufferedReader br;
        private String[] cache;
        private int cacheIndex;

        Scanner(InputStream is) {
            br = new BufferedReader(new InputStreamReader(is));
            cache = new String[0];
            cacheIndex = 0;
        }

        int nextInt() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Integer.parseInt(cache[cacheIndex++]);
        }

        long nextLong() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return Long.parseLong(cache[cacheIndex++]);
        }

        String next() throws IOException {
            if (cacheIndex >= cache.length) {
                cache = br.readLine().split(" ");
                cacheIndex = 0;
            }
            return cache[cacheIndex++];
        }

        void close() throws IOException {
            br.close();
        }

    }

    static class Solution {

        private static int s(char[][] __c) {
            int retVal = 0;
            for (char[] _c : __c) {
                for (char c : _c) {
                    if (c == '.') {
                        retVal++;
                    }
                }
            }
            return retVal;
        }

        static int left;

        private static void dfs(int startX, int startY, char[][] maze, boolean[][] visited) {
            if (visited[startX][startY]) {
                return;
            }
            if (left == 0) {
                return;
            }

            visited[startX][startY] = true;
            left--;

            if (startX > 0 && maze[startX - 1][startY] == '.') {
                dfs(startX - 1, startY, maze, visited);
            }
            if (startY > 0 && maze[startX][startY - 1] == '.') {
                dfs(startX, startY - 1, maze, visited);
            }
            if (startX < maze.length - 1 && maze[startX + 1][startY] == '.') {
                dfs(startX + 1, startY, maze, visited);
            }
            if (startY < maze[0].length - 1 && maze[startX][startY + 1] == '.') {
                dfs(startX, startY + 1, maze, visited);
            }
        }

        public static void main(InputStream is, OutputStream os)
                throws NumberFormatException, IOException {
            PrintWriter pw = new PrintWriter(os);
            Scanner sc = new Scanner(is);

            int n = sc.nextInt();
            int m = sc.nextInt();
            int k = sc.nextInt();

            char[][] maze = new char[n][];
            boolean[][] visited = new boolean[n][];

            for (int i = 0; i < n; i++) {
                maze[i] = sc.next().toCharArray();
                visited[i] = new boolean[m];
            }

            int s = s(maze);

            left = s - k;

            int startX = 0;
            int startY = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (maze[i][j] == '.') {
                        startX = i;
                        startY = j;
                    }
                }

            }

            dfs(startX, startY, maze, visited);

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (maze[i][j] == '.' && !visited[i][j]) {
                        maze[i][j] = 'X';
                    }
                }
            }

            for (char[] c : maze) {
                pw.println(c);
            }

            pw.flush();
            sc.close();
        }
    }

}