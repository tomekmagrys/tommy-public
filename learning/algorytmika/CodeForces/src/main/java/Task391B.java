import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Task391B {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Scanner {

		private final BufferedReader br;
		private String[] cache;
		private int cacheIndex;

		Scanner(InputStream is) {
			br = new BufferedReader(new InputStreamReader(is));
			cache = new String[0];
			cacheIndex = 0;
		}

		int nextInt() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return Integer.parseInt(cache[cacheIndex++]);
		}

		String next() throws IOException {
			if (cacheIndex >= cache.length) {
				cache = br.readLine().split(" ");
				cacheIndex = 0;
			}
			return cache[cacheIndex++];
		}

		void close() throws IOException {
			br.close();
		}

	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner sc = new Scanner(is);

			String inp = sc.next();

			int retVal = 0;

			for (char c = 'A'; c <= 'Z'; c++) {
				ArrayList<Integer> positions = new ArrayList<>();
				for (int i = 0; i < inp.length(); i++) {
					if (inp.charAt(i) == c) {
						positions.add(i);
					}
				}

				for (int i = 1; i < positions.size();) {
					if (positions.get(i) % 2 == positions.get(i - 1) % 2) {
						positions.remove(i);
					} else {
						i++;
					}
				}
				retVal = Math.max(retVal, positions.size());
			}

			pw.println(retVal);
			pw.flush();
			sc.close();
		}
	}

}