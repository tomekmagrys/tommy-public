import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Task268C {

	public static void main(String... args) throws NumberFormatException,
			IOException {
		Solution.main(System.in, System.out);
	}

	static class Solution {

		public static void main(InputStream is, OutputStream os)
				throws NumberFormatException, IOException {
			PrintWriter pw = new PrintWriter(os);
			Scanner s = new Scanner(is);

			int x = s.nextInt();
			int y = s.nextInt();

			pw.println(Math.min(x, y) + 1);

			if (x != y) {
				if (x < y) {
					for (int i = 0; i <= Math.min(x, y); i++) {
						pw.println(i + " " + (i + Math.abs(x - y)));
					}
				} else {
					for (int i = 0; i <= Math.min(x, y); i++) {
						pw.println((i + Math.abs(x - y)) + " " + i);
					}
				}
			} else {
				for (int i = 0; i <= Math.min(x, y); i++) {
					pw.println(Math.min(x, y) - i + " " + i);
				}
			}

			pw.flush();
			s.close();
		}
	}

}