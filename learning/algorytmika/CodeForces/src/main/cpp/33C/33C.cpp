#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>

using namespace std;

int liczby;

int main(int argc, char** argv) {
	int n;
	int suma = 0;
	scanf("%d", &n);

	int maxSum = 0;
	int currentMaxSum = 0;
	for (int i = 0; i < n; i++) {
		scanf("%d", &liczby);
		suma += liczby;
		currentMaxSum = currentMaxSum + liczby > 0 ? currentMaxSum + liczby : 0;
		maxSum = (maxSum > currentMaxSum ? maxSum : currentMaxSum);

	}

	printf("%d\n", 2 * maxSum - suma);

}
