#include <cstdio>
#include <string.h>

char nawiasy[1000001];

int main(int argc, char ** argv) {
	scanf("%s", nawiasy);
	int otw = 0;
	int ok = 0;
	int i = 0;
	for (; nawiasy[i]; i++) {
		if (nawiasy[i] == '(') {
			otw++;
			ok++;
		} else {
			if (otw > 0) {
				otw--;
				ok++;
			}
		}
	}
	printf("%d\n", ok - otw);
	return 0;
}
