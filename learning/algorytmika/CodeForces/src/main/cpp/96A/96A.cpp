#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

char a[101];

int main() {
	scanf("%s", a);

	int zera = a[0] == '0' ? 1 : 0;
	int jedy = a[0] == '1' ? 1 : 0;

	for (int i = 1; a[i]; i++) {
		if (a[i] == '0') {
			if (a[i - 1] == '0') {
				zera++;
			} else {
				zera = 1;
				jedy = 0;
			}
		} else {
			if (a[i - 1] == '0') {
				zera = 0;
				jedy = 1;
			} else {
				jedy++;
			}
		}
		if (jedy == 7 || zera == 7) {
			printf("YES\n");
			return 0;
		}
		//printf("%d %d\n",zera,jedy);
	}
	printf("NO\n");
	return 0;
}
