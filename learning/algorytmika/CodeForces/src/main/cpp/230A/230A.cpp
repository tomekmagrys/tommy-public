#include <cstdio>
#include <algorithm>

using namespace std;

int s,n;

pair<int,int> dragons[1000];

bool mycmp(const pair<int,int> & a,const pair<int,int> & b){
  return a.first < b.first;
}

int main(){

  scanf("%d %d",&s,&n);
  
  for(int i = 0 ; i < n ; i++){
    int x,y;
    scanf("%d %d",&x,&y);
    dragons[i] = make_pair(x,y);
  }
  
  sort(dragons,dragons+n,mycmp);
  
  //printf("%d\n",n);
  for(int i = 0 ; i < n ; i++){
    //printf("%d %d\n",s,dragons[i].first);
    if(s > dragons[i].first){
      s += dragons[i].second;
    } else {
      printf("NO\n");
      return 0;
    }
  }
  
  printf("YES\n");
  
}
