#include <cstdio>

int a, b, c;
int a0 = 0, b0 = 0, c0 = 0;

int main(int argc, char ** argv) {
	scanf("%d", &a);
	scanf("%d", &b);
	c = a + b;
	int mnoznik = 1;
	while (a > 0) {
		int digit = a % 10;

		if (digit) {
			a0 = digit * mnoznik + a0;
			mnoznik *= 10;
		}
		a /= 10;
	}
	mnoznik = 1;
	while (b > 0) {
		int digit = b % 10;

		if (digit) {
			b0 = digit * mnoznik + b0;
			mnoznik *= 10;
		}
		b /= 10;
	}
	mnoznik = 1;
	while (c > 0) {
		int digit = c % 10;

		if (digit) {
			c0 = digit * mnoznik + c0;
			mnoznik *= 10;
		}
		c /= 10;
	}
		printf("YES\n");
	} else {
		printf("NO\n");
	}
	return 0;

}
