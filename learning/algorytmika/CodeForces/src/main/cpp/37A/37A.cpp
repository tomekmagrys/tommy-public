#include <cstdio>

int n;
int towers[1001];
int tower;

int main(int argc, char** argv) {
	scanf("%d", &n);

	for (int i = 0; i < 1001; i++) {
		towers[i] = 0;
	}

	for (int i = 0; i < n; i++) {
		scanf("%d", &tower);
		towers[tower]++;
	}

	int najw = 0;
	int rozne = 0;

	for (int i = 0; i < 1001; i++) {
		if (towers[i] != 0)
			rozne++;
		if (towers[i] > najw)
			najw = towers[i];
	}

	printf("%d %d", najw, rozne);
}
