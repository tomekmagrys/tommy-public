#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;

bool isPrime(int n) {
	for (int i = 2; i * i <= n; i++) {
		if (n % i == 0)
			return false;
	}
	return true;
}

bool almostPrime(int n) {

	int found = 0;

	for (int i = 2; i * i <= n; i++) {
		if (n % i == 0 && i * i < n) {
			if (isPrime(i)) {
				found++;
				if (found > 2)
					return false;
			}
			//else return false;
			if (isPrime(n / i)) {
				found++;
				if (found > 2)
					return false;;
			}
		} else {
			if (i * i == n) {
				found += isPrime(i);
			}
		}
	}
	return found == 2;
}

int n;

int main() {
	scanf("%d", &n);

	int retVal = 0;

	for (int i = 1; i <= n; i++) {
		retVal += almostPrime(i);
		//printf("%d-%d\n",i,almostPrime(i));
	}
	printf("%d\n", retVal);
	return 0;
}
