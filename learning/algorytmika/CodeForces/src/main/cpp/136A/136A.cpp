#include <cstdio>
#include <algorithm>

using namespace std;

int n,perm[101];

int main(){
  scanf("%d",&n);
  
  for(int i = 1 ; i <= n ; i++){
      int temp;
      scanf("%d",&temp);
      perm[temp] = i;
  }
  
  for(int i = 1 ; i <= n ; i++){
    printf("%d ",perm[i]);
  }
  printf("\n");
}