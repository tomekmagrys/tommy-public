#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

int gcd(int a, int b) {

	if (a < b) {
		int temp = a;
		a = b;
		b = temp;
	}
	if (b == 0)
		return a;
	return gcd(b, a % b);
}

int dzielniki[100000];
int iledzielnikow = 0;

int main(int argc, char** argv) {
	int a, b;

	scanf("%d%d", &a, &b);
	int gcdab = gcd(a, b);

	for (int i = 1; i * i <= gcdab; i++) {
		if (gcdab % i == 0) {
			dzielniki[iledzielnikow++] = i;
			dzielniki[iledzielnikow++] = gcdab / i;
		}
	}
	sort(dzielniki, dzielniki + iledzielnikow);

	int n;
	scanf("%d", &n);
	int low, high;
	for (int i = 0; i < n; i++) {
		scanf("%d %d", &low, &high);

		int p = 0;
		int k = iledzielnikow - 1;
		while (p < k) {
			int q = (p + k + 1) / 2;
			if (dzielniki[q] <= high) {
				p = q;
			} else {
				k = q - 1;
			}
		}
		if (low <= dzielniki[p] && high >= dzielniki[p]) {
			printf("%d\n", dzielniki[p]);
			continue;
		}
		printf("-1\n");
	}
}
