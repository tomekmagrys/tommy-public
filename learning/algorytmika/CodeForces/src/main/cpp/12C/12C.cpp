#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

using namespace std;
int n, m;

int ceny[100];
int wspolczynniki[100];

struct nameNumber {
	string name;
	int number;
	friend bool operator <(const nameNumber &a, const nameNumber &b);
};

bool operator <(const nameNumber& a, const nameNumber &b) {
	return a.name < b.name;
}

int main(int argc, char** argv) {
	scanf("%d%d", &n, &m);

	for (int i = 0; i < n; i++) {
		scanf("%d", &ceny[i]);
		wspolczynniki[i] = 0;
	}

	set<nameNumber> names;
	pair<set<nameNumber>::iterator, bool> ret;

	for (int i = 0; i < m; i++) {
		nameNumber doinsert;
		cin >> doinsert.name;
		doinsert.number = 1;
		ret = names.insert(doinsert);
		if (ret.second == false) {
			nameNumber * tmp = (nameNumber *) &(*(ret.first));
			tmp->number++;
		}
	}
	set<nameNumber>::iterator it;
	int i = 0;
	for (it = names.begin(); it != names.end(); it++, i++) {
		//cout << " " << *it;
		wspolczynniki[i] = (*it).number;
		//cout << wspolczynniki[i];

	}
	sort(ceny, ceny + n);
	sort(wspolczynniki, wspolczynniki + n);

	int min = 0;
	int max = 0;

	for (int i = 0; i < n; i++) {
		min += wspolczynniki[i] * ceny[i];
		max += wspolczynniki[i] * ceny[n - 1 - i];
	}
	printf("%d %d\n", max, min);

}
