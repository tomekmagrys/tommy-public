#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>

using namespace std;
int n;

int main(int argc, char** argv) {
	scanf("%d", &n);
	int mnoznik = 1;
	int wynik = 0;
	while (n > 0) {
		if (n % 10 == 1)
			wynik += mnoznik;
		if (n % 10 > 1)
			wynik = 2 * mnoznik - 1;
		n /= 10;
		mnoznik *= 2;
	}
	printf("%d\n", wynik);
}
