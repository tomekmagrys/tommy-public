#include <cstdio>

int main(int argc, char** argv) {
	int n, m, a;
	scanf("%d%d%d", &n, &m, &a);
	long long lln, llm, lla, llpion, llpoz;
	lln = n;
	llm = m;
	lla = a;

	if (lln % lla == 0)
		llpion = lln / lla;
	else
		llpion = lln / lla + 1;
	if (llm % lla == 0)
		llpoz = llm / lla;
	else
		llpoz = llm / lla + 1;

	long long output = llpion * llpoz;

	printf("%I64d", output);
}
