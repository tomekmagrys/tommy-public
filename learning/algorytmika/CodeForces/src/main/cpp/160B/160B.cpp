#include <cstdio>
#include <algorithm>

using namespace std;

int c,d,k,l,m,n,p;
int nl,np;
int temp;

int min(int a,int b,int c){
  return min(min(a,b),c);
}

int main(){

  scanf("%d",&n);
  
  static char digits[201];
  
  scanf("%s",digits);

  sort(digits,digits+n);
  sort(digits+n,digits+2*n);
  
  bool canMatchFirst = true;
  bool canMatchSecond =true;
  
  for(int i = 0 ; i < n ; i++){
    if(digits[i] >= digits[n+i]) canMatchFirst = false;
    if(digits[i] <= digits[n+i]) canMatchSecond = false;
  }
  
  if(canMatchFirst || canMatchSecond){
    printf("YES\n");
  } else {
    printf("NO\n");
  }
}
