#include<cstdio>
#include<algorithm>

int n;

char digit[37];

const long long int potegi10[] = { 1LL, 10LL, 100LL, 1000LL, 10000LL, 100000LL,
		1000000LL, 10000000LL, 100000000LL, 1000000000LL, 10000000000LL,
		100000000000LL, 1000000000000LL, 10000000000000LL, 100000000000000LL,
		1000000000000000LL, 10000000000000000LL, 100000000000000000LL,
		1000000000000000000LL };

char choice[19][19];
long long int memo[19][19];

long long int result(char * digit, int m, int n) {

	if (memo[m][n] >= 0LL) {
		return memo[m][n];
	}

	long long int retVal;
	long long int retVal2;

	if (m > 0) {
		if (n == 0) {
			memo[m][n] = (digit[0] - '0') * potegi10[m - 1]
					+ result(digit + 1, m - 1, n);
			choice[m][n] = 'H';
		} else {
			retVal = (digit[0] - '0') * potegi10[m - 1]
					+ result(digit + 1, m - 1, n);
			retVal2 = (digit[0] - '0') * potegi10[n - 1]
					+ result(digit + 1, m, n - 1);

			if (retVal > retVal2) {
				memo[m][n] = retVal;
				choice[m][n] = 'H';
			} else {
				memo[m][n] = retVal2;
				choice[m][n] = 'M';
			}

		}
	} else {
		if (n > 0) {
			memo[m][n] = (digit[0] - '0') * potegi10[n - 1]
					+ result(digit + 1, m, n - 1);
			choice[m][n] = 'M';
		} else {
			memo[m][n] = 0;
		}
	}
	//printf("%d %d %d\n",m,n,memo[m][n]);
	return memo[m][n];
}

int main() {
	scanf("%d", &n);
	scanf("%s", digit);

	for (int i = 0; i <= n; i++)
		for (int j = 0; j <= n; j++) {
			choice[i][j] = 0;
			memo[i][j] = -1;
		}

	result(digit, n, n);
	//printf("%lld\n",result(digit,n,n));

	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= n; j++) {
			//printf("%c",choice[i][j]);
		}
		//printf("\n");
	}

	for (int i = n, j = n; i > 0 || j > 0;) {
		//printf("%d %d %c\n",i,j,choice[i][j]);
		printf("%c", choice[i][j]);
		if (choice[i][j] == 'M') {
			j--;
		} else if (choice[i][j] == 'H') {
			i--;
		}
	}
	printf("\n");

	return 0;
}
