#include <cstdio>
#include <algorithm>

using namespace std;

int n,k;


int main(){

  scanf("%d %d",&n,&k);
  
  int _max = -1000000001;
  
  for(int i = 0 ;i < n ; i++){
    int f,t;
    scanf("%d %d",&f,&t);
    if(t > k){
      _max = max(_max,f-(t-k));
    } else {
      _max = max(_max,f);
    }
  }
  
  printf("%d\n",_max);

  
}
