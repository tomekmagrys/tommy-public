#include <cstdio>

int n;

int main(int argc, char** argv) {
	scanf("%d", &n);

	if (n > 2 && n % 2 == 0) {
		printf("YES");
	} else {
		printf("NO");
	}
}
