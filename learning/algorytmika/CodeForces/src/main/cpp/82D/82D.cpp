#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

int a[1000][1000];
int b[1000][1000];
int c[1000][1000];
int d[1000][1000];
int n;

int max(int a, int b) {
	return a > b ? a : b;
}

int min(int a, int b) {
	return a < b ? a : b;
}

int main(int argc, char** argv) {

	scanf("%d", &n);

	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i][0]);

	}

	for (int i = 0; i < n; i++) {
		a[i][1] = max(a[i][0], a[n - 1][0]);
		b[i][1] = i;
		c[i][1] = n - 1;
	}

	for (int j = 2; j < n; j++) {
		for (int i = 0; i < n; i++) {
			int kand1 = max(a[i][0], a[n - j][0]) + a[n - j + 1][j - 2];
			int kand2 = max(a[i][0], a[n - j + 1][0]) + a[n - j][j - 2];
			int kand3 = max(a[n - j][0], a[n - j + 1][0]) + a[i][j - 2];
			a[i][j] = kand1;
			b[i][j] = i;
			c[i][j] = n - j;
			d[i][j] = n - j + 1;
			if (kand2 < a[i][j]) {
				a[i][j] = kand2;
				b[i][j] = i;
				c[i][j] = n - j + 1;
				d[i][j] = n - j;
			}
			if (kand3 < a[i][j]) {
				a[i][j] = kand3;
				b[i][j] = n - j;
				c[i][j] = n - j + 1;
				d[i][j] = i;
			}
		}
	}

	printf("%d\n", a[0][n - 1]);

	int i = 0;
	int j = n - 1;

	for (int k = 0; k < n / 2; k++) {
		printf("%d %d\n", b[i][j] + 1, c[i][j] + 1);
		i = d[i][j];
		j -= 2;
	}
	if (n % 2 == 1) {
		printf("%d\n", i + 1);
	}
}
