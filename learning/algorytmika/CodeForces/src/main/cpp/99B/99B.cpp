#include <cstdio>
#include <string.h>

int cups[1000];
int n;
int suma;

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &cups[i]);
		suma += cups[i];
	}
	if (suma % n != 0) {
		printf("Unrecoverable configuration.");
		return 0;
	}

	suma /= n;
	int i = 0, j, k;
	for (; i < n; i++) {
		if (cups[i] != suma) {
			break;
		}
	}
	for (j = i + 1; j < n; j++) {
		if (cups[j] != suma) {
			break;
		}
	}

	for (k = j + 1; k < n; k++) {
		if (cups[k] != suma) {
			break;
		}
	}

	if (i == n) {
		printf("Exemplary pages.");
	} else {
		if (k >= n && j < n) {
			if (cups[i] > cups[j]) {
				printf("%d ml. from cup #%d to cup #%d.", cups[i] - suma, j + 1,
						i + 1);
			} else {
				printf("%d ml. from cup #%d to cup #%d.", cups[j] - suma, i + 1,
						j + 1);
			}
		} else {
			printf("Unrecoverable configuration.");
		}
	}

}
