#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

int a[1000];
int b[1000];
int n;
int maxx = 0;
int tram;

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d %d", &a[i], &b[i]);
		tram += b[i] - a[i];
		maxx = tram > maxx ? tram : maxx;
	}
	printf("%d\n", maxx);
}
