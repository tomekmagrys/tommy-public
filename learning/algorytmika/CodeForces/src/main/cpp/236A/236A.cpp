#include <cstdio>
#include <algorithm>

using namespace std;

int n;

int letters['z'+1];

char login[101];

int main(){

  for(char i = 'a' ; i <= 'z' ; i++) letters[i] = 0;
  
  scanf("%s",login);
  
  for(int i = 0 ; login[i] ; i++){
    letters[login[i]]++;
  }
  
  bool retVal = true;
  
  for(char i = 'a' ; i <= 'z' ; i++) {
    if(letters[i] != 0){
      retVal = !retVal;
    }
  }
  
  if(retVal){
    printf("CHAT WITH HER!\n");
  } else {
    printf("IGNORE HIM!\n");
  }
  
}