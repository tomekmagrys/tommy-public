#include <cstdio>
#include <algorithm>

using namespace std;

int n,k;


int main(){

  scanf("%d",&n);
  
  if(n % 2 == 1) {
    printf("-1\n");
    return 0;
  }
  
  for(int i = 1 ; i <= n ; i++){
    printf("%d ",((i-1)^1)+1);
  }
  printf("\n");
}
