#include <cstdio>
#include <algorithm>

int n;
int liczby[100];
int main(int argc, char** argv) {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &liczby[i]);

	}

	std::sort(liczby, liczby + n);
	int i = 0;
	while (i < n - 1 && liczby[i] == liczby[i + 1]) {
		i++;
	}
	if (i == n - 1) {
		printf("NO\n");
	} else {
		printf("%d\n", liczby[i + 1]);
	}

}
