#include <cstdio>
#include <cstdlib>
#include <string.h>

char wierza[3];
char konik[3];

bool plansza[8][8];

int main(int argc, char** argv) {
	scanf("%s", wierza);
	scanf("%s", konik);

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			plansza[i][j] = true;
		}
	}

	for (int i = 0; i < 8; i++) {
		plansza[wierza[0] - 'a'][i] = false;
		plansza[i][wierza[1] - '1'] = false;
	}

	konik[0] -= 'a';
	konik[1] -= '1';

	plansza[konik[0]][konik[1]] = false;

	if (konik[0] >= 2 && konik[1] >= 1) {
		plansza[konik[0] - 2][konik[1] - 1] = false;
	}
	if (konik[0] >= 2 && konik[1] < 7) {
		plansza[konik[0] - 2][konik[1] + 1] = false;
	}
	if (konik[0] >= 1 && konik[1] < 6) {
		plansza[konik[0] - 1][konik[1] + 2] = false;
	}
	if (konik[0] >= 1 && konik[1] >= 2) {
		plansza[konik[0] - 1][konik[1] - 2] = false;
	}
	if (konik[0] < 6 && konik[1] >= 1) {
		plansza[konik[0] + 2][konik[1] - 1] = false;
	}
	if (konik[0] < 6 && konik[1] < 7) {
		plansza[konik[0] + 2][konik[1] + 1] = false;
	}
	if (konik[0] < 7 && konik[1] < 6) {
		plansza[konik[0] + 1][konik[1] + 2] = false;
	}
	if (konik[0] < 7 && konik[1] >= 2) {
		plansza[konik[0] + 1][konik[1] - 2] = false;
	}

	konik[0] = wierza[0] - 'a';
	konik[1] = wierza[1] - '1';

	if (konik[0] >= 2 && konik[1] >= 1) {
		plansza[konik[0] - 2][konik[1] - 1] = false;
	}
	if (konik[0] >= 2 && konik[1] < 7) {
		plansza[konik[0] - 2][konik[1] + 1] = false;
	}
	if (konik[0] >= 1 && konik[1] < 6) {
		plansza[konik[0] - 1][konik[1] + 2] = false;
	}
	if (konik[0] >= 1 && konik[1] >= 2) {
		plansza[konik[0] - 1][konik[1] - 2] = false;
	}
	if (konik[0] < 6 && konik[1] >= 1) {
		plansza[konik[0] + 2][konik[1] - 1] = false;
	}
	if (konik[0] < 6 && konik[1] < 7) {
		plansza[konik[0] + 2][konik[1] + 1] = false;
	}
	if (konik[0] < 7 && konik[1] < 6) {
		plansza[konik[0] + 1][konik[1] + 2] = false;
	}
	if (konik[0] < 7 && konik[1] >= 2) {
		plansza[konik[0] + 1][konik[1] - 2] = false;
	}

	int suma = 0;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			suma += plansza[i][j];
			//printf("%d ",plansza[i][j]);
		}
		//printf("\n");
	}
	printf("%d\n", suma);

}
