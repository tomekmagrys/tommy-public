#include <cstdio>
#include <algorithm>

using namespace std;

int c,d,k,l,m,n,p,x,y;
int nl,np;
int temp;

int min(int a,int b,int c){
  return min(min(a,b),c);
}

int main(){

  scanf("%d",&n);
  
  if(n <= 2){
    printf("-1\n");
  } else {
    for(int i = n ; i > 1 ; i--){
      printf("%d ",i);
    }
    printf("1\n");
  }
  
}
