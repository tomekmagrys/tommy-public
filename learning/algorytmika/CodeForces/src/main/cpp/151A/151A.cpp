#include <cstdio>
#include <algorithm>

using namespace std;

int c,d,k,l,m,n,p;
int nl,np;

int min(int a,int b,int c){
  return min(min(a,b),c);
}

int main(){

  scanf("%d%d%d%d%d%d%d%d",&n,&k,&l,&c,&d,&p,&nl,&np);
  
  printf("%d\n",min((k*l)/nl,c*d,p/np)/n);
}
