#include <cstdio>
#include <algorithm>

using namespace std;

int c,d,k,l,m,n,p;
int nl,np;
int temp;

int min(int a,int b,int c){
  return min(min(a,b),c);
}

int main(){

  scanf("%d",&n);
  
  int suma = 0;
  
  for(int i = 0 ; i < n ; i++){
    scanf("%d",&temp);
    suma += temp;
  }
  
  int ways = 0;
  
  for(int i = 1 ; i <= 5 ; i++){
    if((suma + i) % (n+1) != 1){
      ways++;
    }
  }
  
  printf("%d\n",ways);
  
}
