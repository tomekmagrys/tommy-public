#include <cstdio>
#include <algorithm>

using namespace std;

int n;
int d[100];
int s,t;

int main(){

  scanf("%d",&n);
  
  for(int i = 0 ; i < n ; i++){
    scanf("%d",&d[i]);
  }
  
  scanf("%d %d",&s,&t);
  
  int route1 = 0;
  int route2 = 0;
  
  s--;
  t--;
  
  for(int i = s ; i != t ; i = (i+1)%n){
    route1 += d[i];
  }
  for(int i = t ; i != s ; i = (i+1)%n){
    route2 += d[i];
  }
  printf("%d\n",min(route1,route2));
  
}
