#include <cstdio>
#include <cstdlib>
#include <string.h>

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	printf("%d ", n);
	int d = 2;
	while (n > 1) {
		if (n % d == 0) {
			n /= d;
			printf("%d ", n);
		} else {
			d++;
		}
	}
}
