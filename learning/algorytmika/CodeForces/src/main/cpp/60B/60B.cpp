#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <queue>

char talerz[11][11][11];
bool queued[10][10][10];

int main(int argc, char** argv) {
	int k, m, n;
	int x, y;
	scanf("%d%d%d", &k, &n, &m);

	for (int i = 0; i < k; i++) {
		for (int j = 0; j < n; j++) {
			scanf("%s", talerz[i][j]);
		}
	}
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < n; j++) {
			for (int o = 0; o < m; o++) {
				queued[i][j][o] = false;
			}
		}
	}
	scanf("%d%d", &x, &y);
	std::queue<int> woda;
	woda.push(0);
	woda.push(x - 1);
	woda.push(y - 1);
	queued[0][x - 1][y - 1] = true;
	int licznik = 0;
	while (!woda.empty()) {

		int woda_k = woda.front();
		woda.pop();
		int woda_n = woda.front();
		woda.pop();
		int woda_m = woda.front();
		woda.pop();
		//printf("%d%d%d\n",woda_k,woda_n,woda_m);
		licznik++;
		if ((woda_k + 1 < k) && (talerz[woda_k + 1][woda_n][woda_m] == '.')
				&& !queued[woda_k + 1][woda_n][woda_m]) {
			woda.push(woda_k + 1);
			woda.push(woda_n);
			woda.push(woda_m);
			queued[woda_k + 1][woda_n][woda_m] = true;
		}
		if ((woda_k - 1 >= 0) && (talerz[woda_k - 1][woda_n][woda_m] == '.')
				&& !queued[woda_k - 1][woda_n][woda_m]) {
			woda.push(woda_k - 1);
			woda.push(woda_n);
			woda.push(woda_m);
			queued[woda_k - 1][woda_n][woda_m] = true;
		}
		if ((woda_n + 1 < n) && (talerz[woda_k][woda_n + 1][woda_m] == '.')
				&& !queued[woda_k][woda_n + 1][woda_m]) {
			woda.push(woda_k);
			woda.push(woda_n + 1);
			woda.push(woda_m);
			queued[woda_k][woda_n + 1][woda_m] = true;
		}
		if ((woda_n - 1 >= 0) && (talerz[woda_k][woda_n - 1][woda_m] == '.')
				&& !queued[woda_k][woda_n - 1][woda_m]) {
			woda.push(woda_k);
			woda.push(woda_n - 1);
			woda.push(woda_m);
			queued[woda_k][woda_n - 1][woda_m] = true;
		}
		if ((woda_m + 1 < m) && (talerz[woda_k][woda_n][woda_m + 1] == '.')
				&& !queued[woda_k][woda_n][woda_m + 1]) {
			woda.push(woda_k);
			woda.push(woda_n);
			woda.push(woda_m + 1);
			queued[woda_k][woda_n][woda_m + 1] = true;
		}
		if ((woda_m - 1 >= 0) && (talerz[woda_k][woda_n][woda_m - 1] == '.')
				&& !queued[woda_k][woda_n][woda_m - 1]) {
			woda.push(woda_k);
			woda.push(woda_n);
			woda.push(woda_m - 1);
			queued[woda_k][woda_n][woda_m - 1] = true;
		}
	}
	printf("%d\n", licznik);
}
