#include <cstdio>
#include <cstdlib>
#include <string.h>

char zwierzeta[1001];

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	scanf("%s", zwierzeta);

	int ileChomikow = 0;

	for (int i = 0; i < n; i++) {
		ileChomikow += (zwierzeta[i] == 'H');
	}

	int ileTygrysow = n;

	for (int i = 0; i < n; i++) {
		int ileTygrysowTemp = 0;
		for (int j = 0; j < ileChomikow; j++) {
			ileTygrysowTemp += (zwierzeta[(i + j) % n] == 'T');
		}
		ileTygrysow = (
				ileTygrysow > ileTygrysowTemp ? ileTygrysowTemp : ileTygrysow);
	}
	printf("%d\n", ileTygrysow);
}
