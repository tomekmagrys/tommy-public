#include <cstdio>
#include <algorithm>

using namespace std;

long long int n;

int main(){

  // 1 
  // 2
  // 3 1
  // 3 2 1
  
  // 1 
  // 2
  // 3
  // 4 1
  // 4 2
  // 4 3 1
  // 4 3 2 1
  
  // (n-1) * 1 + (n-2) * 2 + (n-i) * i + 1 * (n-1) + n
  
  scanf("%d",&n);
  
  printf("%d\n",n*n*(n-1)/2 - n*(n-1)*(2*n-1)/6 + n);  
  
}
