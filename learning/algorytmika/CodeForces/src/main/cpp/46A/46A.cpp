#include <cstdio>

int n;

int main(int argc, char** argv) {

	scanf("%d", &n);

	for (int i = 1, j = 1; i < n; i++) {

		j += i;
		if (j > n)
			j -= n;
		printf("%d ", j);
	}

}
