#include <cstdio>

int gcd(int a, int b) {
	if (b == 0)
		return a;
	if (a < b) {
		int tmp = a;
		a = b;
		b = a;
	}
	return gcd(b, a % b);
}

int main(int argc, char ** argv) {
	int A;
	scanf("%d", &A);
	int suma = 0;
	for (int i = 2; i < A; i++) {
		int kopiaA = A;
		while (kopiaA > 0) {
			suma += kopiaA % i;
			kopiaA /= i;
		}
	}
	int tmpgcd = gcd(suma, A - 2);

	printf("%d/%d\n", suma / tmpgcd, (A - 2) / tmpgcd);
	return 0;

}
