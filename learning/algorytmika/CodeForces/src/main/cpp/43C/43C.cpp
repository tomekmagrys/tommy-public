#include <cstdio>
#include <cstdlib>
#include <string.h>

int temp;
int r[] = { 0, 0, 0 };

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &temp);
		r[temp % 3]++;
	}
	printf("%d", r[0] / 2 + (r[1] > r[2] ? r[2] : r[1]));

}
