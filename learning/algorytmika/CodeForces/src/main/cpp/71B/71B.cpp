#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

//int ceny[100];

int main(int argc, char** argv) {
	int n, k, t;
	scanf("%d%d%d", &n, &k, &t);

	int sumai = n * k * t / 100;

	for (int i = 0; i < n; i++) {
		if (sumai >= k) {
			printf("%d ", k);
			sumai -= k;
		} else if (sumai >= 0) {
			printf("%d ", sumai);
			sumai = 0;
		}
	}

}
