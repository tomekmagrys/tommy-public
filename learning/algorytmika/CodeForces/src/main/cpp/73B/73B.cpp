#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <cmath>
#include <algorithm>
#define DEBUG2




int main(int argc, char** argv)
{
    double x1,y1,x2,y2,x3,y3,x5,y5,x1tab[10000],y1tab[10000],x5tab[10000],y5tab[10000],angle[10000],w[10000];
    int k,n;
    double length;
    double miny[10001];

    scanf("%lf%lf%lf%lf",&x2,&y2,&x3,&y3);
    length = sqrt(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3);
    scanf("%d",&n); // TO DO obsluzyc kilka czolgow
    for(int i = 0 ;i < n ; i++){
      scanf("%lf%lf%lf%lf",&x5tab[i],&y5tab[i],&angle[i],&w[i]);
      x1tab[i] = cos(angle[i]);
      y1tab[i] = sin(angle[i]);

    }
    scanf("%d",&k);



    #ifdef DEBUG
    printf("Czolg zmierza z punkt (%f,%f) do punktu (%f,%f)\n",x2,y2,x3,y3);
    #endif

    for(int i = 0 ;i < n ; i++){

      x1 = x1tab[i];
      y1 = y1tab[i];
      x5 = x5tab[i];
      y5 = y5tab[i]; 
      #ifdef DEBUG
      printf("Atakuje go czolg z punktu (%f,%f)\n",x5,y5);
      printf("Znormalizowana lufa skierowana w kierunku wektora: (%f,%f)\n",x1,y1);
      #endif
                    // (x1*(x3-x2)/sqrt(x2^2 -2*x2*x3+x3^2+y2^2  -2*y2*y3+y3^2 )+y1*(y3-y2)/sqrt(x2^2 -2*x2*x3+x3^2+y2^2  -2*y2*y3+y3^2  ))*x+x1*(x2-x5)+y1*(y2-y5)

      double a,b,c,d,e;
      a = x1*(x3-x2)/sqrt(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3)+y1*(y3-y2)/sqrt(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3);
      b = x1*(x2-x5)+y1*(y2-y5);
      //(x3-x2)^2/(x2^2-2*x2*x3+x3^2+y2^2-2*y2*y3+y3^2)+(y3-y2)^2/(x2^2-2*x2*x3+x3^2+y2^2-2*y2*y3+y3^2)
      c = (x3-x2)*(x3-x2)/(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3)+(y3-y2)*(y3-y2)/(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3);
      //(2*(x2-x5))*(x3-x2)/sqrt(x2^2-2*x2*x3+x3^2+y2^2-2*y2*y3+y3^2)+(2*(y2-y5))*(y3-y2)/sqrt(x2^2-2*x2*x3+x3^2+y2^2-2*y2*y3+y3^2)
      d = (2*(x2-x5))*(x3-x2)/sqrt(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3)+(2*(y2-y5))*(y3-y2)/sqrt(x2*x2-2*x2*x3+x3*x3+y2*y2-2*y2*y3+y3*y3);
      e = (x2-x5)*(x2-x5)+(y2-y5)*(y2-y5);
      #ifdef DEBUG
      printf("%f x + %f\n",a,b);
      printf("---------------------------------------\n");
      printf("%f x^2 + %f x + %f\n",c,d,e);
      printf("Wartosc w jednym koncu: %f\n",acos(b/sqrt(e)));
      printf("Wartosc w drugim koncu: %f\n",acos((a*length+b)/sqrt(c*length*length+d*length+e)));
      #endif
      //printf("%.4f",length/acos(b/sqrt(e)));

      //double min1 = acos(b/sqrt(e))/w[i];
      
//      printf("%lf\n",min1);

        double min1 = acos((a*length+b)/sqrt(c*length*length+d*length+e))/w[i]/length;
        //printf("%lf\n",min1);

      for(int j = 1 ; j < 50 ; j++){
        double x = length*j/50;
        double min2 = acos((a*x+b)/sqrt(c*x*x+d*x+e))/w[i]/x;
        if(min2 < min1) min1 = min2;
      }
       miny[i] = 1/min1;
       //printf("%lf\n",miny[i]);
    }
    //*x+x1*(x2-x5)+y1*(y2-y5));
    if(k >= n){
      printf("%.4f",0);
    }
    else{
    std::sort(miny,miny+n);
     printf("%.4f",miny[n-k-1]);
    }
}
