#include <cstdio>
#include <cstdlib>
#include <string.h>

int n, k, wagi[100];

int main(int argc, char** argv) {
	scanf("%d%d", &n, &k);
	for (int i = 0; i < n; i++) {
		scanf("%d", &wagi[i]);
	}
	int licznik = 0;
	while (wagi[0] < k) {
		for (int i = 0; i < n && wagi[i] < k; i++) {
			if (i == n - 1 || wagi[i] != wagi[i + 1]) {
				wagi[i]++;
			}
		}
		licznik++;
	}
	printf("%d\n", licznik);
}
