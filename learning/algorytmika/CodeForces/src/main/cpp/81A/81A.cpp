#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;
struct Dupa {
	char litera;
	int krotnosc;
};

char input[200001];
vector<Dupa> output;

int main() {
	scanf("%s", input);
	Dupa tmp;
	tmp.litera = input[0];
	tmp.krotnosc = 1;
	output.push_back(tmp);

	for (unsigned int i = 1; input[i]; i++) {
		if (output.size() && input[i] == output[output.size() - 1].litera) {
			output[output.size() - 1].krotnosc++;
		} else {
			while (output.size() && output[output.size() - 1].krotnosc > 1) {
				if (output[output.size() - 1].krotnosc % 2 == 0)
					output.pop_back();
				else
					output[output.size() - 1].krotnosc = 1;
			}
			if (output.size() == 0
					|| input[i] != output[output.size() - 1].litera) {
				tmp.litera = input[i];
				tmp.krotnosc = 1;
				output.push_back(tmp);
			} else {
				output[output.size() - 1].krotnosc++;
			}
		}
	}
	while (output.size() && output[output.size() - 1].krotnosc > 1) {
		if (output[output.size() - 1].krotnosc % 2 == 0)
			output.pop_back();
		else
			output[output.size() - 1].krotnosc = 1;
	}

	for (unsigned int i = 0; i < output.size(); i++) {
		printf("%c", output[i].litera);
	}
	printf("\n");
	return 0;
}
