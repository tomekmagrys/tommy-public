#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;

int x,y;

int main(){
  scanf("%d%d",&x,&y);

  bool ciel = true;

  while(true){
    if(ciel){
      if(x >= 2 && y >=2){
	x -= 2;
	y -= 2;
	ciel = !ciel;
	continue;
      }
      if(x >= 1 && y >= 12){
	x -= 1;
	y -= 12;
	ciel = !ciel;
	continue;
      }
      if(y >= 22){
	y -= 22;
	ciel = !ciel;
	continue;
      }
    }
    else{
      if( y >= 22){
	y -= 22;
	ciel = !ciel;
	continue;
      }
      if(y >= 12 && x >= 1){
	y -= 12;
	x -= 1;
	ciel = !ciel;
	continue;
      }
      if(y >= 2 && x >= 2){
	y -= 2;
	x -= 2;
	ciel = !ciel;
	continue;
      }
    }
    break;
  }
  printf("%s\n",!ciel ? "Ciel" : "Hanako");
  return 0;
}
