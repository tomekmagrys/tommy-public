#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>

using namespace std;

bool mozliwosci[100001];
bool nastroje[100001];

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);

	for (int i = 0; i < n; i++) {
		int tmp;
		scanf("%d", &tmp);
		nastroje[i] = (tmp == 1);
	}

	for (int i = 1; i <= n / 3; i++) {
		if (n % i)
			continue;
		//printf("i=%d\n",i);
		for (int j = 0; j < i; j++) {
			//printf("j=%d\n",j);
			bool bedziej = true;
			for (int k = 0; k < n / i; k++) {
				//printf("k=%d\n",k);
				if (!nastroje[k * i + j]) {
					bedziej = false;
					break;
				}
			}
			if (bedziej) {
				printf("YES\n");
				return 0;
			}
		}
	}
	printf("NO\n");
	return 0;
}
