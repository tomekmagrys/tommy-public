#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

// pierwszy strzela x
// drugi y
// trzeci z

// x + y + z
// y <= n-x <=> x + y <= n
// z <= n-y <=> y + z <= n
// x + z <= n

// (x + y + z) = (x + y + y + z + z + x)/2 <= (n + n + n)/3

int main() {
	int n;
	scanf("%d", &n);

	printf("%d\n", n / 2 * 3);
	return 0;
}
