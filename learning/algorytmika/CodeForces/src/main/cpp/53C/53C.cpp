#include <cstdio>
#include <cstdlib>
#include <string.h>

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);

	for (int i = 1; i <= n / 2; i++) {
		printf("%d %d ", i, n - i + 1);

	}
	if (n % 2 == 1)
		printf("%d ", (n + 1) / 2);
	printf("\n");

}
