#include <cstdio>

int n;
int d[101];
int a, b;

int main(int argc, char** argv) {

	scanf("%d", &n);

	for (int i = 1; i < n; i++) {
		scanf("%d", &d[i]);
	}

	scanf("%d%d", &a, &b);

	int output = 0;

	for (int i = a; i < b; i++) {
		output += d[i];
	}

	printf("%d", output);
}
