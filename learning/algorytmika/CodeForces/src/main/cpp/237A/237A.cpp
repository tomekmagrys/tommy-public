#include <cstdio>
#include <algorithm>

using namespace std;

int d[24][60];
int n,h,m;

int main(){

  scanf("%d",&n);
  
  for(int i = 0 ; i < 24 ; i++){
    for(int j = 0 ; j < 60 ; j++) {
      d[i][j] = 0;
    }
  }
  
  for(int i = 0; i < n ; i++){
    scanf("%d %d",&h,&m);
    d[h][m]++;
  }
  
  int mx = 0;
  
  for(int i = 0 ; i < 24 ; i++){
     for(int j = 0 ; j < 60 ; j++) {
      mx = max(mx,d[i][j]);
    }
  }
  
  printf("%d\n",mx);
  
}
