#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;

char s[101];
char t[101];

int main() {
	scanf("%s%s", s, t);

	if (strlen(s) != strlen(t)) {
		printf("NO\n");
		return 0;
	}

	for (int i = 0; i < strlen(t); i++) {
		if (s[i] != t[strlen(t) - 1 - i]) {
			printf("NO\n");
			return 0;
		}
	}

	printf("YES\n");
	return 0;
}
