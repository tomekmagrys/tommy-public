#include<cstdio>
#include<algorithm>

int n, m;

int count[40];
int taken[1000];

int main() {
	scanf("%d%d", &n, &m);

	for (int i = 0; i < m; i++) {
		scanf("%d", &count[i]);
	}

	//std::sort(count,count+m);

	//int i = 0;

	int pierwszymax = 0;

	for (int i = 1; i < m; i++) {
		if (count[i] > count[pierwszymax]) {
			pierwszymax = i;
		}
	}

	bool nieDaRady = false;

	for (int i = 0; i < n; i++) {
		int max_index = -1;
		int max_value = 0;
		for (int j = 0; j < m; j++) {
			if ((i == 0 || (i < n - 1 && j != taken[i - 1])
					|| (i == n - 1 && j != taken[i - 1] && j != taken[0]))
					&& (count[j] > max_value
							|| (count[j] == max_value && j == pierwszymax
									&& count[j] > 0))) {
				max_value = count[j];
				max_index = j;
			}
		}
		if (max_index >= 0) {
			count[max_index]--;
		} else {
			nieDaRady = true;
		}
		taken[i] = max_index;
		//printf("%d ",taken[i]+1);
	}
	if (nieDaRady) {
		printf("-1");
	} else {
		for (int i = 0; i < n; i++) {
			printf("%d ", taken[i] + 1);
		}
	}
	printf("\n");

	return 0;
}
