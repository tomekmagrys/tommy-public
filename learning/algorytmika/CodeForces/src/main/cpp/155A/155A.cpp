#include <cstdio>
#include <algorithm>

using namespace std;

int points[1000];
int n;
int amazing;

int main(){

  amazing = 0;
  
  scanf("%d",&n);
  
  for(int i = 0 ; i < n ; i++){
    scanf("%d",&points[i]);
  }
  
  int max = points[0];
  int min = points[0];
  
  for(int i = 1 ; i < n ; i++){
    if(points[i] > max){
      amazing++;
      max = points[i];
    }
    if(points[i] < min){
      min = points[i];
      amazing++;
    }
  }
  
  printf("%d\n",amazing);
  
}
