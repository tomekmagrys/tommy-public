#include <cstdio>
#include <cstdlib>
#include <string.h>

int permuty[100001];
int outputy[100000];

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= 10000; i++) {
		permuty[i] = 0;
	}
	for (int i = 0; i < n; i++) {
		int temp;
		scanf("%d", &temp);
		permuty[temp]++;
		outputy[i] = permuty[temp];
	}
	for (int i = 2; i <= 10000; i++) {
		if (permuty[i] > permuty[i - 1]) {
			printf("-1\n");
			return 0;
		}
	}
	printf("%d\n", permuty[1]);
	for (int i = 0; i < n; i++) {
		printf("%d ", outputy[i]);
	}
}
