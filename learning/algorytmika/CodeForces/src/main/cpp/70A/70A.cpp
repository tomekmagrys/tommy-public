#include <cstdio>

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	int wynik = 1;

	for (int i = 1; i < n; i++) {
		wynik *= 3;
		wynik = wynik % 1000003;
	}

	printf("%d\n", wynik);
}
