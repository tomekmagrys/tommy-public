#include <cstdio>
#include <string.h>

char trunek[101];

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);

	//ABSINTH, BEER, BRANDY, CHAMPAGNE, GIN, RUM, SAKE, TEQUILA, VODKA, WHISKEY, WINE
	int dosprawdzenia = 0;
	for (int i = 0; i < n; i++) {
		scanf("%s", trunek);
		if (strcmp(trunek, "ABSINTH") == 0 || strcmp(trunek, "BEER") == 0
				|| strcmp(trunek, "BRANDY") == 0
				|| strcmp(trunek, "CHAMPAGNE") == 0
				|| strcmp(trunek, "GIN") == 0 || strcmp(trunek, "RUM") == 0
				|| strcmp(trunek, "SAKE") == 0 || strcmp(trunek, "TEQUILA") == 0
				|| strcmp(trunek, "VODKA") == 0
				|| strcmp(trunek, "WHISKEY") == 0 || strcmp(trunek, "WINE") == 0
				|| strcmp(trunek, "0") == 0 || strcmp(trunek, "1") == 0
				|| strcmp(trunek, "2") == 0 || strcmp(trunek, "3") == 0
				|| strcmp(trunek, "4") == 0 || strcmp(trunek, "5") == 0
				|| strcmp(trunek, "6") == 0 || strcmp(trunek, "7") == 0
				|| strcmp(trunek, "8") == 0 || strcmp(trunek, "9") == 0
				|| strcmp(trunek, "10") == 0 || strcmp(trunek, "11") == 0
				|| strcmp(trunek, "12") == 0 || strcmp(trunek, "13") == 0
				|| strcmp(trunek, "14") == 0 || strcmp(trunek, "15") == 0
				|| strcmp(trunek, "16") == 0 || strcmp(trunek, "17") == 0) {
			dosprawdzenia++;
		}
	}
	printf("%d\n", dosprawdzenia);
}
