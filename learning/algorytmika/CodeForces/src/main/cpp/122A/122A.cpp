#include <cstdio>
#include <algorithm>

using namespace std;

int n;

int main(){

  scanf("%d",&n);
  
  for(int i = 1 ; i*i <= n ; i++){
    if(n % i == 0){
      int a = i;
      int b = n/i;
      if(a == 4 || a == 7){
	printf("YES\n"); return 0;
      }
      if(b == 4 || b == 7 || b == 44 || b == 47 || b == 74 || b == 77 ||
	b == 444 || b == 447 || b == 474 || b == 477 ||
	b == 744 || b == 747 || b == 774 || b == 777){
	printf("YES\n"); return 0;
      }
    }
  }
  printf("NO\n"); return 0;
}
