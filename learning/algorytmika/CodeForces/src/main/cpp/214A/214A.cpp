#include <cstdio>
#include <algorithm>

using namespace std;

int k,m,n;


int main(){

  scanf("%d%d",&n,&m);
  
  int retVal = 0;
  
  for(int a = 0 ; a <= n ; a++){
    for(int b = 0 ; a*a+b <= m && b*b+a <= n ; b++){
      retVal += (a*a+b == m && b*b+a == n);
    }
  }
  
  printf("%d\n",retVal);
}
