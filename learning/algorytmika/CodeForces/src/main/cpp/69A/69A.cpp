#include <cstdio>
#include <cstdlib>
#include <string.h>

int x, y, z, tempx, tempy, tempz;

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d%d%d", &tempx, &tempy, &tempz);
		x += tempx;
		y += tempy;
		z += tempz;
	}
	if (x == 0 && y == 0 && z == 0) {
		printf("YES\n");
		return 0;
	}
	printf("NO\n");
	return 0;
}
