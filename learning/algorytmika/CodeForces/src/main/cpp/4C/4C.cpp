#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>

using namespace std;
int n;

struct nameNumber {
	string name;
	int number;
	friend bool operator <(const nameNumber &a, const nameNumber &b);
};

bool operator <(const nameNumber& a, const nameNumber &b) {
	return a.name < b.name;
}

int main(int argc, char** argv) {
	scanf("%d", &n);
	set<nameNumber> names;

	pair<set<nameNumber>::iterator, bool> ret;

	for (int i = 0; i < n; i++) {
		nameNumber doinsert;
		cin >> doinsert.name;
		doinsert.number = 0;
		ret = names.insert(doinsert);
		if (ret.second == false) {
			nameNumber * tmp = (nameNumber *) &(*(ret.first));
			tmp->number++;
			cout << (*(ret.first)).name << (*(ret.first)).number << endl;

		} else {
			printf("OK\n");
		}
	}
}
