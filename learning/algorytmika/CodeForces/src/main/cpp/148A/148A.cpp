#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>

int gcd(int n,int m){return m==0?n:gcd(m,n%m);}

int nww(int a,int b){
  return a*b/gcd(a,b);
}

int nww(int a,int b,int c){
  return nww(nww(a,b),c);
}

int nww(int a,int b,int c,int d){
  return nww(nww(nww(a,b),c),d);
}

int main(){
  int k,l,m,n,d;
  
  scanf("%d%d%d%d%d",&k,&l,&m,&n,&d);
  printf("%d\n",-( - d/k - d/l - d/m - d/n + d/nww(k,l) + d/nww(k,m) + d/nww(k,n) + d/nww(l,m) + d/nww(l,n) + d/nww(m,n)
  -d/nww(k,l,m) -d/nww(l,m,n) -d/nww(m,n,k) -d/nww(k,l,n) + d/nww(k,l,m,n)));
}