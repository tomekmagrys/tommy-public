#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;

char input[100001];

int sumOfDigits(int number){
  int retVal = 0;
  while(number > 0){
    retVal += number % 10;
    number /= 10;
  }
  return retVal;
}

int harryPotter(int digit){
  //printf("%d\n",digit);
  //int temp; scanf("%d",&temp);
  if(digit < 10) return 0;
  return harryPotter(sumOfDigits(digit)) + 1;
}

int main(){
  scanf("%s",input);
  if(input[1] == 0){
    printf("0\n");
    return 0;
  }
  int sum = 0;
  for(int i = 0 ; input[i] != 0 ; i++){
    sum += input[i]-'0'; 
  }
  
  printf("%d\n",harryPotter(sum)+1);
  
  return 0;
}