#include <cstdio>
#include <algorithm>

using namespace std;

int k;
int a[12];

int main(){

  scanf("%d",&k);
  
  for(int i = 0 ; i < 12 ; i++){
    scanf("%d",&a[i]);
  }
  
  sort(a,a+12);
  
  int i = 0;
  int suma = 0;
  
  while(suma < k && i < 12){
    suma += a[11-i];
    i++;
  }
  
  if(suma < k){
    printf("%d\n",-1);
  } else {
    printf("%d\n",i);
  }
  
}
