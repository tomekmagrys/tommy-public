#include <cstdio>
#include <algorithm>

using namespace std;

int n;
int money[100];

int main(){
  scanf("%d",&n);

  for(int i = 0 ; i < n ; i++){
    scanf("%d",&money[i]); 
  }
  
  sort(money,money+n);
  
  int suma = 0;
  
  for(int i = 0 ; i < n ; i++){
    suma += money[i];
  }
  
  int newSuma = 0;
  
  for(int i = 0 ; i < n ; i++){
    newSuma += money[n-i-1];
    if(newSuma * 2 > suma){
      printf("%d",i+1);
      return 0;
    }
  }
  
  
  
}