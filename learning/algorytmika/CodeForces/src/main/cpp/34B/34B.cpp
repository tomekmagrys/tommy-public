#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

int ceny[100];

int main(int argc, char** argv) {
	int m, n;
	scanf("%d%d", &n, &m);
	for (int i = 0; i < n; i++) {
		scanf("%d", &ceny[i]);
	}
	sort(ceny, ceny + 100);

	int zysk = 0;
	for (int i = 0; ceny[i] < 0 && i < m; i++) {
		zysk -= ceny[i];
	}
	printf("%d\n", zysk);
}
