#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>

using namespace std;

char _strony[1000000];
bool strony[1002];

int main(int argc, char** argv) {
	scanf("%s", _strony);

	int tempstronka = 0;

	for (int i = 0; _strony[i]; i++) {
		if (_strony[i] == ',') {
			strony[tempstronka] = true;
			tempstronka = 0;
		} else {
			tempstronka = tempstronka * 10 + _strony[i] - '0';
		}
	}
	strony[tempstronka] = true;

	bool pierwszyraz = true;
	strony[0] = false;
	strony[1001] = false;
	int poczatek = 0;
	int koniec = 0;
	for (int i = 1; i <= 1001; i++) {
		if (strony[i] && !strony[i - 1]) {
			poczatek = i;
		}
		if (!strony[i] && strony[i - 1]) {
			koniec = i - 1;
			if (poczatek == koniec) {
				if (!pierwszyraz) {
					printf(",");
				}
				printf("%d", poczatek);
				pierwszyraz = false;
			} else {
				if (!pierwszyraz) {
					printf(",");
				}
				printf("%d-%d", poczatek, koniec);
				pierwszyraz = false;
			}
		}
	}
	printf("\n");
	return 0;
}
