#include <cstdio>
#include <cstdlib>
#include <string.h>

int wysokosci[1000];
int podlewania[1000];

int main(int argc, char** argv) {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &wysokosci[i]);
	}
	for (int i = 0; i < n; i++) {
		podlewania[i] = 1;
		int j = i - 1;
		while (j >= 0 && wysokosci[j] <= wysokosci[j + 1]) {
			podlewania[i]++;
			j--;
		}
		j = i + 1;
		while (j < n && wysokosci[j] <= wysokosci[j - 1]) {
			podlewania[i]++;
			j++;
		}
	}
	int max = 0;
	for (int i = 0; i < n; i++) {
		if (podlewania[i] > max)
			max = podlewania[i];
	}
	printf("%d\n", max);
}
