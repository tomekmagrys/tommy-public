#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

int a, b, n;

int main() {
	scanf("%d%d%d", &a, &b, &n);

	while (n) {
		n -= __gcd(a, n);
		if (n == 0) {
			printf("%d\n", 0);
			break;
		}
		n -= __gcd(b, n);
		if (n == 0) {
			printf("%d\n", 1);
			break;
		}
	}

}
