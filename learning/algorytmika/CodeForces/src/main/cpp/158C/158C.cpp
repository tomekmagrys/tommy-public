#include <cstdio>
#include <cstring>
#include <cstdlib>

char cmd[4];
char param[256];
char gpath[16384];

void cd(char * path,char * param){
  if(param[0] == 0){
    path[0] = 0;
    return;
  }
  if(param[0] == '.'){
    path--;
    while(path[0] == '/'){
      path--;
    }
   while(path[0] != '/'){
      path--;
    }    
    if(param[2] != 0){
      cd(path+1,param+3);
    } else {
      cd(path+1,param+2);
    }
  } else {
    for(;param[0] != '/' && param[0] != 0;){
      path[0] = param[0];
      path++;
      param++;
    }
    path[0] = '/';
    if(param[0] == '/'){
      cd(path+1,param+1);
    } else {
      cd(path+1,param);
    }
  }
}



int main(){
  int n;
  gpath[0] = '/';
  gpath[1] = 0;
  scanf("%d",&n);
  while(n--){
    scanf("%s",cmd);
    if(cmd[0] == 'p'){
      printf("%s\n",gpath);
    } else{
      scanf("%s",param);
      if(param[0] == '/'){
	cd(gpath+1,param+1);
      }else{
	int len = strlen(gpath);
	if(gpath[len-1] != '/'){
	  gpath[len] = '/';	
	  cd(gpath+len+1,param);
	} else {
	  cd(gpath+len,param);	  
	}
      }
    }
  }
}