#include<cstdio>
#include<algorithm>
#include<iostream>

float p(int m,int n){
  if(n == 0) return 1;
  if(m == 0) return 1/(1.0 + n);
  return -1;
}

int m,n;

int main(){
 
  scanf("%d%d",&m,&n);
  printf("%.9f %.9f\n",p(m,n),1-p(m,n));
  return 0;
}
