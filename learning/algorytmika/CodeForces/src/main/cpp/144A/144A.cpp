#include <cstdio>
#include <algorithm>

using namespace std;

int n;

int soldiers[100];

int main(){
  scanf("%d",&n);
  
  for(int i = 0 ; i < n ; i++){
    scanf("%d",&soldiers[i]);
  }
  
  int minIndex = n-1;
  
  int maxIndex = 0;
  
  for(int i = 1 ; i < n ; i++){
     if(soldiers[i] > soldiers[maxIndex]){
       maxIndex = i;
     }
  }
  
  for(int i = n-2 ; i>= 0 ; i--){
     if(soldiers[i] < soldiers[minIndex]){
       minIndex = i;
     }     
  }
  
  printf("%d\n",maxIndex + (n-1-minIndex) - (maxIndex > minIndex));
  
}