#include <cstdio>
#include <algorithm>

using namespace std;

int n;

pair<int,int> points[200];

int main(){

  scanf("%d",&n);
  
  for(int i = 0 ;i < n ; i++){
    int x,y;
    scanf("%d %d",&x,&y);
    points[i] = make_pair(x,y);
  }
  
  int retVal = 0;
  
  for(int i = 0 ; i < n ; i++){
    bool right = false;
    bool left = false;
    bool up = false;
    bool down = false;
    for(int j = 0 ; j < n ; j++){
      if(points[j].first < points[i].first && points[j].second == points[i].second){
	left = true;
      }
      if(points[j].first > points[i].first && points[j].second == points[i].second){
	right = true;
      }
      if(points[j].first == points[i].first && points[j].second > points[i].second){
	up = true;
      }
      if(points[j].first == points[i].first && points[j].second < points[i].second){
	down = true;
      }
      if(left && right && down && up){
	break;
      }
    }
    retVal += (left && right && down && up);
  }
  
  printf("%d\n",retVal);
  
}
