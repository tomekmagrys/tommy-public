#include <cstdio>
#include <algorithm>

using namespace std;

char dubstep[201];

int main(){

  scanf("%s",dubstep);
  
  int i = 0;
  
  bool printSpace = false;
  bool wasLetter = false;
  
  while(dubstep[i]){
    if(dubstep[i] == 'W' && dubstep[i+1] == 'U' && dubstep[i+2] == 'B'){
      i+= 3;
      if(wasLetter) printSpace = true;
      continue;
    }
    if(printSpace){
      printf("%c",' ');
      printSpace = false;
    }
    printf("%c",dubstep[i]);
    wasLetter = true;
    i++;
  }
  
  printf("\n");
}
