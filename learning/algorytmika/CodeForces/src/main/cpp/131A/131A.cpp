#include <cstdio>

char napis[101];

int main(){
  scanf("%s",napis);
  bool caps = true;
  
  for(int i = 1; napis[i] != 0 ; i++){
    if(napis[i] < 'A' || napis[i] > 'Z'){
      caps = false;
    }
  }
  
  if(caps){
    if(napis[0] >= 'A' && napis[0] <= 'Z')
      napis[0] += 'a' - 'A';
    else
      napis[0] += 'A' - 'a';
    for(int i = 1 ; napis[i] != 0 ; i++){
      napis[i] += 'a' - 'A';
    }
  }
  
  printf("%s\n",napis);
  
}