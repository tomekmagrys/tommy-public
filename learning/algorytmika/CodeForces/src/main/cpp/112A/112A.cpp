#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>

using namespace std;

char a[101];
char b[101];

int main() {
	scanf("%s %s", a, b);
	for (int i = 0; a[i]; i++) {
		if (a[i] >= 'A' && a[i] <= 'Z') {
			a[i] += 'a' - 'A';
		}
		if (b[i] >= 'A' && b[i] <= 'Z') {
			b[i] += 'a' - 'A';
		}
		if (a[i] > b[i]) {
			printf("%d\n", 1);
			return 0;
		}
		if (a[i] < b[i]) {
			printf("%d\n", -1);
			return 0;
		}
	}

	printf("0\n");
}
