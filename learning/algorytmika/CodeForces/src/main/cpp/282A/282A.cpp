#include <cstdio>
#include <algorithm>

using namespace std;

int n;
char buf[4];

int main(){
  scanf("%d",&n);
  
  int x = 0;
  
  for(int i = 0 ; i < n ; i++){
    scanf("%s",buf); 
    if(buf[1] == '+'){
      x++;
    } else {
      x--;
    }
  }
  
  printf("%d",x);
    
}