#include <cstdio>
#include <algorithm>

using namespace std;

int t,a;

int main(){

  scanf("%d",&t);
  
  while(t--){
    scanf("%d",&a);
    bool found = false;
    for(int i = 3; i <= 360 ; i++){
      if((i-2)*180 % i == 0 && (i-2)*180 / i == a){
	  found = true;
	  break;
	}
      }
      if(found){
	printf("YES\n");
      } else {
	printf("NO\n");
      }
    }
  
}
