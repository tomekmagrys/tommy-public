#include <cstdio>
#include <cstring>
#include <cstdlib>

int t[20000];

int main(){
  int n;
  int maxSuma = -2000000000;
  scanf("%d",&n);
  for(int i = 0 ; i < n ; i++){
    scanf("%d",&t[i]);
  }
  for(int i = 1 ; i <= n/3 ; i++){ //okres
    if(n % i != 0) continue;
      
    for(int j = 0 ; j < i ; j++){ //poczatek
      int suma = 0;
      for(int k = j ; k < n+j ; k +=  i){
	suma += t[k%n];
      }
      if(suma > maxSuma) maxSuma = suma;
      
    }
    
  }
  printf("%d\n",maxSuma);
}