#include <cstdio>
#include <cstdlib>
#include <string.h>

int temp;

int main(int argc, char** argv) {
	int n, l;
	scanf("%d%d", &n, &l);
	int wynik[101];
	int dlug[101];
	for (int i = 0; i < n; i++) {
		scanf("%d", &dlug[i]);
	}

	for (int k = l; k <= 100; k++) {
		wynik[k] = 0;
		for (int i = 0; i < n; i++) {
			wynik[k] += dlug[i] - dlug[i] % k;
		}
	}

	int max = 0;

	for (int i = l; i <= 100; i++)
		max = (max > wynik[i] ? max : wynik[i]);

	printf("%d\n", max);

}
