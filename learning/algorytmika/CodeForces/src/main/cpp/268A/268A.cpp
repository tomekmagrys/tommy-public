#include <cstdio>
#include <algorithm>

using namespace std;

int n;

int h[30];
int a[30];

int main(){
  scanf("%d",&n);

  for(int i = 0 ; i < n ; i++){
    scanf("%d %d",&h[i],&a[i]);
  }
  
  int retVal = 0;
  
  for(int i = 0 ; i < n ; i++){
    for(int j = 0 ; j < n ; j++){
      if(a[i] == h[j]){
	retVal++;
      }
    }
  }
  
  printf("%d\n",retVal);
  
}