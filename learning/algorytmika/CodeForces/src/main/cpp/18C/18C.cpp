#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

using namespace std;
int n;

int squares[100000];

int main(int argc, char** argv) {
	scanf("%d", &n);
	int suma = 0;

	for (int i = 0; i < n; i++) {
		scanf("%d", &squares[i]);
		suma += squares[i];
	}

	int tempsuma = 0;
	int wynik = 0;
	for (int i = 0; i < n - 1; i++) {
		tempsuma += squares[i];
		if (tempsuma * 2 == suma) {
			wynik++;
		}
	}
	printf("%d\n", wynik);

}
