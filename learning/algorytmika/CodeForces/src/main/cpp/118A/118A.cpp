#include <cstdio>
#include <cstdlib>
#include <string.h>

using namespace std;

char input[101];

int main() {
	scanf("%s", input);
	for (int i = 0; input[i]; i++) {
		if ('A' <= input[i] && input[i] <= 'Z')
			input[i] += 'a' - 'A';

		if (input[i] == 'a' || input[i] == 'o' || input[i] == 'y'
				|| input[i] == 'e' || input[i] == 'u' || input[i] == 'i') {
		} else {
			printf(".%c", input[i]);
		}
	}
	printf("\n");
}
