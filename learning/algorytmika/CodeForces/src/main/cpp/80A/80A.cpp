#include <cstdio>
#include <string.h>

bool isprime(int a) {
	for (int j = 2; j * j <= a; j++) {
		if (a % j == 0) {
			return false;
		}
	}
	return true;
}

int main(int argc, char ** argv) {
	int x, y;
	scanf("%d%d", &x, &y);
	for (int i = x + 1; i < y; i++) {
		if (isprime(i)) {
			printf("NO\n");
			return 0;
		}
	}
	if (isprime(y)) {
		printf("YES\n");
	} else {
		printf("NO\n");
	}
}
