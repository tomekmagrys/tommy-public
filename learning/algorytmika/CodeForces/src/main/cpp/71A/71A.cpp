#include <cstdio>
#include <cstdlib>
#include <string.h>

char slowo[101];

int main(int argc, char** argv) {
	int n; //,m,a;
	scanf("%d", &n);
	while (n--) {
		scanf("%s", &slowo);
		if (strlen(slowo) > 10) {
			printf("%c%d%c\n", slowo[0], strlen(slowo) - 2,
					slowo[strlen(slowo) - 1]);
		} else {
			printf("%s\n", slowo);
		}

	}
}
