#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <algorithm>
#include <vector>

using namespace std;

bool isPrime(int n) {
	for (int i = 2; i * i <= n; i++) {
		if (n % i == 0)
			return false;
	}
	return true;
}

int nextPrime(int n) {
	if (isPrime(n))
		return n;
	return nextPrime(n + 1);
}

int n;

int main() {

	int firstPrime = 2;
	int secondPrime = 3;

	int k, n;
	scanf("%d%d", &n, &k);

	if (k == 0) {
		printf("YES\n");
		return 0;
	}
	if (n < 6) {
		printf("NO\n");
		return 0;
	}

	int i = 0;

	while (i < k && firstPrime + secondPrime + 1 <= n) {
		i += isPrime(firstPrime + secondPrime + 1);
		//printf("%d %d %d\n",i,firstPrime,secondPrime);
		firstPrime = secondPrime;
		secondPrime = nextPrime(secondPrime + 1);
	}

	if (i >= k) {
		printf("YES\n");
		return 0;
	}
	printf("NO\n");
	return 0;
}
