#include<cstdio>
#include<algorithm>

int main() {
	int n;
	scanf("%d", &n);

	int previousPowerOf5 = 1;
	int currentPowerOf5 = 5;

	while (n > currentPowerOf5) {
		//printf("%d %d %d\n",n,previousPowerOf5,currentPowerOf5);
		n -= currentPowerOf5;
		previousPowerOf5 *= 2;
		currentPowerOf5 *= 2;
	}
	//printf("%d %d %d\n",n,previousPowerOf5,currentPowerOf5);
	int result = (n - 1) / previousPowerOf5;

	switch (result) {
	case 0:
		printf("Sheldon\n");
		break;
	case 1:
		printf("Leonard\n");
		break;
	case 2:
		printf("Penny\n");
		break;
	case 3:
		printf("Rajesh\n");
		break;
	case 4:
		printf("Howard\n");
		break;
	}

}
