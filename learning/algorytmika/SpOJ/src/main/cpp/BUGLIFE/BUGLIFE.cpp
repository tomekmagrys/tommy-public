#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string.h>

int bugs[2000];
bool interactions[2000][2000];

bool sukces;
int n, m;

void dfs(int i, int orient) {
	switch (bugs[i]) {
	case 2:
		if (orient == 1)
			sukces = false;
		return;
	case 1:
		if (orient == 2)
			sukces = false;
		return;
	case 0:
		bugs[i] = orient;
		for (int j = 0; j < n; j++) {
			if (interactions[i][j])
				dfs(j, 3 - orient);
		}
	}
}

int main(int argc, char** argv) {
	int tests;
	scanf("%d", &tests);
	for (int z = 1; z <= tests; z++) {
		sukces = true;
		scanf("%d %d", &n, &m);
		for (int i = 0; i < n; i++) {
			bugs[i] = 0;
			for (int j = 0; j < n; j++) {
				interactions[i][j] = false;
			}
		}
		for (int i = 0; i < m; i++) {
			int temp1, temp2;
			scanf("%d %d", &temp1, &temp2);
			interactions[temp1 - 1][temp2 - 1] = true;
			interactions[temp2 - 1][temp1 - 1] = true;
		}
		for (int i = 0; i < n; i++) {
			if (bugs[i] == 0)
				dfs(i, 1);
		}

		printf("Scenario #%d:\n", z);
		if (!sukces) {
			printf("Suspicious bugs found!\n");
		} else {
			printf("No suspicious bugs found!\n");
		}
	}
}
