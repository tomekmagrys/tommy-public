#include <cstdio>
#include <cstdlib>
#include <algorithm>

int wymiary[20000];

int main(int argc, char** argv) {
	int tests;
	scanf("%d", &tests);
	while (tests--) {
		int K, N;
		scanf("%d%d", &N, &K);
		for (int i = 0; i < N; i++) {
			scanf("%d", &wymiary[i]);
		}
		std::sort(wymiary, wymiary + N);
		int minDiff = 0x7fffffff;
		for (int i = 0; i < N - K + 1; i++) {
			if (wymiary[K - 1 + i] - wymiary[i] < minDiff) {
				minDiff = wymiary[K - 1 + i] - wymiary[i];

			}
		}
		printf("%d\n", minDiff);
	}
}
