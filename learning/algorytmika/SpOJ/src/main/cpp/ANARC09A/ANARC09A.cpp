#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string.h>

char braces[2001];

int main(int argc, char** argv) {
	int k = 0;
	while (++k) {
		scanf("%s", braces);
		if (braces[0] == '-')
			break;
		int wynik = 0;
		int openedBraces = 0;
		int n = strlen(braces);
		for (int i = 0; i < n; i++) {
			if (braces[i] == '{') {
				openedBraces++;
			} else {
				if (openedBraces > 0) {
					openedBraces--;
				} else {
					openedBraces++;
					wynik++;
				}
			}
		}

		printf("%d. %d\n", k, wynik + openedBraces / 2);
	}
}
