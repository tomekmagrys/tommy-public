#include<cstdio>
#include<cstring>

int main() {
	int t;
	char * K;
	K = new char[1000002];
	scanf("%d", &t);
	while (t--) {
		K[0] = '0';
		scanf("%s", K + 1);
		int begin = 1;
		int end = strlen(K + 1);
		while (end - begin > 1) {
			begin++;
			end--;
		}
		bool mniejsze = false;
		bool wieksze = false;

		int tempBegin = begin;
		int tempEnd = end;
		while (tempBegin > 0) {
			if (K[tempBegin] < K[tempEnd]) {
				mniejsze = true;
				break;
			}
			if (K[tempBegin] > K[tempEnd]) {
				wieksze = true;
				break;
			}
			tempBegin--;
			tempEnd++;
		}
		if (!wieksze) {
			tempBegin = begin;
			K[tempBegin]++;
			while (K[tempBegin] > '9') {
				K[tempBegin] = '0';
				K[tempBegin - 1]++;
				tempBegin--;
			}
		}
		tempBegin = begin;
		tempEnd = end;
		while (tempBegin > 0) {
			K[tempEnd] = K[tempBegin];
			tempBegin--;
			tempEnd++;
		}
		if (K[0] == '0') {
			printf("%s\n", K + 1);
		} else {
			K[strlen(K + 1)] = '1';
			printf("%s\n", K);
		}
	}
	delete[] K;
}
