#include <cstdio>
#include <cstdlib>
#include <algorithm>

int dyn[1000001];

int main(int argc, char** argv) {
	int K, L, m;
	scanf("%d%d%d", &K, &L, &m);
	dyn[0] = -1;
	for (int i = 1; i <= 1000000; i++) {
		int tempMin = dyn[i - 1];
		if (i >= K && dyn[i - K] < tempMin)
			tempMin = dyn[i - K];
		if (i >= L && dyn[i - L] < tempMin)
			tempMin = dyn[i - L];
		dyn[i] = -tempMin;
	}
	for (int i = 0; i < m; i++) {
		int temp;
		scanf("%d", &temp);
		printf("%c", dyn[temp] == 1 ? 'A' : 'B');
	}
}
