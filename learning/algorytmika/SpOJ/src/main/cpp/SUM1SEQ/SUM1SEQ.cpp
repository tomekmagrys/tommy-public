#include <cstdio>
#include <cstdlib>
#include <algorithm>

int output[10000];

int main(int argc, char** argv) {
	int tests;
	scanf("%d", &tests);
	while (tests--) {
		int n, S;
		scanf("%d%d", &n, &S);
		output[0] = 0;
		for (int i = 1; i < n; i++) {
			if (S >= 0) {
				S -= n - i;
				output[i] = output[i - 1] + 1;
			} else {
				S += n - i;
				output[i] = output[i - 1] - 1;
				;
			}
		}
		if (S == 0) {
			for (int i = 0; i < n; i++) {
				printf("%d\n", output[i]);
			}
		} else {
			printf("No\n\n");
		}
	}
}
