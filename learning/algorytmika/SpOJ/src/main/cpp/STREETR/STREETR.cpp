#include<cstdio>

int n;

int lastdist;

int trees[100000];

int nwd(int a, int b) {
	if (a < b) {
		int temp = a;
		a = b;
		b = temp;
	}
	if (a % b == 0)
		return b;
	return nwd(b, a % b);
}

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &trees[i]);
	}

	int lastdist = trees[1] - trees[0];
	for (int i = 2; i < n; i++) {
		lastdist = nwd(lastdist, trees[i] - trees[i - 1]);
	}

	int wynik = 0;
	for (int i = 1; i < n; i++) {
		wynik += (trees[i] - trees[i - 1]) / lastdist - 1;
	}
	printf("%d\n", wynik);
	return 0;
}
