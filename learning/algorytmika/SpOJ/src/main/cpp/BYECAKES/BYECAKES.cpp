#include <cstdio>
#include <cstdlib>
#include <algorithm>

int main(int argc, char** argv) {
	int E, S, F, M, e, s, f, m;
	while (true) {
		scanf("%d%d%d%d%d%d%d%d", &E, &S, &F, &M, &e, &s, &f, &m);
		if (E == -1)
			break;
		int ileCiastek = 0;
		if ((E + e - 1) / e > ileCiastek)
			ileCiastek = (E + e - 1) / e;
		if ((S + s - 1) / s > ileCiastek)
			ileCiastek = (S + s - 1) / s;
		if ((F + f - 1) / f > ileCiastek)
			ileCiastek = (F + f - 1) / f;
		if ((M + m - 1) / m > ileCiastek)
			ileCiastek = (M + m - 1) / m;
		printf("%d %d %d %d\n", e * ileCiastek - E, s * ileCiastek - S,
				f * ileCiastek - F, m * ileCiastek - M);

	}

}
